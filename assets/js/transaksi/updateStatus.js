 function addForm() {
     jQuery_1_4_3('#addForm').modal('show');
 }

 function updateStatus(type) {

     var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
     var rowsIndex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindexes');
     var data = [];
     var i = 0;
     jQuery_1_4_3.each(rowsIndex, function(key, value) {
         data[i++] = rows[value].Transaction_Iid;
     });

     $.ajax({
         type: "POST",
         url: base_url + "transaksi/updateStatus",
         cache: false,
         data: {
             iid: JSON.stringify(data),
             type: type
         }, //since, you need to delete post of particular id
         success: function(status) {
             if (status == 200) {
                 $('#paidForm').modal('hide');
                 $('#pickupForm').modal('hide');
                 $('#suspiciousForm').modal('hide');
                $('#alertSuccess').show().delay(2000).fadeOut().html('Sukses diubah!');
                 jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
             } else if (status == 404) {
                 $('#paidForm').modal('hide');
                 alert('Data Belum di checked');
             } else if (status == 500) {
                 $('#paidForm').modal('hide');
                 alert('gagal proses');
             } else if (status == 403) {
                 $('#pickupForm').modal('hide');
                 alert('transaksi / baris ini belum merubah status pembayaran');
             }

         }
     });
 }


 function updateStatusIspaid(type) {

     if ($('#uniform-radioBCash span').hasClass('checked') == false && $('#uniform-radioBBank span').hasClass('checked') == false) {
         $('#paidFormAlert').show().delay(2000).fadeOut().html('Metode pembayaran belum dipilih');
         return false;
     }

     if ($('#uniform-radioBCash span').hasClass('checked') == true && $('#cashPaid').val() == '') {
         $('#paidFormAlert').show().delay(2000).fadeOut().html('Jumlah uang cash belum di isi');
         $('#cashPaid').focus();
         return false;
     }

     if ($('#uniform-radioBBank span').hasClass('checked') == true) {
         if ($('#bankPaid').val() == '') {
             $('#paidFormAlert').show().delay(2000).fadeOut().html('Jumlah bank belum di isi');
             return false;
         }

         if ($('#bankAccountValue').val() == '') {
             $('#paidFormAlert').show().delay(2000).fadeOut().html('bank akun belum di isi');
             return false;
         }

     }

     var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
     var rowsIndex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindex');
     var trxId = rows[rowsIndex].Transaction_Iid;
     var i = 0;
     jQuery_1_4_3.each(rowsIndex, function(key, value) {
         data[i++] = rows[value].Iid;
     });

     $.ajax({
         type: "POST",
         url: base_url + "transaksi/updateStatusIspaid",
         cache: false,
         dataType: "json",
         data: $('#trxPaidForm').serialize() + "&trxId=" + trxId + "&type=IsPaid", // since, you need to delete post of particular id
         success: function(data) {
             if (data.status == 200) {
                 $('#bankCashModal').modal('hide');
                 $('span.checked').removeClass('checked');
                 $('#paidStatus').val('');
                 jQuery_1_4_3('#jqxgrid').jqxGrid('refreshdata');
                 jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                 $('#alertSuccess').show().delay(2000).fadeOut().html('Sukses di Simpan!');
                 //window.location.reload();
             }

         }
     });
 }