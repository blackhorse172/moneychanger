<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Laporanpajak extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->library('Excel');
        $this->load->library('fpdf');
        $this->load->helper('file');
    }

    public function jualbeli($mess = null){
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else {
                $mess = $mess;
            }
// ******* GET & SET ERROR MESSAGE ******* //
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['pageform'] = "Laporan Jual Beli Tahunan";
            $data['error'] = $mess;
            $data['title'] = "Laporan Jual Beli Tahunan";
            $data['menu'] = "menu";
            $data['isi'] = "laporan/jualbelitahunan_view";
            $this->load->view('template', $data);
        } else {
            redirect('beranda/noauth');
        }
    }


    public function get_data_jualbelitahunan() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            $dategetfrom = $this->uri->segment(3);
            // $dateget = $this->input->post('fromsortdate', TRUE);
            if ($dategetfrom == '') {
                $dategetfrom = date('Y', strtotime(date('Y-m-d')));
            } else {
                $dategetfrom = date('Y', strtotime($dategetfrom));
            }
            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];
            $branch = $session['branchid'];
            $usertype = $session['usertype'];


            $query = "exec SP_Pajak_RekapJualBeli @companyid='$companyid',@branchid='$branch',@isfilter='$usertype',@tahun='$dategetfrom' ";
            $res = $this->db->query($query)->result();

            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }


    public function posisiKeuangan($mess = null){
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else {
                $mess = $mess;
            }
// ******* GET & SET ERROR MESSAGE ******* //
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['pageform'] = "Laporan Keuangan BI";
            $data['error'] = $mess;
            $data['title'] = "Laporan Keuangan BI";
            $data['menu'] = "menu";
            $data['isi'] = "laporan/posisiKeuangan_view";
            $this->load->view('template', $data);
        } else {
            redirect('beranda/noauth');
        }
    }

}
