<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mautonumber');
    }

    public function index() {
        $session = $this->session->userdata('session_data');
        $puser = $session['nama'];
        $companyname = $session['companyname'];
        $data['authmenu'] = $this->mautonumber->authoritymenu();
        $data['title'] = "$puser | $companyname";
        $data['menu'] = "menu";
        $data['error'] = "";
        $data['pageform'] = "Home";
        $data['isi'] = "accounting/vaccounting";
        $this->load->view('template', $data);
    }

    public function noauth() {
        $session = $this->session->userdata('session_data');
        $puser = $session['nama'];
        $companyname = $session['companyname'];
        $data['authmenu'] = $this->mautonumber->authoritymenu();
        $data['title'] = "$puser | $companyname";
        $data['menu'] = "menu";
        $data['error'] = "3:::Tidak ada Otoritas ke Form tersebut";
        $data['pageform'] = "Home";
        $data['isi'] = "accounting/vaccounting";
        $this->load->view('template', $data);
    }

}
