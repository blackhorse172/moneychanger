<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adjustment extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('mautonumber');
        $this->load->model('AdjustmentModel');
	}

	  function index($mess = null) {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else {
                $mess = $mess;
            }
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['pageform'] = "Adjustment";
            $data['pathform'] = "Adjustment</a> <i class='fa fa-angle-right'></i></li><li><a href='#'>";
            $data['error'] = $mess;
            $data['title'] = "Form Adjustment";
            $data['menu'] = "menu";
            $data['isi'] = "adjustment/index";
            $this->load->view('template', $data);
        } else {
            redirect('beranda/noauth');
        }
    }

	public function get_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            $fromsortdate = date('Y-m-d', strtotime($this->uri->segment(3)));
            $tosortdate = $this->uri->segment(4);
            $tomorrow = date('Y-m-d', strtotime($tosortdate . "+1 days"));
            if ($fromsortdate == '' or $tosortdate == '') {
                $tomorrow = date('Y-m-d', strtotime(date('Y-m-d')));
                $fromsortdate = date('Y-m-d', strtotime($tomorrow . "-1 days"));
            }
            $ret =  array();
            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];
            $branchid = $session['branchid'];

            $query = "exec SP_getAdjustment @companyid='$companyid',@branchid = '$branchid' ";
            $res = $this->db->query($query)->result();
           
            if (!empty($res)) {
                # code...
                $ret = $res;
            }
            echo json_encode($ret);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    public function store()
    {
    	# code...
    	$data = $this->input->post();
        $data['Bunga'] = $this->general->convertSeparator($this->input->post('Bunga'));
        $data['Pajak'] = $this->general->convertSeparator($this->input->post('Pajak'));
    	// print_r($data); exit;
        
        if ($data['AdjustmentType'] == 'cash') {
    		# code...
			$data['AdjustmentType'] = 'currency';
            $data['CurrCode']       = 'IDR';
    	}

        if ($data['AdjustmentType'] == 'currency') {
            # code...
            $data['AdjustmentType'] = 'currency';
        }

        if ($data['AdjustmentType'] == 'bankaccount') {
            # code...
            $data['CurrCode']       = 'IDR';
        }

        if ($data['AdjustmentType'] == 'biayadanbunga') {
            # code...
            $data['CurrCode']       = 'IDR';
        }

         $bankName = explode('-', $data['BankName']);
         $bankName = trim($bankName[0]);

    	    $session = $this->session->userdata('session_data');
            $companyid  = $session['companyid'];
            $branchid   = $session['branchid'];
            $user       = $session['id'];
            $filter     = $session['usertype'];
            
            
            $query = "exec SP_Adjustment @companyid='$companyid',@branchid = '$branchid' , @type='$data[AdjustmentType]',@accountno = '$data[AccountNo]', @currcode='$data[CurrCode]',@quantity = '$data[Quantity]',@quantitypajak = '$data[Pajak]',@quantitybunga = '$data[Bunga]', @tgltransaksi='$data[TransDate]',@remarks = '$data[Remarks]', @user='$user',@isfilter = '$filter', @bankname= '$bankName' ";
            $res = $this->db->query($query)->result();

            //print_r($res); exit();
            if (!$res) {
            	# code...
            	echo json_encode(array('status' => 500));
            	exit;
            }
            

            echo json_encode(array('status' => 200));

    }

}

/* End of file controllername.php */
/* Location: ./application/controllers/controllername.php */