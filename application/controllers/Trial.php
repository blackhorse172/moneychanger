<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Trial extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mautonumber');
        $this->load->model('serverinfo');
        $this->load->model('hash');
    }

    function cekhash() {
        $res = $this->hash->cekhash('MTA1NjEzMTBzVkZEMjNza2xvb08wZjE3MTBhZEZkczU3a0swMTA5MTA=');
        echo $res;
    }

    function gethash() {
        $res = $this->hash->gethash();
        echo $res;
    }

}
