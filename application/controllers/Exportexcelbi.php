<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Exportexcelbi extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    function mingguan($fromsortdate, $tomorrow) {
        // *************************** READ BEGIN ************************** //
        $path = "D:/LOCALHOST_PHP7/acc/uploaded_excel/format/MINGGUAN_BI.xlsx";
        $excelReader = PHPExcel_IOFactory::createReaderForFile($path);
        $objPHPExcel = $excelReader->load($path);
        $bulan = date("m", strtotime($fromsortdate));
        $tahun = date("Y", strtotime($fromsortdate));
        // *************************** READ END ************************** //
        // *************************** WRITE BEGIN ************************** //
//        $objPHPExcel = new PHPExcel
//
//        $objPHPExcel->getDefaultStyle()->getFont()
//                ->setName('Frutiger 45 Light')
//                ->setSize(11)
//                ->setBold(false);
        $style_nominal = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            )
        );
        $style_border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        // ******** USD JUAL ****** //
        $objPHPExcel->setActiveSheetIndex(2);
        $query = "SELECT a.*, @rn:=@rn+1 AS Nomor, b.TotalTransaksi, IF(lower(a.Customer) IN(SELECT lower(Nama) FROM ms_moneychanger),1,0) AS IsResmi, "
                . " b.TotalTransaksi,  "
                . "  ( "
                . " CASE  "
                . "     WHEN b.TotalTransaksi >= '500000000' THEN 1 "
                . "     WHEN b.TotalTransaksi >= '320000000' THEN 2 "
                . "     ELSE 0 "
                . " END) AS KategoriTransaksi "
                . "             FROM trx_transaction a  "
                . "             LEFT JOIN   "
                . "             (  "
                . "              SELECT Customer,SUM(REPLACE(Jumlah_Rate,'-','')) AS TotalTransaksi FROM trx_transaction "
                . "              WHERE TanggalTransaksi >= '$fromsortdate' and TanggalTransaksi < '$tomorrow' "
                . "              GROUP BY Customer  "
                . "             )b ON lower(b.Customer) = lower(a.Customer) "
                . "             WHERE a.TanggalTransaksi >= '$fromsortdate' and a.TanggalTransaksi < '$tomorrow' "
                . "             AND a.Customer NOT IN(select Name from ms_branch) and a.Cur = 'USD' and a.BorS = '-1' "
                . "             ORDER BY a.TanggalTransaksi Asc ";
        $this->db->query("SET @rn=0;");
        $res = $this->db->query($query);

        $rowCount = 7;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TanggalTransaksi)));
            if ($baris->Detail != '' || $baris->Detail != null) {
                $baris->Customer = $baris->Detail;
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Customer);

            $objPHPExcel->getActiveSheet()->getStyle('G' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, number_format($baris->Jumlah, 0, ".", ","));
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, number_format($baris->Rate, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        $objPHPExcel->getActiveSheet()->getStyle('E7:F' . $rowCount)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00000000');
        // ******** USD BELI ****** //
        $objPHPExcel->setActiveSheetIndex(3);
        $query = "SELECT a.*, @rn:=@rn+1 AS Nomor, b.TotalTransaksi, IF(lower(a.Customer) IN(SELECT lower(Nama) FROM ms_moneychanger),1,0) AS IsResmi, "
                . " b.TotalTransaksi,  "
                . "  ( "
                . " CASE  "
                . "     WHEN b.TotalTransaksi >= '500000000' THEN 1 "
                . "     WHEN b.TotalTransaksi >= '320000000' THEN 2 "
                . "     ELSE 0 "
                . " END) AS KategoriTransaksi "
                . "             FROM trx_transaction a  "
                . "             LEFT JOIN   "
                . "             (  "
                . "              SELECT Customer,SUM(REPLACE(Jumlah_Rate,'-','')) AS TotalTransaksi FROM trx_transaction "
                . "              WHERE TanggalTransaksi >= '$fromsortdate' and TanggalTransaksi < '$tomorrow' "
                . "              GROUP BY Customer  "
                . "             )b ON lower(b.Customer) = lower(a.Customer) "
                . "             WHERE a.TanggalTransaksi >= '$fromsortdate' and a.TanggalTransaksi < '$tomorrow' "
                . "             AND a.Customer NOT IN(select Name from ms_branch) and a.Cur = 'USD' and a.BorS = '1' "
                . "             ORDER BY a.TanggalTransaksi Asc ";
        $this->db->query("SET @rn=0");
        $res = $this->db->query($query);

        $rowCount = 7;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TanggalTransaksi)));
            if ($baris->Detail != '' || $baris->Detail != null) {
                $baris->Customer = $baris->Detail;
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Customer);

            $objPHPExcel->getActiveSheet()->getStyle('E' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, number_format($baris->Jumlah, 0, ".", ","));
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, number_format($baris->Rate, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        $objPHPExcel->getActiveSheet()->getStyle('G7:H' . $rowCount)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00000000');
        // ******** EUR JUAL ****** //
        $objPHPExcel->setActiveSheetIndex(5);
        $query = "SELECT a.*, @rn:=@rn+1 AS Nomor, b.TotalTransaksi, IF(lower(a.Customer) IN(SELECT lower(Nama) FROM ms_moneychanger),1,0) AS IsResmi, "
                . " b.TotalTransaksi,  "
                . "  ( "
                . " CASE  "
                . "     WHEN b.TotalTransaksi >= '500000000' THEN 1 "
                . "     WHEN b.TotalTransaksi >= '320000000' THEN 2 "
                . "     ELSE 0 "
                . " END) AS KategoriTransaksi "
                . "             FROM trx_transaction a  "
                . "             LEFT JOIN   "
                . "             (  "
                . "              SELECT Customer,SUM(REPLACE(Jumlah_Rate,'-','')) AS TotalTransaksi FROM trx_transaction "
                . "              WHERE TanggalTransaksi >= '$fromsortdate' and TanggalTransaksi < '$tomorrow' "
                . "              GROUP BY Customer  "
                . "             )b ON lower(b.Customer) = lower(a.Customer) "
                . "             WHERE a.TanggalTransaksi >= '$fromsortdate' and a.TanggalTransaksi < '$tomorrow' "
                . "             AND a.Customer NOT IN(select Name from ms_branch) and a.Cur = 'EUR' and a.BorS = '-1' "
                . "             ORDER BY a.TanggalTransaksi Asc ";
        $this->db->query("SET @rn=0");
        $res = $this->db->query($query);

        $rowCount = 7;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TanggalTransaksi)));
            if ($baris->Detail != '' || $baris->Detail != null) {
                $baris->Customer = $baris->Detail;
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Customer);

            $objPHPExcel->getActiveSheet()->getStyle('G' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, number_format($baris->Jumlah, 0, ".", ","));
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, number_format($baris->Rate, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        $objPHPExcel->getActiveSheet()->getStyle('E7:F' . $rowCount)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00000000');
        // ******** EUR BELI ****** //
        $objPHPExcel->setActiveSheetIndex(6);
        $query = "SELECT a.*, @rn:=@rn+1 AS Nomor, b.TotalTransaksi, IF(lower(a.Customer) IN(SELECT lower(Nama) FROM ms_moneychanger),1,0) AS IsResmi, "
                . " b.TotalTransaksi,  "
                . "  ( "
                . " CASE  "
                . "     WHEN b.TotalTransaksi >= '500000000' THEN 1 "
                . "     WHEN b.TotalTransaksi >= '320000000' THEN 2 "
                . "     ELSE 0 "
                . " END) AS KategoriTransaksi "
                . "             FROM trx_transaction a  "
                . "             LEFT JOIN   "
                . "             (  "
                . "              SELECT Customer,SUM(REPLACE(Jumlah_Rate,'-','')) AS TotalTransaksi FROM trx_transaction "
                . "              WHERE TanggalTransaksi >= '$fromsortdate' and TanggalTransaksi < '$tomorrow' "
                . "              GROUP BY Customer  "
                . "             )b ON lower(b.Customer) = lower(a.Customer) "
                . "             WHERE a.TanggalTransaksi >= '$fromsortdate' and a.TanggalTransaksi < '$tomorrow' "
                . "             AND a.Customer NOT IN(select Name from ms_branch) and a.Cur = 'EUR' and a.BorS = '1' "
                . "             ORDER BY a.TanggalTransaksi Asc ";
        $this->db->query("SET @rn=0");
        $res = $this->db->query($query);

        $rowCount = 7;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TanggalTransaksi)));
            if ($baris->Detail != '' || $baris->Detail != null) {
                $baris->Customer = $baris->Detail;
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Customer);

            $objPHPExcel->getActiveSheet()->getStyle('E' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, number_format($baris->Jumlah, 0, ".", ","));
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, number_format($baris->Rate, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        $objPHPExcel->getActiveSheet()->getStyle('G7:H' . $rowCount)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00000000');
        // ******** AUD JUAL ****** //
        $objPHPExcel->setActiveSheetIndex(8);
        $query = "SELECT a.*, @rn:=@rn+1 AS Nomor, b.TotalTransaksi, IF(lower(a.Customer) IN(SELECT lower(Nama) FROM ms_moneychanger),1,0) AS IsResmi, "
                . " b.TotalTransaksi,  "
                . "  ( "
                . " CASE  "
                . "     WHEN b.TotalTransaksi >= '500000000' THEN 1 "
                . "     WHEN b.TotalTransaksi >= '320000000' THEN 2 "
                . "     ELSE 0 "
                . " END) AS KategoriTransaksi "
                . "             FROM trx_transaction a  "
                . "             LEFT JOIN   "
                . "             (  "
                . "              SELECT Customer,SUM(REPLACE(Jumlah_Rate,'-','')) AS TotalTransaksi FROM trx_transaction "
                . "              WHERE TanggalTransaksi >= '$fromsortdate' and TanggalTransaksi < '$tomorrow' "
                . "              GROUP BY Customer  "
                . "             )b ON lower(b.Customer) = lower(a.Customer) "
                . "             WHERE a.TanggalTransaksi >= '$fromsortdate' and a.TanggalTransaksi < '$tomorrow' "
                . "             AND a.Customer NOT IN(select Name from ms_branch) and a.Cur = 'AUD' and a.BorS = '-1' "
                . "             ORDER BY a.TanggalTransaksi Asc ";
        $this->db->query("SET @rn=0");
        $res = $this->db->query($query);

        $rowCount = 7;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TanggalTransaksi)));
            if ($baris->Detail != '' || $baris->Detail != null) {
                $baris->Customer = $baris->Detail;
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Customer);

            $objPHPExcel->getActiveSheet()->getStyle('G' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, number_format($baris->Jumlah, 0, ".", ","));
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, number_format($baris->Rate, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        $objPHPExcel->getActiveSheet()->getStyle('E7:F' . $rowCount)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00000000');
        // ******** AUD BELI ****** //
        $objPHPExcel->setActiveSheetIndex(9);
        $query = "SELECT a.*, @rn:=@rn+1 AS Nomor, b.TotalTransaksi, IF(lower(a.Customer) IN(SELECT lower(Nama) FROM ms_moneychanger),1,0) AS IsResmi, "
                . " b.TotalTransaksi,  "
                . "  ( "
                . " CASE  "
                . "     WHEN b.TotalTransaksi >= '500000000' THEN 1 "
                . "     WHEN b.TotalTransaksi >= '320000000' THEN 2 "
                . "     ELSE 0 "
                . " END) AS KategoriTransaksi "
                . "             FROM trx_transaction a  "
                . "             LEFT JOIN   "
                . "             (  "
                . "              SELECT Customer,SUM(REPLACE(Jumlah_Rate,'-','')) AS TotalTransaksi FROM trx_transaction "
                . "              WHERE TanggalTransaksi >= '$fromsortdate' and TanggalTransaksi < '$tomorrow' "
                . "              GROUP BY Customer  "
                . "             )b ON lower(b.Customer) = lower(a.Customer) "
                . "             WHERE a.TanggalTransaksi >= '$fromsortdate' and a.TanggalTransaksi < '$tomorrow' "
                . "             AND a.Customer NOT IN(select Name from ms_branch) and a.Cur = 'AUD' and a.BorS = '1' "
                . "             ORDER BY a.TanggalTransaksi Asc ";
        $this->db->query("SET @rn=0");
        $res = $this->db->query($query);

        $rowCount = 7;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TanggalTransaksi)));
            if ($baris->Detail != '' || $baris->Detail != null) {
                $baris->Customer = $baris->Detail;
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Customer);

            $objPHPExcel->getActiveSheet()->getStyle('E' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, number_format($baris->Jumlah, 0, ".", ","));
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, number_format($baris->Rate, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        $objPHPExcel->getActiveSheet()->getStyle('G7:H' . $rowCount)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00000000');
        // ******** CNY JUAL ****** //

        $objPHPExcel->setActiveSheetIndex(11);
        $query = "SELECT a.*, @rn:=@rn+1 AS Nomor, b.TotalTransaksi, IF(lower(a.Customer) IN(SELECT lower(Nama) FROM ms_moneychanger),1,0) AS IsResmi, "
                . " b.TotalTransaksi,  "
                . "  ( "
                . " CASE  "
                . "     WHEN b.TotalTransaksi >= '500000000' THEN 1 "
                . "     WHEN b.TotalTransaksi >= '320000000' THEN 2 "
                . "     ELSE 0 "
                . " END) AS KategoriTransaksi "
                . "             FROM trx_transaction a  "
                . "             LEFT JOIN   "
                . "             (  "
                . "              SELECT Customer,SUM(REPLACE(Jumlah_Rate,'-','')) AS TotalTransaksi FROM trx_transaction "
                . "              WHERE TanggalTransaksi >= '$fromsortdate' and TanggalTransaksi < '$tomorrow' "
                . "              GROUP BY Customer  "
                . "             )b ON lower(b.Customer) = lower(a.Customer) "
                . "             WHERE a.TanggalTransaksi >= '$fromsortdate' and a.TanggalTransaksi < '$tomorrow' "
                . "             AND a.Customer NOT IN(select Name from ms_branch) and a.Cur = 'CNY' and a.BorS = '-1' "
                . "             ORDER BY a.TanggalTransaksi Asc ";
        $this->db->query("SET @rn=0");
        $res = $this->db->query($query);

        $rowCount = 7;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TanggalTransaksi)));
            if ($baris->Detail != '' || $baris->Detail != null) {
                $baris->Customer = $baris->Detail;
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Customer);

            $objPHPExcel->getActiveSheet()->getStyle('G' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, number_format($baris->Jumlah, 0, ".", ","));
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, number_format($baris->Rate, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        $objPHPExcel->getActiveSheet()->getStyle('E7:F' . $rowCount)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00000000');
        // ******** CNY BELI ****** //
        $objPHPExcel->setActiveSheetIndex(12);
        $query = "SELECT a.*, @rn:=@rn+1 AS Nomor, b.TotalTransaksi, IF(lower(a.Customer) IN(SELECT lower(Nama) FROM ms_moneychanger),1,0) AS IsResmi, "
                . " b.TotalTransaksi,  "
                . "  ( "
                . " CASE  "
                . "     WHEN b.TotalTransaksi >= '500000000' THEN 1 "
                . "     WHEN b.TotalTransaksi >= '320000000' THEN 2 "
                . "     ELSE 0 "
                . " END) AS KategoriTransaksi "
                . "             FROM trx_transaction a  "
                . "             LEFT JOIN   "
                . "             (  "
                . "              SELECT Customer,SUM(REPLACE(Jumlah_Rate,'-','')) AS TotalTransaksi FROM trx_transaction "
                . "              WHERE TanggalTransaksi >= '$fromsortdate' and TanggalTransaksi < '$tomorrow' "
                . "              GROUP BY Customer  "
                . "             )b ON lower(b.Customer) = lower(a.Customer) "
                . "             WHERE a.TanggalTransaksi >= '$fromsortdate' and a.TanggalTransaksi < '$tomorrow' "
                . "             AND a.Customer NOT IN(select Name from ms_branch) and a.Cur = 'CNY' and a.BorS = '1' "
                . "             ORDER BY a.TanggalTransaksi Asc ";
        $this->db->query("SET @rn=0");
        $res = $this->db->query($query);

        $rowCount = 7;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TanggalTransaksi)));
            if ($baris->Detail != '' || $baris->Detail != null) {
                $baris->Customer = $baris->Detail;
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Customer);

            $objPHPExcel->getActiveSheet()->getStyle('E' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, number_format($baris->Jumlah, 0, ".", ","));
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, number_format($baris->Rate, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        $objPHPExcel->getActiveSheet()->getStyle('G7:H' . $rowCount)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00000000');
        // ******** SGD JUAL ****** //
        $objPHPExcel->setActiveSheetIndex(14);
        $query = "SELECT a.*, @rn:=@rn+1 AS Nomor, b.TotalTransaksi, IF(lower(a.Customer) IN(SELECT lower(Nama) FROM ms_moneychanger),1,0) AS IsResmi, "
                . " b.TotalTransaksi,  "
                . "  ( "
                . " CASE  "
                . "     WHEN b.TotalTransaksi >= '500000000' THEN 1 "
                . "     WHEN b.TotalTransaksi >= '320000000' THEN 2 "
                . "     ELSE 0 "
                . " END) AS KategoriTransaksi "
                . "             FROM trx_transaction a  "
                . "             LEFT JOIN   "
                . "             (  "
                . "              SELECT Customer,SUM(REPLACE(Jumlah_Rate,'-','')) AS TotalTransaksi FROM trx_transaction "
                . "              WHERE TanggalTransaksi >= '$fromsortdate' and TanggalTransaksi < '$tomorrow' "
                . "              GROUP BY Customer  "
                . "             )b ON lower(b.Customer) = lower(a.Customer) "
                . "             WHERE a.TanggalTransaksi >= '$fromsortdate' and a.TanggalTransaksi < '$tomorrow' "
                . "             AND a.Customer NOT IN(select Name from ms_branch) and a.Cur = 'SGD' and a.BorS = '-1' "
                . "             ORDER BY a.TanggalTransaksi Asc ";
        $this->db->query("SET @rn=0");
        $res = $this->db->query($query);

        $rowCount = 7;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TanggalTransaksi)));
            if ($baris->Detail != '' || $baris->Detail != null) {
                $baris->Customer = $baris->Detail;
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Customer);

            $objPHPExcel->getActiveSheet()->getStyle('G' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, number_format($baris->Jumlah, 0, ".", ","));
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, number_format($baris->Rate, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        $objPHPExcel->getActiveSheet()->getStyle('E7:F' . $rowCount)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00000000');
        // ******** SGD BELI ****** //
        $objPHPExcel->setActiveSheetIndex(15);
        $query = "SELECT a.*, @rn:=@rn+1 AS Nomor, b.TotalTransaksi, IF(lower(a.Customer) IN(SELECT lower(Nama) FROM ms_moneychanger),1,0) AS IsResmi, "
                . " b.TotalTransaksi,  "
                . "  ( "
                . " CASE  "
                . "     WHEN b.TotalTransaksi >= '500000000' THEN 1 "
                . "     WHEN b.TotalTransaksi >= '320000000' THEN 2 "
                . "     ELSE 0 "
                . " END) AS KategoriTransaksi "
                . "             FROM trx_transaction a  "
                . "             LEFT JOIN   "
                . "             (  "
                . "              SELECT Customer,SUM(REPLACE(Jumlah_Rate,'-','')) AS TotalTransaksi FROM trx_transaction "
                . "              WHERE TanggalTransaksi >= '$fromsortdate' and TanggalTransaksi < '$tomorrow' "
                . "              GROUP BY Customer  "
                . "             )b ON lower(b.Customer) = lower(a.Customer) "
                . "             WHERE a.TanggalTransaksi >= '$fromsortdate' and a.TanggalTransaksi < '$tomorrow' "
                . "             AND a.Customer NOT IN(select Name from ms_branch) and a.Cur = 'SGD' and a.BorS = '1' "
                . "             ORDER BY a.TanggalTransaksi Asc ";
        $this->db->query("SET @rn=0");
        $res = $this->db->query($query);

        $rowCount = 7;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TanggalTransaksi)));
            if ($baris->Detail != '' || $baris->Detail != null) {
                $baris->Customer = $baris->Detail;
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Customer);

            $objPHPExcel->getActiveSheet()->getStyle('E' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, number_format($baris->Jumlah, 0, ".", ","));
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, number_format($baris->Rate, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        $objPHPExcel->getActiveSheet()->getStyle('G7:H' . $rowCount)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00000000');
// *************************** WRITE END ************************** //
// ************************************* FOOTER BEGIN ********************************** //
        $filename = 'Mingguan_BI_';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');
        ob_end_clean();
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

//$objWriter->save('D:\LOCALHOST_PHP7\smartdeal\uploaded_excel\format\download/aMyexacel.xlsx');
        $objWriter->save('php://output');
        exit;
// ************************************* FOOTER END ********************************** //
    }
}
