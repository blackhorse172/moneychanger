<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    private $companyid;
    private $branchid;
    public function __construct() {
        parent::__construct();
        //$this->companyid = "SmartTest";
        //$this->branchid = "SmartTest Jakarta";
        $this->companyid = "Naomilia Sukses Valasindo";
        $this->branchid = "PT. Naomilia Sukses Valasindo";
        
        $this->load->model('muser');
        $this->load->model('mautonumber');
        $this->load->model('serverinfo');
        $this->load->model('hash');
    }

    function index() {
        // ******* GET & SET ERROR MESSAGE ******* //
        $message = $this->uri->segment(3);

        if ($message == '-1') {
            $mess = "Password Tidak Cocok!";
            $data['messcount'] = 1;
        } else if ($message == '-2') {
            $mess = "Username Tidak Ditemukan!";
            $data['messcount'] = 1;
        } else if ($message == '-3') {
            $mess = "Username Di Nonaktifkan!";
            $data['messcount'] = 1;
        } else if ($message == '-4') {
            $mess = "Username / Password Kosong";
            $data['messcount'] = 1;
        } else if ($message == '-5') {
            $mess = "Session Timeout. Silahkan login kembali";
            $data['messcount'] = 1;
        } else if ($message == '-6') {
            $mess = "Userid sedang digunakan. Gunakan Userid lain";
            $data['messcount'] = 1;
        } else if ($message == '-7') {
            $mess = "Software Expired. Hubungi Sistem Adminstrator";
            $data['messcount'] = 1;
        } else if ($message == '-9') {
            $mess = "-99 ";
            $data['messcount'] = 1;
        } else {
            $mess = "";
            $data['messcount'] = 0;
        }
        // ******* GET & SET ERROR MESSAGE ******* //
//        $this->serverinfo->checkpooldatabase();

        $data['programversion'] = $this->serverinfo->programversion();
        $data['generated'] = $this->hash->gethash();
        $data['mess'] = $mess;
        $this->load->view('vusers/vlogin', $data);
    }

    function logout() {

//        // *********************LOG*******************//
//        //*******activitylog($branchid,$transno,$transrefno,$transdate,$module,$logtype,$remarks ex :BAMBANG ALFIANTO | Warehouse RABAK | B 9421 UU | TUGUAN | )
//        $this->mautonumber->activitylog('', 'LOGOUT', '', '', 'sess_logout()', 'LOGOUT', '');
//        // *********************LOG*******************//

        $session = $this->session->userdata('session_data'); 
        $userid = $session['id'];      
        $this->db->query("delete from Session_active where UserId= '$userid' ");

        $this->session->sess_destroy();
        redirect('user');
    }

    function cekuser() {
        //print_r(md5('password')); exit;
        $puser = $this->input->post('userid', TRUE);
        $ppass = $this->input->post('password', TRUE);
        $generated = $this->hash->cekhash($this->input->post('generated', TRUE));
        if ($generated == 0) {
            redirect('user/index/-5'); // hash not match
        }
        if ($this->general->getExpiredApp($this->companyid,$this->branchid,'ValidDay') < 0) {
                redirect('user/index/-7');
                return false;
        }
        if ($puser == '' || $ppass == '') {
            redirect('user/index/-4');
        }
        $result = $this->muser->cekuser($puser);
        if (count($result) == 0) {
            redirect('user/index/-2');
        //}
         //else if ($result->UserPassword != md5($ppass)) {
          //  redirect('user/index/-1');
        } else if ($result->IsActive == 0) {
            redirect('user/index/-3');
        } else {
            $usertype = "";
            if ($result->UserPassword == md5($ppass)) {
                $usertype = "0";
            } else if ($result->UserPassword2 == md5($ppass)) { //bi
                $usertype = "1";
            } else {
                redirect('user/index/-1');
                return false;
            }

            $branchresult = $this->muser->getbranchid($puser);
            $branchdefaultcode = $this->muser->getbranchformat($branchresult->BranchId);
            $query = "select distinct CompanyId from stp_userroleauthority where UserId = '$puser'";
            $companyresult = $this->db->query($query);
            $companyresultgetid = $this->db->query($query)->row();
            
            $customerlevel = $this->db->query("select Level from STP_Company where Name like '$companyresultgetid->CompanyId'")->row()->Level;

            // session manual begin //
            $cek_session_manual = $this->db->query("select UserId from Session_active where UserId = '$puser' ")->num_rows();
            //var_dump($cek_session_manual); exit();
            if($cek_session_manual > 0){
                redirect('user/index/-6');
                return false;
            }else{
                 // $this->db->query("insert into Session_active values ('$puser',DATEADD(hour,1,convert(datetime,getdate(),105)),'$companyresultgetid->CompanyId','$branchresult->BranchId') ");
            }
            // session manual end //

            if ($companyresult->num_rows() > 1) {
                $pdata = array(
                    'id' => $result->UserId,
                    'nama' => $result->UserId,
                    'regdate' => $result->InputDate,
                    'branchid' => $branchresult->BranchId,
                    'branchdefaultcode' => $branchdefaultcode->DefaultFormatCode,
                    'usertype' => $usertype,
                    'customerlevel' => $customerlevel
                );  // pass ur data in the array

                $this->session->set_userdata('session_data', $pdata); //Sets the session
                $data['result1'] = $companyresult->num_rows();
                $data['db'] = $this->db->query($query);
                $this->load->view('vusers/vchoosecompany', $data);
            } else {
                $company = $this->muser->cekcompany($companyresultgetid->CompanyId);
                $pdata = array(
                    'id' => $result->UserId,
                    'nama' => $result->UserId,
                    'regdate' => $result->InputDate,
                    'companyid' => $companyresultgetid->CompanyId,
                    'companyname' => $company->Name,
                    'branchid' => $branchresult->BranchId,
                    'branchdefaultcode' => $branchdefaultcode->DefaultFormatCode,
                    'usertype' => $usertype,
                    'customerlevel' => $customerlevel
                );  // pass ur data in the array

                $this->session->set_userdata('session_data', $pdata); //Sets the session
//                // *********************LOG*******************//
//                //*******activitylog($branchid,$transno,$transrefno,$transdate,$module,$logtype,$remarks ex :BAMBANG ALFIANTO | Warehouse RABAK | B 9421 UU | TUGUAN | )
//                $this->mautonumber->activitylog('', 'LOGIN', '', '', 'sess_login()', 'LOGIN', '');
//                // *********************LOG*******************//

                redirect('beranda');
            }
        }
    }

    function choosedcompany() {
        $session = $this->session->userdata('session_data');
        $id = $session['id'];
        $nama = $session['nama'];
        $regdate = $session['regdate'];
        $branchid = $session['branchid'];
        $pcompany = $this->input->post('companychoosed', TRUE);
        $branchresult = $this->muser->getbranchidcompany($id, $pcompany);
        $company = $this->muser->cekcompany($pcompany);
        $pdata = array(
            'id' => $id,
            'nama' => $nama,
            'regdate' => $regdate,
            'branchid' => $branchresult->BranchId,
            'companyid' => $company->Iid,
            'companyname' => $company->Name,
        );  // pass ur data in the array

        $this->session->set_userdata('session_data', $pdata); //Sets the session
        redirect('beranda');
    }

//--------------------------------------------------- TAMBAH USER BEGIN------------------------
    function viewuser($mess = null) {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else if ($message == '-3') {
                $mess = "3:::Gagal Update, User sudah memiliki Role";
            } else if ($message == '-4') {
                $mess = "3:::Gagal Update, User tidak terdaftar";
            } else {
                $mess = $mess;
            }
// ******* GET & SET ERROR MESSAGE ******* //
            $queryuser = "SELECT * from stp_user";
            $queryauth = "SELECT stp_userroleauthority.*, stp_userrole.Code, stp_userrole.Name "
                    . "FROM stp_userroleauthority LEFT JOIN stp_userrole ON stp_userrole.Iid = stp_userroleauthority.RoleId";
            $data['dbuser'] = $this->db->query($queryuser);
            $data['dbrole'] = $this->db->get('stp_userrole');
            $data['dbroleauth'] = $this->db->query($queryauth);
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['pageform'] = "User";
            $data['pathform'] = "Master</a> <i class='fa fa-angle-right'></i></li><li><a href='#'>User";
            $data['error'] = $mess;
            $data['title'] = "Form User";
            $data['menu'] = "menu";
            $data['isi'] = "vmaster/vusers/vusers";
            $this->load->view('template', $data);
        } else {
            redirect('beranda/noauth');
        }
    }

    function newuser($mess = null) {
        $serverauth = $this->serverinfo->authorityserver_forinput('RPH');
        if ($serverauth == 1) {
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['error'] = $mess;
            $data['pageform'] = "User";
            $data['pathform'] = "Master</a> <i class='fa fa-angle-right'></i></li><li><a href='#'>User"
                    . "</a><i class='fa fa-angle-right'></i></li><li><a href='#'>New User";
            $data['title'] = "Form User";
            $data['menu'] = "menu";
            $data['isi'] = "vmaster/vusers/vaddusers";
            $this->load->view('template', $data);
        } else {
            $this->viewuser('3:::Gagal, Harus pada Server RPH');
        }
    }

    function edituserget() {
        $id = $this->uri->segment(3);
        if ($id) {

            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $query = "select * from stp_user where UserId = '$id'";
            $data['db'] = $this->db->query($query)->row();
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['error'] = "";
            $data['pageform'] = "User";
            $data['pathform'] = "Master</a> <i class='fa fa-angle-right'></i></li><li><a href='#'>User"
                    . "</a><i class='fa fa-angle-right'></i></li><li><a href='#'>Edit User";
            $data['title'] = "Form User";
            $data['menu'] = "menu";
            $data['isi'] = "vmaster/vusers/veditusers";
            $this->load->view('template', $data);
        } else {
            show_404();
        }
    }

    function saveuser() {
        $session = $this->session->userdata('session_data');
        $puser = $session['nama'];
        $userid = $this->input->post('userid', TRUE);
        $password = $this->input->post('password', TRUE);
        $password2 = $this->input->post('password2', TRUE);
        $passwordbi = $this->input->post('passwordbi', TRUE);
        $passwordbi2 = $this->input->post('passwordbi2', TRUE);
        $pactivestatus = $this->input->post('activestatus', TRUE);
        $query = "select * from stp_user where UserId = '$userid'";
        $result = $this->db->query($query);
        if ($password == $password2 && $passwordbi == $passwordbi2) {
            if (count($result->row()) > 0) {
                $session = $this->session->userdata('session_data');
                $companyidsession = $session['companyid'];
                $data['companyid'] = $companyidsession;
                $data['no'] = '1';
                $data['authmenu'] = $this->mautonumber->authoritymenu();
                $data['error'] = "User ID sudah digunakan";
                $data['pageform'] = "User";
                $data['pathform'] = "Master</a> <i class='fa fa-angle-right'></i></li><li><a href='#'>User"
                        . "</a><i class='fa fa-angle-right'></i></li><li><a href='#'>New User";
                $data['title'] = "Form User";
                $data['menu'] = "menu";
                $data['isi'] = "vmaster/vusers/vaddusers";
                $this->load->view('template', $data);
            } else {
                $query1 = "insert into stp_user(UserId,UserPassword,UserPassword2,IsActive,InputBy,InputDate,LastEditBy,LastEditDate) "
                        . "values('$userid',LOWER(CONVERT(VARCHAR(32), HashBytes('MD5', '$password'), 2)),LOWER(CONVERT(VARCHAR(32), HashBytes('MD5', '$passwordbi'), 2)),'$pactivestatus','$puser',GETDATE(),'$puser',GETDATE())";

//            $this->mcurrency->simpan($piid, $pcompanyid, $pbranchid, $pcode, $pname, $premarks, $pactivestatus, $puser);
                $this->db->query($query1);
                redirect('user/viewuser/1');
            }
        } else {
            $this->newuser("Password tidak cocok");
        }
    }

    function editsaveuser() {
        $session = $this->session->userdata('session_data');
        $puser = $session['nama'];
        $userid = $this->input->post('userid', TRUE);
        $password = $this->input->post('password', TRUE);
        $password2 = $this->input->post('password2', TRUE);
        $passwordbi = $this->input->post('passwordbi', TRUE);
        $passwordbi2 = $this->input->post('passwordbi2', TRUE);
        $pactivestatus = $this->input->post('activestatus', TRUE);
        $query1 = "update stp_user set UserPassword=LOWER(CONVERT(VARCHAR(32), HashBytes('MD5', '$password'), 2)), "
                . "UserPassword2=LOWER(CONVERT(VARCHAR(32), HashBytes('MD5', '$passwordbi'), 2)), "
                . "IsActive='$pactivestatus',LastEditBy='$puser',LastEditDate=GETDATE() "
                . "where stp_user.UserId = '$userid'";
//            $this->mcurrency->simpan($piid, $pcompanyid, $pbranchid, $pcode, $pname, $premarks, $pactivestatus, $puser);
        if ($password == $password2 && $passwordbi == $passwordbi2) {
            $this->db->query($query1);
            $this->viewuser('1:::Data berhasil di input');
        } else {
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $query = "select * from stp_user where UserId = '$userid'";
            $data['db'] = $this->db->query($query)->row();
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['error'] = "3:::Password Tidak cocok";
            $data['pageform'] = "User";
            $data['pathform'] = "Master</a> <i class='fa fa-angle-right'></i></li><li><a href='#'>User"
                    . "</a><i class='fa fa-angle-right'></i></li><li><a href='#'>Edit User";
            $data['title'] = "Form User";
            $data['menu'] = "menu";
            $data['isi'] = "vmaster/vusers/veditusers";
            $this->load->view('template', $data);
        }
    }

    function deleteuser() {
        $id = $this->uri->segment(3);
        if ($id) {
            $this->db->trans_begin();
            $this->muser->deleteuser($id);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
//                $this->db->_error_message();
                $this->viewuser('Data Sudah Digunakan');
            } else {
                $this->db->trans_commit();
                $this->viewuser('1:::Data berhasil dihapus');
            }
        } else {
            show_404();
        }
    }

//--------------------------------------------------- TAMBAH USER END------------------------
//--------------------------------------------------- TAMBAH ROLEAUTH BEGIN------------------------
    function newuserroleauth($mess = null) {
        $serverauth = $this->serverinfo->authorityserver_forinput('RPH');
        if ($serverauth == 1) {
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $query = "SELECT * from stp_userroleauthority";
            $data['db'] = $this->db->query($query)->row();
            $data['getdata'] = '0';
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['error'] = $mess;
            $data['pageform'] = "User Control";
            $data['pathform'] = "User</a> <i class='fa fa-angle-right'></i></li><li><a href='#'>Role"
                    . "</a><i class='fa fa-angle-right'></i></li><li><a href='#'>Create New UserRole Authority";
            $data['title'] = "Form User";
            $data['menu'] = "menu";
            $data['jqgrid'] = "jqgrid/griduserchoosebranch";
            $data['isi'] = "vmaster/vusers/vadduserroleauth";
            $this->load->view('template', $data);
        } else {
            $this->viewuser('3:::Gagal, Harus pada Server RPH');
        }
    }

    function simpanuserroleauth() {
        $session = $this->session->userdata('session_data');
        $puser = $session['nama'];
        $idura = $this->mautonumber->autonumber('Iid', 'stp_userroleauthority', 'STP-URA-');
        $userid = $this->input->post('useridauto', TRUE);
        $companyid = $this->input->post('companyidauto', TRUE);
        $activestatus = $this->input->post('activestatus', TRUE);
        $rolecode = $this->input->post('rolecode_auto', TRUE);
        $kodecabang = $this->input->post('kodecabang_auto', TRUE);

        $id_role = "";
        $res_koderole = $this->db->query("select Iid from stp_userrole where Code = '$rolecode'");
        if ($res_koderole->num_rows() > 0) {
            $id_role = $res_koderole->row()->Iid;
        } else {
            $this->viewuser('3:::Gagal Input, Role tidak terdaftar');
        }

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>', '</label></div>');
        $this->form_validation->set_rules('useridauto', 'Branch', 'required');
        $this->form_validation->set_rules('rolecode_auto', 'Currency Code', 'required');
        $this->form_validation->set_rules('kodecabang_auto', 'Company', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->newuserroleauth('2:::Data tidak lengkap');
        } else {
            $valid_user = $this->db->query("select UserId From STP_UserRoleAuthority where UserId = '$userid' ")->num_rows();
            $valid_user_cek = $this->db->query("select UserId From STP_User where UserId = '$userid' ")->num_rows();

            if ($valid_user > 0) {
                redirect('user/viewuser/-3');
            } else if ($valid_user_cek < 1) {
                redirect('user/viewuser/-4'); //user tidak ditemukan
            } else {
                $query = "insert into stp_userroleauthority(Iid,CompanyId,BranchId,UserId,RoleId,IsActive,InputBy,InputDate,LastEditBy,LastEditDate) "
                        . "values('$idura','$companyid','$kodecabang','$userid','$id_role','$activestatus','$puser',GETDATE(),'$puser',GETDATE())";
//        echo $query;
                $this->db->query($query);
                redirect('user/viewuser/1');
            }
        }
    }

    function roleauthget() {
        $id = $this->uri->segment(3);
        if ($id) {
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $query = " SELECT stp_userroleauthority.*, stp_userrole.Code AS RoleCode, stp_userrole.Name AS RoleName,ms_branch.Name BranchName "
                    . "FROM stp_userroleauthority "
                    . "LEFT JOIN stp_userrole ON stp_userroleauthority.RoleId = stp_userrole.Iid "
                    . "LEFT JOIN ms_branch ON stp_userroleauthority.BranchId = ms_branch.Name "
                    . "WHERE stp_userroleauthority.Iid = '$id'";
            $data['db'] = $this->db->query($query)->row();
            $data['getdata'] = '0';
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['error'] = "";
            $data['pageform'] = "User Control";
            $data['pathform'] = "User</a> <i class='fa fa-angle-right'></i></li><li><a href='#'>Role"
                    . "</a><i class='fa fa-angle-right'></i></li><li><a href='#'>Edit UserRole Authority";
            $data['title'] = "Form User";
            $data['menu'] = "menu";
            $data['gridsource'] = "user/getgriduserroleauth/";
            $data['jqgrid'] = "jqgrid/griduserchoosebranch";
            $data['isi'] = "vmaster/vusers/vedituserroleauth";
            $this->load->view('template', $data);
        } else {
            show_404();
        }
    }

    function editsimpanuserroleauth() {
        $session = $this->session->userdata('session_data');
        $puser = $session['nama'];
        $idura = $this->input->post('iid', TRUE);
        $userid = $this->input->post('useridauto', TRUE);
        $companyid = $this->input->post('companyidauto', TRUE);
        $activestatus = $this->input->post('activestatus', TRUE);
        $rolecode = $this->input->post('rolecode_auto', TRUE);
        $kodecabang = $this->input->post('kodecabang_auto', TRUE);

        $id_role = "";
        $res_koderole = $this->db->query("select Iid from stp_userrole where Code = '$rolecode'");
        if ($res_koderole->num_rows() > 0) {
            $id_role = $res_koderole->row()->Iid;
        } else {
            $this->viewuser('3:::Gagal Input, Role tidak terdaftar');
        }

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="form-group has-error"><label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>', '</label></div>');
        $this->form_validation->set_rules('useridauto', 'Branch', 'required');
        $this->form_validation->set_rules('rolecode_auto', 'Currency Code', 'required');
        $this->form_validation->set_rules('kodecabang_auto', 'Company', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->newuserroleauth('2:::Data tidak lengkap');
        } else {

            $query = "update stp_userroleauthority set CompanyId='$companyid',UserId='$userid',RoleId='$id_role',BranchId='$kodecabang', "
                    . "IsActive='$activestatus',LastEditBy='$puser',LastEditDate=GETDATE() "
                    . "where stp_userroleauthority.Iid = '$idura'";
//        echo $query;
            $this->db->query($query);

            $this->viewuser('1:::Data berhasil di ubah');
        }
    }

    function roleauthdelete() {
        $id = $this->uri->segment(3);
        if ($id) {
            $this->db->trans_begin();
            $this->muser->deleteuserroleauth($id);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
//                $this->db->_error_message();
                $this->index('Data Sudah Digunakan');
            } else {
                $this->db->trans_commit();
                $this->viewuser('1:::Data berhasil dihapus');
            }
        } else {
            show_404();
        }
    }

//--------------------------------------------------- TAMBAH ROLEAUTH END------------------------
//--------------------------------------------------- TAMBAH ROLE BEGIN------------------------
    function newrole($mess = null) {
        $serverauth = $this->serverinfo->authorityserver_forinput('RPH');
        if ($serverauth == 1) {
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $query = "select FormId from stp_module where IsActive = 1";
            $data['db'] = $this->db->query($query);
            $data['getdata'] = '0';
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['error'] = $mess;
            $data['pageform'] = "User Control";
            $data['pathform'] = "User</a> <i class='fa fa-angle-right'></i></li><li><a href='#'>Role"
                    . "</a><i class='fa fa-angle-right'></i></li><li><a href='#'>Create New Role";
            $data['title'] = "Form Role";
            $data['menu'] = "menu";
            $data['isi'] = "vmaster/vusers/vaddrole";
            $this->load->view('template', $data);
        } else {
            $this->viewuser('3:::Gagal, Harus pada Server RPH');
        }
    }

    function roleget() {
        $id = $this->uri->segment(3);
        if ($id) {
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $queryheader = "select stp_userrole.* "
                    . "from stp_userrole where stp_userrole.Iid = '$id'";
            $query = "SELECT * FROM stp_userrole WHERE Iid = '$id'";
            $queryform = "SELECT * FROM stp_module WHERE IsActive = 1";
            $data['db'] = $this->db->query($query)->row();
            $data['dbheader'] = $this->db->query($queryheader)->row();
            $data['dbform'] = $this->db->query($queryform);
            $data['getdata'] = '0';
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['error'] = "";
            $data['pageform'] = "User Control";
            $data['pathform'] = "User</a> <i class='fa fa-angle-right'></i></li><li><a href='#'>Role"
                    . "</a><i class='fa fa-angle-right'></i></li><li><a href='#'>Edit Role";
            $data['title'] = "Form Role";
            $data['menu'] = "menu";
            $data['isi'] = "vmaster/vusers/veditrole";
            $this->load->view('template', $data);
        } else {
            show_404();
        }
    }

    function roledelete() {
        $id = $this->uri->segment(3);
        if ($id) {
            $this->db->trans_begin();
            $this->muser->deleteuserrole($id);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
//                $this->db->_error_message();
                $this->index('Data Sudah Digunakan');
            } else {
                $this->db->trans_commit();
                $this->viewuser('1:::Data berhasil dihapus');
            }
        } else {
            show_404();
        }
    }

//--------------------------------------------------- TAMBAH ROLE END------------------------
}
