<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invoice_transaction extends CI_Controller {
	public $pdf;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('TransactionModel');
		$this->load->library('fpdf');

	}

	public function index()
	{
		$data = $this->getData();
		$recordall = count($data['transaction']); // DI ISI DENGAN SELURUH JUMLAH RECORD YG INGIN DI CETAK
		$recordperpage = 5; // 5 JUMLAH RECORD DALAM 1 HALAMAN
		$activepage = ceil($recordall / $recordperpage); // JUMLAH ACTIVE PAGE
		$firstrecord = 1;
		$lastrecord = $recordperpage;
		$grandtotal = 0;
		$current_inv = "";
		$data['company'] = $this->general->getCompanyData();
		$dataTable = array_chunk($data['transaction'], $recordperpage);

		$pdf = new fpdf('P','cm','A4');

		$line = 0;
		
		foreach($dataTable as $key) {
			# code...
			$pdf->AddPage();
			$pdf->SetMargins(1, 0.2, 1);
			$this->header($pdf,$data);
			$this->table($pdf,$data);
			foreach ($key as $valueKey => $transaction) {
				// # code...
				if ($valueKey == 0) {
					# code...
					$line = 0.5;
					$grandtotal = $transaction['TotalTransaksi'] ;

				} else {
					# code...
					$line = $line + 0.5;
					$grandtotal += $transaction['TotalTransaksi'] ;

				}

				
		        $pdf->SetFont('Arial', '', 10);
		    	        
		        $pdf->SetXY(2.5,4.5 + $line);
		        $pdf->Cell(1, 0, $valueKey + 1, 0, 'LR', 'L');
		        
		        $pdf->SetXY(5,4.5 + $line);
		        $pdf->Cell(1, 0, $data['currency'][$valueKey], 'LR', 'C');
		        
		        $pdf->SetX(10);
		        $pdf->Cell(1, 0, number_format(str_replace('-', '', $transaction['Jumlah']), 2, ",", "."), 0, 'LR', 'R');
		        $pdf->SetX(13);
		        $pdf->Cell(1, 0, number_format(str_replace('-', '', $transaction['Rate']), 2, ",", "."), 0, 'LR', 'R');
		        $pdf->SetX(18);
		        $pdf->Cell(1, 0, number_format(str_replace('-', '', $transaction['TotalTransaksi']), 2, ",", "."), 0, 'LR', 'R');
		        $pdf->SetY(9.3);
		        $pdf->SetX(2.5);
			}

			//grand total
		         $pdf->SetXY(18,7.7);
		         $pdf->Cell(1, 0, number_format(str_replace('-', '', $grandtotal), 2, ",", "."), 0, 'LR', 'R');

			$this->footer($pdf,$data);
		}
		
        $pdf->Output();
              		
	}

	// Page header
function Header($pdf,$data)
{
   	   $pdf->SetFont('Times', 'B', 12);

       $pdf->SetY(1.5);
       $pdf->SetX(2);
       $pdf->Cell(1, 0, $data['company']->CompanyId, 0, 'LR', 'L');
       $pdf->SetFont('Times', '', 11);
       $pdf->SetX(16);
       $pdf->Cell(1, 0, 'Nota Pembelian / Penjualan', 0, 'LR', 'C');

       $pdf->SetFont('Times', 'B', 12);
       $pdf->SetY(2);
       $pdf->SetX(2);
       $pdf->Cell(1, 0, 'AUTHORIZED MONEY CHANGER', 0, 'LR', 'L');
       $pdf->SetFont('Times', '', 11);
       $pdf->SetX(16);
       $pdf->Cell(1, 0, 'Purchase / Sales Voucher', 0, 'LR', 'C');

		if ($data['trxDataHeader']->Bors == 1) {
	       $pdf->Line(15.2, 1.5, 16.9, 1.5);
	       $pdf->Line(14.5, 2, 15.9, 2);
		} else {
		   $pdf->Line(17.1, 1.5, 18.7, 1.5);
		   $pdf->Line(16.2, 2, 17.1, 2);
		}

        $pdf->SetFont('Arial', '', 11);
        $pdf->SetY(2.5);
        $pdf->SetX(2);
        $pdf->Cell(1, 0, $data['company']->Address, 0, 'LR', 'L');
        $pdf->SetY(3);
        $pdf->SetX(2);
        $pdf->Cell(1, 0, 'Tel', 0, 'LR', 'L');
        $pdf->SetX(4);
        $pdf->Cell(1, 0, $data['company']->Phone1, 0, 'LR', 'L');
        $pdf->SetY(3.5);
        $pdf->SetX(2);
        $pdf->Cell(1, 0, 'Fax', 0, 'LR', 'L');
        $pdf->SetX(4);
        $pdf->Cell(1, 0, '', 0, 'LR', 'L');
        $pdf->SetX(4);
        $pdf->Cell(1, 0, $data['company']->Fax, 0, 'LR', 'L');
        $pdf->SetX(18);
        $pdf->Cell(1, 0, $data['invoiceNo'], 0, 'LR', 'R');
}

function footer($pdf,$data)
{	
		# code...
		$pdf->SetY(7.7);
        $pdf->SetX(2);
        $pdf->Cell(1, 0, 'Customer Details', 0, 'LR', 'L');
        $pdf->SetX(13.2);
        $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');

        //terbilang
         $pdf->SetFont('Times', 'I', 7);
        $pdf->setXY(10.9,8.1);
        if (str_word_count($this->general->terbilang($data['trxDataHeader']->GrandTotal).' RUPIAH KOMA NOL NOL') > 10) {
        	# code...
        	        $pdf->SetFont('Times', 'I', 7);
        }
        $text = $this->general->terbilang($data['trxDataHeader']->GrandTotal).' RUPIAH KOMA NOL NOL';
		// $pdf->Cell(1, 0, wordwrap($text, 60, "'$pdf->Ln(1)'"), 0, 'LR', 'R');
        $pdf->MultiCell(8,0.5,$text);
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetY(8.2);
        $pdf->SetX(2);
        $pdf->Cell(1, 0, 'Nama', 0, 'LR', 'L');
        $pdf->SetX(4);
        $pdf->Cell(1, 0, $data['customer']['nama'], 0, 'LR', 'L');
        $pdf->SetY(8.7);
        $pdf->SetX(2);
        $pdf->Cell(1, 0, 'ID', 0, 'LR', 'L');
        $pdf->SetX(4);
        $pdf->Cell(1, 0, $data['customer']['iid'], 0, 'LR', 'L');
        $pdf->SetY(9.2);
        $pdf->SetX(2);
        $pdf->Cell(1, 0, 'Add', 0, 'LR', 'L');
        $pdf->SetX(4);
        $pdf->Cell(1, 0, $data['customer']['alamat'], 0, 'LR', 'L');
        $pdf->SetY(9.7);
        $pdf->SetX(2);
        $pdf->Cell(1, 0, 'Tel', 0, 'LR', 'L');
        $pdf->SetX(4);
        $pdf->Cell(1, 0, $data['customer']['telp'], 0, 'LR', 'L');
        $pdf->SetY(10.2);
        $pdf->SetX(2);
        $pdf->Cell(1, 0, 'Asal Dana', 0, 'LR', 'L');
        $pdf->SetX(4);
        $pdf->Cell(1, 0, $data['customer']['sumberDana'], 0, 'LR', 'L');
        $pdf->SetY(10.7);
        $pdf->SetX(2);
        $pdf->Cell(1, 0, 'Tuj. Trans.', 0, 'LR', 'L');
        $pdf->SetX(4);
        $pdf->Cell(1, 0, $data['customer']['tujuanTransaksi'], 0, 'LR', 'L');
        $pdf->SetY(11.2);
        $pdf->SetX(2);
        $pdf->Cell(1, 0, 'Kuasa', 0, 'LR', 'L');
        $pdf->SetX(4);
        $pdf->Cell(1, 0, $data['customer']['kuasa1_Nama'], 0, 'LR', 'L');
        $pdf->SetY(11.7);
        $pdf->SetX(2);
        $pdf->Cell(1, 0, 'ID Kuasa', 0, 'LR', 'L');
        $pdf->SetX(4);
        $pdf->Cell(1, 0, $data['customer']['kuasa1_ID'], 0, 'LR', 'L');

        $pdf->SetY(10);
        $pdf->SetX(14);
        $pdf->Cell(1, 0,'Jakarta, ' . date('d-M-Y', strtotime('2017-03-03')), 0, 'LR', 'L');

        $pdf->SetFont('Arial', 'B', 9);
        $pdf->SetY(12.2);
        $pdf->SetX(15.5);
        $pdf->Cell(1, 0, '( Kasir )', 0, 'LR', 'L');
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->SetY(12.2);
        $pdf->SetX(2);
        $pdf->Cell(1, 0, 'Perhatian / Notice', 0, 'LR', 'L');
        $pdf->SetX(10.5);
        $pdf->Cell(1, 0, '( customer )', 0, 'LR', 'L');
        $pdf->SetFont('Arial', '', 8);
        $pdf->SetY(12.5);
        $pdf->SetX(2);
        $pdf->Cell(1, 0, 'Kekurangan penerimaan uang tidak ditanggung setelah meninggalkan loket / kasir', 0, 'LR', 'L');
        $pdf->SetY(12.8);
        $pdf->SetX(2);
        $pdf->Cell(1, 0, 'Claim for shortage of cash after leaving our counter can not be considered', 0, 'LR', 'L');
        
}

function getData()
{
	# code...
	$id = $this->input->get('id');
	$customerData = $this->general->getCustomerData($this->input->get('customerName'));
	$trxDataHeader = $this->TransactionModel->getRowHeader($id);
	$trxData = $this->TransactionModel->getDataDetailByTransactionId($trxDataHeader->Transaction_Iid);

	$currency = array();
	$customerId = $this->input->get('customerId');

	foreach ($trxData as $key => $value) {
		# code...
		$currency[$key] = $this->general->getCurrencyById($value['CurrencyId']);
	}

	$data['id'] = $id; 	
	$data['customer'] = $customerData;
	$data['currency'] = $currency;
	$data['transaction'] = $trxData;
	$data['trxDataHeader'] = $trxDataHeader;
	$data['invoiceNo'] = $trxDataHeader->InvoiceNo;
	
	return $data;
}

function table($pdf,$data) {
				
				$pdf->SetFont('Arial', 'B', 10);
			    $pdf->SetY(4.1);
			    $pdf->SetX(2.4);
			    $pdf->Cell(1, 0, 'NO', 0, 'LR', 'L');
			    $pdf->SetX(5);
			    $pdf->Cell(1, 0, 'CURR', 0, 'LR', 'C');
			    $pdf->SetX(8.8);
			    $pdf->Cell(1, 0, 'AMOUNT', 0, 'LR', 'C');
			    $pdf->SetX(12);
			    $pdf->Cell(1, 0, 'RATE', 0, 'LR', 'C');
			    $pdf->SetX(16);
			    $pdf->Cell(1, 0, 'TOTAL', 0, 'LR', 'C');


		        $pdf->Line(2, 3.8, 19, 3.8);
		        $pdf->Line(2, 4.4, 19, 4.4);
		        $pdf->Line(2, 4.45, 19, 4.45);
		        $pdf->Line(2, 3.8, 2, 7.4); // garis kiri
		        $pdf->Line(19, 3.8, 19, 7.4); // garis kanan
		        $pdf->Line(2, 7.4, 19, 7.4); //garis bawah

		        $pdf->Line(14, 7.4, 19, 7.4);
		        $pdf->Line(11, 8, 19, 8);
		        $pdf->Line(14, 7.4, 14, 8); // garis kiri
		        $pdf->Line(19, 7.4, 19, 8); // garis kanan

		        $pdf->Line(11, 7.4, 11, 8); // garis kiri tambahan   

		        $pdf->Line(3.6, 3.8, 3.6, 7.4); // garis kiri
		        $pdf->Line(7.5, 3.8, 7.5, 7.4); // garis kiri
		        $pdf->Line(11, 3.8, 11, 7.4); // garis kiri
		        $pdf->Line(14, 3.8, 14, 7.4); // garis kiri

		 		$pdf->Line(14.2, 1.75, 18.8, 1.75);
		        $pdf->Line(14.2, 1.78, 18.8, 1.78);
	
}

}

/* End of file Invoice_transaction.php */
/* Location: ./application/controllers/Invoice_transaction.php */