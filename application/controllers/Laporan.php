<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->library('Excel');
        $this->load->library('fpdf');
        $this->load->helper('file');
    }

    public function transaksi($mess = null){
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else {
                $mess = $mess;
            }
// ******* GET & SET ERROR MESSAGE ******* //
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['pageform'] = "Laporan Transaksi";
            $data['error'] = $mess;
            $data['title'] = "Laporan Transaksi";
            $data['menu'] = "menu";
            $data['isi'] = "laporan/transaksi_view";
            $this->load->view('template', $data);
        } else {
            redirect('beranda/noauth');
        }
    }

     public function exportXls()
    {
        $data = array();

        $fromsortdate = date('Y-m-d', strtotime($this->uri->segment(3)));
        $tosortdate = date('Y-m-d', strtotime($this->uri->segment(4)));
        

        # code...
        if ($this->uri->segment(3) !== null && $this->uri->segment(3) != null ) {
            # code...
            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];
            $branch = $session['branchid'];
            $usertype = $session['usertype'];
            $type = 'transaksi';

           $q = "exec SP_Report_All @type='$type',@companyid='$companyid',@branchid='$branch',@usertype='$usertype',@fromdate='$fromsortdate',@todate='$tosortdate' ";

        $query = $this->db->query($q);
    
        $style_nominal = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            )
        );

        $objPHPExcel = new PHPExcel;
        $objPHPExcel->setActiveSheetIndex(0);
        $rowCount = 5;
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . 3, 'NO.');
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . 3, 'KODE');
        $objPHPExcel->getActiveSheet()->SetCellValue('D' . 3, 'NAMA');
        $objPHPExcel->getActiveSheet()->SetCellValue('E' . 3, 'PROFIL');
        $objPHPExcel->getActiveSheet()->SetCellValue('F' . 3, 'STATUS');
        $objPHPExcel->getActiveSheet()->SetCellValue('G' . 3, 'CURRENCY');
        $objPHPExcel->getActiveSheet()->SetCellValue('H' . 3, 'B/S');
        $objPHPExcel->getActiveSheet()->SetCellValue('I' . 3, 'JUMLAH');
        $objPHPExcel->getActiveSheet()->SetCellValue('J' . 3, 'RATE');
        $objPHPExcel->getActiveSheet()->SetCellValue('K' . 3, 'TOTAL TRANSAKSI');
        $objPHPExcel->getActiveSheet()->SetCellValue('L' . 3, 'TANGGAL TRANSAKSI');
        $objPHPExcel->getActiveSheet()->SetCellValue('M' . 3, 'TANGGAL PEMBAYARAN');
        $objPHPExcel->getActiveSheet()->SetCellValue('N' . 3, 'TANGGAL PENGAMBILAN BARANG');
        $objPHPExcel->getActiveSheet()->SetCellValue('O' . 3, 'DI INPUT OLEH');
        $objPHPExcel->getActiveSheet()->SetCellValue('P' . 3, 'TANGGAL INPUT');

        $nomor = 1;
        foreach ($query->result() as $baris) {
             $objPHPExcel->getActiveSheet()->getStyle('I' . $rowCount)->applyFromArray($style_nominal);
             $objPHPExcel->getActiveSheet()->getStyle('J' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('K' . $rowCount)->applyFromArray($style_nominal);

            $objPHPExcel->getActiveSheet()->getStyle('I' . $rowCount)->getNumberFormat()->setFormatCode('#,##0.00');
             $objPHPExcel->getActiveSheet()->getStyle('J' . $rowCount)->getNumberFormat()->setFormatCode('#,##0.00');
            $objPHPExcel->getActiveSheet()->getStyle('K' . $rowCount)->getNumberFormat()->setFormatCode('#,##0.00');

            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $baris->Kode);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Nama);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $baris->Profil);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $baris->TransStatus);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $baris->CURR_CODE);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $baris->BorS);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $baris->Jumlah);

            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $baris->Rate);
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $baris->TotalTransaksi);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $baris->TglTransaksi);
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $baris->PaidDate);
            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $baris->PickUpDate);
            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $baris->InputBy);
            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $baris->InputDate);

            $rowCount = $rowCount + 1;
            $nomor++;
        }
// *************************** WRITE END ************************** //
// ************************************* FOOTER BEGIN ********************************** //
        $filename = "ExportData_transaksi_" . Date("d-m-Y");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=' . $filename . '.xlsx');
        header('Cache-Control: max-age=0');
        ob_end_clean();
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

        $objWriter->save('php://output');
        exit;

        // $this->load->view('analisa/analisa_profile_template', $data);
        
        }


    }

    public function ppatk($mess = null){
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else {
                $mess = $mess;
            }
// ******* GET & SET ERROR MESSAGE ******* //
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['pageform'] = "Laporan PPATK";
            $data['error'] = $mess;
            $data['title'] = "Laporan PPATK";
            $data['menu'] = "menu";
            $data['isi'] = "laporan/ppatk_view";
            $this->load->view('template', $data);
        } else {
            redirect('beranda/noauth');
        }
    }

     public function labaRugi($mess = null){
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else {
                $mess = $mess;
            }
// ******* GET & SET ERROR MESSAGE ******* //
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['pageform'] = "Laporan Laba Rugi";
            $data['error'] = $mess;
            $data['title'] = "Laporan Laba Rugi";
            $data['menu'] = "menu";
            $data['isi'] = "laporan/labaRugi_view";
            $this->load->view('template', $data);
        } else {
            redirect('beranda/noauth');
        }
    }

    public function neraca($mess = null){
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else {
                $mess = $mess;
            }
// ******* GET & SET ERROR MESSAGE ******* //
            $dateget = $this->input->post('fromsortdate', TRUE);
            if ($dateget == '') {
                $dateget = date('Y-m-d', strtotime(date('Y-m-d')));
                $data['dategetelement'] = $dateget;
            } else {
                $dateget = date('Y-m-d', strtotime($dateget));
                $data['dategetelement'] = $dateget;
            }
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $branchid = $session['branchid'];
            $usertype = $session['usertype'];
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['totaldebit'] = $this->db->query("exec SP_Report_All @type='neracadebit',@companyid='$companyidsession',@branchid='$branchid',@usertype='$usertype',@fromdate='$dateget',@todate='$dateget' ")->row()->TotalTransaksi;
            $data['totalcredit'] = $this->db->query("exec SP_Report_All @type='neracacredit',@companyid='$companyidsession',@branchid='$branchid',@usertype='$usertype',@fromdate='$dateget',@todate='$dateget' ")->row()->TotalTransaksi;
            $data['pageform'] = "Laporan Neraca";
            $data['error'] = $mess;
            $data['title'] = "Laporan Neraca";
            $data['menu'] = "menu";
            $data['isi'] = "laporan/neraca_view";
            $this->load->view('template', $data);
        } else {
            redirect('beranda/noauth');
        }
    }

    public function bi($mess = null){
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else {
                $mess = $mess;
            }
// ******* GET & SET ERROR MESSAGE ******* //
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['pageform'] = "Laporan BI";
            $data['error'] = $mess;
            $data['title'] = "Laporan BI";
            $data['menu'] = "menu";
            $data['isi'] = "laporan/bi_view";
            $this->load->view('template', $data);
        } else {
            redirect('beranda/noauth');
        }   
    }

    public function get_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];
            $branch = $session['branchid'];
            $usertype = $session['usertype'];

            $fromsortdate = date('Y-m-d', strtotime($this->uri->segment(3)));
            $tosortdate = $this->uri->segment(4);
            $type = $this->uri->segment(5);

            $tomorrow = date('Y-m-d', strtotime($tosortdate));
            if ($fromsortdate == '' or $tosortdate == '') {
                $tomorrow = date('Y-m-d', strtotime(date('Y-m-d')));
                $fromsortdate = date('Y-m-d', strtotime($tomorrow));
            }

            $query = "exec SP_Report_All @type='$type',@companyid='$companyid',@branchid='$branch',@usertype='$usertype',@fromdate='$fromsortdate',@todate='$tomorrow' ";
            $res = $this->db->query($query)->result();

            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    # STOCK BEGIN

    public function stock($mess = null){
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else {
                $mess = $mess;
            }
// ******* GET & SET ERROR MESSAGE ******* //
            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];
            $branch = $session['branchid'];
            $usertype = $session['usertype'];
            $tanggal = date('Y-m-d');
            $tanggalawal = date('Y-m-d',strtotime($date1 . "-1 days"));
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['pageform'] = "Stok";
            $data['totalstokawal'] = $this->db->query("exec SP_GetStock_OnIDR @companyid='$companyid',@branchid='$branch',@isfilter='$usertype',@tanggal='$tanggalawal' ")->row()->total;
            $data['totalstokakhir'] = $this->db->query("exec SP_GetStock_OnIDR @companyid='$companyid',@branchid='$branch',@isfilter='$usertype',@tanggal='$tanggal' ")->row()->total;
            $data['error'] = $mess;
            $data['title'] = "Stok";
            $data['menu'] = "menu";
            $data['isi'] = "laporan/stock_view";
            $this->load->view('template', $data);
        } else {
            redirect('beranda/noauth');
        }
    }

    public function get_data_stock() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];
            $branch = $session['branchid'];
            $usertype = $session['usertype'];

            $tanggal = date('Y-m-d');
            $query = "exec SP_GetStock @companyid='$companyid',@branchid='$branch',@isfilter='$usertype',@tanggal='$tanggal' ";
            $res = $this->db->query($query)->result();

            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    public function get_data_stock_awal() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];
            $branch = $session['branchid'];
            $usertype = $session['usertype'];

            $tanggal = date('Y-m-d',strtotime($date1 . "-1 days"));
            $query = "exec SP_GetStock @companyid='$companyid',@branchid='$branch',@isfilter='$usertype',@tanggal='$tanggal' ";
            $res = $this->db->query($query)->result();

            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    public function get_data_gross_profit() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];
            $branch = $session['branchid'];
            $usertype = $session['usertype'];

            $tanggal = date('Y-m-d',strtotime($date1 . "-1 days"));
            $query = "exec SP_Get_GrossProfit @companyid='$companyid',@branchid='$branch',@usertype='$usertype',@tahun='2017' ";
            $res = $this->db->query($query)->result();

            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    public function rekapharian($mess = null){
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else {
                $mess = $mess;
            }
// ******* GET & SET ERROR MESSAGE ******* //
            $dateget = $this->input->post('fromsortdate', TRUE);
            if ($dateget == '') {
                $dateget = date('Y-m-d', strtotime(date('Y-m-d')));
                $data['dategetelement'] = $dateget;
            } else {
                $dateget = date('Y-m-d', strtotime($dateget));
                $data['dategetelement'] = $dateget;
            }

            $dategetto = $this->input->post('tosortdate', TRUE);
            if ($dategetto == '') {
                $dategetto = date('Y-m-d', strtotime(date('Y-m-d')));
                $data['dategetelementto'] = $dategetto;
            } else {
                $dategetto = date('Y-m-d', strtotime($dategetto));
                $data['dategetelementto'] = $dategetto;
            }

            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];
            $branch = $session['branchid'];
            $usertype = $session['usertype'];
            $data['saldoawalrupiah'] = $this->db->query("exec SP_Get_SaldoAwalRupiah @companyid='$companyid',@branchid='$branch',@usertype='$usertype',@fromdate='$dateget' ")->row()->Total;
            $data['rekapjual'] = $this->db->query("exec SP_Get_RekapJualBeli_ByDate @companyid='$companyid',@branchid='$branch',@usertype='$usertype',@fromdate='$dateget',@todate='$dategetto' ")->row()->RekapJual;
            $data['rekapbeli'] = $this->db->query("exec SP_Get_RekapJualBeli_ByDate @companyid='$companyid',@branchid='$branch',@usertype='$usertype',@fromdate='$dateget',@todate='$dategetto' ")->row()->RekapBeli;
            $data['totalbiaya'] = $this->db->query("exec SP_GetTotalBiaya @companyid='$companyid',@branchid='$branch',@usertype='$usertype',@fromdate='$dateget',@todate='$dategetto' ")->row()->Total;
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['pageform'] = "Rekap Transaksi Harian";
            $data['error'] = $mess;
            $data['title'] = "Rekap Transaksi Harian";
            $data['menu'] = "menu";
            $data['isi'] = "laporan/rekapharian_view";
            $this->load->view('template', $data);
        } else {
            redirect('beranda/noauth');
        }
    }

    public function get_data_rekapharian() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            $dategetfrom = $this->uri->segment(3);
            $dategetto = $this->uri->segment(4);
            // $dateget = $this->input->post('fromsortdate', TRUE);
            if ($dategetfrom == '') {
                $dategetfrom = date('Y-m-d', strtotime(date('Y-m-d')));
            } else {
                $dategetfrom = date('Y-m-d', strtotime($dategetfrom));
            }
            if ($dategetto == '') {
                $dategetto = date('Y-m-d', strtotime(date('Y-m-d')));
            } else {
                $dategetto = date('Y-m-d', strtotime($dategetto));
            }
            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];
            $branch = $session['branchid'];
            $usertype = $session['usertype'];


            $query = "exec SP_GetRekapHarian @companyid='$companyid',@branchid='$branch',@isfilter='$usertype',@daritanggal='$dategetfrom',@sampaitanggal='$dategetto' ";
            $res = $this->db->query($query)->result();

            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }


    # laporan BI BEGIN ================================================================================

    public function bulanan_index($mess = null){
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else {
                $mess = $mess;
            }
// ******* GET & SET ERROR MESSAGE ******* //
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['pageform'] = "Laporan Bulanan BI";
            $data['error'] = $mess;
            $data['title'] = "Laporan Bulanan BI";
            $data['menu'] = "menu";
            $data['isi'] = "laporan/bulanan_bi_view";
            $this->load->view('template', $data);
        } else {
            redirect('beranda/noauth');
        }
    }

    public function get_data_kurstengah_bi() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];
            $branch = $session['branchid'];
            $usertype = $session['usertype'];

            $tanggal = date('Y-m-d',strtotime($date1 . "-1 days"));
            $query = "exec SP_Get_KursTengah_BI ";
            $res = $this->db->query($query)->result();

            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    public function export() {
        $tipe = $this->input->post('tipe', TRUE);
        $fromsortdate = date('Y-m-d', strtotime($this->input->post('fromsortdate', TRUE)));
        $tomorrow = date('Y-m-d', strtotime($this->input->post('tosortdate', TRUE)));
//        echo $transdate . '-' . $tipe . '-' . $fromsortdate . '-' . $tomorrow;
        if ($tipe == 'mingguan') {
            $this->mingguan($fromsortdate, $tomorrow);
        } else if ($tipe == 'bulanan') {
            $this->bulanan($bulan, $tahun);
        } else {
             $this->mingguan($fromsortdate, $tomorrow);
            // $this->harian($fromsortdate, $tomorrow);
        }
    }

    function mingguan($fromsortdate, $tomorrow) {
        // *************************** READ BEGIN ************************** //
        $path = "D:/LOCALHOST_PHP7/acc/uploaded_excel/format/MINGGUAN_BI.xlsx";
        $excelReader = PHPExcel_IOFactory::createReaderForFile($path);
        $objPHPExcel = $excelReader->load($path);
        // *************************** READ END ************************** //
        // *************************** WRITE BEGIN ************************** //
        $session = $this->session->userdata('session_data');
        $companyid = $session['companyid'];
        $branchid = $session['branchid'];
        $usertype = $session['usertype'];
////        $objPHPExcel = new PHPExcel
//
//        $objPHPExcel->getDefaultStyle()->getFont()
//                ->setName('Frutiger 45 Light')
//                ->setSize(11)
//                ->setBold(false);
        $style_nominal = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            )
        );
        $style_border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        // ******** USD JUAL ****** //
        $objPHPExcel->setActiveSheetIndex(2);
            $query = "exec SP_Report_BI @companyid='$companyid',@branchid='$branchid',@curr='USD',@bors='-1',@fromdate='$fromsortdate',@todate='$tomorrow',@isfilter='$usertype' ";
            $res = $this->db->query($query);

        $rowCount = 7;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TglTransaksi)));
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Nama);

            $objPHPExcel->getActiveSheet()->getStyle('G' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, number_format($baris->Jumlah, 0, ".", ","));
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, number_format($baris->Rate, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        $objPHPExcel->getActiveSheet()->getStyle('E7:F' . $rowCount)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00000000');
        // ******** USD BELI ****** //
        $objPHPExcel->setActiveSheetIndex(3);
        $query = "exec SP_Report_BI @companyid='$companyid',@branchid='$branchid',@curr='USD',@bors='1',@fromdate='$fromsortdate',@todate='$tomorrow',@isfilter='$usertype' ";
            $res = $this->db->query($query);

        $rowCount = 7;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TglTransaksi)));
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Nama);

            $objPHPExcel->getActiveSheet()->getStyle('E' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, number_format($baris->Jumlah, 0, ".", ","));
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, number_format($baris->Rate, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        $objPHPExcel->getActiveSheet()->getStyle('G7:H' . $rowCount)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00000000');
        // ******** USD STOCK ****** //
        $objPHPExcel->setActiveSheetIndex(4);
        $query = "exec SP_Report_BI_Stock @companyid='$companyid',@branchid='$branchid',@curr='USD',@fromdate='$fromsortdate',@todate='$tomorrow',@isfilter='$usertype' ";
            $res = $this->db->query($query);

        $rowCount = 6;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TransDate)));
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, number_format($baris->Quantity, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        // ******** EUR JUAL ****** //
        $objPHPExcel->setActiveSheetIndex(5);
       $query = "exec SP_Report_BI @companyid='$companyid',@branchid='$branchid',@curr='EUR',@bors='-1',@fromdate='$fromsortdate',@todate='$tomorrow',@isfilter='$usertype' ";
            $res = $this->db->query($query);

        $rowCount = 7;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TglTransaksi)));
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Nama);

            $objPHPExcel->getActiveSheet()->getStyle('G' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, number_format($baris->Jumlah, 0, ".", ","));
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, number_format($baris->Rate, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        $objPHPExcel->getActiveSheet()->getStyle('E7:F' . $rowCount)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00000000');
        // ******** EUR BELI ****** //
        $objPHPExcel->setActiveSheetIndex(6);
       $query = "exec SP_Report_BI @companyid='$companyid',@branchid='$branchid',@curr='EUR',@bors='1',@fromdate='$fromsortdate',@todate='$tomorrow',@isfilter='$usertype' ";
            $res = $this->db->query($query);

        $rowCount = 7;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TglTransaksi)));
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Nama);

            $objPHPExcel->getActiveSheet()->getStyle('E' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, number_format($baris->Jumlah, 0, ".", ","));
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, number_format($baris->Rate, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        $objPHPExcel->getActiveSheet()->getStyle('G7:H' . $rowCount)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00000000');
        // ******** EUR STOCK ****** //
        $objPHPExcel->setActiveSheetIndex(7);
        $query = "exec SP_Report_BI_Stock @companyid='$companyid',@branchid='$branchid',@curr='EUR',@fromdate='$fromsortdate',@todate='$tomorrow',@isfilter='$usertype' ";
            $res = $this->db->query($query);

        $rowCount = 6;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TransDate)));
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, number_format($baris->Quantity, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        // ******** AUD JUAL ****** //
        $objPHPExcel->setActiveSheetIndex(8);
        $query = "exec SP_Report_BI @companyid='$companyid',@branchid='$branchid',@curr='AUD',@bors='-1',@fromdate='$fromsortdate',@todate='$tomorrow',@isfilter='$usertype' ";
            $res = $this->db->query($query);

        $rowCount = 7;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TglTransaksi)));
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Nama);

            $objPHPExcel->getActiveSheet()->getStyle('G' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, number_format($baris->Jumlah, 0, ".", ","));
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, number_format($baris->Rate, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        $objPHPExcel->getActiveSheet()->getStyle('E7:F' . $rowCount)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00000000');
        // ******** AUD BELI ****** //
        $objPHPExcel->setActiveSheetIndex(9);
        $query = "exec SP_Report_BI @companyid='$companyid',@branchid='$branchid',@curr='AUD',@bors='1',@fromdate='$fromsortdate',@todate='$tomorrow',@isfilter='$usertype' ";
            $res = $this->db->query($query);

        $rowCount = 7;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TglTransaksi)));
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Nama);

            $objPHPExcel->getActiveSheet()->getStyle('E' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, number_format($baris->Jumlah, 0, ".", ","));
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, number_format($baris->Rate, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        $objPHPExcel->getActiveSheet()->getStyle('G7:H' . $rowCount)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00000000');
        // ******** AUD STOCK ****** //
        $objPHPExcel->setActiveSheetIndex(10);
        $query = "exec SP_Report_BI_Stock @companyid='$companyid',@branchid='$branchid',@curr='AUD',@fromdate='$fromsortdate',@todate='$tomorrow',@isfilter='$usertype' ";
            $res = $this->db->query($query);

        $rowCount = 6;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TransDate)));
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, number_format($baris->Quantity, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        // ******** CNY JUAL ****** //

        $objPHPExcel->setActiveSheetIndex(11);
        $query = "exec SP_Report_BI @companyid='$companyid',@branchid='$branchid',@curr='CNY',@bors='-1',@fromdate='$fromsortdate',@todate='$tomorrow',@isfilter='$usertype' ";
            $res = $this->db->query($query);

        $rowCount = 7;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TglTransaksi)));
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Nama);

            $objPHPExcel->getActiveSheet()->getStyle('G' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, number_format($baris->Jumlah, 0, ".", ","));
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, number_format($baris->Rate, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        $objPHPExcel->getActiveSheet()->getStyle('E7:F' . $rowCount)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00000000');
        // ******** CNY BELI ****** //
        $objPHPExcel->setActiveSheetIndex(12);
        $query = "exec SP_Report_BI @companyid='$companyid',@branchid='$branchid',@curr='CNY',@bors='1',@fromdate='$fromsortdate',@todate='$tomorrow',@isfilter='$usertype' ";
            $res = $this->db->query($query);

        $rowCount = 7;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TglTransaksi)));
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Nama);

            $objPHPExcel->getActiveSheet()->getStyle('E' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, number_format($baris->Jumlah, 0, ".", ","));
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, number_format($baris->Rate, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        $objPHPExcel->getActiveSheet()->getStyle('G7:H' . $rowCount)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00000000');
        // ******** CNY STOCK ****** //
        $objPHPExcel->setActiveSheetIndex(13);
        $query = "exec SP_Report_BI_Stock @companyid='$companyid',@branchid='$branchid',@curr='CNY',@fromdate='$fromsortdate',@todate='$tomorrow',@isfilter='$usertype' ";
            $res = $this->db->query($query);

        $rowCount = 6;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TransDate)));
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, number_format($baris->Quantity, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        // ******** SGD JUAL ****** //
        $objPHPExcel->setActiveSheetIndex(14);
        $query = "exec SP_Report_BI @companyid='$companyid',@branchid='$branchid',@curr='SGD',@bors='-1',@fromdate='$fromsortdate',@todate='$tomorrow',@isfilter='$usertype' ";
            $res = $this->db->query($query);

        $rowCount = 7;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TglTransaksi)));
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Nama);

            $objPHPExcel->getActiveSheet()->getStyle('G' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, number_format($baris->Jumlah, 0, ".", ","));
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, number_format($baris->Rate, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        $objPHPExcel->getActiveSheet()->getStyle('E7:F' . $rowCount)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00000000');
        // ******** SGD BELI ****** //
        $objPHPExcel->setActiveSheetIndex(15);
        $query = "exec SP_Report_BI @companyid='$companyid',@branchid='$branchid',@curr='SGD',@bors='1',@fromdate='$fromsortdate',@todate='$tomorrow',@isfilter='$usertype' ";
            $res = $this->db->query($query);

        $rowCount = 7;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TglTransaksi)));
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $baris->Nama);

            $objPHPExcel->getActiveSheet()->getStyle('E' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, number_format($baris->Jumlah, 0, ".", ","));
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, number_format($baris->Rate, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
        $objPHPExcel->getActiveSheet()->getStyle('G7:H' . $rowCount)->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('00000000');
        // ******** SGD STOCK ****** //
        $objPHPExcel->setActiveSheetIndex(16);
        $query = "exec SP_Report_BI_Stock @companyid='$companyid',@branchid='$branchid',@curr='SGD',@fromdate='$fromsortdate',@todate='$tomorrow',@isfilter='$usertype' ";
            $res = $this->db->query($query);

        $rowCount = 6;
        $nomor_forexcel = 1;
        foreach ($res->result() as $baris) {
            // allborder //
            $objPHPExcel->getActiveSheet()->getStyle('B' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $rowCount)->applyFromArray($style_border);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_border);
            // allborder //
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $nomor_forexcel);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-m-Y', strtotime($baris->TransDate)));
            $objPHPExcel->getActiveSheet()->getStyle('D' . $rowCount)->applyFromArray($style_nominal);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, number_format($baris->Quantity, 0, ".", ","));
            $rowCount = $rowCount + 1;
            $nomor_forexcel = $nomor_forexcel + 1;
        }
        $rowCount = $rowCount - 1;
// *************************** WRITE END ************************** //
// ************************************* FOOTER BEGIN ********************************** //
        $filename = 'Mingguan_BI_';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');
        ob_end_clean();
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

//$objWriter->save('D:\LOCALHOST_PHP7\smartdeal\uploaded_excel\format\download/aMyexacel.xlsx');
        $objWriter->save('php://output');
        exit;
// ************************************* FOOTER END ********************************** //
    }

    function bulanan($bulan, $tahun) {
        define('FPDF_FONTPATH', $this->config->item('fonts_path'));
        $querydetail = $this->db->query("select * from MS_Customer ")->result();
        $data['hasildetail'] = $querydetail;
        $data['no'] = 1;
        $data['judul'] = 'PT.';
// Load view “pdf_report” untuk menampilkan hasilnya
        $this->load->view('laporan/vbi_bulanan_downloadpdf', $data);
    }


    public function updateDataBulananBi()
    {
        # code...
        $data = $this->input->post();
        $session = $this->session->userdata('session_data');
        $companyid = $session['companyid'];
        $branchid = $session['branchid'];
        $filter = $session['usertype'];
        $this->db->where('CompanyId', $companyid);
        $this->db->where('BranchId', $branchid);
        $this->db->where('IsFilter', $filter);
        $this->db->where('CurrencyId', $data['field']['CurrencyId']);
        $this->db->from('MS_Bulanan_BI');

        $row = $this->db->get()->row();

        if (count($row) > 0) {
            # code...
            $object = array(
                'MiddleRate' => $data['updated_param'], 
                'LastEditBy' => $session['id'], 
                'LastEditDate' => date('Y-m-d'), 
            );
            $this->db->where('CurrencyId',  $data['field']['CurrencyId']);
            $update = $this->db->update('MS_Bulanan_BI', $object);

            echo json_encode(array('status' => 200));
        
        } else {

            $object = array(
                'Iid' => $this->general->getIid('BI','MS_Bulanan_BI'), 
                'MiddleRate' => $data['updated_param'], 
                'MiddleRateDate' => date('Y-m-d'), 
                'CurrencyId' => $data['field']['CurrencyId'], 
                'CurrencyCode' => $data['field']['CURR_CODE'], 
                'InputBy' => $session['id'], 
                'InputDate' => date('Y-m-d'), 
                'LastEditBy' => $session['id'], 
                'LastEditDate' => date('Y-m-d'), 
                'CurrencyCode' => $data['field']['CURR_CODE'],
                'CompanyId' => $companyid, 
                'BranchId' => $branchid, 
                'IsFilter' => $filter, 
            );

            $this->db->insert('MS_Bulanan_BI', $object);
             echo json_encode(array('status' => 200));

        }


    }

    public function bulananBIexportXLS($from,$to)
    {
        # code...
        $session = $this->session->userdata('session_data');
        $companyid = $session['companyid'];
        $branchid = $session['branchid'];
        $filter = $session['usertype'];

         $from = date('Y-m-d', strtotime($from));
         $to = date('Y-m-d', strtotime($to));

        $query = "exec SP_Report_BI_Bulanan @companyid='$companyid', @branchid='$branchid', @isfilter='$filter', @fromdate='$from', @todate='$to' ";
        $res = $this->db->query($query)->result();
        
        if (count($res) > 0 ) {
                  # code...
        $data['rows'] = $res;
        $data['title'] = 'Laporan BI';
        
        $this->load->view('laporan/bulanan_bi_xls', $data);
        }      
        
        
    }
}
