<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// *********************** ROLE SPLIT *************************
// [0] = nama form
// [1] = create
// [2] = view
// [3] = update
// [4] = delete
// [5] = approve
// [6] = print
// [7] = preview
// [8] = addon
// *********************** ROLE SPLIT *************************
class Role extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('muser');
        $this->load->model('mautonumber');
    }

    function viewuser($mess = null) {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else {
                $mess = $mess;
            }
// ******* GET & SET ERROR MESSAGE ******* //
            $queryuser = "SELECT * from STP_User";
            $queryauth = "SELECT STP_UserROleAuthority.*, STP_UserRole.Code, STP_UserRole.Name "
                    . "FROM STP_UserROleAuthority LEFT JOIN STP_UserRole ON STP_UserRole.Iid = STP_UserROleAuthority.RoleId";
            $data['dbuser'] = $this->db->query($queryuser);
            $data['dbrole'] = $this->db->query(" select * from STP_UserRole ");
            $data['dbroleauth'] = $this->db->query($queryauth);
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['pageform'] = "User";
            $data['pathform'] = "Master</a> <i class='fa fa-angle-right'></i></li><li><a href='#'>User";
            $data['error'] = $mess;
            $data['title'] = "Form User";
            $data['menu'] = "menu";
            $data['isi'] = "vmaster/vusers/vusers";
            $this->load->view('template', $data);
        } else {
            redirect('chome/noauth');
        }
    }

    function saverole() {
        $session = $this->session->userdata('session_data');
        $puser = $session['nama'];
        $piid = $this->mautonumber->autonumber('Iid', 'stp_userrole', 'STP-ROLE-');
        $pcompanyid = $this->input->post('companyid', TRUE);
        $pbranchid = $this->input->post('branchid', TRUE);
        $pcode = $this->input->post('code', TRUE);
        $pname = $this->input->post('name', TRUE);
        $premarks = $this->input->post('remarks', TRUE);
        $pactivestatus = $this->input->post('activestatus', TRUE);

        $query = "select FormId from stp_module where IsActive = 1";
        $countform = $this->db->query($query);

        $this->form_validation->set_rules('code', '', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->viewuser('3:::Gagal Input, Data tidak Lengkap');
        } else {
            $changed = 0;
// LOOP for get authority checbox
            foreach ($countform->result() as $baris):

                if ($_POST[$baris->FormId]) {
                    $jasar[$baris->FormId] = '';
                    $jasa = $_POST[$baris->FormId];
                    foreach ($jasa as $js) {
                        $jasar[$baris->FormId] = $jasar[$baris->FormId] . $js . "::";
                    }
                    $jasar[$baris->FormId] = substr($jasar[$baris->FormId], 0, -4);

//CREATE

                    if ($jasar[$baris->FormId] == 'create') {
                        $jasar[$baris->FormId] = 'create::::::::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::delete') {
                        $jasar[$baris->FormId] = 'create::::::delete::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update') {
                        $jasar[$baris->FormId] = 'create::::update::::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::approve') {
                        $jasar[$baris->FormId] = 'create::::::::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::print') {
                        $jasar[$baris->FormId] = 'create::::::::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::preview') {
                        $jasar[$baris->FormId] = 'create::::::::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::addon') {
                        $jasar[$baris->FormId] = 'create::::::::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::delete::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = 'create::::update::delete::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::delete::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = 'create::view::::delete::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = 'create::view::update::::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::delete::print::preview::addon') {
                        $jasar[$baris->FormId] = 'create::view::update::delete::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::delete::approve::preview::addon') {
                        $jasar[$baris->FormId] = 'create::view::update::delete::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::delete::approve::print::addon') {
                        $jasar[$baris->FormId] = 'create::view::update::delete::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::delete::approve::print::preview') {
                        $jasar[$baris->FormId] = 'create::view::update::delete::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::delete::approve::print::preview::addon') { //ilang 2
                        $jasar[$baris->FormId] = 'create::::::delete::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = 'create::::update::::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::delete::print::preview::addon') {
                        $jasar[$baris->FormId] = 'create::::update::delete::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::delete::approve::preview::addon') {
                        $jasar[$baris->FormId] = 'create::::update::delete::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::delete::approve::print::addon') {
                        $jasar[$baris->FormId] = 'create::::update::delete::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::delete::approve::print::preview') {
                        $jasar[$baris->FormId] = 'create::::update::delete::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = 'create::view::::::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::delete::print::preview::addon') {
                        $jasar[$baris->FormId] = 'create::view::::delete::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::delete::approve::preview::addon') {
                        $jasar[$baris->FormId] = 'create::view::::delete::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::delete::approve::print::addon') {
                        $jasar[$baris->FormId] = 'create::view::::delete::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::delete::approve::print::preview') {
                        $jasar[$baris->FormId] = 'create::view::::delete::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::print::preview::addon') {
                        $jasar[$baris->FormId] = 'create::view::update::::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::approve::preview::addon') {
                        $jasar[$baris->FormId] = 'create::view::update::::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::approve::print::addon') {
                        $jasar[$baris->FormId] = 'create::view::update::::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::approve::print::preview') {
                        $jasar[$baris->FormId] = 'create::view::update::::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::delete::preview::addon') {
                        $jasar[$baris->FormId] = 'create::view::update::delete::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::delete::print::addon') {
                        $jasar[$baris->FormId] = 'create::view::update::delete::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::delete::print::preview') {
                        $jasar[$baris->FormId] = 'create::view::update::delete::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::delete::approve::addon') {
                        $jasar[$baris->FormId] = 'create::view::update::delete::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::delete::approve::preview') {
                        $jasar[$baris->FormId] = 'create::view::update::delete::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::delete::approve::print') { //ilang 2
                        $jasar[$baris->FormId] = 'create::view::update::delete::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::approve::print::preview::addon') { //ilang 3
                        $jasar[$baris->FormId] = 'create::::::::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::delete::print::preview::addon') {
                        $jasar[$baris->FormId] = 'create::::::delete::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::delete::approve::preview::addon') {
                        $jasar[$baris->FormId] = 'create::::::delete::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::delete::approve::print::addon') {
                        $jasar[$baris->FormId] = 'create::::::delete::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::delete::approve::print::preview') {
                        $jasar[$baris->FormId] = 'create::::::delete::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::print::preview::addon') {
                        $jasar[$baris->FormId] = 'create::::update::::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::approve::preview::addon') {
                        $jasar[$baris->FormId] = 'create::::update::::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::approve::print::addon') {
                        $jasar[$baris->FormId] = 'create::::update::::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::approve::print::preview') {
                        $jasar[$baris->FormId] = 'create::::update::::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::delete::preview::addon') {
                        $jasar[$baris->FormId] = 'create::::update::delete::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::delete::print::addon') {
                        $jasar[$baris->FormId] = 'create::::update::delete::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::delete::print::preview') {
                        $jasar[$baris->FormId] = 'create::::update::delete::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::delete::approve::addon') {
                        $jasar[$baris->FormId] = 'create::::update::delete::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::delete::approve::preview') {
                        $jasar[$baris->FormId] = 'create::::update::delete::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::delete::approve::print') {
                        $jasar[$baris->FormId] = 'create::::update::delete::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::print::preview::addon') {
                        $jasar[$baris->FormId] = 'create::view::::::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::approve::preview::addon') {
                        $jasar[$baris->FormId] = 'create::view::::::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::approve::print::addon') {
                        $jasar[$baris->FormId] = 'create::view::::::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::approve::print::preview') {
                        $jasar[$baris->FormId] = 'create::view::::::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::delete::preview::addon') {
                        $jasar[$baris->FormId] = 'create::view::::delete::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::delete::print::addon') {
                        $jasar[$baris->FormId] = 'create::view::::delete::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::delete::print::preview') {
                        $jasar[$baris->FormId] = 'create::view::::delete::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::delete::approve::addon') {
                        $jasar[$baris->FormId] = 'create::view::::delete::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::delete::approve::preview') {
                        $jasar[$baris->FormId] = 'create::view::::delete::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::delete::approve::print') {
                        $jasar[$baris->FormId] = 'create::view::::delete::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::preview::addon') {
                        $jasar[$baris->FormId] = 'create::view::update::::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::print::addon') {
                        $jasar[$baris->FormId] = 'create::view::update::::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::print::preview') {
                        $jasar[$baris->FormId] = 'create::view::update::::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::delete::addon') {
                        $jasar[$baris->FormId] = 'create::view::update::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::delete::preview') {
                        $jasar[$baris->FormId] = 'create::view::update::delete::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::delete::print') {
                        $jasar[$baris->FormId] = 'create::view::update::delete::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::delete::approve') { //ilang 3
                        $jasar[$baris->FormId] = 'create::view::update::delete::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::print::preview::addon') {// ilang 4
                        $jasar[$baris->FormId] = 'create::::::::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::approve::preview::addon') {
                        $jasar[$baris->FormId] = 'create::::::::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::approve::print::addon') {
                        $jasar[$baris->FormId] = 'create::::::::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::approve::print::preview') {
                        $jasar[$baris->FormId] = 'create::::::::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::delete::preview::addon') {
                        $jasar[$baris->FormId] = 'create::::::delete::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::delete::print::addon') {
                        $jasar[$baris->FormId] = 'create::::::delete::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::delete::print::preview') {
                        $jasar[$baris->FormId] = 'create::::::delete::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::delete::approve::addon') {
                        $jasar[$baris->FormId] = 'create::::::delete::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::delete::approve::preview') {
                        $jasar[$baris->FormId] = 'create::::::delete::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::delete::approve::print') {
                        $jasar[$baris->FormId] = 'create::::::delete::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::preview::addon') {
                        $jasar[$baris->FormId] = 'create::::update::::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::print::addon') {
                        $jasar[$baris->FormId] = 'create::::update::::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::print::preview') {
                        $jasar[$baris->FormId] = 'create::::update::::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::delete::addon') {
                        $jasar[$baris->FormId] = 'create::::update::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::delete::preview') {
                        $jasar[$baris->FormId] = 'create::::update::delete::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::delete::approve') {
                        $jasar[$baris->FormId] = 'create::::update::delete::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::preview::addon') {
                        $jasar[$baris->FormId] = 'create::view::::::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::print::addon') {
                        $jasar[$baris->FormId] = 'create::view::::::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::print::preview') {
                        $jasar[$baris->FormId] = 'create::view::::::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::addon') {
                        $jasar[$baris->FormId] = 'create::view::update::::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::preview') {
                        $jasar[$baris->FormId] = 'create::view::update::::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::approve') {
                        $jasar[$baris->FormId] = 'create::view::update::::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::delete') { //ilang 4
                        $jasar[$baris->FormId] = 'create::view::update::delete::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::preview::addon') { //ilang 5
                        $jasar[$baris->FormId] = 'create::::::::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::print::addon') {
                        $jasar[$baris->FormId] = 'create::::::::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::print::preview') {
                        $jasar[$baris->FormId] = 'create::::::::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::addon') {
                        $jasar[$baris->FormId] = 'create::::update::::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::delete') {
                        $jasar[$baris->FormId] = 'create::::update::delete::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::preview') {
                        $jasar[$baris->FormId] = 'create::::update::::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::addon') {
                        $jasar[$baris->FormId] = 'create::::::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::approve::preview') {
                        $jasar[$baris->FormId] = 'create::::::::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::approve::addon') {
                        $jasar[$baris->FormId] = 'create::::::::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::delete::preview') {
                        $jasar[$baris->FormId] = 'create::::::delete::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::addon') {
                        $jasar[$baris->FormId] = 'create::view::::::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::preview') {
                        $jasar[$baris->FormId] = 'create::view::::::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update') {
                        $jasar[$baris->FormId] = 'create::view::update::::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::approve') {//ilang 5
                        $jasar[$baris->FormId] = 'create::::update::::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::delete::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = 'create::view::update::delete::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::approve') {
                        $jasar[$baris->FormId] = 'create::view::::::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update') {
                        $jasar[$baris->FormId] = 'create::view::update::::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::delete') {
                        $jasar[$baris->FormId] = 'create::view::::delete::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::print') {
                        $jasar[$baris->FormId] = 'create::view::::::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::preview') {
                        $jasar[$baris->FormId] = 'create::view::::::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::addon') {
                        $jasar[$baris->FormId] = 'create::view::::::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::delete::print') {
                        $jasar[$baris->FormId] = 'create::::update::delete::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::delete::print::preview') {
                        $jasar[$baris->FormId] = 'create::::update::delete::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::approve::preview') {
                        $jasar[$baris->FormId] = 'create::view::update::::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::delete::print') {
                        $jasar[$baris->FormId] = 'create::::::delete::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::print') {
                        $jasar[$baris->FormId] = 'create::view::update::::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::approve::print') {
                        $jasar[$baris->FormId] = 'create::view::update::::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::approve::print') {
                        $jasar[$baris->FormId] = 'create::view::::::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::approve::print') {
                        $jasar[$baris->FormId] = 'create::::::::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::delete::approve') {
                        $jasar[$baris->FormId] = 'create::::::delete::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::delete::approve') {
                        $jasar[$baris->FormId] = 'create::view::::delete::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::delete::addon') {
                        $jasar[$baris->FormId] = 'create::view::::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::approve::addon') {
                        $jasar[$baris->FormId] = 'create::view::::::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::update::approve::addon') {
                        $jasar[$baris->FormId] = 'create::view::update::::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::delete::addon') {
                        $jasar[$baris->FormId] = 'create::::::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::approve::preview') {
                        $jasar[$baris->FormId] = 'create::::update::::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::approve::preview') {
                        $jasar[$baris->FormId] = 'create::view::::::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::approve::preview') {
                        $jasar[$baris->FormId] = 'create::view::::::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view') {
                        $jasar[$baris->FormId] = 'create::view::::::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::update::approve::addon') {
                        $jasar[$baris->FormId] = 'create::::update::::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::delete::print') {
                        $jasar[$baris->FormId] = 'create::view::::delete::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'create::view::delete::preview') {
                        $jasar[$baris->FormId] = 'create::view::::delete::::::preview::';
                        $changed = 1;
                    }
//CREATE
//VIEW
                    if ($jasar[$baris->FormId] == 'view') {
                        $jasar[$baris->FormId] = '::view::::::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update') {
                        $jasar[$baris->FormId] = '::view::update::::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::delete') {
                        $jasar[$baris->FormId] = '::view::::delete::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::approve') {
                        $jasar[$baris->FormId] = '::view::::::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::print') {
                        $jasar[$baris->FormId] = '::view::::::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::preview') {
                        $jasar[$baris->FormId] = '::view::::::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::addon') {
                        $jasar[$baris->FormId] = '::view::::::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update') {
                        $jasar[$baris->FormId] = '::view::::::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::delete::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::view::::delete::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::view::update::::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::delete::print::preview::addon') {
                        $jasar[$baris->FormId] = '::view::update::delete::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::delete::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::view::update::delete::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::delete::approve::print::addon') {
                        $jasar[$baris->FormId] = '::view::update::delete::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::delete::approve::print::preview') {
                        $jasar[$baris->FormId] = '::view::update::delete::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::view::::::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::delete::print::preview::addon') {
                        $jasar[$baris->FormId] = '::view::::delete::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::delete::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::view::::delete::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::delete::approve::print::addon') {
                        $jasar[$baris->FormId] = '::view::::delete::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::delete::approve::print::preview') {
                        $jasar[$baris->FormId] = '::view::::delete::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::print::preview::addon') {
                        $jasar[$baris->FormId] = '::view::update::::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::view::update::::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::approve::print::addon') {
                        $jasar[$baris->FormId] = '::view::update::::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::approve::print::previe') {
                        $jasar[$baris->FormId] = '::view::update::::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::delete::preview::addon') {
                        $jasar[$baris->FormId] = '::view::update::delete::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::delete::print::addon') {
                        $jasar[$baris->FormId] = '::view::update::delete::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::delete::print::preview') {
                        $jasar[$baris->FormId] = '::view::update::delete::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::delete::approve::addon') {
                        $jasar[$baris->FormId] = '::view::update::delete::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::delete::approve::preview') {
                        $jasar[$baris->FormId] = '::view::update::delete::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::delete::approve::print') {
                        $jasar[$baris->FormId] = '::view::update::delete::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::print::preview::addon') {
                        $jasar[$baris->FormId] = '::view::::::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::view::::::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::approve::print::addon') {
                        $jasar[$baris->FormId] = '::view::::::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::approve::print::preview') {
                        $jasar[$baris->FormId] = '::view::::::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::delete::preview::addon') {
                        $jasar[$baris->FormId] = '::view::::delete::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::delete::print::addon') {
                        $jasar[$baris->FormId] = '::view::::delete::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::delete::print::preview') {
                        $jasar[$baris->FormId] = '::view::::delete::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::delete::approve::addon') {
                        $jasar[$baris->FormId] = '::view::::delete::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::delete::approve::preview') {
                        $jasar[$baris->FormId] = '::view::::delete::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::delete::approve::print') {
                        $jasar[$baris->FormId] = '::view::::delete::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::preview::addon') {
                        $jasar[$baris->FormId] = '::view::update::::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::print::addon') {
                        $jasar[$baris->FormId] = '::view::update::::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::print::preview') {
                        $jasar[$baris->FormId] = '::view::update::::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::delete::addon') {
                        $jasar[$baris->FormId] = '::view::update::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::delete::preview') {
                        $jasar[$baris->FormId] = '::view::update::delete::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::delete::approve') {
                        $jasar[$baris->FormId] = '::view::update::delete::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::preview::addon') {
                        $jasar[$baris->FormId] = '::view::::::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::print::addon') {
                        $jasar[$baris->FormId] = '::view::::::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::print::preview') {
                        $jasar[$baris->FormId] = '::view::::::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::approve::addon') {
                        $jasar[$baris->FormId] = '::view::::::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::approve::preview') {
                        $jasar[$baris->FormId] = '::view::::::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::approve::print') {
                        $jasar[$baris->FormId] = '::view::::::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::delete::addon') {
                        $jasar[$baris->FormId] = '::view::::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::delete::preview') {
                        $jasar[$baris->FormId] = '::view::::delete::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::delete::print') {
                        $jasar[$baris->FormId] = '::view::::delete::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::delete::approve') {
                        $jasar[$baris->FormId] = '::view::::delete::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::addon') {
                        $jasar[$baris->FormId] = '::view::update::::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::preview') {
                        $jasar[$baris->FormId] = '::view::update::::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::approve') {
                        $jasar[$baris->FormId] = '::view::update::::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::delete') {
                        $jasar[$baris->FormId] = '::view::update::delete::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::delete::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::view::update::delete::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::approve::print') {
                        $jasar[$baris->FormId] = '::view::::::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::approve::preview') {
                        $jasar[$baris->FormId] = '::view::::::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::approve::addon') {
                        $jasar[$baris->FormId] = '::view::::::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::approve') {
                        $jasar[$baris->FormId] = '::view::update::::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::approve::print') {
                        $jasar[$baris->FormId] = '::view::update::::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::approve::preview') {
                        $jasar[$baris->FormId] = '::view::update::::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::approve::addon') {
                        $jasar[$baris->FormId] = '::view::update::::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::addon') {
                        $jasar[$baris->FormId] = '::view::update::::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::delete::addon') {
                        $jasar[$baris->FormId] = '::view::::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::print::addon') {
                        $jasar[$baris->FormId] = '::view::::::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::preview::addon') {
                        $jasar[$baris->FormId] = '::view::::::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::print::preview') {
                        $jasar[$baris->FormId] = '::view::::::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::delete::preview') {
                        $jasar[$baris->FormId] = '::view::::delete::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::preview') {
                        $jasar[$baris->FormId] = '::view::update::::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::delete::print') {
                        $jasar[$baris->FormId] = '::view::update::delete::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::print') {
                        $jasar[$baris->FormId] = '::view::update::::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::delete::print') {
                        $jasar[$baris->FormId] = '::view::::delete::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::delete::approve') {
                        $jasar[$baris->FormId] = '::view::::delete::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::delete') {
                        $jasar[$baris->FormId] = '::view::update::delete::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'view::update::approve::print::preview') {
                        $jasar[$baris->FormId] = '::view::update::::approve::print::preview::';
                        $changed = 1;
                    }
//VIEW
//UPDATE
                    if ($jasar[$baris->FormId] == 'update') {
                        $jasar[$baris->FormId] = '::::update::::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::approve') {
                        $jasar[$baris->FormId] = '::::update::::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::print') {
                        $jasar[$baris->FormId] = '::::update::::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::preview') {
                        $jasar[$baris->FormId] = '::::update::::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::addon') {
                        $jasar[$baris->FormId] = '::::update::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::delete') {
                        $jasar[$baris->FormId] = '::::update::delete::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::update::::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::delete::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::update::delete::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::delete::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::::update::delete::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::delete::approve::print::addon') {
                        $jasar[$baris->FormId] = '::::update::delete::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::delete::approve::print::preview') {
                        $jasar[$baris->FormId] = '::::update::delete::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::update::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::delete::preview::addon') {
                        $jasar[$baris->FormId] = '::::update::delete::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::delete::approve::addon') {
                        $jasar[$baris->FormId] = '::::update::delete::approve::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::delete::approve::print') {
                        $jasar[$baris->FormId] = '::::update::delete::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::delete::preview::addon') {
                        $jasar[$baris->FormId] = '::::update::delete::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::::update::::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::approve::print::addon') {
                        $jasar[$baris->FormId] = '::::update::::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::approve::print::preview') {
                        $jasar[$baris->FormId] = '::::update::::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::delete::print::addon') {
                        $jasar[$baris->FormId] = '::::update::delete::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::delete::print::preview') {
                        $jasar[$baris->FormId] = '::::update::delete::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::delete::approve::preview::') {
                        $jasar[$baris->FormId] = '::::update::delete::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::preview::addon') {
                        $jasar[$baris->FormId] = '::::update::::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::delete::addon') {
                        $jasar[$baris->FormId] = '::::update::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::delete::approve') {
                        $jasar[$baris->FormId] = '::::update::delete::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::print::addon') {
                        $jasar[$baris->FormId] = '::::update::::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::print::preview') {
                        $jasar[$baris->FormId] = '::::update::::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::approve::addon') {
                        $jasar[$baris->FormId] = '::::update::::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::approve::print') {
                        $jasar[$baris->FormId] = '::::update::::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::delete::print') {
                        $jasar[$baris->FormId] = '::::update::delete::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::delete::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::update::delete::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::delete::preview') {
                        $jasar[$baris->FormId] = '::::update::delete::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::approve::preview') {
                        $jasar[$baris->FormId] = '::::update::::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'update::delete::approve::preview') {
                        $jasar[$baris->FormId] = '::::update::delete::approve::::preview::';
                        $changed = 1;
                    }
//UPDATE
//DELETE
                    if ($jasar[$baris->FormId] == 'delete') {
                        $jasar[$baris->FormId] = '::::::delete::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'delete::approve') {
                        $jasar[$baris->FormId] = '::::::delete::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'delete::print') {
                        $jasar[$baris->FormId] = '::::::delete::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'delete::preview') {
                        $jasar[$baris->FormId] = '::::::delete::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'delete::addon') {
                        $jasar[$baris->FormId] = '::::::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'delete::approve::print::addon') {
                        $jasar[$baris->FormId] = '::::::delete::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'delete::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::::::delete::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'delete::approve::print::addon') {
                        $jasar[$baris->FormId] = '::::::delete::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'delete::print::prevew::addon') {
                        $jasar[$baris->FormId] = '::::::delete::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'delete::approve::print') {
                        $jasar[$baris->FormId] = '::::::delete::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'delete::print::preview') {
                        $jasar[$baris->FormId] = '::::::delete::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'delete::preview::addon') {
                        $jasar[$baris->FormId] = '::::::delete::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'delete::approve::addon') {
                        $jasar[$baris->FormId] = '::::::delete::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'delete::print::addon') {
                        $jasar[$baris->FormId] = '::::::delete::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'delete::preview::addon') {
                        $jasar[$baris->FormId] = '::::::delete::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'delete::approve::preview') {
                        $jasar[$baris->FormId] = '::::::delete::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'delete::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::::delete::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'delete::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::::delete::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'delete::approve::print::preview') {
                        $jasar[$baris->FormId] = '::::::delete::approve::print::preview::';
                        $changed = 1;
                    }
//DELETE
//APPROVE
                    if ($jasar[$baris->FormId] == 'approve') {
                        $jasar[$baris->FormId] = '::::::::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'approve::print') {
                        $jasar[$baris->FormId] = '::::::::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'approve::preview') {
                        $jasar[$baris->FormId] = '::::::::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'approve::addon') {
                        $jasar[$baris->FormId] = '::::::::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'approve::print::addon') {
                        $jasar[$baris->FormId] = '::::::::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'approve::preview::addon') {
                        $jasar[$baris->FormId] = '::::::::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'approve::print::preview') {
                        $jasar[$baris->FormId] = '::::::::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::::::approve::print::preview::addon';
                        $changed = 1;
                    }
//APPROVE
//PRINT
                    if ($jasar[$baris->FormId] == 'print') {
                        $jasar[$baris->FormId] = '::::::::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'print::addon') {
                        $jasar[$baris->FormId] = '::::::::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'print::preview') {
                        $jasar[$baris->FormId] = '::::::::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'print::preview::addon') {
                        $jasar[$baris->FormId] = '::::::::::print::preview::addon';
                        $changed = 1;
                    }
//PRINT
//PREVIEW
                    if ($jasar[$baris->FormId] == 'preview') {
                        $jasar[$baris->FormId] = '::::::::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == 'preview::addon') {
                        $jasar[$baris->FormId] = '::::::::::::preview::addon';
                        $changed = 1;
                    }
//PREVIEW
//ADDON
                    if ($jasar[$baris->FormId] == 'addon') {
                        $jasar[$baris->FormId] = '::::::::::::::addon';
                        $changed = 1;
                    }
//ADDON

                    $a = explode('::', $jasar[$baris->FormId]);
//PENAMBAHAN "::" PER AUTH AGAR SAMA RATA
                    if (count($a) == 0) {
                        $jasar[$baris->FormId] = $jasar[$baris->FormId] . "::::::::::::::::";
                    }
                    if (count($a) == 1) {
                        $jasar[$baris->FormId] = $jasar[$baris->FormId] . "::::::::::::::";
                    }
                    if (count($a) == 2) {
                        $jasar[$baris->FormId] = $jasar[$baris->FormId] . "::::::::::::";
                    }
                    if (count($a) == 3) {
                        $jasar[$baris->FormId] = $jasar[$baris->FormId] . "::::::::::";
                    }
//PENAMBAHAN "::" PER AUTH AGAR SAMA RATA
                    $jasar[$baris->FormId] = $baris->FormId . "::" . $jasar[$baris->FormId] . "|";
                }

            endforeach;
// LOOP for get authority checbox
// LOOP for get formname
            $authority = '';
            foreach ($countform->result() as $baris):
                $authority = $authority . $jasar[$baris->FormId];
            endforeach;
//        echo $authority;
// LOOP for get formname
            if ($changed == 1) {
                $qinsert = "insert into stp_userrole(Iid,CompanyId,BranchId,Code,Name,Remarks,RoleAuthority,IsActive,InputBy,InputDate,LastEditBy,LastEditDate) "
                        . "values('$piid','$pcompanyid','$pbranchid','$pcode','$pname','$premarks','$authority','$pactivestatus','$puser',GETDATE(),'$puser',GETDATE())";
//                echo $qinsert;
                $this->db->query($qinsert);
                redirect('role/viewuser/1');
            } else {
                echo "Format belum terdaftar";
            }
        }
    }

    function editsaverole() {
        $session = $this->session->userdata('session_data');
        $puser = $session['nama'];
        $piid = $this->input->post('iid', TRUE);
        $pcompanyid = $this->input->post('companyid', TRUE);
//        $pbranchid = $this->input->post('branchid', TRUE);
        $pcode = $this->input->post('code', TRUE);
        $pname = $this->input->post('name', TRUE);
        $premarks = $this->input->post('remarks', TRUE);
        $pactivestatus = $this->input->post('activestatus', TRUE);

        $query = "select FormId from stp_module where IsActive = 1";
        $countform = $this->db->query($query);

        $this->form_validation->set_rules('iid', '', 'required');
        $this->form_validation->set_rules('code', '', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->viewuser('3:::Gagal Input, Data tidak Lengkap');
        } else {

            $changed = 0;
// LOOP for get authority checbox
            foreach ($countform->result() as $baris):

                if ($_POST[$baris->FormId]) {
                    $jasar[$baris->FormId] = '';
                    $jasa = $_POST[$baris->FormId];
                    foreach ($jasa as $js) {
                        $jasar[$baris->FormId] = $jasar[$baris->FormId] . $js . "::";
                    }
                    $jasar[$baris->FormId] = substr($jasar[$baris->FormId], 0, -2);

//CREATE BEDA DENGAN SAVE
                    if ($jasar[$baris->FormId] == '::create') {
                        $jasar[$baris->FormId] = '::create::::::::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::delete') {
                        $jasar[$baris->FormId] = '::create::::::delete::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update') {
                        $jasar[$baris->FormId] = '::create::::update::::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::approve') {
                        $jasar[$baris->FormId] = '::create::::::::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::print') {
                        $jasar[$baris->FormId] = '::create::::::::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::preview') {
                        $jasar[$baris->FormId] = '::create::::::::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::addon') {
                        $jasar[$baris->FormId] = '::create::::::::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::delete::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::create::::update::delete::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::delete::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::create::view::::delete::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::create::view::update::::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::delete::print::preview::addon') {
                        $jasar[$baris->FormId] = '::create::view::update::delete::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::delete::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::create::view::update::delete::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::delete::approve::print::addon') {
                        $jasar[$baris->FormId] = '::create::view::update::delete::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::delete::approve::print::preview') {
                        $jasar[$baris->FormId] = '::create::view::update::delete::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::delete::approve::print::preview::addon') { //ilang 2
                        $jasar[$baris->FormId] = '::create::::::delete::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::create::::update::::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::delete::print::preview::addon') {
                        $jasar[$baris->FormId] = '::create::::update::delete::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::delete::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::create::::update::delete::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::delete::approve::print::addon') {
                        $jasar[$baris->FormId] = '::create::::update::delete::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::delete::approve::print::preview') {
                        $jasar[$baris->FormId] = '::create::::update::delete::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::create::view::::::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::delete::print::preview::addon') {
                        $jasar[$baris->FormId] = '::create::view::::delete::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::delete::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::create::view::::delete::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::delete::approve::print::addon') {
                        $jasar[$baris->FormId] = '::create::view::::delete::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::delete::approve::print::preview') {
                        $jasar[$baris->FormId] = '::create::view::::delete::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::print::preview::addon') {
                        $jasar[$baris->FormId] = '::create::view::update::::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::create::view::update::::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::approve::print::addon') {
                        $jasar[$baris->FormId] = '::create::view::update::::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::approve::print::preview') {
                        $jasar[$baris->FormId] = '::create::view::update::::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::delete::preview::addon') {
                        $jasar[$baris->FormId] = '::create::view::update::delete::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::delete::print::addon') {
                        $jasar[$baris->FormId] = '::create::view::update::delete::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::delete::print::preview') {
                        $jasar[$baris->FormId] = '::create::view::update::delete::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::delete::approve::addon') {
                        $jasar[$baris->FormId] = '::create::view::update::delete::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::delete::approve::preview') {
                        $jasar[$baris->FormId] = '::create::view::update::delete::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::delete::approve::print') { //ilang 2
                        $jasar[$baris->FormId] = '::create::view::update::delete::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::approve::print::preview::addon') { //ilang 3
                        $jasar[$baris->FormId] = '::create::::::::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::delete::print::preview::addon') {
                        $jasar[$baris->FormId] = '::create::::::delete::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::delete::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::create::::::delete::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::delete::approve::print::addon') {
                        $jasar[$baris->FormId] = '::create::::::delete::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::delete::approve::print::preview') {
                        $jasar[$baris->FormId] = '::create::::::delete::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::print::preview::addon') {
                        $jasar[$baris->FormId] = '::create::::update::::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::create::::update::::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::approve::print::addon') {
                        $jasar[$baris->FormId] = '::create::::update::::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::approve::print::preview') {
                        $jasar[$baris->FormId] = '::create::::update::::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::delete::preview::addon') {
                        $jasar[$baris->FormId] = '::create::::update::delete::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::delete::print::addon') {
                        $jasar[$baris->FormId] = '::create::::update::delete::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::delete::print::preview') {
                        $jasar[$baris->FormId] = '::create::::update::delete::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::delete::approve::addon') {
                        $jasar[$baris->FormId] = '::create::::update::delete::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::delete::approve::preview') {
                        $jasar[$baris->FormId] = '::create::::update::delete::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::delete::approve::print') {
                        $jasar[$baris->FormId] = '::create::::update::delete::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::print::preview::addon') {
                        $jasar[$baris->FormId] = '::create::view::::::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::create::view::::::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::approve::print::addon') {
                        $jasar[$baris->FormId] = '::create::view::::::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::approve::print::preview') {
                        $jasar[$baris->FormId] = '::create::view::::::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::delete::preview::addon') {
                        $jasar[$baris->FormId] = '::create::view::::delete::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::delete::print::addon') {
                        $jasar[$baris->FormId] = '::create::view::::delete::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::delete::print::preview') {
                        $jasar[$baris->FormId] = '::create::view::::delete::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::delete::approve::addon') {
                        $jasar[$baris->FormId] = '::create::view::::delete::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::delete::approve::preview') {
                        $jasar[$baris->FormId] = '::create::view::::delete::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::delete::approve::print') {
                        $jasar[$baris->FormId] = '::create::view::::delete::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::preview::addon') {
                        $jasar[$baris->FormId] = '::create::view::update::::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::print::addon') {
                        $jasar[$baris->FormId] = '::create::view::update::::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::print::preview') {
                        $jasar[$baris->FormId] = '::create::view::update::::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::delete::addon') {
                        $jasar[$baris->FormId] = '::create::view::update::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::delete::preview') {
                        $jasar[$baris->FormId] = '::create::view::update::delete::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::delete::print') {
                        $jasar[$baris->FormId] = '::create::view::update::delete::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::delete::approve') { //ilang 3
                        $jasar[$baris->FormId] = '::create::view::update::delete::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::print::preview::addon') {// ilang 4
                        $jasar[$baris->FormId] = '::create::::::::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::create::::::::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::approve::print::addon') {
                        $jasar[$baris->FormId] = '::create::::::::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::approve::print::preview') {
                        $jasar[$baris->FormId] = '::create::::::::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::delete::preview::addon') {
                        $jasar[$baris->FormId] = '::create::::::delete::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::delete::print::addon') {
                        $jasar[$baris->FormId] = '::create::::::delete::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::delete::print::preview') {
                        $jasar[$baris->FormId] = '::create::::::delete::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::delete::approve::addon') {
                        $jasar[$baris->FormId] = '::create::::::delete::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::delete::approve::preview') {
                        $jasar[$baris->FormId] = '::create::::::delete::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::delete::approve::print') {
                        $jasar[$baris->FormId] = '::create::::::delete::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::preview::addon') {
                        $jasar[$baris->FormId] = '::create::::update::::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::print::addon') {
                        $jasar[$baris->FormId] = '::create::::update::::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::print::preview') {
                        $jasar[$baris->FormId] = '::create::::update::::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::delete::addon') {
                        $jasar[$baris->FormId] = '::create::::update::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::delete::preview') {
                        $jasar[$baris->FormId] = '::create::::update::delete::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::delete::approve') {
                        $jasar[$baris->FormId] = '::create::::update::delete::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::preview::addon') {
                        $jasar[$baris->FormId] = '::create::view::::::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::print::addon') {
                        $jasar[$baris->FormId] = '::create::view::::::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::print::preview') {
                        $jasar[$baris->FormId] = '::create::view::::::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::addon') {
                        $jasar[$baris->FormId] = '::create::view::update::::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::preview') {
                        $jasar[$baris->FormId] = '::create::view::update::::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::approve') {
                        $jasar[$baris->FormId] = '::create::view::update::::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::delete') { //ilang 4
                        $jasar[$baris->FormId] = '::create::view::update::delete::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::preview::addon') { //ilang 5
                        $jasar[$baris->FormId] = '::create::::::::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::print::addon') {
                        $jasar[$baris->FormId] = '::create::::::::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::print::preview') {
                        $jasar[$baris->FormId] = '::create::::::::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::addon') {
                        $jasar[$baris->FormId] = '::create::::update::::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::delete') {
                        $jasar[$baris->FormId] = '::create::::update::delete::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::preview') {
                        $jasar[$baris->FormId] = '::create::::update::::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::addon') {
                        $jasar[$baris->FormId] = '::create::::::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::approve::preview') {
                        $jasar[$baris->FormId] = '::create::::::::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::approve::addon') {
                        $jasar[$baris->FormId] = '::create::::::::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::delete::preview') {
                        $jasar[$baris->FormId] = '::create::::::delete::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::addon') {
                        $jasar[$baris->FormId] = '::create::view::::::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::preview') {
                        $jasar[$baris->FormId] = '::create::view::::::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update') {
                        $jasar[$baris->FormId] = '::create::view::update::::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::approve') {//ilang 5
                        $jasar[$baris->FormId] = '::create::::update::::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::delete::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::create::view::update::delete::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::approve') {
                        $jasar[$baris->FormId] = '::create::view::::::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update') {
                        $jasar[$baris->FormId] = '::create::view::update::::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::delete') {
                        $jasar[$baris->FormId] = '::create::view::::delete::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::print') {
                        $jasar[$baris->FormId] = '::create::view::::::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::preview') {
                        $jasar[$baris->FormId] = '::create::view::::::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::addon') {
                        $jasar[$baris->FormId] = '::create::view::::::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::delete::print') {
                        $jasar[$baris->FormId] = '::create::::update::delete::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::delete::print::preview') {
                        $jasar[$baris->FormId] = '::create::::update::delete::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::approve::preview') {
                        $jasar[$baris->FormId] = '::create::view::update::::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::delete::print') {
                        $jasar[$baris->FormId] = '::create::::::delete::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::approve::print') {
                        $jasar[$baris->FormId] = '::create::view::::::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::approve::print') {
                        $jasar[$baris->FormId] = '::create::::::::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::delete::approve') {
                        $jasar[$baris->FormId] = '::create::::::delete::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::delete::approve') {
                        $jasar[$baris->FormId] = '::create::view::::delete::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::delete::addon') {
                        $jasar[$baris->FormId] = '::create::view::::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::approve::addon') {
                        $jasar[$baris->FormId] = '::create::view::::::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::update::approve::addon') {
                        $jasar[$baris->FormId] = '::create::view::update::::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::delete::addon') {
                        $jasar[$baris->FormId] = '::create::::::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::approve::preview') {
                        $jasar[$baris->FormId] = '::create::view::::::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::approve::preview') {
                        $jasar[$baris->FormId] = '::create::view::::::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view') {
                        $jasar[$baris->FormId] = '::create::view::::::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::update::approve::addon') {
                        $jasar[$baris->FormId] = '::create::::update::::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::delete::print') {
                        $jasar[$baris->FormId] = '::create::view::::delete::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::create::view::delete::preview') {
                        $jasar[$baris->FormId] = '::create::view::::delete::::::preview::';
                        $changed = 1;
                    }
//CREATE
//VIEW BEDA DENGAN SAVE
                    if ($jasar[$baris->FormId] == '::view') {
                        $jasar[$baris->FormId] = '::::view::::::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update') {
                        $jasar[$baris->FormId] = '::::view::update::::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::delete') {
                        $jasar[$baris->FormId] = '::::view::::delete::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::approve') {
                        $jasar[$baris->FormId] = '::::view::::::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::print') {
                        $jasar[$baris->FormId] = '::::view::::::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::preview') {
                        $jasar[$baris->FormId] = '::::view::::::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::addon') {
                        $jasar[$baris->FormId] = '::::view::::::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update') {
                        $jasar[$baris->FormId] = '::::view::::::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::delete::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::view::::delete::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::view::update::::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::delete::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::view::update::delete::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::delete::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::::view::update::delete::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::delete::approve::print::addon') {
                        $jasar[$baris->FormId] = '::::view::update::delete::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::delete::approve::print::preview') {
                        $jasar[$baris->FormId] = '::::view::update::delete::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::view::::::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::delete::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::view::::delete::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::delete::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::::view::::delete::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::delete::approve::print::addon') {
                        $jasar[$baris->FormId] = '::::view::::delete::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::delete::approve::print::preview') {
                        $jasar[$baris->FormId] = '::::view::::delete::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::view::update::::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::::view::update::::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::approve::print::addon') {
                        $jasar[$baris->FormId] = '::::view::update::::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::approve::print::previe') {
                        $jasar[$baris->FormId] = '::::view::update::::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::delete::preview::addon') {
                        $jasar[$baris->FormId] = '::::view::update::delete::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::delete::print::addon') {
                        $jasar[$baris->FormId] = '::::view::update::delete::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::delete::print::preview') {
                        $jasar[$baris->FormId] = '::::view::update::delete::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::delete::approve::addon') {
                        $jasar[$baris->FormId] = '::::view::update::delete::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::delete::approve::preview') {
                        $jasar[$baris->FormId] = '::::view::update::delete::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::delete::approve::print') {
                        $jasar[$baris->FormId] = '::::view::update::delete::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::view::::::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::::view::::::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::approve::print::addon') {
                        $jasar[$baris->FormId] = '::::view::::::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::approve::print::preview') {
                        $jasar[$baris->FormId] = '::::view::::::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::delete::preview::addon') {
                        $jasar[$baris->FormId] = '::::view::::delete::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::delete::print::addon') {
                        $jasar[$baris->FormId] = '::::view::::delete::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::delete::print::preview') {
                        $jasar[$baris->FormId] = '::::view::::delete::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::delete::approve::addon') {
                        $jasar[$baris->FormId] = '::::view::::delete::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::delete::approve::preview') {
                        $jasar[$baris->FormId] = '::::view::::delete::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::delete::approve::print') {
                        $jasar[$baris->FormId] = '::::view::::delete::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::preview::addon') {
                        $jasar[$baris->FormId] = '::::view::update::::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::print::addon') {
                        $jasar[$baris->FormId] = '::::view::update::::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::print::preview') {
                        $jasar[$baris->FormId] = '::::view::update::::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::delete::addon') {
                        $jasar[$baris->FormId] = '::::view::update::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::delete::preview') {
                        $jasar[$baris->FormId] = '::::view::update::delete::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::delete::approve') {
                        $jasar[$baris->FormId] = '::::view::update::delete::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::preview::addon') {
                        $jasar[$baris->FormId] = '::::view::::::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::print::addon') {
                        $jasar[$baris->FormId] = '::::view::::::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::print::preview') {
                        $jasar[$baris->FormId] = '::::view::::::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::approve::addon') {
                        $jasar[$baris->FormId] = '::::view::::::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::approve::preview') {
                        $jasar[$baris->FormId] = '::::view::::::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::approve::print') {
                        $jasar[$baris->FormId] = '::::view::::::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::delete::addon') {
                        $jasar[$baris->FormId] = '::::view::::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::delete::preview') {
                        $jasar[$baris->FormId] = '::::view::::delete::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::delete::print') {
                        $jasar[$baris->FormId] = '::::view::::delete::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::delete::approve') {
                        $jasar[$baris->FormId] = '::::view::::delete::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::addon') {
                        $jasar[$baris->FormId] = '::::view::update::::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::preview') {
                        $jasar[$baris->FormId] = '::::view::update::::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::approve') {
                        $jasar[$baris->FormId] = '::::view::update::::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::delete') {
                        $jasar[$baris->FormId] = '::::view::update::delete::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::delete::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::view::update::delete::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::approve::print') {
                        $jasar[$baris->FormId] = '::::view::::::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::approve::preview') {
                        $jasar[$baris->FormId] = '::::view::::::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::approve::addon') {
                        $jasar[$baris->FormId] = '::::view::::::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::approve') {
                        $jasar[$baris->FormId] = '::::view::update::::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::approve::print') {
                        $jasar[$baris->FormId] = '::::view::update::::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::approve::preview') {
                        $jasar[$baris->FormId] = '::::view::update::::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::approve::addon') {
                        $jasar[$baris->FormId] = '::::view::update::::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::addon') {
                        $jasar[$baris->FormId] = '::::view::update::::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::delete::addon') {
                        $jasar[$baris->FormId] = '::::view::::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::print::addon') {
                        $jasar[$baris->FormId] = '::::view::::::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::preview::addon') {
                        $jasar[$baris->FormId] = '::::view::::::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::print::preview') {
                        $jasar[$baris->FormId] = '::::view::::::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::delete::preview') {
                        $jasar[$baris->FormId] = '::::view::::delete::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::preview') {
                        $jasar[$baris->FormId] = '::::view::update::::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::delete::print') {
                        $jasar[$baris->FormId] = '::::view::update::delete::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::print') {
                        $jasar[$baris->FormId] = '::::view::update::::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::delete::print') {
                        $jasar[$baris->FormId] = '::::view::::delete::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::delete::approve') {
                        $jasar[$baris->FormId] = '::::view::::delete::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::delete') {
                        $jasar[$baris->FormId] = '::::view::update::delete::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::view::update::approve::print::preview') {
                        $jasar[$baris->FormId] = '::::view::update::::approve::print::preview::';
                        $changed = 1;
                    }
//VIEW
//UPDATE BEDA DENGAN SAVE
                    if ($jasar[$baris->FormId] == '::update') {
                        $jasar[$baris->FormId] = '::::::update::::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::delete') {
                        $jasar[$baris->FormId] = '::::::update::delete::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::approve') {
                        $jasar[$baris->FormId] = '::::::update::::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::print') {
                        $jasar[$baris->FormId] = '::::::update::::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::preview') {
                        $jasar[$baris->FormId] = '::::::update::::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::addon') {
                        $jasar[$baris->FormId] = '::::::update::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::delete') {
                        $jasar[$baris->FormId] = '::::::update::delete::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::::update::::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::delete::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::::update::delete::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::delete::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::::::update::delete::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::delete::approve::print::addon') {
                        $jasar[$baris->FormId] = '::::::update::delete::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::delete::approve::print::preview') {
                        $jasar[$baris->FormId] = '::::::update::delete::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::::update::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::delete::preview::addon') {
                        $jasar[$baris->FormId] = '::::::update::delete::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::delete::approve::addon') {
                        $jasar[$baris->FormId] = '::::::update::delete::approve::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::delete::approve::print') {
                        $jasar[$baris->FormId] = '::::::update::delete::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::delete::preview::addon') {
                        $jasar[$baris->FormId] = '::::::update::delete::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::::::update::::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::approve::print::addon') {
                        $jasar[$baris->FormId] = '::::::update::::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::approve::print::preview') {
                        $jasar[$baris->FormId] = '::::::update::::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::delete::print::addon') {
                        $jasar[$baris->FormId] = '::::::update::delete::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::delete::print::preview') {
                        $jasar[$baris->FormId] = '::::::update::delete::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::delete::approve::preview::') {
                        $jasar[$baris->FormId] = '::::::update::delete::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::preview::addon') {
                        $jasar[$baris->FormId] = '::::::update::::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::delete::addon') {
                        $jasar[$baris->FormId] = '::::::update::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::delete::approve') {
                        $jasar[$baris->FormId] = '::::::update::delete::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::print::addon') {
                        $jasar[$baris->FormId] = '::::::update::::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::print::preview') {
                        $jasar[$baris->FormId] = '::::::update::::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::approve::addon') {
                        $jasar[$baris->FormId] = '::::::update::::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::approve::print') {
                        $jasar[$baris->FormId] = '::::::update::::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::delete::print') {
                        $jasar[$baris->FormId] = '::::::update::delete::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::delete::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::::update::delete::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::delete::preview') {
                        $jasar[$baris->FormId] = '::::::update::delete::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::approve::preview') {
                        $jasar[$baris->FormId] = '::::::update::::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::update::delete::approve::preview') {
                        $jasar[$baris->FormId] = '::::::update::delete::approve::::preview::';
                        $changed = 1;
                    }
//UPDATE
//DELETE BEDA DENGAN SAVE
                    if ($jasar[$baris->FormId] == '::delete') {
                        $jasar[$baris->FormId] = '::::::::delete::::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::delete::print') {
                        $jasar[$baris->FormId] = '::::::::delete::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::delete::preview') {
                        $jasar[$baris->FormId] = '::::::::delete::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::delete::addon') {
                        $jasar[$baris->FormId] = '::::::::delete::::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::delete::approve::print::addon') {
                        $jasar[$baris->FormId] = '::::::::delete::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::delete::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::::::::delete::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::delete::approve::print::addon') {
                        $jasar[$baris->FormId] = '::::::::delete::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::delete::print::prevew::addon') {
                        $jasar[$baris->FormId] = '::::::::delete::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::delete::approve::print') {
                        $jasar[$baris->FormId] = '::::::::delete::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::delete::print::preview') {
                        $jasar[$baris->FormId] = '::::::::delete::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::delete::preview::addon') {
                        $jasar[$baris->FormId] = '::::::::delete::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::delete::approve::addon') {
                        $jasar[$baris->FormId] = '::::::::delete::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::delete::print::addon') {
                        $jasar[$baris->FormId] = '::::::::delete::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::delete::preview::addon') {
                        $jasar[$baris->FormId] = '::::::::delete::::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::delete::approve::preview') {
                        $jasar[$baris->FormId] = '::::::::delete::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::delete::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::::::delete::approve::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::delete::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::::::delete::::print::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::delete::approve::print::preview') {
                        $jasar[$baris->FormId] = '::::::::delete::approve::print::preview::';
                        $changed = 1;
                    }
//DELETE
//APPROVE BEDA DENGAN SAVE
                    if ($jasar[$baris->FormId] == '::approve') {
                        $jasar[$baris->FormId] = '::::::::::approve::::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::approve::print') {
                        $jasar[$baris->FormId] = '::::::::::approve::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::approve::preview') {
                        $jasar[$baris->FormId] = '::::::::::approve::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::approve::addon') {
                        $jasar[$baris->FormId] = '::::::::::approve::::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::approve::print::addon') {
                        $jasar[$baris->FormId] = '::::::::::approve::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::approve::preview::addon') {
                        $jasar[$baris->FormId] = '::::::::::approve::::preview::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::approve::print::preview') {
                        $jasar[$baris->FormId] = '::::::::::approve::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::approve::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::::::::approve::print::preview::addon';
                        $changed = 1;
                    }
//APPROVE
//PRINT BEDA DENGAN SAVE
                    if ($jasar[$baris->FormId] == '::print') {
                        $jasar[$baris->FormId] = '::::::::::::print::::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::print::addon') {
                        $jasar[$baris->FormId] = '::::::::::print::::addon';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::print::preview') {
                        $jasar[$baris->FormId] = '::::::::::::print::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::print::preview::addon') {
                        $jasar[$baris->FormId] = '::::::::::::print::preview::addon';
                        $changed = 1;
                    }
//PRINT
//PREVIEW BEDA DENGAN SAVE
                    if ($jasar[$baris->FormId] == '::preview') {
                        $jasar[$baris->FormId] = '::::::::::::::preview::';
                        $changed = 1;
                    }
                    if ($jasar[$baris->FormId] == '::preview::addon') {
                        $jasar[$baris->FormId] = '::::::::::::::preview::addon';
                        $changed = 1;
                    }
//PREVIEW
//ADDON BEDA DENGAN SAVE
                    if ($jasar[$baris->FormId] == '::addon') {
                        $jasar[$baris->FormId] = '::::::::::::::::addon';
                        $changed = 1;
                    }
//ADDON


                    $a = explode('::', $jasar[$baris->FormId]);
//PENAMBAHAN "::" PER AUTH AGAR SAMA RATA
//                if (count($a) == 0) {
//                    $jasar[$baris->FormId] = $jasar[$baris->FormId] . "::::::::::";
//                }
                    if (count($a) == 1) {

                        $jasar[$baris->FormId] = $jasar[$baris->FormId] . "::::::::::::::::";
                    }
                    if (count($a) == 2) {
                        $jasar[$baris->FormId] = $jasar[$baris->FormId] . "::::::::::::::";
                    }
                    if (count($a) == 3) {
                        $jasar[$baris->FormId] = $jasar[$baris->FormId] . "::::::::::::";
                    }
//PENAMBAHAN "::" PER AUTH AGAR SAMA RATA
                    $jasar[$baris->FormId] = $baris->FormId . $jasar[$baris->FormId] . "|";
                }

            endforeach;
// LOOP for get authority checbox
// LOOP for get formname
            $authority = '';
            foreach ($countform->result() as $baris):
                $authority = $authority . $jasar[$baris->FormId];
            endforeach;
//        echo $authority;
// LOOP for get formname
            if ($changed == 1) {

                $qinsert = "update stp_userrole set CompanyId = '$pcompanyid', Code = '$pcode', Name = '$pname', Remarks = '$premarks', RoleAuthority = '$authority', "
                        . "IsActive = '$pactivestatus', LastEditBy = '$puser', LastEditDate = GETDATE() "
                        . "Where Iid = '$piid'";
//                echo $qinsert;
                $this->db->query($qinsert);
                redirect('role/viewuser/2');
            } else {
                echo "Format belum terdaftar";
            }
        }
    }

}
