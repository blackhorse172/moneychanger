<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kasirvalas extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mautonumber');
        $this->load->model('serverinfo');
    }

//--------------------------------------------------- TAMBAH USER BEGIN------------------------
    function index($mess = null) {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else {
                $mess = $mess;
            }
// ******* GET & SET ERROR MESSAGE ******* //
            $queryuser = "SELECT * from stp_user";
            $queryauth = "SELECT stp_userroleauthority.*, stp_userrole.Code, stp_userrole.Name "
                    . "FROM stp_userroleauthority LEFT JOIN stp_userrole ON stp_userrole.Iid = stp_userroleauthority.RoleId";
            $data['dbuser'] = $this->db->query($queryuser);
            $data['dbrole'] = $this->db->get('stp_userrole');
            $data['dbroleauth'] = $this->db->query($queryauth);
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['pageform'] = "Customer";
            $data['pathform'] = "Master</a> <i class='fa fa-angle-right'></i></li><li><a href='#'>User";
            $data['error'] = $mess;
            $data['title'] = "Form User";
            $data['menu'] = "menu";
            $data['isi'] = "vmaster/customer/customer_view";
            $this->load->view('template', $data);
        } else {
            redirect('beranda/noauth');
        }
    }

    public function get_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            $fromsortdate = date('Y-m-d', strtotime($this->uri->segment(3)));
            $tosortdate = $this->uri->segment(4);
            $tomorrow = date('Y-m-d', strtotime($tosortdate . "+1 days"));
            if ($fromsortdate == '' or $tosortdate == '') {
                $tomorrow = date('Y-m-d', strtotime(date('Y-m-d')));
                $fromsortdate = date('Y-m-d', strtotime($tomorrow . "-1 days"));
            }

            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];

            $query = "SELECT * from MS_Customer where CompanyId = '$companyid' ";
            $res = $this->db->query($query)->result();

            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    function update_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'update');
        if ($resultauth == 1) {
            $session = $this->session->userdata('session_data');
            $puser = $session['id'];
            $companyid_session = $session['companyid'];

            $iid = $this->input->post('iid');
            $companyid = $this->input->post('companyid');
            $branchid = $this->input->post('branchid');
            $kode = $this->input->post('kode');
            $nama = $this->input->post('nama');
            $type = $this->input->post('type');
            $alias = $this->input->post('alias');
            $alamat = $this->input->post('alamat');
            $tempatlahir = $this->input->post('tempatlahir');
            $tanggallahir = date("Y-m-d", strtotime($this->input->post('tanggallahir')));
            $noktp = $this->input->post('noktp');
            $noidlain = $this->input->post('noidlain');
            $cif = $this->input->post('cif');
            $npwp = $this->input->post('npwp');
            $profil = $this->input->post('profil');
            $telp = $this->input->post('telp');
            $rekening = $this->input->post('rekening');
            $pemilikrekening = $this->input->post('pemilikrekening');
            $contactperson = $this->input->post('contactperson');
            $kuasa1nama = $this->input->post('kuasa1nama');
            $kuasa1address = $this->input->post('kuasa1address');
            $kuasa1id = $this->input->post('kuasa1id');
            $kuasa2nama = $this->input->post('kuasa2nama');
            $kuasa2address = $this->input->post('kuasa2address');
            $kuasa2id = $this->input->post('kuasa2id');
            $sumberdana = $this->input->post('sumberdana');
            $tujuantransaksi = $this->input->post('tujuantransaksi');
            $catatan = $this->input->post('catatan');
            $isactive = $this->input->post('isactive');

            if ($kode == '' || $nama == '') {
                echo "Field Kode & Nama tidak boleh kosong";
                return false;
            }

            $validation = 0;
            if ($iid == '') {
                $cek_nama = $this->db->query("select Iid from MS_Customer Where Kode like '$kode' ");
                if ($cek_nama->num_rows() > 0) {
                    $validation = -1;
                } else {
                    $iid = $this->db->query("exec SP_GetAutoNumber @id='Iid',@table='MS_Customer',@formattengah='MS/CM',@transdate=null")->row()->ret;
                    $query = "insert into MS_Customer (Iid,CompanyId,BranchId,Kode,Nama,Type,Alias,Alamat,TempatLahir,"
                            . "TanggalLahir,NoKTP,NoIDLain,CIF,NPWP,Profil,Telp,Rekening,PemilikRekening,ContactPerson,"
                            . "Kuasa1_Nama,Kuasa1_Address,Kuasa1_ID,Kuasa2_Nama,Kuasa2_Address,Kuasa2_ID,SumberDana,"
                            . "TujuanTransaksi,Catatan,IsActive,"
                            . "InputBy,InputDate) values "
                            . "('$iid','$companyid_session','$branchid','$kode','$nama','$type','$alias','$alamat','$tempatlahir','$tanggallahir','$noktp',"
                            . "'$noidlain','$cif','$npwp','$profil','$telp','$rekening','$pemilikrekening','$contactperson','$kuasa1nama','$kuasa1address','$kuasa1id','$kuasa2nama',"
                            . "'$kuasa2address','$kuasa2id','$sumberdana','$tujuantransaksi','$catatan',1,"
                            . "'$puser',GETDATE())";
                    $this->db->query($query);
                }
            } else {
                $cek_nama = $this->db->query("select Iid from MS_Customer Where Kode like '$kode' And Iid != '$iid' ");
                if ($cek_nama->num_rows() > 0) {
                    $validation = -2;
                } else {
                    $query = "update MS_Customer set CompanyId='$companyid_session',BranchId='$branchid',Kode='$kode',Nama='$nama',"
                            . "Type='$type',Alias='$alias',Alamat='$alamat',TempatLahir='$tempatlahir',"
                            . "TanggalLahir='$tanggallahir',NoKTP='$noktp',NoIDLain='$noidlain',CIF='$cif',NPWP='$npwp',"
                            . "Profil='$profil',Telp='$telp',Rekening='$rekening',PemilikRekening='$pemilikrekening',ContactPerson='$contactperson',"
                            . "Kuasa1_Nama='$kuasa1nama',Kuasa1_Address='$kuasa1address',Kuasa1_ID='$kuasa1id',Kuasa2_Nama='$kuasa2nama',Kuasa2_Address='$kuasa2address',Kuasa2_ID='$kuasa2id',SumberDana='$sumberdana',"
                            . "TujuanTransaksi='$tujuantransaksi',Catatan='$catatan',IsActive='$isactive',LastEditBy='$puser',LastEditDate=GETDATE() "
                            . "where Iid='$iid' ";
                    $this->db->query($query);
                }
            }

            if ($validation == -1) {
                echo "Kode sudah terdaftar";
            } elseif ($validation == -2) {
                echo "Kode sudah digunakan";
            } elseif ($this->db->affected_rows() > 0) {
                echo "1";
//                echo $kode;
            } else {
                echo "gagal update";
            }
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    function delete_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'delete');
        if ($resultauth == 1) {
            $session = $this->session->userdata('session_data');
            $puser = $session['id'];
            $iid = $this->input->post('iid');
            $query = "delete from MS_Customer "
                    . "where Iid='$iid' ";

            $this->db->query($query);
            if ($this->db->affected_rows() > 0) {
                echo "1";
            } else {
                echo "gagal update";
            }
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

//--------------------------------------------------- TAMBAH ROLE END------------------------
}
