<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Branch extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mautonumber');
        $this->load->model('serverinfo');
    }

    function index($mess = null) {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else {
                $mess = $mess;
            }
// ******* GET & SET ERROR MESSAGE ******* //
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['pageform'] = "Branch";
            $data['error'] = $mess;
            $data['title'] = "Form User";
            $data['menu'] = "menu";
            $data['isi'] = "stp/branch_view";
            $this->load->view('template', $data);
        } else {
            redirect('beranda/noauth');
        }
    }

    public function get_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            $fromsortdate = date('Y-m-d', strtotime($this->uri->segment(3)));
            $tosortdate = $this->uri->segment(4);
            $tomorrow = date('Y-m-d', strtotime($tosortdate . "+1 days"));
            if ($fromsortdate == '' or $tosortdate == '') {
                $tomorrow = date('Y-m-d', strtotime(date('Y-m-d')));
                $fromsortdate = date('Y-m-d', strtotime($tomorrow . "-1 days"));
            }
//        echo $fromsortdate;
            $query = "SELECT * from MS_Branch ORDER BY Iid DESC";
            $res = $this->db->query($query)->result();

            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    function update_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'update');
        if ($resultauth == 1) {
            $session = $this->session->userdata('session_data');
            $puser = $session['id'];
            $iid = $this->input->post('iid');
            $companyid = $this->input->post('companyid');
            $kode = $this->input->post('kode');
            $nama = $this->input->post('nama');
            $alamat = $this->input->post('alamat');
            $phone1 = $this->input->post('phone1');
            $phone2 = $this->input->post('phone2');
            $fax = $this->input->post('fax');
            $mobile = $this->input->post('mobile');
            $npwpno = $this->input->post('npwpno');
            $npwpname = $this->input->post('npwpname');
            $npwpaddress = $this->input->post('npwpaddress');
            $activestatus = $this->input->post('activestatus');
            $catatan = $this->input->post('catatan');
            $validation = 0;
            if ($iid == '') {
                $cek_nama = $this->db->query("select Iid from Ms_Branch Where Code like '$kode' ");
                if ($cek_nama->num_rows() > 0) {
                    $validation = -1;
                } else {
                    $iid = $this->db->query("exec SP_GetAutoNumber @id='Iid',@table='MS_Branch',@formattengah='MS/BR',@transdate=null")->row()->ret;
                    $query = "insert into MS_Branch (Iid,CompanyId,Code,Name,Address,Phone1,Phone2,Fax,Mobile,NPWPNo,NPWPName,NPWPAddress,IsActive,Remarks,"
                            . "InputBy,InputDate) values ('$iid','$companyid','$kode','$nama','$alamat','$phone1','$phone2','$fax','$mobile',"
                            . "'$npwpno','$npwpname','$npwpaddress','1','$catatan',"
                            . "'$puser',GETDATE())";
                    $this->db->query($query);
                }
            } else {
                $cek_nama = $this->db->query("select Iid from Ms_Branch Where Code like '$kode' And Iid != '$iid' ");
                if ($cek_nama->num_rows() > 0) {
                    $validation = -2;
                } else {
                    $query = "update MS_Branch set CompanyId='$companyid',Code='$kode',Name='$nama',Address='$alamat',Phone1='$phone1',Phone2='$phone2',"
                            . "Fax='$fax',Mobile='$mobile',NPWPNo='$npwpno',NPWPName='$npwpname',NPWPAddress='$npwpaddress',IsActive='$activestatus',"
                            . "Remarks='$catatan',LastEditBy='$puser',LastEditDate=GETDATE() "
                            . "where Iid='$iid' ";
                    $this->db->query($query);
                }
            }

            if ($validation == -1) {
                echo "Kode sudah terdaftar";
            } elseif ($validation == -2) {
                echo "Kode sudah digunakan";
            } elseif ($this->db->affected_rows() > 0) {
                echo "1";
            } else {
                echo "gagal update";
            }
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    function delete_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'delete');
        if ($resultauth == 1) {
            $session = $this->session->userdata('session_data');
            $puser = $session['id'];
            $iid = $this->input->post('iid');
            $query = "delete from MS_Branch "
                    . "where Iid='$iid' ";

            $this->db->query($query);
            if ($this->db->affected_rows() > 0) {
                echo "1";
            } else {
                echo "gagal update";
            }
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

     function store() {
        $data = $this->input->post();
        if (empty($data['Iid'])) {
            # code...
            $data['Iid'] =  $this->general->getStpIid('STPBR','MS_Branch');
            // $data['CompanyId'] = $this->general->companyId;
            // $data['BranchId'] = $this->general->branchId;
            try {
            $result = $this->db->insert('MS_Branch', $data);
                 if (! $result) {
                     # code...
                    throw new Exception("Error Processing Request", 1);
                    
                 }
            } catch (Exception $e) {
                echo json_encode(array('status'=>500,'message'=>$e));
            }

            echo json_encode(array('status' => 200));

        } else {
         
            try {
                    $this->db->where('Iid', $data['Iid']);
                   $result = $this->db->update('MS_Branch', $data);
                     if (! $result) {
                         # code...
                        throw new Exception("Error Processing Request", 1);
                        
                     }
                } catch (Exception $e) {
                    echo json_encode(array('status'=>500,'message'=>$e));
                }

                echo json_encode(array('status' => 200));            

        }
    }

    function checkBranchCode(){
        $data = $this->input->post();
        $branchId = $this->general->branchId;
        $companyId = $this->input->post('CompanyId');

        $check = $this->db->query("SELECT * FROM MS_Branch WHERE Code LIKE LOWER('$data[Code]') AND CompanyId = '$companyId' ");

        if (count($check->result_array()) > 0) {
            # code...
            echo json_encode(array('status' => 400));
            exit;
        }
            echo json_encode(array('status' => 200));
    }

    function checkBranchName(){
        $data = $this->input->post();
        $branchId = $this->general->branchId;
        $companyId = $this->input->post('CompanyId');

        $check = $this->db->query("SELECT * FROM MS_Branch WHERE Name LIKE LOWER('$data[Name]') AND CompanyId = '$companyId' ");

     if (count($check->result_array()) > 0) {
            # code...
            echo json_encode(array('status' => 400));
            exit;
        }
            echo json_encode(array('status' => 200));


    }

    function checkDefaultFormatCode(){
        $data = $this->input->post();
        $branchId = $this->general->branchId;
        $companyId = $this->input->post('CompanyId');

        $check = $this->db->query("SELECT * FROM MS_Branch WHERE DefaultFormatCode LIKE LOWER('$data[DefaultFormatCode]') AND CompanyId = '$companyId' ");

     if (count($check->result_array()) > 0) {
            # code...
            echo json_encode(array('status' => 400));
            exit;
        }
            echo json_encode(array('status' => 200));
    }   

//--------------------------------------------------- TAMBAH ROLE END------------------------
}
