<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mautonumber');
        $this->load->model('serverinfo');
    }

    function index($mess = null) {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else {
                $mess = $mess;
            }
// ******* GET & SET ERROR MESSAGE ******* //
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['pageform'] = "Company";
            $data['error'] = $mess;
            $data['title'] = "Form User";
            $data['menu'] = "menu";
            $data['isi'] = "stp/company_view";
            $this->load->view('template', $data);
        } else {
            redirect('beranda/noauth');
        }
    }

    public function get_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            $fromsortdate = date('Y-m-d', strtotime($this->uri->segment(3)));
            $tosortdate = $this->uri->segment(4);
            $tomorrow = date('Y-m-d', strtotime($tosortdate . "+1 days"));
            if ($fromsortdate == '' or $tosortdate == '') {
                $tomorrow = date('Y-m-d', strtotime(date('Y-m-d')));
                $fromsortdate = date('Y-m-d', strtotime($tomorrow . "-1 days"));
            }
//        echo $fromsortdate;

            $query = "SELECT a.*,b.CUST_LEVEL_CODE as Level from STP_Company a "
                    . "left join [192.168.1.8].SIRIUSDB_DEV.DBO.REF_CUST_LEVEL b on b.REF_CUST_LEVEL_ID = a.Level "
                    . "ORDER BY Iid DESC";
            $res = $this->db->query($query)->result();

            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    public function get_cust_level() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {

            $query = "select REF_CUST_LEVEL_ID, CUST_LEVEL_CODE, CUST_LEVEL_NAME "
                    . "FROM [192.168.1.8].SIRIUSDB_DEV.DBO.REF_CUST_LEVEL ";
            $res = $this->db->query($query)->result();

            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    public function get_data_bankAccount($companyId) {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {

            
            $session = $this->session->userdata('session_data');
            //$companyId = $session['companyid'];
            if (!is_null($this->input->post('companyId'))) {
                # code...
                $companyId = $this->input->post('companyId');
            } 

            
            $this->db->select('*');
            $this->db->from('MS_BankAccount');
            $this->db->where('CompanyId', $companyId);
            $res =$this->db->get()->result_array();
            // print_r($res);
            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    function update_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'update');
        if ($resultauth == 1) {
            $session = $this->session->userdata('session_data');
            $puser = $session['id'];
            $iid = $this->input->post('iid');
            $nama = $this->input->post('nama');
            $level = $this->input->post('level');
            $activestat = $this->input->post('activestat');
            $validation = 0;
            if ($iid == '') {
                $cek_nama = $this->db->query("select Iid from STP_Company Where Name like '$nama' ");
                if ($cek_nama->num_rows() > 0) {
                    $validation = -1;
                } else {
                    $levelid = $this->db->query("select REF_CUST_LEVEL_ID FROM [192.168.1.8].SIRIUSDB_DEV.DBO.REF_CUST_LEVEL where CUST_LEVEL_CODE = '$level' ")->row()->REF_CUST_LEVEL_ID;
                    $iid = $this->db->query("exec SP_GetAutoNumber @id='Iid',@table='STP_Company',@formattengah='STP/CPY',@transdate=null")->row()->ret;
                    $query = "insert into STP_Company (Iid,Name,Level,IsActive,"
                            . "InputBy,InputDate) values ('$iid','$nama','$levelid','1','$puser',GETDATE())";
                    $this->db->query($query);
                }
            } else {
                $cek_nama = $this->db->query("select Iid from STP_Company Where Name like '$nama' And Iid != '$iid' ");
                if ($cek_nama->num_rows() > 0) {
                    $validation = -2;
                } else {
                    $levelid = $this->db->query("select REF_CUST_LEVEL_ID FROM [192.168.1.8].SIRIUSDB_DEV.DBO.REF_CUST_LEVEL where CUST_LEVEL_CODE = '$level' ")->row()->REF_CUST_LEVEL_ID;
                    $query = "update STP_Company set Name='$nama',Level='$levelid',"
                            . "IsActive='$activestat',LastEditBy='$puser',LastEditDate=GETDATE() "
                            . "where Iid='$iid' ";
                    $this->db->query($query);
                }
            }

            if ($validation == -1) {
                echo "Nama sudah terdaftar";
            } elseif ($validation == -2) {
                echo "Nama sudah digunakan";
            } elseif ($this->db->affected_rows() > 0) {
                echo "1";
            } else {
                echo "gagal update";
            }
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    function delete_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'delete');
        if ($resultauth == 1) {
            $session = $this->session->userdata('session_data');
            $puser = $session['id'];
            $iid = $this->input->post('iid');
            $query = "delete from STP_Company "
                    . "where Iid='$iid' ";

            $this->db->query($query);
            if ($this->db->affected_rows() > 0) {
                echo "1";
            } else {
                echo "gagal update";
            }
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

     function store() {
           
        $data['Name'] = $this->input->post('Name');
        $data['Level'] = $this->input->post('Level');
        $data['IsActive'] = 1;
        $data['Iid'] = $this->input->post('Iid');

       // print_r($this->input->post()); exit();
        $session = $this->session->userdata('session_data');

        $BankName = $this->input->post('BankName');
        $AccountNo = $this->input->post('AccountNo');
        $AccountName = $this->input->post('AccountName');
        $AccountAddress = $this->input->post('AccountAddress');
        $iidBank = $this->general->getAutoNumber();
        $companyId     = $this->input->post('Name');
        $category     = $this->input->post('Category');
        $InputBy = $session['nama'];
        $InputDate = date('Y-m-d H:i:s');

        if (empty($data['Iid'])) {
            # code...
            $data['Iid'] =  $this->general->getStpIid('STPCM','STP_Company');
            $data['InputBy'] = $this->general->getUserIdActive();
            $data['InputDate'] = $this->general->getDateNow();

            try {
            $result = $this->db->insert('STP_Company', $data);
                 if ($result != 1) {
                     # code...
                    throw new Exception("Error Processing Request", 1);
                    
                 }
            } catch (Exception $e) {
                echo json_encode(array('status'=>500,'message'=>$e));
            }

          

        for ($i=0; $i < count($this->input->post('BankName')) ; $i++) { 
            # code...
              $this->db->query("INSERT INTO MS_BankAccount (Iid,CompanyId,AccountNo,AccountName,AccountAddress,BankName,InputBy,InputDate,Category) VALUES (
                    $iidBank,
                    '$companyId',
                    '$AccountNo[$i]',
                    '$AccountName[$i]',
                    '$AccountAddress[$i]',
                    '$BankName[$i]',
                    '$InputBy',
                    '$InputDate',
                    '$category[$i]'
                    ) ");
        }
            //print_r('2');

            echo json_encode(array('status' => 200));

        } else {

            $this->db->where('CompanyId', $companyId);
            $this->db->delete('MS_BankAccount');

            for ($i=0; $i < count($this->input->post('BankName')) ; $i++) { 
            # code...
              $this->db->query("INSERT INTO MS_BankAccount (Iid,CompanyId,AccountNo,AccountName,AccountAddress,BankName,InputBy,InputDate,Category) VALUES (
                    $iidBank,
                    '$companyId',
                    '$AccountNo[$i]',
                    '$AccountName[$i]',
                    '$AccountAddress[$i]',
                    '$BankName[$i]',
                    '$InputBy',
                    '$InputDate',
                    '$category[$i]'
                    ) ");
            }

            
            $data['LastEditBy'] = $this->general->getUserIdActive();
            $data['LastEditDate'] = $this->general->getDateNow();
         
            try {
                    $this->db->where('Iid', $data['Iid']);
                   $result = $this->db->update('STP_Company', $data);
                     if ($result != 1) {
                         # code...
                        throw new Exception("Error Processing Request", 1);
                        
                     }
                } catch (Exception $e) {
                    echo json_encode(array('status'=>500,'message'=>$e));
                }

                echo json_encode(array('status' => 200));            

        }  

    }

    function checkCompanyCode(){
        $data = $this->input->post();
        $branchId = $this->general->branchId;
        $companyId = $this->general->companyId;

        $check = $this->db->query("SELECT * FROM STP_Company WHERE Name LIKE LOWER('$data[Name]') ");

        if (count($check->result_array()) > 0) {
            # code...
            echo json_encode(array('status' => 400));
            exit;
        }
            echo json_encode(array('status' => 200));

    }

//--------------------------------------------------- TAMBAH ROLE END------------------------
}
