<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mautonumber');
    }

    public function index($mess = null) {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'create');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else {
                $mess = $mess;
            }

            // ******* GET & SET ERROR MESSAGE ******* //

            $data['db_hrd'] = $this->db->query("SELECT a.BranchId,COUNT(a.Iid) ManPower,SUM(a.Gaji) TotalGaji,SUM(a.UangMakan)TotalUM  "
                    . " FROM trx_manpower a "
                    . "                  INNER JOIN "
                    . "                  ( "
                    . "                  SELECT MAX(Iid) MaxOrder, BranchId, NIK "
                    . "                  FROM trx_manpower GROUP BY NIK, BranchId "
                    . "                  ) b ON b.MaxOrder = a.Iid "
                    . " GROUP BY a.BranchId ");

            $session = $this->session->userdata('session_data');
            $puser = $session['nama'];
            $companyname = $session['companyname'];
            $data['no'] = 1;
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['title'] = "$puser | $companyname";
            $data['menu'] = "menu";
            $data['error'] = $mess;
            $data['pageform'] = "Dashboard";
            $data['isi'] = "reporting/vdashboard";
            $this->load->view('template', $data);
        } else {
            redirect('beranda/noauth');
        }
    }

    public function get_data_hrd() {
        $branchid = $this->uri->segment(3);
//        echo $fromsortdate;
        $query = "SELECT a.*, @rn:=@rn+1 AS Nomor FROM trx_manpower a "
                . " INNER JOIN "
                . " ( "
                . " SELECT MAX(Iid) MaxOrder, BranchId, NIK "
                . " FROM trx_manpower GROUP BY NIK, BranchId "
                . " ) b ON b.MaxOrder = a.Iid "
                . " where a.BranchId = '$branchid' ";
        $this->db->query("SET @rn=0;");
        $res = $this->db->query($query)->result();

        echo json_encode($res);
    }

}
