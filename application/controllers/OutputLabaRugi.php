<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OutputLabaRugi extends CI_Controller {
	public $pdf;
  public $jumlahAset;
  public $jumlahKewajiban;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('TransactionModel');
		$this->load->library('fpdf');

	}

	public function index()
	{
		$data = $this->getData('aset');
    $kewajiban =  $this->getData('kewajiban');

    // print_r($data);
    // exit();
    // $this->jumlahAset = $data->KAS + $data->BANK + $data->UKA_KAS + $data->UKA_BANK + $data->PIUTANG_TC + $data->PIUTANG_LAINLAIN + $data->PIUTANG_SEWA + $data->ASURANSI + $data->ASET_TETAP + $data->ASET_LAINLAIN + $data->AKUN_PENYUSUTAN; 
    // $this->jumlahKewajiban = $kewajiban->PINJAMAN_RUPIAH + $kewajiban->PINJAMAN_UKA + $kewajiban->UTANG_SEWA + $kewajiban->UTANG_LAINLAIN + $kewajiban->MODAL + $kewajiban->LABA + $kewajiban->RUGI;
  

    $pdf = new fpdf('P','cm','A4');
		
		$pdf->SetFont('Times', 'B', 11);
		$pdf->SetMargins(1, 0.2, 1);
		
		$pdf->AddPage();
        //kline
        $pdf->SetXY(1,2);
        $pdf->MultiCell(19,3,'',1,'C');
               
		    $pdf->SetXY(1,5);
        $pdf->MultiCell(19,22.5,'',1,'C');
        // $pdf->Line(10.5, 5, 10.5,18);
        $pdf->Ln();
        //line garis total kiri
        // $pdf->Line(10, 7.2, 6.5,7.2);
        // $pdf->Line(10, 9.7, 6.5,9.7);
        // $pdf->Line(10, 14.2, 6.5,14.2);

        // $pdf->Line(10, 15.3, 7,15.3);
        // $pdf->Line(10, 15.4, 7,15.4);

        // //line garis kanan
        // $pdf->Line(16.5, 7.8, 19.5,7.8);
        // $pdf->Line(16.5, 14.3, 19.5,14.3);

        // $pdf->Line(16.5, 15.3, 19.5,15.3);
        // $pdf->Line(16.5, 15.4, 19.5,15.4);
        // $pdf->Line(10, 9.7, 6.5,9.7);
        // $pdf->Line(10, 14.2, 6.5,14.2);

        // $pdf->Line(10, 15.2, 7,15.2);


        $this->header($pdf,$data);
        $this->bodyRight($pdf,$data);
        //$this->bodyLeft($pdf,$kewajiban);
        
        $pdf->Output();
              		
	}

	// Page header
function Header($pdf,$data = array() )
{
   	   $pdf->SetFont('Times', 'B', 12);
       $year = '2017';//$this->input->post('year');
       $pdf->SetXY(10,2.5);
       $pdf->Cell(1, 0, 'LAPORAN Laba Rugi', 0, 'LR', 'C');

        $pdf->SetXY(10,3);
       $pdf->Cell(1, 0, 'Per 31 Desember '.$year, 0, 'LR', 'C');
        $pdf->SetXY(10,3.5);
       $pdf->Cell(1, 0, 'dalam Rupiah', 0, 'LR', 'C');
 
}

function bodyRight($pdf,$data = array())
{	
		
   	   $pdf->SetFont('Times', 'B', 13);
       
       $pdf->SetXY(1,5.5);
       $pdf->Cell(1, 0, 'Pendapatan Dan Beban', 0, 'LR', 'L');

       $pdf->SetXY(1,6);
       $pdf->Cell(1, 0, 'Pendapatan Operasional', 0, 'LR', 'L');
   	   
       $pdf->SetFont('Times', '', 13);
      

       $pdf->SetXY(1.5,6.5);
       $pdf->Cell(1, 0, 'Penjualan UKA', 0, 'LR', 'L');

	     $pdf->SetXY(1.5,7);
       $pdf->Cell(1, 0, 'Pencairan TC', 0, 'LR', 'L');

       $pdf->SetXY(15,6.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');

	     $pdf->SetXY(15,7);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');  

       $pdf->SetXY(15,7.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');

       //value 
       $pdf->SetXY(16,6.5);
       $pdf->Cell(1, 0, number_format($data->PENJUALAN_UKA,2), 0, 'LR', 'L');

      $pdf->SetXY(16,7);
       $pdf->Cell(1, 0, number_format($data->PENCAIRAN_TC,2), 0, 'LR', 'L');

       //value
       $pdf->SetXY(16,7.5);
       $pdf->Cell(1, 0, number_format($data->PENJUALAN_UKA+$data->PENCAIRAN_TC,2), 0, 'LR', 'L');
       $A = $data->PENJUALAN_UKA+$data->PENCAIRAN_TC;
       //========================================================================//
       //=======================================================================//
       $pdf->SetXY(1.5,9);
       $pdf->Cell(1, 0, 'Saldo Awal UKA dan TC ', 0, 'LR', 'L');

       $pdf->SetXY(1.5,9.5);
       $pdf->Cell(1, 0, 'Pembelian UKA dan TC', 0, 'LR', 'L');

       $pdf->SetXY(1.5,10);
       $pdf->Cell(1, 0, 'Saldo Akhir UKA dan TC (-/-)', 0, 'LR', 'L');

       $pdf->SetXY(10,9);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');

       $pdf->SetXY(10,9.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');  

       $pdf->SetXY(10,10);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');

      //  //value 
       $pdf->SetXY(11,9);
       $pdf->Cell(1, 0, number_format($data->SALDO_AWAL_UKA_DAN_TC,2), 0, 'LR', 'L');

       $pdf->SetXY(11,9.5);
       $pdf->Cell(1, 0, number_format($data->PEMBELIAN_UKA_DAN_TC,2), 0, 'LR', 'L');

       $pdf->SetXY(11,10);
       $pdf->Cell(1, 0, number_format($data->SALDO_AKHIR_UKA_DAN_TC,2), 0, 'LR', 'L');
       $B = $data->SALDO_AWAL_UKA_DAN_TC + $data->PEMBELIAN_UKA_DAN_TC - $data->SALDO_AKHIR_UKA_DAN_TC; 
       $pdf->SetXY(15,11);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       $pdf->SetXY(16,11);
       $pdf->Cell(1, 0, number_format($B,2), 0, 'LR', 'L');

       $pdf->SetXY(15,11.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       
       $pdf->SetXY(16,11.5);
       $pdf->Cell(1, 0, number_format($A-$B,2), 0, 'LR', 'L');
       $pdf->SetFont('Times', 'B', 13);
       $C = $A-$B;
       $pdf->SetXY(1,11.5);
       $pdf->Cell(1, 0, 'Pendapatan/(Rugi) Operasional Kotor UKA-TC', 0, 'LR', 'L');

       $pdf->SetXY(1,12);
       $pdf->Cell(1, 0, 'Pendapatan Operasional Lainnya', 0, 'LR', 'L');
       
       $pdf->SetXY(1,12.5);
       $pdf->Cell(1, 0, 'Pendapatan/(Rugi) Operasional Kotor', 0, 'LR', 'L');

        $pdf->SetXY(1,13);
       $pdf->Cell(1, 0, 'Beban Operasional', 0, 'LR', 'L');
        
       $pdf->SetFont('Times', '', 13);

       $pdf->SetXY(1.5,13.5);
       $pdf->Cell(1, 0, 'Gaji, Upah, dan Tunjangan', 0, 'LR', 'L');
       $pdf->SetXY(1.5,14);
       $pdf->Cell(1, 0, 'Sewa', 0, 'LR', 'L');
       $pdf->SetXY(1.5,14.5);
       $pdf->Cell(1, 0, 'Iklan dan Promosi', 0, 'LR', 'L');
       $pdf->SetXY(1.5,15);
       $pdf->Cell(1, 0, 'Air, Listrik, dan Telepon', 0, 'LR', 'L');
       $pdf->SetXY(1.5,15.5);
       $pdf->Cell(1, 0, 'Transportasi dan Perjalan', 0, 'LR', 'L');
       $pdf->SetXY(1.5,16);
       $pdf->Cell(1, 0, 'Pemeliharaan Kendaraan', 0, 'LR', 'L');
       $pdf->SetXY(1.5,16.5);
       $pdf->Cell(1, 0, 'Penyusutan Asset Tetap', 0, 'LR', 'L');
       $pdf->SetXY(1.5,17);
       $pdf->Cell(1, 0, 'Asuransi', 0, 'LR', 'L');
       $pdf->SetXY(1.5,17.5);
       $pdf->Cell(1, 0, 'Lain-lain *)', 0, 'LR', 'L');

       $pdf->SetXY(10,13.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       $pdf->SetXY(10,14);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       $pdf->SetXY(10,14.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');

       $pdf->SetXY(10,15);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       $pdf->SetXY(10,15.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       $pdf->SetXY(10,16);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');

       $pdf->SetXY(10,16.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       $pdf->SetXY(10,17);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       $pdf->SetXY(10,17.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       /*
       ==============================
          value
       ==============================
       */

       $pdf->SetXY(11,13.5);
       $pdf->Cell(1, 0, number_format($data->GAJI_UPAH_TUNJANGAN,2), 0, 'LR', 'L');
       $pdf->SetXY(11,14);
       $pdf->Cell(1, 0, number_format($data->SEWA,2), 0, 'LR', 'L');
       $pdf->SetXY(11,14.5);
       $pdf->Cell(1, 0, number_format($data->IKLAN_DAN_PROMOSI,2), 0, 'LR', 'L');

       $pdf->SetXY(11,15);
       $pdf->Cell(1, 0, number_format($data->AIR_LISTRIK_TELEPON,2), 0, 'LR', 'L');
       $pdf->SetXY(11,15.5);
       $pdf->Cell(1, 0, number_format($data->TRANSPORTASI,2), 0, 'LR', 'L');
       $pdf->SetXY(11,16);
       $pdf->Cell(1, 0, number_format($data->PEMELIHARAAN_KENDARAAN,2), 0, 'LR', 'L');

       $pdf->SetXY(11,16.5);
       $pdf->Cell(1, 0,number_format($data->PENYUSUTAN_ASSET_TETAP,2), 0, 'LR', 'L');
       $pdf->SetXY(11,17);
       $pdf->Cell(1, 0, number_format($data->ASURANSI,2), 0, 'LR', 'L');
       $pdf->SetXY(11,17.5);
       $pdf->Cell(1, 0, number_format($data->LAIN_LAIN,2), 0, 'LR', 'L');

       $D = $data->GAJI_UPAH_TUNJANGAN + $data->SEWA + $data->IKLAN_DAN_PROMOSI + $data->AIR_LISTRIK_TELEPON + $data->TRANSPORTASI + $data->PEMELIHARAAN_KENDARAAN + $data->PENYUSUTAN_ASSET_TETAP + $data->ASURANSI + $data->LAIN_LAIN;

       $pdf->SetFont('Times', 'B', 13);

       $pdf->SetXY(1,18.5);
       $pdf->Cell(1, 0, 'Pendapatan/(Rugi) Operasional Bersih', 0, 'LR', 'L');

       $pdf->SetXY(1,19);
       $pdf->Cell(1, 0, 'Pendapatan/(Beban) Lain-lain', 0, 'LR', 'L');

       $pdf->SetFont('Times', '', 13);

       $pdf->SetXY(15,18);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       $pdf->SetXY(15,18.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');

       $pdf->SetXY(16,18);
       $pdf->Cell(1, 0, number_format($D,2), 0, 'LR', 'L');

       $pdf->SetXY(16,18.5);
       $pdf->Cell(1, 0, number_format($C-$D,2), 0, 'LR', 'L');
       $E = $C - $D;
       $pdf->SetXY(1.5,19.5);
       $pdf->Cell(1, 0, 'Pendapatan Bunga Bank', 0, 'LR', 'L');
       $pdf->SetXY(1.5,20);
       $pdf->Cell(1, 0, 'Beban Administrasi Bank', 0, 'LR', 'L');
       $pdf->SetXY(1.5,20.5);
       $pdf->Cell(1, 0, 'Beban Bunga Pinjaman', 0, 'LR', 'L');
       $pdf->SetXY(1.5,21);
       $pdf->Cell(1, 0, 'Laba/(rugi) Penjualan Aset Tetap', 0, 'LR', 'L');
       $pdf->SetXY(1.5,21.5);
       $pdf->Cell(1, 0, '- laba', 0, 'LR', 'L');
       $pdf->SetXY(1.5,22);
       $pdf->Cell(1, 0, '- rugi(-/-)', 0, 'LR', 'L');
       $pdf->SetXY(1.5,22.5);
       $pdf->Cell(1, 0, 'Laba/(Rugi) Selisih KURS', 0, 'LR', 'L');
       $pdf->SetXY(1.5,23);
       $pdf->Cell(1, 0, '- laba', 0, 'LR', 'L');
       $pdf->SetXY(1.5,23.5);
       $pdf->Cell(1, 0, '- rugi(-/-)', 0, 'LR', 'L');
       $pdf->SetXY(1.5,24);
       $pdf->Cell(1, 0, 'Lain Lain (nett)', 0, 'LR', 'L');
       $pdf->SetXY(1.5,24.5);
       $pdf->Cell(1, 0, '- Pendapatan', 0, 'LR', 'L');
       $pdf->SetXY(1.5,25);
       $pdf->Cell(1, 0, '- Beban', 0, 'LR', 'L');

       $pdf->SetXY(1.5,26);
       $pdf->Cell(1, 0, 'Laba/Rugi Sebelum Pajak Penghasilan', 0, 'LR', 'L');
       $pdf->SetXY(1.5,26.5);
       $pdf->Cell(1, 0, 'Pajak Penghasilan', 0, 'LR', 'L');
       $pdf->SetXY(1.5,27);
       $pdf->Cell(1, 0, 'Laba/Rugi Bersih', 0, 'LR', 'L');

       //pendapatan beban
       $pdf->SetXY(10,19.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       $pdf->SetXY(10,20);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       $pdf->SetXY(10,20.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');

       $pdf->SetXY(10,21.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
      $pdf->SetXY(10,22);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');

      
       $pdf->SetXY(10,23);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       $pdf->SetXY(10,23.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');

        $pdf->SetXY(10,24.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       $pdf->SetXY(10,25);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');

        $pdf->SetXY(15,25.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
        $pdf->SetXY(15,26);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
      $pdf->SetXY(15,26.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       $pdf->SetXY(15,27);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');

       /*
       ==============================
          value
       ==============================
       */

       $pdf->SetXY(11,19.5);
       $pdf->Cell(1, 0, number_format($data->PENDAPATAN_BUNGA_BANK,2), 0, 'LR', 'L');
       $pdf->SetXY(11,20);
       $pdf->Cell(1, 0, number_format(($data->BEBAN_ADMINISTRASI_BANK * -1),2), 0, 'LR', 'L');
       $pdf->SetXY(11,20.5);
       $pdf->Cell(1, 0, number_format(($data->BEBAN_BUNGA_PINJAMAN * -1),2), 0, 'LR', 'L');

       $pdf->SetXY(11,21.5);
       $pdf->Cell(1, 0, number_format($data->LABA_ASSET_TETAP,2), 0, 'LR', 'L');
       $pdf->SetXY(11,22);
       $pdf->Cell(1, 0, number_format(($data->RUGI_ASSET_TETAP * -1),2), 0, 'LR', 'L');
       
       $pdf->SetXY(11,23);
       $pdf->Cell(1, 0, number_format($data->LABA_SELISIH_KURS,2), 0, 'LR', 'L');
       $pdf->SetXY(11,23.5);
       $pdf->Cell(1, 0, number_format(($data->RUGI_SELISIH_KURS * -1),2), 0, 'LR', 'L');
       
       $pdf->SetXY(11,24.5);
       $pdf->Cell(1, 0, number_format($data->PENDAPATAN,2), 0, 'LR', 'L');
       $pdf->SetXY(11,25);
       $pdf->Cell(1, 0, number_format(($data->BEBAN * -1),2), 0, 'LR', 'L');

       $F = $data->PENDAPATAN_BUNGA_BANK + $data->BEBAN_ADMINISTRASI_BANK + $data->BEBAN_BUNGA_PINJAMAN + $data->LABA_ASSET_TETAP + $data->RUGI_ASSET_TETAP + $data->LABA_SELISIH_KURS + $data->RUGI_SELISIH_KURS + $data->PENDAPATAN + $data->BEBAN;

        $pdf->SetXY(16,25.5);
       $pdf->Cell(1, 0, number_format($F,2), 0, 'LR', 'L');       

       $pdf->SetXY(16,26);
       $pdf->Cell(1, 0, number_format($E-$F,2), 0, 'LR', 'L');
       $G = $E-$F;
        $pdf->SetXY(16,26.5);
       $pdf->Cell(1, 0, number_format($data->PAJAK_PENGHASILAN,2), 0, 'LR', 'L'); 

       $pdf->SetXY(16,27);
       $pdf->Cell(1, 0, number_format($G-$data->PAJAK_PENGHASILAN,2), 0, 'LR', 'L');       
       $H = $G - $data->PAJAK_PENGHASILAN;
}

function bodyLeft($pdf,$data = array())
{	
		
   	   

}

function getData($type = NULL)
{
	# code...
    $data = $this->input->post();
    $session = $this->session->userdata('session_data');
    $branchid = $this->general->branchId;
    $companyid = $this->general->companyId;
    $filter = $session['usertype'];
    $data['year'] = '2017';

    $from = date('Y-m-d', strtotime($data['fromsortdate']));
    $to = date('Y-m-d', strtotime($data['tosortdate']));

   $query = "exec SP_Pajak_LabaRugi @companyid='$companyid', @branchid='$branchid', @isfilter='$filter', @fromdate='$from', @todate= '$to' ";
             $data = $this->db->query($query)->row();
 
   return $data;
}

}

/* End of file Invoice_transaction.php */
/* Location: ./application/controllers/Invoice_transaction.php */