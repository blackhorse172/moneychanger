<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rate extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mautonumber');
        $this->load->model('serverinfo');
    }

    function index($mess = null) {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else {
                $mess = $mess;
            }
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['pageform'] = "Rate";
            $data['pathform'] = "Master</a> <i class='fa fa-angle-right'></i></li><li><a href='#'>User";
            $data['error'] = $mess;
            $data['title'] = "Form User";
            $data['menu'] = "menu";
            $data['isi'] = "transaksi/rate_view";
            $this->load->view('template', $data);
        } else {
            redirect('beranda/noauth');
        }
    }

    public function get_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            $fromsortdate = date('Y-m-d', strtotime($this->uri->segment(3)));
            $tosortdate = $this->uri->segment(4);
            $tomorrow = date('Y-m-d', strtotime($tosortdate . "+1 days"));
            if ($fromsortdate == '' or $tosortdate == '') {
                $tomorrow = date('Y-m-d', strtotime(date('Y-m-d')));
                $fromsortdate = date('Y-m-d', strtotime($tomorrow . "-1 days"));
            }

            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];

            $query = "exec SP_GetRate @companyid='$companyid' ";
            $res = $this->db->query($query)->result();

            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    public function get_data_curr() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {

            $res = $this->general->getCurrencyData('master');

            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    function update_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'update');
        if ($resultauth == 1) {
            $session = $this->session->userdata('session_data');
            $puser = $session['id'];
            $companyid_session = $session['companyid'];

            $iid = $this->input->post('iid');
            $curr_name = $this->input->post('curr_name');
            $adjbuy = $this->input->post('adjbuy');
            $adjsell = $this->input->post('adjsell');

            $validation = 0;
            if ($iid == '') {
                $curr_id = $this->db->query("select REF_CURR_ID from [192.168.1.8].SIRIUSDB_DEV.dbo.REF_CURR where IS_ACTIVE = 1 and CURR_CODE != 'IDR' AND CURR_CODE='$curr_name' ")->row()->REF_CURR_ID;
                $query = "insert into MS_Rate (CompanyId,CurrencyId,Buy_Disc_MC,Sell_Disc_MC,InputBy,InputDate) values "
                        . "('$companyid_session','$curr_id','$adjbuy','$adjsell',"
                        . "'$puser',GETDATE())";
                $this->db->query($query);
            } else {
                $curr_id = $this->db->query("select REF_CURR_ID from [192.168.1.8].SIRIUSDB_DEV.dbo.REF_CURR where IS_ACTIVE = 1 and CURR_CODE != 'IDR' AND CURR_CODE='$curr_name' ")->row()->REF_CURR_ID;
                $query = "update MS_Rate set CompanyId='$companyid_session',CurrencyId='$curr_id',Buy_Disc_MC='$adjbuy',Sell_Disc_MC='$adjsell',"
                        . "LastEditBy='$puser',LastEditDate=GETDATE() "
                        . "where Iid='$iid' ";
                $this->db->query($query);
            }

            if ($validation == -1) {
                echo "Kode sudah terdaftar";
            } elseif ($validation == -2) {
                echo "Kode sudah digunakan";
            } elseif ($this->db->affected_rows() > 0) {
                echo "1";
//                echo $kode;
            } else {
                echo "gagal update";
            }
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    function delete_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'delete');
        if ($resultauth == 1) {
            $session = $this->session->userdata('session_data');
            $puser = $session['id'];
            $iid = $this->input->post('iid');
            $query = "delete from MS_Rate "
                    . "where Iid='$iid' ";

            $this->db->query($query);
            if ($this->db->affected_rows() > 0) {
                echo "1";
            } else {
                echo "gagal update";
            }
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }


       function store() {
        $data = $this->input->post();
        $data['CurrencyId'] = $this->general->getCurrencyBySP($data['CurrencyId']);
        $data['CompanyId'] = $this->general->companyId;
        $data['BranchId'] = $this->general->branchId;
        $data['Buy_Disc_MC'] = $this->general->convertSeparator($data['Buy_Disc_MC']);
        $data['Sell_Disc_MC'] = $this->general->convertSeparator($data['Sell_Disc_MC']);

        
        if (empty($data['Iid'])) {
            # code...
            $data['Iid'] =  $this->general->getIid('RT','MS_Rate');
            $data['InputBy'] = $this->general->getUserIdActive();
            $data['InputDate'] = $this->general->getDateNow();
            
            try {
            $result = $this->db->insert('MS_Rate', $data);
                 if ($result != 1) {
                     # code...
                    throw new Exception("Error Processing Request", 1);
                    
                 }
            } catch (Exception $e) {
                echo json_encode(array('status'=>500,'message'=>$e));
            }

            echo json_encode(array('status' => 200));

        } else {
            $data['LastEditBy'] = $this->general->getUserIdActive();
            $data['LastEditDate'] = $this->general->getDateNow();
         
            try {
                    $this->db->where('Iid', $data['Iid']);
                   $result = $this->db->update('MS_Rate', $data);
                     if ($result != 1) {
                         # code...
                        throw new Exception("Error Processing Request", 1);
                        
                     }
                } catch (Exception $e) {
                    echo json_encode(array('status'=>500,'message'=>$e));
                }

                echo json_encode(array('status' => 200));            

        }  

    }

//--------------------------------------------------- TAMBAH ROLE END------------------------
}
