<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cost extends CI_Controller {

	 function index($mess = null) {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else {
                $mess = $mess;
            }
// ******* GET & SET ERROR MESSAGE ******* //
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['pageform'] = "Kas Kecil / Petty Cash";
            $data['error'] = $mess;
            $data['title'] = "Form User";
            $data['menu'] = "menu";
            $data['isi'] = "vmaster/cost/cost_view";
            $this->load->view('template', $data);
        } else {
            redirect('beranda/noauth');
        }
    }

    public function get_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
              $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];
            $branchid = $session['branchid'];
			$filter = $session['usertype'];
            $this->db->where('CompanyId', $companyid);
            $this->db->where('BranchId', $branchid);
            $this->db->where('IsFilter', $filter);
            $res =   $this->db->from('MS_Biaya')->get()->result();

            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

     public function get_data_code() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
              $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];
            $branchid = $session['branchid'];
            $filter = $session['usertype'];
            
            $query = $this->db->query("SELECT Iid,Code,Name,CONCAT(Code,'-',Name) as RES FROM MS_Accounting_Code ");
            echo json_encode($query->result());
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    public function get_name_code() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];
            $branchid = $session['branchid'];
            $filter = $session['usertype'];
            $data = $this->input->post();
            
            $query = $this->db->query("SELECT Name,Code FROM MS_Accounting_Code where Iid = '$data[code]' ");
            
            echo json_encode(array('status' => 200,'items'=>$query->row()));

        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

     function checkCostCode(){
        $data = $this->input->post();
        $branchId = $this->general->branchId;
        $companyId = $this->general->companyId;

        $check = $this->db->query("SELECT * FROM MS_Biaya WHERE Kode LIKE LOWER('$data[Kode]') ");

        if (count($check->result_array()) > 0) {
            # code...
            echo json_encode(array('status' => 400));
            exit;
        }
            echo json_encode(array('status' => 200));

    }

	public function store($iid = null)
	{
		$data = $this->input->post();

        // print_r($data);
        // exit();
		$session = $this->session->userdata('session_data');
		$branchid = $this->general->branchId;
		$companyid = $this->general->companyId;
		$filter = $session['usertype'];
		$code = 'PM/'.$branchid;
        $data['Input_By'] = $session['nama'];
        $data['Input_By'] = $session['nama'];
            $data['Input_Date'] = date('Y-m-d');
            $data['Amount'] = $this->general->convertSeparator($data['Amount']); 

		 if (empty($data['Iid'])) {
			$data['Iid'] =$this->general->getIids('Iid','CST','MS_Biaya'); 
			
			 	
             $query = "exec SP_InsertMaster_Biaya @companyid='$companyid', @branchid='$branchid', @isfilter='$filter', @formattengah ='$code', @iid='$data[Iid]', @kode='$data[Kode]', @nama='$data[Nama]', @jenis='$data[Jenis]', @remarks='$data[Remarks]', @user='$data[Input_By]', @isupdate='0', @amount='$data[Amount]', @tglTransaksi='$data[TransDate]',@accounting_iid='$data[AccountingIid]' ";
             $res = $this->db->query($query)->result();
             
            if(!$res) {
                 # code...
                  echo json_encode(array('status' => 400));
            }

            echo json_encode(array('status' => 200));

        } else {
			$data['Edit_By'] = $session['nama'];
			$data['Edit_Date'] = date('Y-m-d');
        
           $query = "exec SP_InsertMaster_Biaya @companyid='$companyid', @branchid='$branchid', @isfilter='$filter', @formattengah ='$code', @iid='$data[Iid]', @kode='$data[Kode]', @nama='$data[Nama]', @jenis='$data[Jenis]', @remarks='$data[Remarks]', @user='$data[Input_By]', @isupdate='1', @amount='$data[Amount]', @tglTransaksi='$data[TransDate]',@accounting_iid='$data[AccountingIid]' ";
             $res = $this->db->query($query)->result();
             
            if(!$res) {
                 # code...
                  echo json_encode(array('status' => 400));
            }

                echo json_encode(array('status' => 200));            

        }  
	}

	public function update($id)
	{
		# code...
	}

	 function delete_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'delete');
        if ($resultauth == 1) {
            $session = $this->session->userdata('session_data');
            $puser = $session['id'];
            $iid = $this->input->post('iid');
            $query = "delete from MS_Biaya "
                    . "where Iid='$iid' ";

            $this->db->query($query);
            if ($this->db->affected_rows() > 0) {
                echo "1";
            } else {
                echo "gagal update";
            }
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

}

/* End of file cost.php */
/* Location: ./application/controllers/cost.php */