<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OutputBiNeraca extends CI_Controller {
	public $pdf;
  public $jumlahAset;
  public $jumlahKewajiban;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('TransactionModel');
		$this->load->library('fpdf');

	}

	public function index()
	{
		$data = $this->getData('aset');
    $kewajiban =  $this->getData('kewajiban');

    $this->jumlahAset = $data->KAS + $data->BANK + $data->UKA_KAS + $data->UKA_BANK + $data->PIUTANG_TC + $data->PIUTANG_LAINLAIN + $data->PIUTANG_SEWA + $data->ASURANSI + $data->ASET_TETAP + $data->ASET_LAINLAIN + $data->AKUN_PENYUSUTAN; 
    $this->jumlahKewajiban = $kewajiban->PINJAMAN_RUPIAH + $kewajiban->PINJAMAN_UKA + $kewajiban->UTANG_SEWA + $kewajiban->UTANG_LAINLAIN + $kewajiban->MODAL + $kewajiban->LABA + $kewajiban->RUGI;
  

    $pdf = new fpdf('P','cm','A4');
		
		$pdf->SetFont('Times', 'B', 11);
		$pdf->SetMargins(1, 0.2, 1);
		
		$pdf->AddPage();
        //kline
        $pdf->SetXY(1,2);
        $pdf->MultiCell(19,3,'',1,'C');
               
		$pdf->SetXY(1,5);
        $pdf->MultiCell(19,13,'',1,'C');
        $pdf->Line(10.5, 5, 10.5,18);
        $pdf->Ln();
        //line garis total kiri
        $pdf->Line(10, 7.2, 6.5,7.2);
        $pdf->Line(10, 9.7, 6.5,9.7);
        $pdf->Line(10, 14.2, 6.5,14.2);

        $pdf->Line(10, 15.8, 7,15.8);
        $pdf->Line(10, 15.9, 7,15.9);

        //line garis kanan
        $pdf->Line(16.5, 7.8, 19.5,7.8);
        $pdf->Line(16.5, 14.7, 19.5,14.7);

        $pdf->Line(16.5, 15.8, 19.5,15.8);
        $pdf->Line(16.5, 15.9, 19.5,15.9);
        // $pdf->Line(10, 9.7, 6.5,9.7);
        // $pdf->Line(10, 14.2, 6.5,14.2);

        // $pdf->Line(10, 15.2, 7,15.2);


        $this->header($pdf,$data);
        $this->bodyRight($pdf,$data);
        $this->bodyLeft($pdf,$kewajiban);
        
        $pdf->Output();
              		
	}

	// Page header
function Header($pdf,$data = array() )
{
   	   $pdf->SetFont('Times', 'B', 12);
       $year = $this->input->post('year');
       $pdf->SetXY(10,2.5);
       $pdf->Cell(1, 0, 'LAPORAN POSISI KEUANGAN', 0, 'LR', 'C');

        $pdf->SetXY(10,3);
       $pdf->Cell(1, 0, 'Per 31 Desember '.$year, 0, 'LR', 'C');
        $pdf->SetXY(10,3.5);
       $pdf->Cell(1, 0, 'dalam Rupiah', 0, 'LR', 'C');
 
}

function bodyRight($pdf,$data = array())
{	
		
   	   $pdf->SetFont('Times', 'B', 9);
       
       $pdf->SetXY(1,5.2);
       $pdf->Cell(1, 0, 'ASET', 0, 'LR', 'C');
   	   $pdf->SetFont('Times', '', 9);
       
       $pdf->SetXY(1,6);
       $pdf->Cell(1, 0, 'Kas dan Bank dalam Rp', 0, 'LR', 'L');

       $pdf->SetXY(1.5,6.5);
       $pdf->Cell(1, 0, 'Kas', 0, 'LR', 'L');

	   $pdf->SetXY(1.5,7);
       $pdf->Cell(1, 0, 'Bank', 0, 'LR', 'L');

       $pdf->SetXY(6.5,6.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');

	   $pdf->SetXY(6.5,7);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');  

       //value 
       $pdf->SetXY(7,6.5);
       $pdf->Cell(1, 0, number_format($data->KAS,2), 0, 'LR', 'L');

	    $pdf->SetXY(7,7);
       $pdf->Cell(1, 0, number_format($data->BANK,2), 0, 'LR', 'L');

       $pdf->SetXY(7,7.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       //value
       $pdf->SetXY(7.5,7.5);
       $pdf->Cell(1, 0, number_format($data->BANK+$data->KAS,2), 0, 'LR', 'L');

       //Kas dan Bank dalam UKA
       $pdf->SetXY(1,8.5);
       $pdf->Cell(1, 0, 'Kas dan Bank dalam UKA', 0, 'LR', 'L');

       $pdf->SetXY(1.5,9);
       $pdf->Cell(1, 0, 'Kas', 0, 'LR', 'L');

	   $pdf->SetXY(1.5,9.5);
       $pdf->Cell(1, 0, 'Bank', 0, 'LR', 'L');

       $pdf->SetXY(6.5,9);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');

	   $pdf->SetXY(6.5,9.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');  

       //value 
       $pdf->SetXY(7,9);
       $pdf->Cell(1, 0, number_format($data->UKA_KAS,2), 0, 'LR', 'L');

	   $pdf->SetXY(7,9.5);
       $pdf->Cell(1, 0, number_format($data->UKA_BANK,2), 0, 'LR', 'L');

       $pdf->SetXY(7,10);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       //value
       $pdf->SetXY(7.5,10);
       $pdf->Cell(1, 0, number_format($data->UKA_KAS+$data->UKA_BANK,2), 0, 'LR', 'L');

       //rincian
       $pdf->SetXY(1,11);
       $pdf->Cell(1, 0, 'Piutang TC', 0, 'LR', 'L');
       $pdf->SetXY(7,11);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       $pdf->SetXY(7.5,11);
       $pdf->Cell(1, 0, number_format(0,2), 0, 'LR', 'L');

       $pdf->SetXY(1,11.5);
       $pdf->Cell(1, 0, 'Piutang Dagang', 0, 'LR', 'L');
       $pdf->SetXY(7,11.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       $pdf->SetXY(7.5,11.5);
       $pdf->Cell(1, 0, number_format($data->PIUTANG_DAGANG,2), 0, 'LR', 'L');


       $pdf->SetXY(1,12);
       $pdf->Cell(1, 0, 'Piutang Lain Lain', 0, 'LR', 'L');
       $pdf->SetXY(7,12);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       $pdf->SetXY(7.5,12);
       $pdf->Cell(1, 0, number_format($data->PIUTANG_LAINLAIN,2), 0, 'LR', 'L');

       $pdf->SetXY(1,12.5);
       $pdf->Cell(1, 0, 'Sewa dibayar dimuka', 0, 'LR', 'L');
       $pdf->SetXY(7,12.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
      $pdf->SetXY(7.5,12.5);
       $pdf->Cell(1, 0, number_format($data->PIUTANG_SEWA,2), 0, 'LR', 'L');

       $pdf->SetXY(1,13);
       $pdf->Cell(1, 0, 'Asuransi dibayar', 0, 'LR', 'L');
       $pdf->SetXY(7,13);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
        $pdf->SetXY(7.5,13);
       $pdf->Cell(1, 0, number_format($data->ASURANSI,2), 0, 'LR', 'L');

       $pdf->SetXY(1,13.5);
       $pdf->Cell(1, 0, 'Aset Tetap', 0, 'LR', 'L');
       
       $pdf->SetXY(1.5,14);
       $pdf->Cell(1, 0, 'Harga Perolehan', 0, 'LR', 'L');
       $pdf->SetXY(6.5,14);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
        $pdf->SetXY(7,14);
       $pdf->Cell(1, 0, number_format($data->ASET_TETAP,2), 0, 'LR', 'L');

	     $pdf->SetXY(1.5,14.5);
       $pdf->Cell(1, 0, 'Akum.Penyusutan(-/-)', 0, 'LR', 'L');
       $pdf->SetXY(6.5,14.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
        $pdf->SetXY(7,14.5);
       $pdf->Cell(1, 0, number_format($data->AKUN_PENYUSUTAN,2), 0, 'LR', 'L');

       
       $pdf->SetXY(7,15);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');  
       $pdf->SetXY(7.5,15);
       $pdf->Cell(1, 0, number_format($data->AKUN_PENYUSUTAN+$data->ASET_TETAP,2), 0, 'LR', 'L');

       $pdf->SetXY(1,15.5);
       $pdf->Cell(1, 0, 'Aset Lain-lain', 0, 'LR', 'L');
         $pdf->SetXY(7,15.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
        $pdf->SetXY(7.5,15.5);
       $pdf->Cell(1, 0, number_format($data->ASET_LAINLAIN,2), 0, 'LR', 'L');       

        $pdf->SetFont('Times', 'B', 9);
	   $pdf->SetXY(1,16.2);
       $pdf->Cell(1, 0, 'Jumlah aset', 0, 'LR', 'L');
	   $pdf->SetXY(7,16.2);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');  
         $pdf->SetXY(7.5,16.2 );
       $pdf->Cell(1, 0, number_format($this->jumlahAset,2), 0, 'LR', 'L');       


}

function bodyLeft($pdf,$data = array())
{	
		
   	   $pdf->SetFont('Times', 'B', 9);
       
       $pdf->SetXY(10.5,5.2);
       $pdf->Cell(1, 0, 'LIABILITAS DAN EKUITAS', 0, 'LR', 'L');
   	   $pdf->SetFont('Times', '', 9);

   	   $pdf->SetXY(10.5,6);
       $pdf->Cell(1, 0, 'LIABILITAS', 0, 'LR', 'L');

       $pdf->SetXY(10.5,6.5);
       $pdf->Cell(1, 0, 'Pinjaman yang diterima', 0, 'LR', 'L');

       $pdf->SetXY(11,7);
       $pdf->Cell(1, 0, 'Dalam Rupiah', 0, 'LR', 'L');

	     $pdf->SetXY(11,7.5);
       $pdf->Cell(1, 0, 'Dalam UKA', 0, 'LR', 'L');

       $pdf->SetXY(16.5,7);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       $pdf->SetXY(17,7);
       $pdf->Cell(1, 0, number_format($data->PINJAMAN_RUPIAH,2), 0, 'LR', 'L');

	     $pdf->SetXY(16.5,7.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L'); 
       $pdf->SetXY(17,7.5);
       $pdf->Cell(1, 0, number_format($data->PINJAMAN_UKA,2), 0, 'LR', 'L');

       $pdf->SetXY(16.5,8);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');  
       $pdf->SetXY(17,8);
       $pdf->Cell(1, 0, number_format($data->PINJAMAN_RUPIAH + $data->PINJAMAN_UKA,2), 0, 'LR', 'L');


       $pdf->SetXY(10.5,9);
       $pdf->Cell(1, 0, 'Utang sewa', 0, 'LR', 'L');

       $pdf->SetXY(16.5,9);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
        $pdf->SetXY(17,9);
       $pdf->Cell(1, 0, number_format($data->UTANG_SEWA,2), 0, 'LR', 'L');

        $pdf->SetXY(10.5,9.5);
       $pdf->Cell(1, 0, 'Utang Dagang', 0, 'LR', 'L');

       $pdf->SetXY(16.5,9.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
        $pdf->SetXY(17,9.5);
       $pdf->Cell(1, 0, number_format($data->UTANG_DAGANG,2), 0, 'LR', 'L');

        $pdf->SetXY(10.5,10);
       $pdf->Cell(1, 0, 'Utang Lain-lain', 0, 'LR', 'L');
       $pdf->SetXY(16.5,10);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');  
       $pdf->SetXY(17,10);
       $pdf->Cell(1, 0, number_format($data->UTANG_LAINLAIN,2), 0, 'LR', 'L');

         $pdf->SetFont('Times', 'B', 9);
       $pdf->SetXY(10.5,12.5);
       $pdf->Cell(1, 0, 'EKUITAS', 0, 'LR', 'L');
         $pdf->SetFont('Times', '', 9);

       $pdf->SetXY(10.5,13);
       $pdf->Cell(1, 0, 'Modal Disetor', 0, 'LR', 'L');
       $pdf->SetXY(16.5,13);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
       $pdf->SetXY(17,13);
       $pdf->Cell(1, 0, number_format($data->MODAL,2), 0, 'LR', 'L');

       $pdf->SetXY(10.5,13.5);
       $pdf->Cell(1, 0, 'Laba ditahan /(akum. rugi) - net', 0, 'LR', 'L');

       $pdf->SetXY(11,14);
       $pdf->Cell(1, 0, '- Laba', 0, 'LR', 'L');


	     $pdf->SetXY(11,14.5);
       $pdf->Cell(1, 0, '- Rugi', 0, 'LR', 'L');

       $laba = 0;
       $rugi = 0;
       $labaRugi = $this->getLabaRugi();

       if ($labaRugi < 0) {
          # code...
          $rugi = $labaRugi * -1;
        } else {
          $laba = $labaRugi;
        } 



       $pdf->SetXY(16.5,14);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');
        $pdf->SetXY(17,14);
       $pdf->Cell(1, 0, number_format($laba,2), 0, 'LR', 'L');
	     $pdf->SetXY(16.5,14.5);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');  
       $pdf->SetXY(17,14.5);
       $pdf->Cell(1, 0, number_format($rugi,2), 0, 'LR', 'L');
	   $pdf->SetXY(16.5,15);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');  
          $pdf->SetXY(17,15);
       $pdf->Cell(1, 0, number_format($laba + $rugi,2), 0, 'LR', 'L');

  $pdf->SetFont('Times', 'B', 9);
	   $pdf->SetXY(10.5,16.2);
       $pdf->Cell(1, 0, 'Jumlah LIABILITAS dan EKUITAS', 0, 'LR', 'L');

         $A = $data->PINJAMAN_RUPIAH + $data->PINJAMAN_UKA;
       $totalEkuitas = $A + $data->UTANG_SEWA + $data->UTANG_DAGANG + $data->UTANG_LAINLAIN + $data->MODAL - $laba + $rugi;


       $pdf->SetXY(16.5,16.2);
       $pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');  
          $pdf->SetXY(17,16.2);
       $pdf->Cell(1, 0, number_format($totalEkuitas,2), 0, 'LR', 'L');

}

function getData($type = NULL)
{
	# code...
    $data = $this->input->post();
    //print_r($data);
    $session = $this->session->userdata('session_data');
    $branchid = $this->general->branchId;
    $companyid = $this->general->companyId;
    $filter = $session['usertype'];
      $from = date('Y-m-d', strtotime($data['fromsortdate']));
         $to = date('Y-m-d', strtotime($data['tosortdate']));

   $query = "exec SP_Pajak_Neraca @companyid='$companyid', @branchid='$branchid', @isfilter='$filter', @type='$type' , @fromdate='$from',@todate='$to' ";
             $data = $this->db->query($query)->row();
 
   return $data;
}

function getLabaRugi() 
{

   $data = $this->input->post();
    $session = $this->session->userdata('session_data');
    $branchid = $this->general->branchId;
    $companyid = $this->general->companyId;
    $filter = $session['usertype'];
    $data['year'] = '2017';

    $from = date('Y-m-d', strtotime($data['fromsortdate']));
    $to = date('Y-m-d', strtotime($data['tosortdate']));

   $query = "exec SP_Pajak_LabaRugi @companyid='$companyid', @branchid='$branchid', @isfilter='$filter', @fromdate='$from', @todate= '$to' ";
             $data = $this->db->query($query)->row();
 
    $A = $data->PENJUALAN_UKA+$data->PENCAIRAN_TC;
    $B = $data->SALDO_AWAL_UKA_DAN_TC + $data->PEMBELIAN_UKA_DAN_TC - $data->SALDO_AKHIR_UKA_DAN_TC; 
    $C = $A-$B;
    $D = $data->GAJI_UPAH_TUNJANGAN + $data->SEWA + $data->IKLAN_DAN_PROMOSI + $data->AIR_LISTRIK_TELEPON + $data->TRANSPORTASI + $data->PEMELIHARAAN_KENDARAAN + $data->PENYUSUTAN_ASSET_TETAP + $data->ASURANSI + $data->LAIN_LAIN;
    $E = $C - $D;
    $F = $data->PENDAPATAN_BUNGA_BANK + ($data->BEBAN_ADMINISTRASI_BANK * -1) + ($data->BEBAN_BUNGA_PINJAMAN * -1) + $data->LABA_ASSET_TETAP + ($data->RUGI_ASSET_TETAP * -1) + $data->LABA_SELISIH_KURS + ($data->RUGI_SELISIH_KURS * -1) + $data->PENDAPATAN + ($data->BEBAN * -1);
     $G = $E-$F;
     $H = $G - $data->PAJAK_PENGHASILAN;

     return $H;
}

}

/* End of file Invoice_transaction.php */
/* Location: ./application/controllers/Invoice_transaction.php */