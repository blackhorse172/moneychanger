<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dropdownlist extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function company() {
        $query = "SELECT Name from STP_Company ";
        $res = $this->db->query($query)->result();

        echo json_encode($res);
    }

}
