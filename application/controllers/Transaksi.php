<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('mautonumber');
        $this->load->model('TransactionModel');
        $this->load->model('serverinfo');
        $this->load->model('Tx_PaymentModel','txPaymentModel');
        $this->load->model('Tx_PaymentDetailModel','txPaymentDetailModel');
    }

//--------------------------------------------------- TAMBAH USER BEGIN------------------------
    function index($mess = null) {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else {
                $mess = $mess;
            }
// ******* GET & SET ERROR MESSAGE ******* //
            $queryuser = "SELECT * from stp_user";
            $queryauth = "SELECT stp_userroleauthority.*, stp_userrole.Code, stp_userrole.Name "
                    . "FROM stp_userroleauthority LEFT JOIN stp_userrole ON stp_userrole.Iid = stp_userroleauthority.RoleId";
            $data['dbuser'] = $this->db->query($queryuser);
            $data['dbrole'] = $this->db->get('stp_userrole');
            $data['dbroleauth'] = $this->db->query($queryauth);
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['pageform'] = "Transaksi";
            $data['pathform'] = "User";
            $data['error'] = $mess;
            $data['title'] = "Form User";
            $data['rates'] = $this->get_data_rate();
            $data['menu'] = "menu";
            $data['isi'] = "transaksi/transaksi_header_view";
            $this->load->view('template', $data);
        } else {
            redirect('beranda/noauth');
        }
    }

    public function get_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            $fromsortdate = date('Y-m-d', strtotime($this->uri->segment(3)));
            $tosortdate = $this->uri->segment(4);
            $tomorrow = date('Y-m-d', strtotime($tosortdate . "+1 days"));
            if ($fromsortdate == '' or $tosortdate == '') {
                $tomorrow = date('Y-m-d', strtotime(date('Y-m-d')));
                $fromsortdate = date('Y-m-d', strtotime($tomorrow . "-1 days"));
            }

            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];
            $branchid = $session['branchid'];
            $filter = $session['usertype'];
            $transactionIid = $this->input->post('transactionId');
            
              $query = "exec SP_GetTransaction_Data @companyid='$companyid', @branchid='$branchid', @filter='$filter', @transactionid='$transactionIid' ";
            
            $res = array();
            if (isset($transactionIid)) {
                # code...
                $res = $this->db->query($query)->result();
                echo json_encode($res);
                exit;
            }


            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    public function get_data_header() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            $fromsortdate = date('Y-m-d', strtotime($this->uri->segment(3)));
            $tosortdate = $this->uri->segment(4);
            $tomorrow = date('Y-m-d', strtotime($tosortdate . "+1 days"));
            if ($fromsortdate == '' or $tosortdate == '') {
                $tomorrow = date('Y-m-d', strtotime(date('Y-m-d')));
                $fromsortdate = date('Y-m-d', strtotime($tomorrow . "-1 days"));
            }

            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];
            $branchid = $session['branchid'];
            $filter = $session['usertype'];

              $query = "exec SP_GetTransaction_Data_Header @companyid='$companyid', @branchid='$branchid', @filter='$filter' ";
            //$res = $this->TransactionModel->getTransactionHeader();//$this->db->query($query)->result();
              $res = $this->db->query($query)->result();
            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    public function get_data_rate() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            $fromsortdate = date('Y-m-d', strtotime($this->uri->segment(3)));
            $tosortdate = $this->uri->segment(4);
            $tomorrow = date('Y-m-d', strtotime($tosortdate . "+1 days"));
            if ($fromsortdate == '' or $tosortdate == '') {
                $tomorrow = date('Y-m-d', strtotime(date('Y-m-d')));
                $fromsortdate = date('Y-m-d', strtotime($tomorrow . "-1 days"));
            }

            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];

            $query = "exec SP_GetRate @companyid='$companyid' ";
            $res = $this->db->query($query)->result();

            return $res;
            // echo json_encode($res);
        } else {
            return false;
        }
    }


    public function get_data_detail($tx,$branchId = 'S',$date = 'D',$number = '0') {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            $fromsortdate = date('Y-m-d', strtotime($this->uri->segment(3)));
            $tosortdate = $this->uri->segment(4);
            $tomorrow = date('Y-m-d', strtotime($tosortdate . "+1 days"));
            if ($fromsortdate == '' or $tosortdate == '') {
                $tomorrow = date('Y-m-d', strtotime(date('Y-m-d')));
                $fromsortdate = date('Y-m-d', strtotime($tomorrow . "-1 days"));
            }

            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];
            $branchid = $session['branchid'];
            $filter = $session['usertype'];

            $transactionIid = $tx.'/'.$branchId.'/'.$date.'/'.$number;//$this->input->post('transactionId');
            //print_r($transactionIid); die;
              $query = "exec SP_GetTransaction_Data @companyid='$companyid', @branchid='$branchid', @filter='$filter', @transactionid='$transactionIid' ";
            
            $res = array();
            if (isset($transactionIid)) {
                # code...
                $res = $this->db->query($query)->result();
                echo json_encode($res);
                exit;
            }


            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    public function get_data_curr() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {

          $res = $this->general->getCurrencyData('transaksi');

            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

     public function get_bank_account() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];
          $data = array();
          $this->db->select('*');
          $this->db->where('CompanyId', $companyid);
          $this->db->from('MS_BankAccount');
          $ret = $this->db->get()->result();
            foreach ($ret as $key => $value) {
                # code...
                $data[$key]['AccountNo'] = $value->AccountNo;
                $data[$key]['BankName'] = $value->BankName;
                $data[$key]['Iid'] = $value->Iid;
                $data[$key]['Category'] = $value->Category;
                $data[$key]['AccountName'] = $value->Category;
                $data[$key]['AccountAddress'] = $value->AccountAddress;
                $data[$key]['IsActive'] = $value->IsActive;
                $data[$key]['BankNameView'] = $value->BankName.' - '.$value->AccountNo;
            }
        
            echo json_encode($data);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    public function get_data_customer() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {

            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];

            $query = "select Nama + ', ' + Kode As Name, Nama from MS_Customer where IsActive = 1 and CompanyId = '$companyid' order by Nama asc ";
            $res = $this->db->query($query)->result();
          
            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    public function get_rate_init() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
//
            $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];
            $currencycode = $this->input->post('currencycode');
            $bors = $this->input->post('bors');
      
            $res = $this->db->query("exec SP_GetRate_ByCurrCode @companyid='$companyid',@curr_code='$currencycode',@bors='$bors' ")->row()->ret;
            
            echo $res;
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    function update_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'update');
        if ($resultauth == 1) {
            $session = $this->session->userdata('session_data');
            $puser = $session['id'];
            $companyid_session = $session['companyid'];

//            iid: field.Iid, companyid: field.CompanyId, branchid: field.BranchId, invno: field.InvNo,
//            namacustomer: field.NamaCustomer, bors: field.BorS, curr_code: field.CurrencyCode, tgltrans: field.TglTransaksi,
//            jumlah: field.Jumlah, rate: field.Rate, totaltransaksi: field.TotalTransaksi, ispaid: field.IsPaid,
//            ispickup: field.IsPickUp, iscash: field.IsCash, remarks: field.Remarks

            $iid = $this->input->post('iid');
            $companyid = $this->input->post('companyid');
            $branchid = $this->input->post('branchid');
            $invno = $this->input->post('invno');
            $namacustomer = $this->input->post('namacustomer');
            $bors = $this->input->post('bors');
            $curr_code = $this->input->post('curr_code');
            $jumlah = $this->input->post('jumlah');
            $rate = $this->input->post('rate');
            $tgltrans = date("Y-m-d", strtotime($this->input->post('tgltrans')));
            $totaltransaksi = $this->input->post('totaltransaksi');
            $ispaid = $this->input->post('ispaid');
            $ispickup = $this->input->post('ispickup');
            $iscash = $this->input->post('iscash');
            $remarks = $this->input->post('remarks');

//            echo $curr_code;
            if ($namacustomer == '') {
                echo "Nama Customer tidak boleh kosong";
                return false;
            }
            if ($curr_code == '') {
                echo "Field Valas tidak boleh kosong";
                return false;
            }
            if ($bors == '') {
                echo "B/S tidak boleh kosong";
                return false;
            }

            $isupdate = 0;
            $validation = 0;
            if ($iid == '') {
                $isupdate = 0;
                $iid = $this->db->query("exec SP_GetAutoNumber_TX @id='Iid',@table='TX_Transaction',@formattengah='MS/CM',@transdate=null,"
                                . "@companyid='$companyid_session',@branchid='$branchid' ")->row()->ret;
            } else {
                $isupdate = 1;
            }

            $cek_nama = $this->db->query("select Iid from MS_Customer Where Nama like '$namacustomer' ");
            $cek_curr = $this->db->query("select REF_CURR_ID, CURR_CODE from [192.168.1.8].SIRIUSDB_DEV.dbo.REF_CURR "
                    . "Inner join MS_Rate on MS_Rate.CurrencyId = [192.168.1.8].SIRIUSDB_DEV.dbo.REF_CURR.REF_CURR_ID "
                    . "where IS_ACTIVE = 1 and CURR_CODE != 'IDR' and CURR_CODE = '$curr_code' ");
            if ($cek_nama->num_rows() <= 0) {
                $validation = -1;
            } else if ($cek_curr->num_rows() <= 0) {
                $validation = -2;
            } else {
                $cust_id = $cek_nama->row()->Iid;
                $curr_id = $cek_curr->row()->REF_CURR_ID;

                $query = "exec SP_Transaction @formatId='SMS',@companyid='$companyid_session',@branchid='$branchid',@custid='$cust_id',"
                        . "@bors='$bors',@currencyid='$curr_id',@tgltransaksi='$tgltrans',@jumlah='$jumlah',@rate='$rate',@ispaid='$ispaid',"
                        . "@ispickup='$ispickup',@remarks='$remarks',@iscash='$iscash',@puser='$puser',@isupdate='$isupdate' ";
                $this->db->query($query);
            }


            if ($validation == -1) {
                echo "Customer tidak terdaftar";
            } elseif ($validation == -2) {
                echo "Valas tidak ditemukan";
            } elseif ($this->db->affected_rows() > 0) {
                echo "1";
//                echo $kode;
            } else {
                echo "gagal update";
            }
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

    public function delete_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'delete');
        if ($resultauth == 1) {
            $session = $this->session->userdata('session_data');
            $puser = $session['id'];
            $companyid_session = $session['companyid'];
            $iid = json_decode( $this->input->post('iid'));
            $branchCode = $this->general->branchCode;
             //check row has a paid
        $checkPayment  = $this->TransactionModel->cekPaidStatusByTrxId($iid);

        if (count($checkPayment) > 0) {
         # code...
           echo 'Tidak Bisa Di delete, Salah satu data status sudah melakakukan pembayaran lunas';
           exit;
        } 

                $query = "exec SP_Delete_Transaction @iid='$iid',@puser='$puser', @formatid='$branchCode' ";
                # code...
                try {
                    $res = $this->db->query($query)->row()->ret;
                    if (!$res) {
                        throw new Exception();   
                    }
                } catch (Exception $e) {
                    echo $e;
                    exit;
                }
                    
            echo "1";
        }

        //     if ($res > 0) {
        //         echo "1";
        //     } else {
        //         echo "gagal update";
        //     }
        // } else {
        //     echo "tidak ada otoritas merubah data";
        // }
    }

    public function updateStatus() {

        $session = $this->session->userdata('session_data');
        $iids           = json_decode( $this->input->post('iid'));
        $type           = $this->input->post('type');
        $statusIsType   = array('IsPaid','IsPickUp','IsCash','IsSuspicious');
        $statusUnType   = array('UnPaid','UnPickUp','UnCash','UnSuspicious');
        $invno          = "INV". $session['branchid'];
        $date           = date('Y-m-d');
        

        if (count($iids) == 0) {
            echo "404";
            exit;
        }

        if ($type == 'IsPickUp' || $type == 'UnPickUp') {
            // checking status paid before
             foreach ($iids as $iid) {
                # code...
                $data[] = $this->TransactionModel->cekPaidStatusByTrxId($iid);
        }
            
            if (empty($data[0])) {
                # code....
                echo "403";
                exit();
            }
        }
       
        if (in_array($type, $statusIsType)) {
             
            foreach ($iids as $iid) {
             if ($type == 'IsPickUp') {
                 $this->db->query("UPDATE TX_Transaction_Header SET PickupDate = '$date' WHERE Transaction_Iid = '$iid' ");
             }

                # code...
                $result = $this->db->query("UPDATE TX_Transaction_Header set $type = 1 where Transaction_Iid = '$iid'");
                    if (!$result) {
                        throw new Exception("Error Processing Request", 500);   
                        exit;
                    }
                   ;
            }
        } 

        if (in_array($type, $statusUnType)) {
            $type = str_replace('Un', 'Is', $type);
            foreach ($iids as $iid) {
                # code...
                $result = $this->db->query("UPDATE TX_Transaction_Header set $type = 0 where Transaction_Iid = '$iid'");
                    if (!$result) {
                        throw new Exception("Error Processing Request", 500);   
                        exit;
                    }

            }
        }

         echo '200';
        
    }

    public function store()
    {
        # code...
        $countBuy   = count($this->input->post('valasArrayBuy'));
        $countSell  = count($this->input->post('valasArray'));
        if ($countSell > 0) {
            # code...
            $this->storeSell();
        }

        if ($countBuy > 0) {
            # code...
            $this->storeBuy();
        }

        echo json_encode(array('status' => "200"));

    }

    public function storeSell()
    {
            $valasArray = $this->input->post('valasArray');
            $rateArray = $this->input->post('rateArray');
            $jumlahArray = $this->input->post('jumlahArray');

            $session = $this->session->userdata('session_data');
            $puser = $session['id'];
            $companyid_session = $session['companyid'];
            $branchid = $session['branchid'];
            $namacustomer = $this->input->post('customer');
            $bors = '1'; //$this->input->post('bs');
            // $curr_code = $this->input->post('valasValue');
            $jumlah = str_replace(',', '', $this->input->post('jumlah'));
            $rate = $this->general->convertSeparator($this->input->post('rateValue'));
            $tgltrans = date("Y-m-d", strtotime($this->input->post('date')));
            $custid = '';
            $curr_id = '';
            $formatId= $session['branchdefaultcode'];
            $cek_nama = $this->db->query("select Iid from MS_Customer Where Nama like '$namacustomer' ");
            $transactionIid = $this->general->getIids('Transaction_Iid','TX','TX_Transaction_Header');
            $total = 0;
          
            if ($cek_nama->num_rows() <= 0) {
                echo json_encode(array('status' => '400', 'message' => 'Data Tidak Ditemukan'));
                exit;
            }
            
            $cust_id = $cek_nama->row()->Iid;
                $count = count($this->input->post('valasArray')) -1;
           //print_r($count); exit;
            for ($i=0; $i <= $count;  $i++) { 
                # code...
                $curr_id = $this->general->getCurrencyBySP($valasArray[$i]);

                $rateArray[$i] = $this->general->convertSeparator($rateArray[$i]);
                $jumlahArray[$i] = $this->general->convertSeparator($jumlahArray[$i]);

                $query = "exec SP_Transaction @transid='$transactionIid',@formatid='$formatId',@companyid='$companyid_session',@branchid='$branchid',@custid='$cust_id',"
                        . "@bors='$bors',@currencyid='$curr_id',@tgltransaksi='$tgltrans',@jumlah='$jumlahArray[$i]',@rate='$rateArray[$i]',@ispaid=0,"
                        . "@ispickup=0,@remarks='NULL',@iscash=0,@puser='$puser',@isupdate='' ";
                
                        $result = $this->db->query($query);
                        
                         if ( ! $result) {
                            echo json_encode(array('status' => 500,'messages'=>'gagal Input'));
                         }
                
            }
            
            $total = $this->TransactionModel->getTotalTransactionByTrxid($transactionIid);

               //insert header transaction
            $headerData = array(
                'Transaction_Iid' => $transactionIid,
                'CompanyId'       => $companyid_session,
                'branchId'        => $branchid,
                'CustId'          => $cust_id,
                'InputBy'         => $puser,
                'InputDate'       => $tgltrans,
                'Bors'            => $bors,
                'TglTransaksi'    => $tgltrans,
                'GrandTotal'  => $total
            );

            $this->db->set($headerData);
            $insert = $this->db->insert('TX_Transaction_Header');
           
            // echo json_encode(array('status' => "200"));
        
       
    }

    public function storeBuy()
    {
            $valasArray = $this->input->post('valasArrayBuy');
            $rateArray = $this->input->post('rateArrayBuy');
            $jumlahArray = $this->input->post('jumlahArrayBuy');

            $session = $this->session->userdata('session_data');
            $puser = $session['id'];
            $companyid_session = $session['companyid'];
            $branchid = $session['branchid'];
            $namacustomer = $this->input->post('customer');
            $bors = '-1'; //$this->input->post('bs');
            // $curr_code = $this->input->post('valasValueBuy');
            $jumlah = str_replace(',', '', $this->input->post('jumlah'));
            // $rate = $this->general->convertSeparator($this->input->post('rateValue'));
            $tgltrans = date("Y-m-d", strtotime($this->input->post('date')));
            $custid = '';
            $curr_id = '';
            $formatId= $session['branchdefaultcode'];
            $cek_nama = $this->db->query("select Iid from MS_Customer Where Nama like '$namacustomer' ");
            $transactionIid = $this->general->getIids('Transaction_Iid','TX','TX_Transaction_Header');
            $total = 0;
           
            if ($cek_nama->num_rows() <= 0) {
                echo json_encode(array('status' => '400', 'message' => 'Data Tidak Ditemukan'));
                exit;
            }
            
            $cust_id = $cek_nama->row()->Iid;
                $count = count($this->input->post('valasArrayBuy')) -1;
            for ($i=0; $i <= $count;  $i++) { 
                # code...
                $curr_id = $this->general->getCurrencyBySP($valasArray[$i]);

                $rateArray[$i] = $this->general->convertSeparator($rateArray[$i]);
                $jumlahArray[$i] = $this->general->convertSeparator($jumlahArray[$i]);

                $query = "exec SP_Transaction @transid='$transactionIid',@formatid='$formatId',@companyid='$companyid_session',@branchid='$branchid',@custid='$cust_id',"
                        . "@bors='$bors',@currencyid='$curr_id',@tgltransaksi='$tgltrans',@jumlah='$jumlahArray[$i]',@rate='$rateArray[$i]',@ispaid=0,"
                        . "@ispickup=0,@remarks='NULL',@iscash=0,@puser='$puser',@isupdate='' ";
                
                        $result = $this->db->query($query);
                        
                         if ( ! $result) {
                            echo json_encode(array('status' => 500,'messages'=>'gagal Input'));
                         }
                
            }
            
            $total = $this->TransactionModel->getTotalTransactionByTrxid($transactionIid);

               //insert header transaction
            $headerData = array(
                'Transaction_Iid' => $transactionIid,
                'CompanyId'       => $companyid_session,
                'branchId'        => $branchid,
                'CustId'          => $cust_id,
                'InputBy'         => $puser,
                'InputDate'       => $tgltrans,
                'Bors'            => $bors,
                'TglTransaksi'    => $tgltrans,
                'GrandTotal'  => $total
            );

            $this->db->set($headerData);
            $insert = $this->db->insert('TX_Transaction_Header');
           
            // echo json_encode(array('status' => "200"));
    }

    public function checkAmountPaid() {

        // $customers          =  json_decode($this->input->post('namaCustomer'));
        // $firstCustomer[0]   = $customers[0];
        // $bors          =  json_decode($this->input->post('bors'));
        // $firstBors[0]   = $bors[0];
        // $amount             = json_decode($this->input->post('amount'));
        $grandTotalAmount   = 0;
        $iid                = $this->input->post('transactionId');
        $data               = [];
        $incomingPaid       = 0;
        $differenceAmount   = 0;
        $changeValue        = 0;
        $statusPaid         = 0;

        $trxDetail = $this->TransactionModel->getDetailByTransactionId($iid);

        //check row has a paid
        $checkPayment  = $this->TransactionModel->cekPaidStatusByTrxId($iid);
      
        if (count($checkPayment) > 0) {
         # code...
           echo json_encode(array('status' => 400, 'messages' => 'Transaksi Ini Sudah Lunas' ));
           exit;
        } 

        //get payment 
        $paymentId = $this->txPaymentModel->getPaymentIdByTrxId($iid);

        if (! is_null($paymentId)) {
            # code...
            //get amount
            $incomingPaid = $this->txPaymentDetailModel->getIncomingAmountByPaymentId($paymentId);
        }

      
        foreach ($trxDetail as $key => $value) {
            $grandTotalAmount = $grandTotalAmount + $value->TotalTransaksi;
        }


        if ($grandTotalAmount >= 0) {
            $differenceAmount = $grandTotalAmount - $incomingPaid;
        } else {
            $differenceAmount = $grandTotalAmount - ($incomingPaid * -1);
        }

        echo json_encode(array('status' => 200,'items'=>['amount'=>$grandTotalAmount,
            'incomingAmount' =>$incomingPaid,
            'paymentId'     => $paymentId,
            'differenceAmount' =>$differenceAmount,
            'statusPaid'       => $statusPaid
            ]));
    
    }

     public function checkStatusPaid() {

        $customers          =  json_decode($this->input->post('namaCustomer'));
        $firstCustomer[0]   = $customers[0];
        $bors               =  json_decode($this->input->post('bors'));
        $firstBors[0]       = $bors[0];
        $amount             = json_decode($this->input->post('amount'));
        $grandTotalAmount   = 0;
        $iid                = json_decode($this->input->post('iid'));
        $custId             = $this->input->post('customerId');
        $invNo              = json_decode($this->input->post('invNo'));
        $firstInv[0]        = $invNo[0];
        $data               = [];
        $incomingPaid       = 0;
        $differenceAmount   = 0;
        $changeValue        = 0;
        $statusPaid         = 0;
        $typePrint          = $this->input->post('typePrint');

        $count= array_diff($customers, $firstCustomer);
        if (count($count) > 0) {
            # code...
           echo json_encode(array('status' => 400, 'messages' => 'hanya boleh pilih satu customer yang sama' ));
           exit;
        }

        //check bors is merged
        $countBors= array_diff($bors, $firstBors);
        if (count($countBors) > 0) {
            # code...
           echo json_encode(array('status' => 400, 'messages' => 'hanya boleh pilih satu tipe Buy Or Sell' ));
           exit;
        }

        //check row has a paid
        $checkPayment  = $this->TransactionModel->cekPaidStatusByTrxId($iid[0]);
        
        if (empty($checkPayment)) {
         # code...
           echo json_encode(array('status' => 400, 'messages' => 'Transaksi ini belum lunas pembayaran' ));
           exit;
        }

        //check invoice number has different number
        $countInv= array_diff($invNo, $firstInv);
        if (count($countInv) > 0) {
            # code...
           echo json_encode(array('status' => 400, 'messages' => 'Tidak bisa mencetak invoice dengan nomor berbeda' ));
           exit;
        }

        if (isset($checkPayment)) {
            # code...
            if ($checkPayment->InvoiceNo == NULL || $checkPayment->InvoiceNo == '') {
                # code...
                //generate Invoice By Transaction ID
                $this->invoiceGenerate($iid);
            }    
        }
        
       echo json_encode(array('status' => 200,'items'=>[
            'iid'=>$checkPayment->Transaction_Iid,
            'customerName' => $firstCustomer[0],
            'customerId' => $custId,
            'type' => $typePrint
            ]));

    
    }

    public function updateStatusIspaid()
    {
       
        $session = $this->session->userdata('session_data');
        $iids           = json_decode($this->input->post('iid'));
        $type           = $this->input->post('type');
        $custId         = $this->general->getCustomerId($this->input->post('customerName'));
        $cashAmount     = $this->general->convertSeparator($this->input->post('cashPaid'));
        $bankAmount     = $this->general->convertSeparator($this->input->post('transferPaid'));
        $changeAmount   = $this->input->post('changeValue');
        $paidStatus     = $this->input->post('paidStatus');
        $isBank         = 0;
        $isCash         = 0;
        $inputBy        = $this->general->getUserIdActive();
        $inputDate      = $this->general->getDateNow();
        $totalPaidAmount     = $this->input->post('paidAmountValue');
        $paidValue      = $this->input->post('paidAmountInValue') + ($cashAmount + $bankAmount);
        $trxId          = $this->input->post('trxId');
        $branchId     = $this->general->branchId;
        $companyId     = $this->general->companyId;
        
        if ($this->input->post('paymentBy') == 'cash') {
                # code...
            $isCash =1;
        }else $isBank = 1;
      // print_r($this->input->post()); exit;
        if ($this->input->post('paymentBy') == 'cash') {
                # code...
            $isCash =1;
        }else $isBank = 1;
       
        //check if paid > total amount = update status is paid
        if ($totalPaidAmount <= 0) {
            # code...
            $totalPaidAmount = $totalPaidAmount * -1;
            if (($paidValue - $totalPaidAmount) >= 0) {
                # code...

                    $resultDetail = $this->db->query("UPDATE TX_Transaction  set IsPaid = 1, PaidDate = '$inputDate' where Transaction_Iid = '$trxId'");
                        if (!$resultDetail) {
                            throw new Exception("Error Processing Request", 500);   
                            exit;
                        }

                    $resultHeader = $this->db->query("UPDATE TX_Transaction_Header  set IsPaid = 1 where Transaction_Iid = '$trxId'");
                        if (!$resultHeader) {
                            throw new Exception("Error Processing Request", 500);   
                            exit;
                        }
            }
        } else {
            
            if (($paidValue - $totalPaidAmount) >= 0) {
                    # code...
                    $resultDetail = $this->db->query("UPDATE TX_Transaction set IsPaid = 1, PaidDate = '$inputDate' where Transaction_Iid = '$trxId'");
                        if (!$resultDetail) {
                            throw new Exception("Error Processing Request", 500);   
                            exit;
                        }

                    $resultHeader = $this->db->query("UPDATE TX_Transaction_Header  set IsPaid = 1 where Transaction_Iid = '$trxId'");
                        if (!$resultHeader) {
                            throw new Exception("Error Processing Request", 500);   
                            exit;
                        }
            }
        }
        
            $number = $this->general->getAutoNumber();
            $payments = $this->txPaymentModel->checkPaymentIdByTransactionId($trxId);
            $bors = $this->TransactionModel->getDataHeaderByIid($trxId);
            
            if (count($payments) > 0) {
                 
                    foreach ($payments as $key => $value) {
                   
                        $result = $this->db->query("INSERT INTO TX_Payment_Detail (iid,paymentId,cashAmount,bankAmount,isCash,isBank,inputDate,inputBy) VALUES (
                              $number,
                             '$value[paymentId]',
                             '$cashAmount',
                             '$bankAmount',
                             '$isCash',
                             '$isBank',
                             '$inputDate',
                             '$inputBy'
                        )");
                    }
                    
            } else {
                $paymentNo = $this->general->getPaymentNo();
                
                    $result = $this->db->query("INSERT INTO TX_Payment (iid,paymentId,transactionId,CompanyId,BranchId,BorS) VALUES (
                     $number,
                     '$paymentNo',
                     '$trxId',
                     '$companyId',
                     '$branchId',
                     '$bors')");
                
                $payments = $this->txPaymentModel->checkPaymentIdByTransactionId($trxId);
                  if (count($payments) > 0) {
                     foreach ($payments as $key => $value) {
                             
                        $detail = $this->db->query("INSERT INTO TX_Payment_Detail (iid,paymentId,cashAmount,bankAmount,isCash,isBank,inputDate,inputBy) VALUES (
                             $number,
                             '$value[paymentId]',
                             '$cashAmount',
                             '$bankAmount',
                             '$isCash',
                             '$isBank',
                             '$inputDate',
                             '$inputBy'
                        )");
                        }
                  }
            }
      
        $this->storeBkStock($trxId,$cashAmount,$bankAmount,'paid');
        $this->storeLogTransaction($trxId,$cashAmount,$bankAmount,'paid');
           echo json_encode(array('status' => 200));
    }


    private function storeBkStock($id,$cashAmount,$bankAmount,$type)
    {
        # code...
        $txData = $this->TransactionModel->getTransactionHeaderByTransactioId($id);
        $getPaymentId = '';
        $Iid = $this->general->getAutoNumber();
        $currencyid = $this->db->query("select REF_CURR_ID, CURR_CODE from [192.168.1.8].SIRIUSDB_DEV.dbo.REF_CURR "
                    . "where IS_ACTIVE = 1 and CURR_CODE = 'IDR'")->row()->REF_CURR_ID;
        $date = date('Y-m-d');
        $amount = ($cashAmount > 0 ) ? $cashAmount : $bankAmount;
        $remarks = ($type == 'paid') ? 'Paid' : 'Void';
        $inputBy        = $this->general->getUserIdActive();
        $inputDate      = $this->general->getDateNow();
        $quantity       = 0;
        $branchCode     = $this->general->branchId;
       
        $accountNo = $this->input->post('bankAccountValue');
        $paymentBy = $this->input->post('paymentBy');
        $bankName =  explode('-', $this->input->post('bankName'));
        $bankName = trim($bankName[0]);

        if (count($txData) > 0) {
            # code...
            foreach ($txData as $key => $value) {
                if ($value->Bors == 1) {
                    # code...
                    $quantity = $amount * 1;
                } else {
                    # code...
                    $quantity = $amount * (-1);
                }
                $companyid = $value->CompanyId;
                $bors = $value->Bors;
                $iid = $value->Transaction_Iid;
                $getPaymentId = $this->txPaymentModel->getIidByTransaction($id);
                
                if ($paymentBy === 'bank' ) {
                    # code...

                    $this->db->query("INSERT INTO BK_Stock (CompanyId,BranchId,CurrencyId,TransRefNO,TransNo,TransDate,Quantity,Bors,Rate,Remarks,InputBy,InputDate,Iid,PaymentType,AccountNo,BankName) VALUES (
                    '$companyid',
                    '$branchCode',
                    'IDR',
                    '$iid',
                    '$getPaymentId',
                    '$date',
                    '$quantity',
                    '$bors',
                    '0',
                    '$remarks',
                    '$inputBy',
                    '$inputDate',
                     $Iid,
                    '$paymentBy',
                    '$accountNo',
                    '$bankName'
                    ) ");
                } else {
                    $this->db->query("INSERT INTO BK_Stock (CompanyId,BranchId,CurrencyId,TransRefNO,TransNo,TransDate,Quantity,Bors,Rate,Remarks,InputBy,InputDate,Iid) VALUES (
                    '$companyid',
                    '$branchCode',
                    'IDR',
                    '$iid',
                    '$getPaymentId',
                    '$date',
                    '$quantity',
                    '$bors',
                    '0',
                    '$remarks',
                    '$inputBy',
                    '$inputDate',
                    $Iid   
                    ) ");
                }

                
                
            }
        } 
    }

    private function invoiceGenerate($iid)
    {
        
        $invoiceNo = $this->general->getInvoiceNo();
        
        //check if transaction id has invoice number
        $iids = $this->TransactionModel->getIdsNotHaveInvoice($iid[0]);
        $grandTotalTransaction = 0;
        $user= $this->general->getUserIdActive();
        $date = $this->general->getDateNow();            
        $Iid = $this->general->getPaymentNo();      
        $changeAmount = $this->txPaymentModel->getChangeAmountByTransactionId($iid);
        $branchId     = $this->general->branchId;
        $companyId     = $this->general->companyId;


      
        if (count($iids) > 0) {
            # code...
            foreach ($iids as $key => $value) {
                # code...
                $this->db->query("UPDATE TX_Transaction_Header set InvoiceNo = '$invoiceNo' where Transaction_Iid = '$value[Transaction_Iid]'");    
                $custId = $value['CustId'];
                $grandTotalTransaction += $value['GrandTotal'];
            }
                //insert int TX_INV
                $this->db->query("INSERT INTO TX_Invoice (Iid,InvoiceNo,custId,TotalAmount,InputBy,InputDate,ChangeAmount,CompanyId,BranchId) VALUES (
                    '$Iid',
                    '$invoiceNo',
                    '$custId',
                    '$grandTotalTransaction',
                    '$user',
                    '$date',
                    '$changeAmount',
                    '$companyId',
                    '$branchId'
                    ) ");

        }
        return true;

    }

     private function storeLogTransaction($id,$cashAmount,$bankAmount,$type)
    {
        # code...
        $txData = $this->TransactionModel->getTransactionHeaderByTransactioId($id);
        $getPaymentId = '';
        $Iid = $this->general->getAutoNumber();
        $currencyid = $this->db->query("select REF_CURR_ID, CURR_CODE from [192.168.1.8].SIRIUSDB_DEV.dbo.REF_CURR "
                    . "where IS_ACTIVE = 1 and CURR_CODE = 'IDR'")->row()->REF_CURR_ID;
        $date = date('Y-m-d');
        $amount = ($cashAmount > 0 ) ? $cashAmount : $bankAmount;
        $remarks = ($type == 'paid') ? 'Paid' : 'Void';
        $inputBy        = $this->general->getUserIdActive();
        $inputDate      = $this->general->getDateNow();
        $quantity       = 0;
        $branchCode     = $this->general->branchId;

        $accountNo = $this->input->post('bankAccountValue');
        $paymentBy = $this->input->post('paymentBy');
        $bankName =  explode('-', $this->input->post('bankName'));
        $bankName = trim($bankName[0]);

       
        if (count($txData) > 0) {
            # code...
            foreach ($txData as $key => $value) {
                if ($value->Bors == 1) {
                    # code...
                    $quantity = $amount * 1;
                } else {
                    # code...
                    $quantity = $amount * (-1);
                }
                $companyid = $value->CompanyId;
                $bors = $value->Bors;
                $iid = $value->Transaction_Iid;
                $getPaymentId = $this->txPaymentModel->getIidByTransaction($id);
                
               if ($paymentBy === 'bank' ) {
                    # code...
                    $this->db->query("INSERT INTO LOG_Transaction (CompanyId,BranchId,CurrencyId,TransRefNO,TransNo,TransDate,Quantity,Bors,Rate,Remarks,InputBy,InputDate,Iid,PaymentType,AccountNo,BankName) VALUES (
                    '$companyid',
                    '$branchCode',
                    'IDR',
                    '$iid',
                    '$getPaymentId',
                    '$date',
                    '$quantity',
                    '$bors',
                    '0',
                    '$remarks',
                    '$inputBy',
                    '$inputDate',
                     $Iid,
                    '$paymentBy',
                    '$accountNo',
                    '$bankName'
                    ) ");
                } else {
                    $this->db->query("INSERT INTO LOG_Transaction (CompanyId,BranchId,CurrencyId,TransRefNO,TransNo,TransDate,Quantity,Bors,Rate,Remarks,InputBy,InputDate,Iid) VALUES (
                    '$companyid',
                    '$branchCode',
                    'IDR',
                    '$iid',
                    '$getPaymentId',
                    '$date',
                    '$quantity',
                    '$bors',
                    '0',
                    '$remarks',
                    '$inputBy',
                    '$inputDate',
                    $Iid   
                    ) ");
                }

            }
        } 
    }

    function updateIsFilter() {
          $iids           = json_decode($this->input->post('iid'));
          $isFilter       = json_decode($this->input->post('isFilter'));
          
          if(count($iids)> 0) {
            for ($i=0; $i < count($iids) ; $i++) { 
                # code...
                if($isFilter[$i] == 0 ) {
                    $this->db->query("UPDATE TX_Transaction  set IsFilter = 1 where Iid = '$iids[$i]'");
                } else {
                    $this->db->query("UPDATE TX_Transaction  set IsFilter = 0 where Iid = '$iids[$i]'");
                }
            }
 
            echo json_encode(array('status' => 200));
          }
    }

    function checkingIsFilter()
    {
        # code...
        $data['Transaction_Iid'] = json_decode($this->input->post('Transaction_Iid'));
        $data['Iid'] = json_decode($this->input->post('Iid'));

        $this->db->where('a.Transaction_Iid', $data['Transaction_Iid']);
        $this->db->where('a.IsFilter', 1);
        $this->db->from('TX_Transaction as a');
        $this->db->join('TX_Transaction_Header as b', 'a.Transaction_Iid = b.Transaction_Iid', 'left');
        $res = $this->db->get()->row();

        $this->db->where('Iid', $data['Iid']);
        $this->db->from('TX_Transaction');
        $tx = $this->db->get()->row();

       
        if (!empty($res->InvoiceNo)) {
            # code...
            echo json_encode(array('status' => 400, 'messages'=> 'Transaksi ini tidak bisa dirubah dikarenakan sudah cetak invoice'));
            exit();
        }
        //var_dump($tx->IsFilter,$res->Transaction_Iid,$tx->Transaction_Iid); exit();
        if (count($res) > 0) {
                     if ($tx->IsFilter === '1') {
                    # code...
                      
                    echo json_encode(array('status' => 200));
                    exit;
                }
                # code...
                echo json_encode(array('status' => 400, 'messages'=> 'Pelaporan hanya boleh 1 di setiap transaksi'));            
                exit;
            } else {

                 echo json_encode(array('status' => 200));
                    exit;
            }
        
       

       




            

    }

//--------------------------------------------------- TAMBAH ROLE END------------------------
}
