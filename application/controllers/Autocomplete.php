<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Autocomplete extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function custauto() {
        if (isset($_GET['term'])) {
            $productname = strtolower($_GET['term']);
            $result = $this->db->query(" exec SP_getCustomer_Ajax '$productname' ")->result();
            if (count($result) > 0) {
                foreach ($result as $pr)
                    $arr_result[] = $pr->CUST_NAME;

                echo json_encode($arr_result);
            }
        }
    }

    public function useridauto() {
        if (isset($_GET['term'])) {
            $productname = strtolower($_GET['term']);
            $result = $this->db->query("select UserId from stp_user where UserId Like '%$productname%' And IsActive = 1 ")->result();
            if (count($result) > 0) {
                foreach ($result as $pr)
                    $arr_result[] = $pr->UserId;

                echo json_encode($arr_result);
            }
        }
    }

    public function companyidauto() {
        if (isset($_GET['term'])) {
            $productname = strtolower($_GET['term']);
            $result = $this->db->query("select Name from STP_Company where Name Like '%$productname%' And IsActive = 1 ")->result();
            if (count($result) > 0) {
                foreach ($result as $pr)
                    $arr_result[] = $pr->Name;

                echo json_encode($arr_result);
            }
        }
    }

    public function rolecode_auto() {
        if (isset($_GET['term'])) {
            $productname = strtolower($_GET['term']);
            $result = $this->db->query("select Code from stp_userrole where Code Like '%$productname%' And IsActive = 1 ")->result();
            if (count($result) > 0) {
                foreach ($result as $pr)
                    $arr_result[] = $pr->Code;

                echo json_encode($arr_result);
            }
        }
    }

    public function kodecabang_auto() {
        if (isset($_GET['term'])) {
            $productname = strtolower($_GET['term']);
            $result = $this->db->query("select Name from ms_branch where Name Like '%$productname%' ")->result();
            if (count($result) > 0) {
                foreach ($result as $pr)
                    $arr_result[] = $pr->Name;

                echo json_encode($arr_result);
            }
        }
    }

}
