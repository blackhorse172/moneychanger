<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Thermal_print extends CI_Controller {
	public $pdf;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('TransactionModel');
		$this->load->library('fpdf');

	}

	public function index()
	{
		$data = $this->getData();
		$recordall = count($data['transaction']); // DI ISI DENGAN SELURUH JUMLAH RECORD YG INGIN DI CETAK
		$recordperpage = 5; // 5 JUMLAH RECORD DALAM 1 HALAMAN
		$activepage = ceil($recordall / $recordperpage); // JUMLAH ACTIVE PAGE
		$firstrecord = 1;
		$lastrecord = $recordperpage;
		$grandtotal = 0;
		$current_inv = "";
		$data['company'] = $this->general->getCompanyData();
		$dataTable = array_chunk($data['transaction'], $recordperpage);
		$height = 150;
		$count = 0;

		foreach ($data['transaction'] as $key => $value) {
			# code...
			if (($key + 1) > 2) {
				# code...
				$height = $height + 5;
			}
				//$height = $height + 5;

			$count++;			
		
		}

		// print_r($height);
		
		// exit;

		$pdf = new fpdf('P','mm',array(80,$height));



		$pdf->AddPage();
		$pdf->SetMargins(1, 0.2, 1);
		$this->header($pdf,$data);
		$this->table($pdf,$data);
		$this->footer($pdf,$data);
		
		
        $pdf->Output();
              		
	}

	// Page header
function Header($pdf,$data)
{
   	   $pdf->SetFont('Times', 'B', 12);
   	   

		if ($data['trxDataHeader']->Bors == 1) {
	        $pdf->SetXY(40,30);
       		$pdf->Cell(1, 0, 'WE SELL', 0, 'LR', 'C');
		} else {
			$pdf->SetXY(40,30);
			$pdf->Cell(1, 0, 'WE BUY', 0, 'LR', 'C');

		}

		   $pdf->Line(72, 35, 8, 35);

        $pdf->SetFont('Arial', '', 8);
        $pdf->SetXY(8,40);
        $pdf->Cell(1, 0, 'Waktu' , 0, 'LR', 'L');

        $pdf->SetXY(35,40);
        $pdf->Cell(1, 0, ':' , 0, 'LR', 'L');

        $pdf->SetXY(37,40);
        $pdf->Cell(1, 0, $data['trxDataHeader']->TglTransaksi , 0, 'LR', 'L');

        $pdf->SetXY(8,45);
        $pdf->Cell(1, 0, 'Transaction No' , 0, 'LR', 'L');

        $pdf->SetXY(35,45);
        $pdf->Cell(1, 0, ':' , 0, 'LR', 'L');

		$pdf->SetXY(37,45);
        $pdf->Cell(1, 0, $data['trxDataHeader']->Transaction_Iid , 0, 'LR', 'L');

        $pdf->SetFont('Arial', 'B', 11);

        $pdf->SetXY(8,50);
        $pdf->Cell(1, 0, 'Pelanggan' , 0, 'LR', 'L');

        $pdf->SetXY(35,50);
        $pdf->Cell(1, 0, ':' , 0, 'LR', 'L');

        $pdf->SetXY(37,48);
        $pdf->MultiCell(40, 4, $data['customer']['nama']);

}

function footer($pdf,$data)
{	
		# code...
		
        //$pdf->SetX(13.2);
        //$pdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');

       
        
}

function getData()
{
	# code...
	$id = $this->input->get('id');
	$customerData = $this->general->getCustomerData($this->input->get('customerName'));
	$trxDataHeader = $this->TransactionModel->getRowHeader($id);
	$trxData = $this->TransactionModel->getDataDetailByTransactionId($trxDataHeader->Transaction_Iid);

	$currency = array();
	$customerId = $this->input->get('customerId');

	foreach ($trxData as $key => $value) {
		# code...
		$currency[$key] = $this->general->getCurrencyById($value['CurrencyId']);
	}

	$data['id'] = $id; 	
	$data['customer'] = $customerData;
	$data['currency'] = $currency;
	$data['transaction'] = $trxData;
	$data['trxDataHeader'] = $trxDataHeader;
	$data['invoiceNo'] = $trxDataHeader->InvoiceNo;
	
	return $data;
}

function table($pdf,$data) {
				
	// Column widths
    $w = array(17, 17, 15, 15);
    $header = array('Currency','Amount','Rate','Note');
    $pdf->SetFont('Arial', 'B', 8);
    
	$pdf->Ln();
	$pdf->setX(8);
    // Header
    for($i=0;$i<count($header);$i++) {
        $pdf->Cell($w[$i],7,$header[$i],1,0,'C');
    }
	$pdf->Ln();
    $line = 0;
    // Data
    $pdf->SetFont('Arial', '', 8);

    foreach($data['transaction'] as $key => $row)
    {

    	$pdf->setX(8);
        $pdf->Cell($w[0],6,$data['currency'][$key],1,0,'LR');
        $pdf->Cell($w[1],6,number_format(str_replace('-', '', $row['Jumlah']), 2, ",", "."),1,0,'LR');
        $pdf->Cell($w[2],6,number_format(str_replace('-', '', $row['Rate']), 2, ",", "."),1,0,'LR');
        $pdf->Cell($w[3],6,'',1,0,'LR');
        
        $pdf->Ln();

        if (($key + 1) > 1) {
        	# code...
        	$line = $line + 5;
        }
        
    }

    //footer
    $pdf->SetY(75 + $line);
    $pdf->SetX(8);
    $pdf->Cell(1, 0, 'Total', 0, 'LR', 'L');
    $pdf->SetX(18);
    $pdf->Cell(1, 0, ':', 0, 'LR', 'L');
    $pdf->SetX(20);
    $pdf->Cell(1, 0, number_format(str_replace('-', '', $data['trxDataHeader']->GrandTotal), 2, ",", "."), 0, 'LR', 'L');
 
    $pdf->SetY(85 + $line);
    $pdf->SetX(8);
    $pdf->Cell(1, 0, 'Catatan', 0, 'L');

 	$pdf->SetY(87 + $line);
    $pdf->SetX(8);
    $pdf->MultiCell(65,17,'',1);

	$pdf->SetY(108 + $line);
    $pdf->SetX(8);
    $pdf->Cell(1, 0, 'Nama User :', 0, 'L');
    $pdf->SetX(25);
    $pdf->Cell(1, 0, $data['trxDataHeader']->InputBy, 0, 'L');



    $pdf->SetY(115 + $line);
    $pdf->SetX(13);
    $pdf->Cell(1, 0, 'Teller :', 0, 'L');
    $pdf->SetX(27);
    $pdf->Cell(1, 0, 'Checkded By :', 0, 'L');
    $pdf->SetX(50);
    $pdf->Cell(1, 0, 'Cashier Valas :', 0, 'L');

    $pdf->SetY(129 + $line);
    $pdf->SetX(10);
	$pdf->Cell(1, 0, '('.$data['trxDataHeader']->InputBy.')', 0, 'L');
	$pdf->SetX(27);
	$pdf->Cell(1, 0, '(.....................)', 0, 'L');
	$pdf->SetX(50);
	$pdf->Cell(1, 0, '(.....................)', 0, 'L');

  $pdf->Line(72, 145, 8, 145);

}

}

/* End of file Invoice_transaction.php */
/* Location: ./application/controllers/Invoice_transaction.php */