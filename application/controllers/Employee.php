<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {

	 function index($mess = null) {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
            // ******* GET & SET ERROR MESSAGE ******* //
            $message = $this->uri->segment(3);
            if ($message == '1') {
                $mess = "1:::Data berhasil di input";
            } else if ($message == '2') {
                $mess = "1:::Data berhasil di ubah";
            } else if ($message == '3') {
                $mess = "1:::Data berhasil di hapus";
            } else {
                $mess = $mess;
            }
// ******* GET & SET ERROR MESSAGE ******* //
            $session = $this->session->userdata('session_data');
            $companyidsession = $session['companyid'];
            $data['companyid'] = $companyidsession;
            $data['no'] = '1';
            $data['authmenu'] = $this->mautonumber->authoritymenu();
            $data['pageform'] = "Employee";
            $data['error'] = $mess;
            $data['title'] = "Form User";
            $data['menu'] = "menu";
            $data['isi'] = "vmaster/employee/employee_view";
            $this->load->view('template', $data);
        } else {
            redirect('beranda/noauth');
        }
    }

    public function get_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'view');
        if ($resultauth == 1) {
              $session = $this->session->userdata('session_data');
            $companyid = $session['companyid'];
            $branchid = $session['branchid'];
			$filter = $session['usertype'];
            $this->db->where('CompanyId', $companyid);
            $this->db->where('BranchId', $branchid);
            $this->db->where('IsFilter', $filter);
            $res =   $this->db->from('MS_Employee')->get()->result();

            echo json_encode($res);
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

     function checkEmployeeCode(){
        $data = $this->input->post();
        $branchId = $this->general->branchId;
        $companyId = $this->general->companyId;

        $check = $this->db->query("SELECT * FROM MS_Employee WHERE Nik LIKE LOWER('$data[Nik]') ");

        if (count($check->result_array()) > 0) {
            # code...
            echo json_encode(array('status' => 400));
            exit;
        }
            echo json_encode(array('status' => 200));

    }

	public function store($iid = null)
	{
		$data = $this->input->post();
		$session = $this->session->userdata('session_data');
		$data['BranchId'] = $this->general->branchId;
		$data['CompanyId'] = $this->general->companyId;
		
		# code...
		 if (empty($data['Iid'])) {
			$data['Iid'] =$this->general->getIids('Iid','EMP','MS_Employee'); 
			$data['Input_By'] = $session['nama'];
			$data['Input_Date'] = date('Y-m-d');
			$data['Employee_Salary'] = $this->general->convertSeparator($data['Employee_Salary']); 
			 
		 	
		 	$this->db->insert('MS_Employee', $data);

            echo json_encode(array('status' => 200));

        } else {
			$data['Edit_By'] = $session['nama'];
			$data['Edit_Date'] = date('Y-m-d');
        	$this->db->where('Iid', $data['Iid']);
        	$this->db->update('MS_Employee', $data);
                echo json_encode(array('status' => 200));            

        }  
	}

	public function update($id)
	{
		# code...
	}

	 function delete_data() {
        $resultauth = $this->mautonumber->cekauthority('ms_user', 'delete');
        if ($resultauth == 1) {
            $session = $this->session->userdata('session_data');
            $puser = $session['id'];
            $iid = $this->input->post('iid');
            $query = "delete from MS_Employee "
                    . "where Iid='$iid' ";

            $this->db->query($query);
            if ($this->db->affected_rows() > 0) {
                echo "1";
            } else {
                echo "gagal update";
            }
        } else {
            echo "tidak ada otoritas merubah data";
        }
    }

}

/* End of file employee.php */
/* Location: ./application/controllers/employee.php */