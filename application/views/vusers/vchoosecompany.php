<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>Choose Company</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<?php echo base_url(); ?>_template/bs/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?php echo base_url(); ?>_template/bs/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url(); ?>_template/bs/css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-black">
        <div class="form-box" id="login-box">
            <div class="header">Choose Company</div>
            <?php echo form_open(base_url() . 'user/choosedcompany'); ?>
            <form role="form">
                <div class="body bg-gray">
                    <table align="center">
                        <tr>
                            <td>
                                <select class="btn btn-default" name="companychoosed" id="companychoosed">
                                    <?php foreach ($db->result() as $baris): ?>
                                        <option value="<?php echo $baris->CompanyId; ?>"><?php echo $baris->CompanyId; ?></option>
                                        <?php
                                    endforeach;
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="footer"> 
                    <button type="submit" class="btn bg-olive btn-block">OK</button>
                </div>
            </form>
            <?php echo form_close() ?>
        </div>
        <!-- jQuery 2.0.2 -->
        <script src="<?php echo base_url(); ?>_template/bs/js/googleapis.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo base_url(); ?>_template/bs/js/bootstrap.min.js" type="text/javascript"></script>

    </body>
</html>