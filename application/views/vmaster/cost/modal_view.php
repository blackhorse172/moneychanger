		<div class="modal fade" id="addCostModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Cost</h4>
					</div>
					<br/>
					<?php $this->load->view('errors/alert'); ?>
					<div class="modal-body">
						<form id="costForm" role="form">
							<input type="hidden" name="Iid" id="Iid" value="">
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Kode</label>
								<div class="col-sm-10">
									<div id="code"></div>
									<input type="hidden" class="form-control" id="Kode" name="Kode">
									<input type="hidden" class="form-control" id="AccountingIid" name="AccountingIid">
									<input type="hidden" class="form-control" id="Jenis" name="Jenis">
								</div>
							</div>

							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Nama</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Nama" name="Nama" readonly>
								</div>
							</div>

							<!-- <div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Jenis</label>
								<div class="col-sm-10">
										<select name="Jenis" id="Jenis"  class="form-control">
											<option value=""></option>
											<option value="ASET TETAP">Aset Tetap</option>
											<option value="ASET LAIN-LAIN">Aset</option>
											<option value="ASURANSI">Asuransi</option>
											<option value="UTANG SEWA">Utang Sewa</option>
											<option value="UTANG LAIN-LAIN">Utang Lain Lain</option>
											<option value="PIUTANG TC">Piutang TC</option>
											<option value="PIUTANG SEWA">Piutang Sewa</option>
											<option value="PIUTANG LAIN-LAIN">Piutang Lain Lain</option>
											<option value="MODAL">Modal Disetor</option>
											<option value="LABA">Laba</option>
											<option value="RUGI">Rugi</option>
										</select>
								</div>
							</div> -->

								<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Jumlah</label>
								<div class="col-sm-10">
									<input type="text" class="auto form-control" id="Amount" name="Amount">
								</div>
							</div>

							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Tanggal Pembayaran</label>
								<div class="col-sm-10">
									<input type="date" class="form-control" id="TransDate" name="TransDate">
								</div>
							</div>

					
						<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Catatan</label>
								<div class="col-sm-10">
										<textarea name="Remarks" id="Remarks" class="form-control"></textarea>
								</div>
							</div>
						

						
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" onclick="clearCostForm()" data-dismiss="modal">Batal</button>
						<button type="button" class="btn btn-primary" onclick="storeCost()">Simpan</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

<style type="text/css" media="screen">
.fullWidth {
	width: 75%;
}	
</style>