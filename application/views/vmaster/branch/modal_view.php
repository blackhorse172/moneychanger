		<div class="modal fade" id="addBranchModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							<span class="sr-only">Close</span>
						</button> -->
						<h4 class="modal-title">Branch</h4>
					</div>
					<div class="modal-body">
						<form id="branchForm" role="form">
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Perusahaan</label>
								<div class="col-sm-10">
								<div id="companySelect"></div>
								</div>
							</div>
					
							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Kode Cabang</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Code" onchange="checkBranchCode()" name="Code">
								</div>
							</div>

							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Id Sistem</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="DefaultFormatCode" onchange="checkDefaultFormatCode()" name="DefaultFormatCode">
								</div>
							</div>

							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Nama</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Name" onchange="checkBranchName()" name="Name">
								</div>
							</div>

							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Alamat</label>
								<div class="col-sm-10">
									<textarea name="Address" id="Address" class="form-control"></textarea>
								</div>
							</div>

							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">No telepon 1</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Phone1" name="Phone1">
								</div>
							</div>

								<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">No Telepon 3</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Phone2" name="Phone2">
								</div>
							</div>

							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Fax</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Fax" name="Fax">
								</div>
							</div>

							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">No Handphone</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Mobile" name="Mobile">
								</div>
							</div>

							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Nomor NPWP</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="NPWPNo" name="NPWPNo">
								</div>
							</div>

							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Nama NPWP</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="NPWPName" name="NPWPName">
								</div>
							</div>

							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Alamat NPWP</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="NPWPAddress" name="NPWPAddress">
								</div>
							</div>

							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Catatan</label>
								<div class="col-sm-10">
									<textarea name="Remarks" id="Remarks" class="form-control"></textarea>
								</div>
							</div>

							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Active</label>
								<div class="col-sm-10" id="parentDisactive">
								<label class="radio-inline">
									<input type="radio" name="IsActive" id="disactive" value="0" placeholder="">Disactive
 									</label>
								</div>

								<div class="col-sm-10" id="parentActive">
								<label class="radio-inline">
									<input type="radio" name="IsActive" id="active"    value="1" placeholder="">Active </label>
								</div>
							</div>
							
							<div class="form-group row">
								<div class="col-sm-10">
									<input type="hidden" id="CompanyId" name="CompanyId">
									<input type="hidden" class="form-control" id="Iid" name="Iid">
								</div>
							</div>
						
						
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" onclick="clearBranchForm()" data-dismiss="modal">Batal</button>
						<button type="button" class="btn btn-primary" onclick="storeBranch()">Simpan</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

<style type="text/css" media="screen">
.fullWidth {
	width: 75%;
}	
</style>