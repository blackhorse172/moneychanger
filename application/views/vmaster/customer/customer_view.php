<?php $this->load->view('jqwidgetslink'); ?>
<script type="text/javascript">
    var base_url = "<?php echo base_url(); ?>";
    var jQuery_1_4_3 = $.noConflict(true);
    jQuery_1_4_3(document).ready(function () {
        // prepare the data
        var url = "<?php echo site_url('customer/get_data'); ?>";
        var url_dd_company = "<?php echo site_url('dropdownlist/company'); ?>";
        var source =
                {
                    datatype: "json",
                    addRow: function (rowID, rowData, position, commit) {
                        // synchronize with the server - send insert command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                        commit(true);
                    },
                    updaterow: function (rowid, rowdata, commit) {
                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                    deleteRow: function (rowID, commit) {
                        // synchronize with the server - send delete command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        commit(true);
                    },
                    datafields:
                            [
                                {name: 'Iid', type: 'string'},
                                {name: 'CompanyId', type: 'string'},
                                {name: 'BranchId', type: 'string'},
                                {name: 'Kode', type: 'string'},
                                {name: 'Nama', type: 'string'},
                                {name: 'Type', type: 'string'},
                                {name: 'Alias', type: 'string'},
                                {name: 'Alamat', type: 'string'},
                                {name: 'TempatLahir', type: 'string'},
                                {name: 'TanggalLahir', type: 'date'},
                                {name: 'NoKTP', type: 'string'},
                                {name: 'NoIDLain', type: 'string'},
                                {name: 'CIF', type: 'string'},
                                {name: 'NPWP', type: 'string'},
                                {name: 'Profil', type: 'string'},
                                {name: 'Telp', type: 'string'},
                                {name: 'Rekening', type: 'string'},
                                {name: 'PemilikRekening', type: 'string'},
                                {name: 'ContactPerson', type: 'string'},
                                {name: 'Kuasa1_Nama', type: 'string'},
                                {name: 'Kuasa1_Address', type: 'string'},
                                {name: 'Kuasa1_ID', type: 'string'},
                                {name: 'Kuasa2_Nama', type: 'string'},
                                {name: 'Kuasa2_Address', type: 'string'},
                                {name: 'Kuasa2_ID', type: 'string'},
                                {name: 'SumberDana', type: 'string'},
                                {name: 'TujuanTransaksi', type: 'string'},
                                {name: 'TanggalTransaksiTerakhir', type: 'date'},
                                {name: 'Catatan', type: 'string'},
                                {name: 'IsActive', type: 'string'},
                                {name: 'InputBy', type: 'string'},
                                {name: 'InputDate', type: 'date'},
                                {name: 'LastEditBy', type: 'string'},
                                {name: 'LastEditDate', type: 'date'}
                            ],
                    id: 'Iid',
                    url: url,
                    root: 'data'
                };
        var source_dd_company =
                {
                    datatype: "json",
                    datafields:
                            [
                                {name: 'Name', type: 'string'}
                            ],
                    url: url_dd_company,
                    async: true
                };
        var dataAdapter = new jQuery_1_4_3.jqx.dataAdapter(source);
        var dataAdapter_dd_company = new jQuery_1_4_3.jqx.dataAdapter(source_dd_company);
        // initialize jqxGrid
        jQuery_1_4_3("#jqxgrid").on('bindingcomplete', function () {
            jQuery_1_4_3("#jqxgrid").jqxGrid('autoresizecolumns');
        });
        jQuery_1_4_3("#jqxgrid").jqxGrid(
                {
//                    width: '100%',
                    height: 530,
                    source: dataAdapter,
                    editable: true,
                    showfilterrow: true,
                    filterable: true,
                    altRows: true,
                    selectionmode: 'singlerow',
                    editmode: 'programmatic',
                    columnsresize: true,
                    autowidth: true,
//                    autoheight: true,
                    sortable: true,
                    showtoolbar: true,
                    renderToolbar: function (toolBar)
                    {
                        var toTheme = function (className) {
                            if (theme == "")
                                return className;
                            return className + " " + className + "-" + theme;
                        }

                        var container = jQuery_1_4_3("<div style='overflow: hidden; position: relative; height: 100%; width: 100%;'></div>");
                        var buttonTemplate = "<div style='float: left; padding: 3px; margin: 2px;'><div style='margin: 4px; width: 16px; height: 16px;'></div></div>";
                        var addButton = jQuery_1_4_3(buttonTemplate);
                        var editButton = jQuery_1_4_3(buttonTemplate);
                        var deleteButton = jQuery_1_4_3(buttonTemplate);
                        var cancelButton = jQuery_1_4_3(buttonTemplate);
                        var updateButton = jQuery_1_4_3(buttonTemplate);
                        container.append(addButton);
                        container.append(updateButton);
                        container.append(editButton);
                        container.append(deleteButton);
                        container.append(cancelButton);
                        toolBar.append(container);
                        addButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        addButton.find('div:first').addClass(toTheme('jqx-icon-plus'));
                        addButton.jqxTooltip({position: 'bottom', content: "Add"});
                        editButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        editButton.find('div:first').addClass(toTheme('jqx-icon-edit'));
                        editButton.jqxTooltip({position: 'bottom', content: "Edit"});
                        deleteButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        deleteButton.find('div:first').addClass(toTheme('jqx-icon-delete'));
                        deleteButton.jqxTooltip({position: 'bottom', content: "Delete"});
                        updateButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        updateButton.find('div:first').addClass(toTheme('jqx-icon-save'));
                        updateButton.jqxTooltip({position: 'bottom', content: "Save Changes"});
                        cancelButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        cancelButton.find('div:first').addClass(toTheme('jqx-icon-cancel'));
                        cancelButton.jqxTooltip({position: 'bottom', content: "Cancel"});
                        var updateButtons = function (action) {
                            switch (action) {
                                case "Select":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: false});
                                    editButton.jqxButton({disabled: false});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                                case "Unselect":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: true});
                                    editButton.jqxButton({disabled: true});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                                case "Edit":
                                    addButton.jqxButton({disabled: true});
                                    deleteButton.jqxButton({disabled: true});
                                    editButton.jqxButton({disabled: true});
                                    cancelButton.jqxButton({disabled: false});
                                    updateButton.jqxButton({disabled: false});
                                    break;
                                case "End Edit":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: false});
                                    editButton.jqxButton({disabled: false});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                            }
                        }
                        var rowKey = null;
                        jQuery_1_4_3("#jqxgrid").on('rowselect', function (event) {
                            if (cancelButton.jqxButton('disabled') || updateButton.jqxButton('disabled')) {
                                var args = event.args;
                                rowKey = args.rowindex;
                                var rowData = args.row;
                                updateButtons('Select');
                            }
                        });
                        addButton.click(function (event) {
                            // if (!addButton.jqxButton('disabled')) {

                            //     jQuery_1_4_3("#jqxgrid").jqxGrid('addrow', {}, {}, 'first');
                            // }
                            $('#addCustomerModal').modal({backdrop: 'static', keyboard: false}); 
                            $('#addCustomerModal').modal('show');
                            $('.modal-dialog').css('width', '50%');
                            $('#activeGroup').attr('hidden', 'true');
                            //$('#addCustomerModal').css('background-color', 'lightgrey');;

                        });
                        cancelButton.click(function (event) {
                            if (!cancelButton.jqxButton('disabled')) {
                                jQuery_1_4_3("#jqxgrid").jqxGrid('endrowedit', rowKey, true); // if true, the changes are canceled.
                                editrow = -1;
                                updateButtons('Unselect');

                                var rowindex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindex');
                                jQuery_1_4_3('#jqxgrid').jqxGrid('unselectrow', rowindex);
                                jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                            }
                        });
                        updateButton.click(function (event) {//save changes
                            if (!updateButton.jqxButton('disabled')) {

                                jQuery_1_4_3("#jqxgrid").jqxGrid('endrowedit', rowKey, false); //is false, the changes are saved
                                var datarow;
                                var selectedrowindex = jQuery_1_4_3("#jqxgrid").jqxGrid('getselectedrowindex');
                                var rowscount = jQuery_1_4_3("#jqxgrid").jqxGrid('getdatainformation').rowscount;

                                editrow = -1;
                                updateButtons('Unselect');

                                jQuery_1_4_3('#jqxgrid').jqxGrid('unselectrow', selectedrowindex);

                                var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                var field = rows[selectedrowindex];
                                if (field.TanggalLahir === null) {
                                    field.TanggalLahir = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
                                }

                                var TanggalLahir_converted = convert_date(field.TanggalLahir);
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>customer/update_data",
                                    cache: false,
                                    data: {iid: field.Iid, companyid: field.CompanyId, branchid: field.BranchId,
                                        kode: field.Kode, nama: field.Nama, type: field.Type, alias: field.Alias,
                                        alamat: field.Alamat, tempatlahir: field.TempatLahir, tanggallahir: TanggalLahir_converted, noktp: field.NoKTP,
                                        noidlain: field.NoIDLain, cif: field.CIF, npwp: field.NPWP, profil: field.Profil,
                                        telp: field.Telp, rekening: field.Rekening, pemilikrekening: field.PemilikRekening, contactperson: field.ContactPerson,
                                        kuasa1nama: field.Kuasa1_Nama, kuasa1address: field.Kuasa1_Address, kuasa1id: field.Kuasa1_ID, kuasa2nama: field.Kuasa2_Nama,
                                        kuasa2address: field.Kuasa2_Address, kuasa2id: field.Kuasa2_ID, sumberdana: field.SumberDana,
                                        tujuantransaksi: field.TujuanTransaksi, catatan: field.Catatan, isactive: field.IsActive}, // since, you need to delete post of particular id
                                    success: function (reaksi) {
//                                        alert(reaksi);
                                        if (reaksi == '1') {
                                            if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                                                var id = jQuery_1_4_3("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                                                var commit = jQuery_1_4_3("#jqxgrid").jqxGrid('updaterow', id, datarow);
                                            }
                                        } else {
                                            alert(reaksi);
                                        }
                                    }
                                });
                                jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                            }
                        });
                        editButton.click(function () {
                            if (!editButton.jqxButton('disabled')) {
                                $('#addCustomerModal').modal({backdrop: 'static', keyboard: false}); 

                                $('#addCustomerModal').modal('show');
                                $('.modal-dialog').css('width', '50%');
                                $('#activeGroup').removeAttr('hidden');
                                var datarow;
                                var selectedrowindex = jQuery_1_4_3("#jqxgrid").jqxGrid('getselectedrowindex');
                                var rowscount = jQuery_1_4_3("#jqxgrid").jqxGrid('getdatainformation').rowscount;

                                var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                var field = rows[selectedrowindex];
                                if (field.TanggalLahir === null) {
                                    field.TanggalLahir = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
                                }

                                var TanggalLahir_converted = convert_date(field.TanggalLahir);

                                $('#Iid').val(field.Iid);
                                $('#Kode').val(field.Kode);
                                $('#Nama').val(field.Nama);
                                $('#type').val(field.type);
                                $('#Alias').val(field.Alias);
                                $('#Alamat').val(field.Alamat);
                                $('#TempatLahirt').val(field.TempatLahirt);
                                $('#TanggalLahir').val(field.TanggalLahir);
                                $('#NoKTP').val(field.NoKTP);
                                $('#NoIDLain').val(field.NoIDLain);
                                $('#NPWP').val(field.NPWP);
                                $('#Profil').val(field.Profil);
                                $('#Telp').val(field.Telp);
                                $('#Rekening').val(field.Rekening);
                                $('#PemilikRekening').val(field.PemilikRekening);
                                $('#Kuasa1_ID').val(field.Kuasa1_ID);
                                $('#Kuasa1_Nama').val(field.Kuasa1_Nama);
                                $('#Kuasa2_Address').val(field.Kuasa2_Address);

                                $('#addCustomerModal').modal('show');
                                
                                if (field.IsActive == "1") {
                                  $('#parentActive span').addClass('checked');
                                } else {
                                    //$('#active').prop('checked', 'true');
                                      $('#parentDisactive span').addClass('checked');
                                }


                            }
                        });
                        deleteButton.click(function () {
                            if (!deleteButton.jqxButton('disabled')) {
                                var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                var selectedrowindex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindex');
                                var field = rows[selectedrowindex];
                                if (field.Kode === '') {
                                    var id = jQuery_1_4_3("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                                    jQuery_1_4_3('#jqxgrid').jqxGrid('deleterow', id);
                                } else {
                                    if (confirm("Anda yakin menghapus data dengan Kode '" + field.Kode + "'?")) {
                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url() ?>customer/delete_data",
                                            cache: false,
                                            data: {iid: field.Iid}, // since, you need to delete post of particular id
                                            success: function (reaksi) {
                                                if (reaksi == '1') {
                                                    var id = jQuery_1_4_3("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                                                    jQuery_1_4_3('#jqxgrid').jqxGrid('deleterow', id);
                                                } else {
                                                    alert(reaksi);
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        });
                        jQuery_1_4_3("#jqxgrid").on('endrowedit', function (event) {
                            updateButtons('End Edit');
                        });
                    },
                    columns: [
                        {text: 'Iid', columntype: 'textbox', datafield: 'Iid', width: 70, hidden: true},
                        {text: 'Kode Customer', pinned: true, columntype: 'textbox', datafield: 'Kode', width: 150, validation: function (cell, value) {
                                if (value == null) {
                                    return {result: false, message: "Harus diisi"};
                                }
                                return true;
                            }
                        },
                        {text: 'Nama Customer', pinned: true, columntype: 'textbox', datafield: 'Nama', width: 210},
                        {text: 'Jenis Customer', columntype: 'textbox', datafield: 'Type', width: 160},
                        {text: 'Alias', columntype: 'textbox', datafield: 'Alias', width: 160},
                        {text: 'Alamat', columntype: 'textbox', datafield: 'Alamat', width: 310},
                        {text: 'Tempat Lahir', columntype: 'textbox', datafield: 'TempatLahir', width: 210},
                        {
                            text: 'Tgl. Lahir', datafield: 'TanggalLahir', columntype: 'datetimeinput', width: 110, align: 'right', cellsalign: 'right', cellsformat: 'd/M/yyyy'
                        },
                        {text: 'No KTP', columntype: 'textbox', datafield: 'NoKTP', width: 210},
                        {text: 'No ID Lain', columntype: 'textbox', datafield: 'NoIDLain', width: 210},
                        {text: 'CIF', columntype: 'textbox', datafield: 'CIF', width: 210},
                        {text: 'NPWP', columntype: 'textbox', datafield: 'NPWP', width: 210},
                        {text: 'Profil', columntype: 'textbox', datafield: 'Profil', width: 210},
                        {text: 'Telp', columntype: 'textbox', datafield: 'Telp', width: 210},
                        {text: 'Rekening', columntype: 'textbox', datafield: 'Rekening', width: 210},
                        {text: 'Pemilik Rekening', columntype: 'textbox', datafield: 'PemilikRekening', width: 210},
                        {text: 'Contact Person', columntype: 'textbox', datafield: 'ContactPerson', width: 210},
                        {text: 'Kuasa 1 Nama', columntype: 'textbox', datafield: 'Kuasa1_Nama', width: 210},
                        {text: 'Kuasa 1 Address', columntype: 'textbox', datafield: 'Kuasa1_Address', width: 210},
                        {text: 'Kuasa 1 ID', columntype: 'textbox', datafield: 'Kuasa1_ID', width: 210},
                        {text: 'Kuasa 2 Nama', columntype: 'textbox', datafield: 'Kuasa2_Nama', width: 210},
                        {text: 'Kuasa 2 Address', columntype: 'textbox', datafield: 'Kuasa2_Address', width: 210},
                        {text: 'Kuasa 2 ID', columntype: 'textbox', datafield: 'Kuasa2_ID', width: 210},
                        {text: 'Sumber Dana', columntype: 'textbox', datafield: 'SumberDana', width: 210},
                        {text: 'Tujuan Transaksi', columntype: 'textbox', datafield: 'TujuanTransaksi', width: 210},
                        {
                            text: 'Tgl. Transaksi Terakhir', datafield: 'TanggalTransaksiTerakhir', columntype: 'datetimeinput', width: 110, align: 'right', cellsalign: 'right', cellsformat: 'd/M/yyyy'
                        },
                        {text: 'Catatan', columntype: 'textbox', datafield: 'Catatan', width: 210},
                        {text: 'Status Aktif', columntype: 'textbox', datafield: 'IsActive', width: 210},
                        {text: 'InputBy', columntype: 'textbox', datafield: 'InputBy', width: 210},
                        {
                            text: 'Tgl. Input', datafield: 'InputDate', columntype: 'datetimeinput', width: 110, align: 'right', cellsalign: 'right', cellsformat: 'd/M/yyyy'
                        },
                        {text: 'LastEditBy', columntype: 'textbox', datafield: 'LastEditBy', width: 210},
                        {
                            text: 'Tgl. Edit', datafield: 'LastEditDate', columntype: 'datetimeinput', width: 110, align: 'right', cellsalign: 'right', cellsformat: 'd/M/yyyy'
                        }
                    ]
                });
    }
    );
    function convert_date(str) {
        var date = new Date(str),
                mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }


function storeCustomer() {
        var status = checkCodeCustomer();

        if (status == '200') {
            alert('Kode ada yang sama');
            $('#Kode').focus();
            return false;
        }
        if ($('#Kode').val() == '') {
            alert('Kode Harus D isi');
            $('#Kode').focus();
            return false;
        }

        if ($('#Nama').val() == '') {
            alert('Nama Harus D isi');
              $('#Nama').focus();
            return false;
        }

         if ($('#Alamat').val() == '') {
            alert('Alamat Harus D isi');
             $('#Alamat').focus();
            return false;
        }

         if ($('#Telp').val() == '') {
            alert('Telp Harus D isi');
              $('#Telp').focus();
            return false;
        }


         $.ajax({
         type: "POST",
         url: base_url + "customer/store",
         cache: false,
         dataType: "json",
         data: $('#customerForm').serialize(), // since, you need to delete post of particular id
         success: function(data) {
             if (data.status == 200) {
                 jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                 $('#addCustomerModal').modal('hide');
                 $('#customerForm').closest('form').find("input[type=text], textarea,input[type=date],input[type=hidden]").val("");

                 //window.location.reload();
             }

         }
     });
 }


 function clearCustomerForm() {
     // body...
        $('#customerForm').closest('form').find("input[type=text],input[type=date],input[type=hidden]").val("");      
 }

 function checkCodeCustomer() {
    var status = "";
     $.ajax({
         type: "POST",
         url: base_url + "customer/checkCodeCustomer",
         cache: false,
         dataType: "json",
         data: $('#customerForm').serialize(), // since, you need to delete post of particular id
         success: function(data) {
             if (data.status == 200) {
                 alert('Kode Customer Ada yang sama');
                 $('#Kode').val('');
                 $('#Kode').focus();
                status = data.status;
                 //jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                 //$('#addCustomerModal').modal('hide');
                 //$('#customerForm').closest('form').find("input[type=text], textarea,input[type=date],input[type=hidden]").val("");

                 //window.location.reload();
             }

         }
     });

    return status;
 }


</script>
<h3 class="page-title">
    <?php echo $pageform ?></h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="index.html">Master</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Customer</a>
        </li>
    </ul>
</div>
<table>
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<div class="row">
    <div class="col-md-12">
        <div class='box'>
            <div class="box-body table-responsive">
                <div id='jqxWidget'>
                    <div id="jqxgrid"></div>
                    <div>
                        <div id="cellbegineditevent"></div>
                        <div id="cellendeditevent"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('vmaster/customer/modal_view');?>
