		<div class="modal fade" id="addCustomerModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							<span class="sr-only">Close</span>
						</button> -->
						<h4 class="modal-title">Add Customer</h4>
					</div>
					<div class="modal-body">
						<form id="customerForm" role="form">
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Kode Customer <span class="required" style="color:red;">*</span></label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Kode" name="Kode" onchange="checkCodeCustomer()" placeholder="Kode Customer" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="inputPassword3" class="col-sm-2 form-control-label">Nama Customer <span class="required" style="color:red;">*</span></label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Nama" name="Nama" placeholder="Nama" required>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Jenis Customer </label>
								<div class="col-sm-10">
								<label class="radio-inline">
								<input type="radio" name="Type" value="Perorangan" placeholder=""> Perorangan </label>
								<label class="radio-inline">
								<input type="radio" name="Type" value="Money Changer" placeholder=""> Money Changer / PT </label>
									<!-- <input type="text" class="form-control" id="Type" name="Type" placeholder="Jenis Customer"> -->
								</div>
							</div>
							
							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Alias <span class="required" style="color:red;">*</span></label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Alias" name="Alias" placeholder="Alias">
								</div>
							</div>
							
							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Alamat <span class="required" style="color:red;">*</span></label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Alamat" name="Alamat" placeholder="Alamat">
								</div>
							</div>
							
							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Tempat Lahir</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="TempatLahir" name="TempatLahir" placeholder="Tempat Lahir">
								</div>
							</div>
							
							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Tanggal Lahir</label>
								<div class="col-sm-10">
									<input type="date" class="form-control" id="TanggalLahir" name="TanggalLahir" placeholder="Tanggal Lahir">
								</div>
							</div>
							
							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">No KTP</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="NoKTP" name="NoKTP" placeholder="No KTP">
								</div>
							</div>

							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">No ID Lain</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="N" name="TempatLahir" placeholder="Tempat Lahir">
								</div>
							</div>
							
						<!-- 	<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">CIF</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="TanggalLahir" name="TanggalLahir" placeholder="Tanggal Lahir">
								</div>
							</div>
							 -->
							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">No NPWP</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="NPWP" name="NPWP" placeholder="NPWP">
								</div>
							</div>

							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Profil</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Profil" name="Profil" placeholder="Profil">
								</div>
							</div>
							
							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Telp <span class="required" style="color:red;">*</span></label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Telp" name="Telp" placeholder="Telepon">
								</div>
							</div>
							
							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Rekening</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Rekening" name="Rekening" placeholder="Rekening">
								</div>
							</div>


							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Pemilik Rekening</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="PemilikRekening" name="PemilikRekening" placeholder="Pemilik Rekening">
								</div>
							</div>
							
							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Kuasa id</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Kuasa1_ID" name="Kuasa1_ID" placeholder="Kuasa Id">
								</div>
							</div>

							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Nama Kuasa</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Kuasa1_Nama" name="Kuasa1_Nama" placeholder="Nama Kuasa">
								</div>
							</div>
							
							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Alamat Kuasa</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Kuasa1_Address" name="Kuasa1_Address" placeholder="Kuasa Id">
								</div>
							</div>

							<div class="form-group row" id="activeGroup" hidden="true">
								<label for="inputtext3" class="col-sm-2 form-control-label">Active</label>
								
								<div class="col-sm-10" id="parentDisactive">
									<label class="radio-inline"><input type="radio" name="IsActive" id="disactive" value="0" placeholder="" selected>Disactive</label>
								</div>

								<div class="col-sm-10" id="parentActive">
									<label class="radio-inline"><input type="radio" name="IsActive" id="active"    value="1" placeholder="">Active</label>
								</div>
							</div>

							
							<div class="form-group row">
								<div class="col-sm-10">
									<input type="hidden" class="form-control" id="Iid" name="Iid" value="">
								</div>
							</div>
						
						
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" onclick="clearCustomerForm()" data-dismiss="modal">Batal</button>
						<button type="button" class="btn btn-primary" onclick="storeCustomer()">Simpan</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

<style type="text/css" media="screen">
.fullWidth {
	width: 75%;
}	
</style>