		<div class="modal fade" id="addCompanyModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Company</h4>
					</div>
					<div class="modal-body">
						<form id="companyForm" role="form">
							<input type="hidden" name="FormType" id="FormType" value="">
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Name</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" onchange="checkCompanyCode()" id="Name" name="Name">
								<div id="Valas"></div>
								</div>
							</div>
					
							
							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Level</label>
								<div class="col-sm-10">
									<div id="companyLevel"></div>
									<input type="hidden" class="form-control" id="Level" name="Level">
								</div>
							</div>
							
							<div class="form-group row" hidden="true" id="activeGroup"/>
								<label for="inputtext3" class="col-sm-2 form-control-label">Active</label>
								<div class="col-sm-10" id="parentDisactive">
								<label class="radio-inline">
									<input type="radio" name="IsActive" id="disactive" value="0" placeholder="">Disactive
									</label>

								</div>

								<div class="col-sm-10" id="parentActive">
								<label class="radio-inline">
									<input type="radio" name="IsActive" id="active"    value="1" placeholder="">Active
									</label>
								</div>
							</div>
							
							<div class="form-group row">
								<div class="col-sm-10">
									<input type="hidden" class="form-control" id="Iid" name="Iid">
								</div>
							</div>

					<div class="modal-header">
						<h4 class="modal-title">Bank Account</h4>
					</div>

					<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Bank</label>
								<div class="col-sm-10">
								<select id="BankName" class="form-control" required>
									<option value="">--Pilih Bank--</option>
									<option value="BCA">BCA</option>
									<option value="Mandiri">Mandiri</option>
									<option value="BNI">BNI</option>
									<option value="BRI">BRI</option>
									<option value="Permata">Permata</option>
									<option value="ANZ">ANZ</option>
									<option value="CIMB">CIMB</option>
									
								</select>
								</div>
					</div>

					<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Nomor Rekening Akun</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="AccountNo">
								</div>
					</div>

					<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Nama Akun Pemilik</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="AccountName">
								</div>
					</div>

					<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Alamat Bank Akun</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="AccountAddress">
								</div>
					</div>

					<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Kategori Akun</label>
								<div class="col-sm-10">
								<select id="Category" class="form-control" required>
									<option value="">--Pilih Bank--</option>
									<option value="0">Tidak Terlapor</option>
									<option value="1">Terlapor</option>
								</select>
								</div>
					</div>


 						<div class="modal-footer">
                                <button type="button" onclick="addRowBankAccount()" class="btn btn-primary">Tambah</button>
                          </div>

            		<div class="form-group row">
                            <table class="table" id="BankAccountTable">
                                <thead>
                                    <tr>
                                        <th>Bank</th>
                                        <th>Nomor Akun</th>
                                        <th>Nama Akun</th>
                                        <th>Alamat</th>
                                        <th>Kategori</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="rowTable">
                                </tbody>
                            </table>
           	 		</div>
						<!-- 	<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Active</label>
								<div class="col-sm-10" id="parentDisactive">
								<label class="radio-inline">
									<input type="radio" name="IsActive" id="disactive" value="0" placeholder="">Disactive
									</label>

								</div>

								<div class="col-sm-10" id="parentActive">
								<label class="radio-inline">
									<input type="radio" name="IsActive" id="active"    value="1" placeholder="">Active
									</label>
								</div>
							</div> -->
						
						
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" onclick="clearCompanyForm()" data-dismiss="modal">Batal</button>
						<button type="button" class="btn btn-primary" onclick="storeCompany()">Simpan</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

<style type="text/css" media="screen">
.fullWidth {
	width: 75%;
}	
</style>