		<div class="modal fade" id="addRateModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							<span class="sr-only">Close</span>
						</button> -->
						<h4 class="modal-title">Rate</h4>
					</div>
					<div class="modal-body">
						<form id="rateForm" role="form">
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Valas</label>
								<div class="col-sm-10">
								<!-- <select class="form-control" id="Valas" name="Valas">	
								</select> -->
								<div id="Valas"></div>
								</div>
							</div>
					
							
							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Adj Beli</label>
								<div class="col-sm-10">
									<input type="text" readonly class="auto form-control" id="Buy_Disc_MC" name="Buy_Disc_MC">
								</div>
							</div>
							
							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Adj Jual</label>
								<div class="col-sm-10">
									<input type="text" readonly class="auto form-control" id="Sell_Disc_MC" name="Sell_Disc_MC">
								</div>
							</div>
							
							<div class="form-group row">
								<div class="col-sm-10">
									<input type="hidden" class="auto form-control" id="Iid" name="Iid">
									<input type="hidden" class="auto form-control" id="CurrencyId" name="CurrencyId">
								</div>
							</div>
						
						
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" onclick="clearRateForm()" data-dismiss="modal">Batal</button>
						<button type="button" class="btn btn-primary" onclick="storeRate()">Simpan</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

<style type="text/css" media="screen">
.fullWidth {
	width: 75%;
}	
</style>