		<div class="modal fade" id="addBankAccountMasterModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">BankAccountMaster</h4>
					</div>
					<br/>
					<?php $this->load->view('errors/alert'); ?>
					<div class="modal-body">
						<form id="bankAccountMasterForm" role="form">
							<input type="hidden" name="Iid" id="Iid" value="">
							

							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Nomor Bank Akun</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="AccountNo" name="AccountNo">
								</div>
							</div>

							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Nama Bank Akun</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="AccountName" name="AccountName">
								</div>
							</div>
							<div class="form-group row">

							<label for="inputEmail3" class="col-sm-2 form-control-label">Bank</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="BankName" name="BankName">
								</div>
							</div>

							<div class="form-group row">
				
							<label for="inputEmail3" class="col-sm-2 form-control-label">Alamat Akun Bank</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="AccountAddress" name="AccountAddress">
								</div>
							</div>

								<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Tipe Status</label>
								<div class="col-sm-10">
								<select name="Category" id="Category"  class="form-control" required>
											<option value=""></option>
											<option value="0">Terlapor</option>
											<option value="1">Tidak Terlapor</option>
										
										</select>
								</div>
							</div>

						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" onclick="clearBankAccountMasterForm()" data-dismiss="modal">Batal</button>
						<button type="button" class="btn btn-primary" onclick="storeBankAccountMaster()">Simpan</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

<style type="text/css" media="screen">
.fullWidth {
	width: 75%;
}	
</style>