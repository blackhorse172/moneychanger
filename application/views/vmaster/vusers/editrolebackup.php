<table align="center">
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<?php echo form_open(base_url() . 'user/editsaverole'); ?>
<form class="horizontal-form">
    <div class="portlet light bg-inverse">
        <div class="portlet-body form">
            <!-- BEGIN FORM-->

            <div class="form-body">
                <!--<h3 class="form-section">Person Info</h3>-->
                <div class="row">
                    <!-- ______________________COLOUMN BEGIN_____________________________________________-->
                    <div class="col-md-6 ">
                        <!--                                <fieldset class="field">
                                                            <legend class="field">Personalia:</legend>-->

                        <div class="form-body">
                            <table>
                                <tr>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Company ID :
                                    </label>
                                    <div class="col-md-7">
                                        <input type="hidden" class="form-control" name="iid" id="iid" readonly="true" value="<?php echo $dbheader->Iid ?>">
                                        <select class="form-control" name="companyid" id="companyid">
                                            <option value=""></option>
                                            <option value="KAR">KAR</option>
                                            <option value="SPAA">SPAA</option>
                                        </select>
                                    </div>
                                </div>
                                </tr><br>
<!--                                <tr>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Branch&nbsp;Code&nbsp;: <span class="required">*</span>  </label>
                                    <div class="col-md-7">
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button" onclick="popupbranchmaster()"><i class="fa fa-search input-circle"></i></button>
                                            </span>
                                            <input type="hidden" name="branchid" id="branchid" class="form-control" readonly onclick="popupbranchmaster()"/>
                                            <input type="text" class="form-control" name="branchcodename" id="branchcodename" readonly="true" data-toggle="tooltip" onclick="popupbranchmaster()">
                                            
                                        </div>

                                    </div>
                                </div>
                                </tr><br>-->
                                <tr>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Active&nbsp;Status&nbsp;: </label>
                                    <div class="radio-list">
                                        <?php if ($dbheader->IsActive == 1) { ?>
                                            <div class="col-md-7">
                                                <label class="radio-inline">
                                                    <input type="radio" name="activestatus" id="activestatus1" value="1" checked> Active </label>

                                                <label class="radio-inline">
                                                    <input type="radio" name="activestatus" id="activestatus2" value="0"> Inactive </label>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-md-7">
                                                <label class="radio-inline">
                                                    <input type="radio" name="activestatus" id="activestatus1" value="1" > Active </label>

                                                <label class="radio-inline">
                                                    <input type="radio" name="activestatus" id="activestatus2" value="0" checked> Inactive </label>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- _______________________COLOUMN END____________________________________________-->
                    <!-- ________________________COLOUMN BEGIN___________________________________________-->
                    <div class="col-md-6 ">
                        <!--                                <fieldset class="field">
                                                            <legend class="field">Personalia:</legend>-->

                        <div class="form-body">
                            <table>
                                <tr>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Role&nbsp;Code&nbsp;:</label>
                                    <div class="col-md-7">
                                        <input type="text" style="height: 30px;" class="form-control input-circle" name="code" id="code" value="<?php echo $dbheader->Code; ?>"><?php echo form_error('code'); ?>
                                    </div>
                                </div>
                                </tr><br>
                                <tr>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Role&nbsp;Name&nbsp;:</label>
                                    <div class="col-md-7">
                                        <input type="text" style="height: 30px;" class="form-control input-circle" name="name" id="name" value="<?php echo $dbheader->Name; ?>"><?php echo form_error('name'); ?>
                                    </div>
                                </div>
                                </tr><br>
                                <tr>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Remarks :</label>
                                    <div class="col-md-7">
                                        <textarea class="form-control input-circle" rows="2" name="remarks" id="remarks"><?php echo $dbheader->Remarks; ?></textarea>
                                    </div>
                                </div>

                                </tr>
                            </table>
                        </div>
                        <!--</fieldset>-->
                    </div>
                    <!-- ________________________COLOUMN END___________________________________________-->

                    <!-- END FORM-->
                </div>

            </div>
        </div>
        <!--/.col (right) -->
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">

                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_5">
                        <thead>
                            <tr>
                                <th>Form&nbsp;ID</th>
                                <th>Create</th>
                                <th>View</th>
                                <th>Update</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $authority = $dbheader->RoleAuthority;
                            ?>
                            <?php foreach ($db->result() as $baris): ?>
                                <tr align="center">
                                    <td><h6><?php echo $baris->FormId; ?></h6></td>

                                    <?php
                                    $a = 2;
                                    $arrdata = explode('|', $authority); //explode form
                                    for ($i = 0; $i < count($arrdata); $i++) {
                                        $arrauth = explode('::', $arrdata[$i]); //explode auth
                                        for ($j = 0; $j < count($arrauth); $j++) {
                                            if ($arrauth[$j] == $baris->FormId) {// split form
                                                $x = 0; // variabel loop untuk penamaan value checbox
                                                for ($z = 0; $z < count($arrauth); $z++) {
                                                    if ($x == 1) {
                                                        $valuechekbox = 'create';
                                                    } else if ($x == 2) {
                                                        $valuechekbox = 'view';
                                                    } else if ($x == 3) {
                                                        $valuechekbox = 'update';
                                                    } else if ($x == 4) {
                                                        $valuechekbox = 'delete';
                                                    }
                                                    if ($arrauth[$z] == 'create') {
                                                        ?>
                                                        <td><input type="checkbox" id="<?php echo $baris->FormId; ?>" name="<?php echo $baris->FormId; ?>[]" value="create" checked></td>
                                                        <?php
                                                        $a = 3;
                                                    } else if ($arrauth[$z] == 'view') {
                                                        ?>
                                                        <td><input type="checkbox" id="<?php echo $baris->FormId; ?>" name="<?php echo $baris->FormId; ?>[]" value="view" checked></td>
                                                        <?php
                                                        $a = 3;
                                                    } else if ($arrauth[$z] == 'update') {
                                                        ?>
                                                        <td><input type="checkbox" id="<?php echo $baris->FormId; ?>" name="<?php echo $baris->FormId; ?>[]" value="update" checked></td>
                                                        <?php
                                                        $a = 3;
                                                    } else if ($arrauth[$z] == 'delete') {
                                                        ?>
                                                        <td><input type="checkbox" id="<?php echo $baris->FormId; ?>" name="<?php echo $baris->FormId; ?>[]" value="delete" checked></td>
                                                        <?php
                                                        $a = 3;
                                                    } else if ($arrauth[$z] == '') {
                                                        ?>
                                                        <td><input type="checkbox" id="<?php echo $baris->FormId; ?>" name="<?php echo $baris->FormId; ?>[]" value="<?php echo $valuechekbox; ?>"></td>

                                                        <?php
                                                    } $x++;
                                                }
                                            }

                                            if ($a == 2) {
                                                if ($arrauth[0] == '') {
                                                    ?>
                                                    <td><input type="checkbox" id="<?php echo $baris->FormId; ?>" name="<?php echo $baris->FormId; ?>[]" value="create"></td>
                                                    <td><input type="checkbox" id="<?php echo $baris->FormId; ?>" name="<?php echo $baris->FormId; ?>[]" value="view"></td>
                                                    <td><input type="checkbox" id="<?php echo $baris->FormId; ?>" name="<?php echo $baris->FormId; ?>[]" value="update"></td>
                                                    <td><input type="checkbox" id="<?php echo $baris->FormId; ?>" name="<?php echo $baris->FormId; ?>[]" value="delete"></td>
                                                    <?php
                                                    $a = 2;
                                                }
                                            }
                                        }
                                    }
                                    ?>


                            <input type="hidden" id="<?php echo $baris->FormId; ?>" name="<?php echo $baris->FormId; ?>[]" value="">

                            </tr>
                            <?php
                            $no++;
                        endforeach;
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <table align="center">
                <tr><td><button type="submit" class="btn btn green">Save</button></td></tr>
            </table>


            <!-- END EXAMPLE TABLE PORTLET-->

        </div>
    </div>
</form>
<?php echo form_close() ?>