<table align="center">
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<div class="tab-pane" id="tab_1">
    <div class="portlet light bg-inverse">
        <div class="portlet-body form">

            <!-- BEGIN FORM-->
            <form class="horizontal-form" action="<?php echo base_url() . 'user/editsimpanuserroleauth' ?>" method='post' name='formuserroleauth' onsubmit="send_value()">
                <div class="form-body">
                    <!--<h3 class="form-section">Person Info</h3>-->
                    <div class="row">
                        <!-- ______________________COLOUMN BEGIN_____________________________________________-->
                        <div class="col-md-6 ">
                            <!--                                <fieldset class="field">
                                                                <legend class="field">Personalia:</legend>-->

                            <div class="form-body">
                                <table>
                                    <tr>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">User&nbsp;ID&nbsp;: <span class="required">*</span></label>
                                        <div class="col-md-7">
                                            <input type="hidden" style="height: 29px;" class="form-control input-circle" name="iid" id="iid" value="<?php echo $db->Iid ?>">
                                            <input type="text" style="height: 29px;" class="form-control input-circle" name="useridauto" id="useridauto" value="<?php echo $db->UserId ?>">
                                        </div>
                                    </div>
                                    </tr><br>
                                    <tr>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Company&nbsp;ID&nbsp;: <span class="required">*</span></label>
                                        <div class="col-md-7">
                                            <input type="text" style="height: 29px;" class="form-control" name="companyidauto" id="companyidauto" value="<?php echo $db->CompanyId ?>">
                                        </div>
                                    </div>
                                    </tr><br>
                                    <tr>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Active&nbsp;Status&nbsp;: </label>
                                        <div class="radio-list">
                                            <?php if ($db->IsActive == 1) { ?>
                                                <div class="col-md-7">
                                                    <label class="radio-inline">
                                                        <input type="radio" name="activestatus" id="activestatus1" value="1" checked> Active </label>

                                                    <label class="radio-inline">
                                                        <input type="radio" name="activestatus" id="activestatus2" value="0"> Inactive </label>
                                                </div>
                                            <?php } else { ?>
                                                <div class="col-md-7">
                                                    <label class="radio-inline">
                                                        <input type="radio" name="activestatus" id="activestatus1" value="1" > Active </label>

                                                    <label class="radio-inline">
                                                        <input type="radio" name="activestatus" id="activestatus2" value="0"> Inactive </label>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- _______________________COLOUMN END____________________________________________-->
                        <!-- ________________________COLOUMN BEGIN___________________________________________-->
                        <div class="col-md-6 ">
                            <!--                                <fieldset class="field">
                                                                <legend class="field">Personalia:</legend>-->

                            <div class="form-body">
                                <table>
                                    <tr>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Role&nbsp;Code&nbsp;: <span class="required">*</span></label>
                                        <div class="col-md-7">
                                            <input type="text" value="<?php echo $db->RoleCode ?>" style="height: 29px;" class="form-control" name="rolecode_auto" id="rolecode_auto">
                                        </div>
                                    </div>
                                    </tr><br>
                                    <tr>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Cabang&nbsp;: <span class="required">*</span></label>
                                        <div class="col-md-7">
                                            <input type="text" value="<?php echo $db->BranchName ?>" style="height: 29px;" class="form-control" name="kodecabang_auto" id="kodecabang_auto">
                                        </div>
                                    </div>
                                    </tr><br>
                                </table>
                            </div>
                            <!--</fieldset>-->
                        </div>
                        <!-- ________________________COLOUMN END___________________________________________-->
                    </div>
                    <table align="center">
                        <tr>
                            <td>
                                <button type="submit" class="btn green">Input</button>
                            </td>
                        </tr>
                    </table>
                    <!-- END FORM-->
                </div>
            </form>
        </div>
    </div>
    <!--/.col (right) -->
</div>