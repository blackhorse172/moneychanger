
<div class="row">
    <div class="col-md-12">
        <table align="center">
            <tr>
                <?php if ($error == '') { ?>
                    <?php
                } else {
                    $error = explode(":::", $error);
                    if ($error[0] == 1) {
                        ?>
                    <div class="alert alert-success">
                        <strong>Success!</strong> <?php echo $error[1]; ?>
                    </div>
                <?php } else if ($error[0] == 2) { ?>
                    <div class="alert alert-warning">
                        <strong>Warning!</strong> <?php echo $error[1]; ?>
                    </div>
                <?php } else { ?>
                    <div class="alert alert-danger">
                        <strong>Warning!</strong> <?php echo $error[1]; ?>
                    </div>
                    <?php
                }
            }
            ?>
            </tr>
        </table>
        <div class="portlet box yellow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Users Form
                </div>

            </div>
            <div class="portlet-body">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_1_1" data-toggle="tab">
                            User </a>
                    </li>
                    <li>
                        <a href="#tab_1_2" data-toggle="tab">
                            Role </a>
                    </li>
                    <li>
                        <a href="#tab_1_3" data-toggle="tab">
                            Set User Authority Role </a>
                    </li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="tab_1_1">

                        <div class="row">
                            <div class="col-md-12">

                                <div class="portlet box green-haze">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <?php echo form_open(base_url() . 'user/newuser'); ?>
                                            <button type="submit" class="btn green">Create New User</button>
                                            <?php echo form_close() ?>

                                        </div>

                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                            <thead>
                                                <tr>
                                                    <th>Action</th>
                                                    <th>User ID</th>
                                                    <th>Activestat</th>
                                                    <th>LastEdit By</th>
                                                    <th>LastEditDate</th>
                                                    <th>Input By</th>
                                                    <th>Input Date</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($dbuser->result() as $baris): ?>
                                                    <?php
                                                    if ($baris->IsActive == 1) {
                                                        $activestat = "Active";
                                                    } else {
                                                        $activestat = "Inactive";
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td align="center">
                                                            <?php
                                                            if ($baris->UserId != 'admin') {
                                                                echo anchor('user/edituserget/' . $baris->UserId, ' ', array('class' => 'fa fa-edit', 'title' => 'Edit',
                                                                    'onclick' => "return confirm('Anda yakin ingin mengubah?')"));
                                                                ?>
                                                                <?php
                                                                echo anchor('user/deleteuser/' . $baris->UserId, ' ', array('class' => 'glyphicon glyphicon-trash font-blue', 'title' => 'Delete',
                                                                    'onclick' => "return confirm('Anda yakin ingin Mengapus?')"));
                                                            }
                                                            ?>
                                                        </td>
                                                        <td align="center"><h6><?php echo $baris->UserId; ?></h6></td>
                                                        <td align="center"><h6><?php echo $activestat; ?></h6></td>
                                                        <td align="center"><h6><?php echo $baris->LastEditBy; ?></h6></td>
                                                        <td align="center"><h6><?php echo $baris->LastEditDate; ?></h6></td>
                                                        <td align="center"><h6><?php echo $baris->InputBy; ?></h6></td>
                                                        <td align="center"><h6><?php echo $baris->InputDate; ?></h6></td>

                                                    </tr>
                                                    <?php
                                                    $no++;
                                                endforeach;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab_1_2">

                        <div class="row">
                            <div class="col-md-12">

                                <div class="portlet box green-haze">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <?php echo form_open(base_url() . 'user/newrole'); ?>
                                            <button type="submit" class="btn green">Create New Role</button>
                                            <?php echo form_close() ?>

                                        </div>

                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover" id="sample_4">
                                            <thead>
                                                <tr>
                                                    <th>Action</th>
                                                    <!--<th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Role ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>-->
                                                    <!--<th>Company ID</th>-->
                                                    <th>Code</th>
                                                    <th>Name</th>
                                                    <th>Activestat</th>
                                                    <th>Remarks</th>
                                                    <!--<th>Role Authority</th>-->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($dbrole->result() as $baris): ?>
                                                    <?php
                                                    if ($baris->IsActive == 1) {
                                                        $activestat = "Active";
                                                    } else {
                                                        $activestat = "Inactive";
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td>

                                                            <?php
                                                            echo anchor('user/roleget/' . $baris->Iid, ' ', array('class' => 'fa fa-edit', 'title' => 'Edit',
                                                                'onclick' => "return confirm('Anda yakin ingin Mengubah?')"));
                                                            ?>
                                                            <?php
                                                            if ($baris->Iid != 'STP-ROLE-00001') {
                                                                echo anchor('user/roledelete/' . $baris->Iid, ' ', array('class' => 'glyphicon glyphicon-trash font-blue', 'title' => 'Delete',
                                                                    'onclick' => "return confirm('Anda yakin ingin Mengapus?')"));
                                                            }
                                                            ?>
                                                        </td>
                                                        <!--<td align="center"><h6><?php echo $baris->Iid; ?></h6></td>-->
                                                        <!--<td align="center"><h6><?php echo $baris->CompanyId; ?></h6></td>-->
                                                        <td align="center"><h6><?php echo $baris->Code; ?></h6></td>
                                                        <td align="center"><h6><?php echo $baris->Name; ?></h6></td>
                                                        <td align="center"><h6><?php echo $activestat; ?></h6></td>
                                                        <td align="center"><h6><?php echo $baris->Remarks; ?></h6></td>
                                                        <!--<td><h6><?php echo $baris->RoleAuthority; ?></h6></td>-->


                                                    </tr>
                                                    <?php
                                                    $no++;
                                                endforeach;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab_1_3">

                        <div class="row">
                            <div class="col-md-12">

                                <div class="portlet box green-haze">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <?php echo form_open(base_url() . 'user/newuserroleauth'); ?>
                                            <button type="submit" class="btn green">Create New UserRole Authority</button>
                                            <?php echo form_close() ?>

                                        </div>

                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover" id="sample_3">
                                            <thead>
                                                <tr>
                                                    <th>Action</th>
                                                    <th>User ID</th>
                                                    <th>Company ID</th>
                                                    <th>Role Code</th>
                                                    <th>Branch ID</th>
                                                    <th>Activestat</th>
                                                    <th>InputBy</th>
                                                    <th>InputDate</th>
                                                    <th>LastEditBy</th>
                                                    <th>LastEditDate</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($dbroleauth->result() as $baris): ?>
                                                    <?php
                                                    if ($baris->IsActive == 1) {
                                                        $activestat = "Active";
                                                    } else {
                                                        $activestat = "Inactive";
                                                    }
                                                    ?>
                                                    <tr align="center">
                                                        <td>
                                                            <?php
                                                            if ($baris->UserId != 'admin') {
                                                                echo anchor('user/roleauthget/' . $baris->Iid, ' ', array('class' => 'fa fa-edit', 'title' => 'Edit',
                                                                    'onclick' => "return confirm('Anda yakin ingin mengubah?')"));
                                                                ?>
                                                                <?php
                                                                echo anchor('user/roleauthdelete/' . $baris->Iid, ' ', array('class' => 'glyphicon glyphicon-trash font-blue', 'title' => 'Delete',
                                                                    'onclick' => "return confirm('Anda yakin ingin Mengapus?')"));
                                                            }
                                                            ?>
                                                        </td>
                                                        <td><h6><?php echo $baris->UserId; ?></h6></td>
                                                        <td><h6><?php echo $baris->CompanyId; ?></h6></td>
                                                        <td><h6><?php echo $baris->Code; ?></h6></td>
                                                        <td><h6><?php echo $baris->BranchId; ?></h6></td>
                                                        <td><h6><?php echo $activestat; ?></h6></td>
                                                        <td><h6><?php echo $baris->InputBy; ?></h6></td>
                                                        <td><h6><?php echo $baris->InputDate; ?></h6></td>
                                                        <td><h6><?php echo $baris->LastEditBy; ?></h6></td>
                                                        <td><h6><?php echo $baris->LastEditDate; ?></h6></td>

                                                    </tr>
                                                    <?php
                                                    $no++;
                                                endforeach;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->

                            </div>
                        </div>
                    </div>

                </div>
                <div class="clearfix margin-bottom-20">
                </div>


            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>