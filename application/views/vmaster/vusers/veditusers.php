<table align="center">
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<div class="tab-pane" id="tab_1">
    <div class="portlet light bg-inverse">

        <div class="portlet-body form">
            
            <!-- BEGIN FORM-->
            <?php echo form_open(base_url() . 'user/editsaveuser'); ?>
            <form class="horizontal-form">
                <div class="form-body">
                    <!--<h3 class="form-section">Person Info</h3>-->
                    <div class="row">
                        <!-- ______________________COLOUMN BEGIN_____________________________________________-->
                        <div class="col-md-12 ">
                            <!--                                <fieldset class="field">
                                                                <legend class="field">Personalia:</legend>-->

                            <div class="form-body">
                                <table>
                                    <tr>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">User ID :</label>
                                        <div class="col-md-7">
                                            <input type="text" style="height: 30px;" class="form-control input-circle" name="userid" id="userid" value="<?php echo $db->UserId; ?>" readonly="true">
                                        </div>
                                    </div>
                                    </tr><br>
                                    <tr>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Password 1 : <span class="required">*</span></label>
                                        <div class="col-md-7">
                                            <input type="password" style="height: 30px;" class="form-control input-circle" name="password" id="password" value="<?php echo $db->UserPassword; ?>">
                                        </div>
                                    </div>
                                    </tr><br>
                                    <tr>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Repeat Password 1 : <span class="required">*</span></label>
                                        <div class="col-md-7">
                                            <input type="password" style="height: 30px;" class="form-control input-circle" name="password2" id="password2" value="<?php echo $db->UserPassword; ?>">
                                        </div>
                                    </div>
                                    </tr><br>
                                    <tr>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Password 2 : <span class="required">*</span></label>
                                        <div class="col-md-7">
                                            <input type="password" style="height: 30px;" class="form-control input-circle" name="passwordbi" id="passwordbi" value="<?php echo $db->UserPassword2; ?>">
                                        </div>
                                    </div>
                                    </tr><br>
                                    <tr>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Repeat Password 2 : <span class="required">*</span></label>
                                        <div class="col-md-7">
                                            <input type="password" style="height: 30px;" class="form-control input-circle" name="passwordbi2" id="passwordbi2" value="<?php echo $db->UserPassword2; ?>">
                                        </div>
                                    </div>
                                    </tr><br>
                                    <tr>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Active&nbsp;Status&nbsp;: </label>
                                        <div class="radio-list">
                                            <?php if ($db->IsActive == 1) { ?>
                                                <div class="col-md-7">
                                                    <label class="radio-inline">
                                                        <input type="radio" name="activestatus" id="activestatus1" value="1" checked> Active </label>

                                                    <label class="radio-inline">
                                                        <input type="radio" name="activestatus" id="activestatus2" value="0"> Inactive </label>
                                                </div>
                                            <?php } else { ?>
                                                <div class="col-md-7">
                                                    <label class="radio-inline">
                                                        <input type="radio" name="activestatus" id="activestatus1" value="1" > Active </label>

                                                    <label class="radio-inline">
                                                        <input type="radio" name="activestatus" id="activestatus2" value="0" checked> Inactive </label>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- _______________________COLOUMN END____________________________________________-->
                        <!-- ________________________COLOUMN BEGIN___________________________________________-->

                        <!-- ________________________COLOUMN END___________________________________________-->
                    </div><br><br><br>
                    <table align="center">
                        <tr>
                            <td>
                                <button type="submit" class="btn green">Save</button>
                            </td>
                        </tr>
                    </table>
                    <!-- END FORM-->
                </div>
            </form>
            <?php echo form_close() ?>
        </div>
    </div>
    <!--/.col (right) -->
</div>