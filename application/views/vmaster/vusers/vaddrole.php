
<?php echo form_open(base_url() . 'role/saverole'); ?>
<form class="horizontal-form">
    <div class="portlet light bg-inverse">
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <table align="center">
                <tr>
                    <td>
                        <?php
                        if ($error == '') {

                        } else {
                            ?>
                            <div class="alert alert-danger">
                                <strong>Warning!</strong> <?php echo $error; ?>
                            </div>
                        <?php } ?>
                    </td>
                </tr>
            </table>
            <div class="form-body">
                <!--<h3 class="form-section">Person Info</h3>-->
                <div class="row">
                    <!-- ______________________COLOUMN BEGIN_____________________________________________-->
                    <div class="col-md-6 ">
                        <!--                                <fieldset class="field">
                                                            <legend class="field">Personalia:</legend>-->

                        <div class="form-body">
                            <table>
                                <tr>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Active&nbsp;Status&nbsp; </label>
                                    <div class="radio-list">
                                        <div class="col-md-7">
                                            <label class="radio-inline">
                                                <input type="radio" name="activestatus" id="activestatus1" value="1" checked> Active </label>

                                            <label class="radio-inline">
                                                <input type="radio" name="activestatus" id="activestatus2" value="0"> Inactive </label>
                                        </div>
                                    </div>
                                </div>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- _______________________COLOUMN END____________________________________________-->
                    <!-- ________________________COLOUMN BEGIN___________________________________________-->
                    <div class="col-md-6 ">
                        <!--                                <fieldset class="field">
                                                            <legend class="field">Personalia:</legend>-->

                        <div class="form-body">
                            <table>
                                <tr>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Role&nbsp;Code&nbsp;: <span class="required">*</span></label>
                                    <div class="col-md-7">
                                        <input type="text" style="height: 30px;" class="form-control input-circle" name="code" id="code"><?php echo form_error('code'); ?>
                                    </div>
                                </div>
                                </tr><br>
                                <tr>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Role&nbsp;Name&nbsp;:</label>
                                    <div class="col-md-7">
                                        <input type="text" style="height: 30px;" class="form-control input-circle" name="name" id="name"><?php echo form_error('name'); ?>
                                    </div>
                                </div>
                                </tr><br>
                                <tr>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Remarks :</label>
                                    <div class="col-md-7">
                                        <textarea class="form-control input-circle" rows="2" name="remarks" id="remarks"></textarea>
                                    </div>
                                </div>

                                </tr>
                            </table>
                        </div>
                        <!--</fieldset>-->
                    </div>
                    <!-- ________________________COLOUMN END___________________________________________-->

                    <!-- END FORM-->
                </div>

            </div>
        </div>
        <!--/.col (right) -->
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">



                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_6">
                        <thead>
                            <tr>
                                <th>Form&nbsp;ID</th>
                                <th>Create</th>
                                <th>View</th>
                                <th>Update</th>
                                <th>Delete</th>
                                <th>Approve</th>
                                <th>Print</th>
                                <th>Preview</th>
                                <th>Addon</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($db->result() as $baris): ?>
                                <tr align="center">
                                    <td><h6><?php echo $baris->FormId; ?></h6></td>
                                    <td><input class="checkbox-inline" type="checkbox" id="<?php echo $baris->FormId; ?>" name="<?php echo $baris->FormId; ?>[]" value="create"></td>
                                    <td><input class="checkbox-inline" type="checkbox" id="<?php echo $baris->FormId; ?>" name="<?php echo $baris->FormId; ?>[]" value="view"></td>
                                    <td><input class="checkbox-inline" type="checkbox" id="<?php echo $baris->FormId; ?>" name="<?php echo $baris->FormId; ?>[]" value="update"></td>
                                    <td><input class="checkbox-inline" type="checkbox" id="<?php echo $baris->FormId; ?>" name="<?php echo $baris->FormId; ?>[]" value="delete"></td>
                                    <td><input class="checkbox-inline" type="checkbox" id="<?php echo $baris->FormId; ?>" name="<?php echo $baris->FormId; ?>[]" value="approve"></td>
                                    <td><input class="checkbox-inline" type="checkbox" id="<?php echo $baris->FormId; ?>" name="<?php echo $baris->FormId; ?>[]" value="print"></td>
                                    <td><input class="checkbox-inline" type="checkbox" id="<?php echo $baris->FormId; ?>" name="<?php echo $baris->FormId; ?>[]" value="preview"></td>
                                    <td><input class="checkbox-inline" type="checkbox" id="<?php echo $baris->FormId; ?>" name="<?php echo $baris->FormId; ?>[]" value="addon"></td>


                            <input type="hidden" id="<?php echo $baris->FormId; ?>" name="<?php echo $baris->FormId; ?>[]" value="">



                            </tr>
                            <?php
                            $no++;
                        endforeach;
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <table align="center">
                <tr><td><button type="submit" class="btn btn green">Input</button></td></tr>
            </table>


            <!-- END EXAMPLE TABLE PORTLET-->

        </div>
    </div>
</form>
<?php echo form_close() ?>