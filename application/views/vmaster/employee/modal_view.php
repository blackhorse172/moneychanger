		<div class="modal fade" id="addEmployeeModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Employee</h4>
					</div>
					<br/>
					<?php $this->load->view('errors/alert'); ?>
					<div class="modal-body">
						<form id="employeeForm" role="form">
							<input type="hidden" name="Iid" id="Iid" value="">
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Nik</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" onchange="checkEmployeeCode()" id="Nik" name="Nik">
								</div>
							</div>

							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Nama</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Employee_Name" name="Employee_Name">
								</div>
							</div>

								<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Alamat</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Employee_Address" name="Employee_Address">
								</div>
							</div>

								<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Telepon</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="Employee_Phone" name="Employee_Phone">
								</div>
							</div>

								<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Gaji</label>
								<div class="col-sm-10">
									<input type="text" class="auto form-control" id="Employee_Salary" name="Employee_Salary">
								</div>
							</div>

								<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Status</label>
								<div class="col-sm-10">
										<select name="Employee_Status" id="Employee_Status">
											<option value=""></option>
											<option value="Belum Kawin">Belum Kawin</option>
											<option value="Kawin">Kawin</option>
										</select>
								</div>
							</div>
					
						<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Catatan</label>
								<div class="col-sm-10">
										<textarea name="Remarks" id="Remarks" class="form-control"></textarea>
								</div>
							</div>
						

						
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" onclick="clearEmployeeForm()" data-dismiss="modal">Batal</button>
						<button type="button" class="btn btn-primary" onclick="storeEmployee()">Simpan</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

<style type="text/css" media="screen">
.fullWidth {
	width: 75%;
}	
</style>