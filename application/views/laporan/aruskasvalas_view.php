
<link rel="stylesheet" href="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/styles/jqx.base.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/scripts/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxdata.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxmenu.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.filter.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.sort.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.edit.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.selection.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxlistbox.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxdropdownlist.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxcheckbox.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxcalendar.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxnumberinput.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxdatetimeinput.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/globalization/globalize.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/scripts/demos.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/demos/sampledata/generatedata.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/globalization/globalize.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/demos/jqxgrid/localization.js"></script>
<style type="text/css">
    .redClass
    {
        background-color: #FF0000;
    }
    .greenClass
    {
        background-color: #228B22;
    }
    .blueClass
    {
        background-color: #87CEEB;
    }
    .orangeClass
    {
        background-color: #FFA500;
    }
    .yellowClass
    {
        background-color: #FFFF00;
    }
    .whiteClass
    {
        background-color: White;
    }
</style>
<script type="text/javascript">
    var jQuery_1_4_3 = $.noConflict(true);
    jQuery_1_4_3(document).ready(function () {
        var d = new Date();
        var monthget = d.getMonth() + 1;
        var dateget = d.getDate() + '-' + monthget + '-' + d.getFullYear();
        // prepare the data
//        var url = "http://localhost/smartdeal/akunting/get_data";
        var url = "<?php echo site_url('aruskas/get_data/') ?>" + dateget + '/' + dateget;
        //var url = "<?php echo site_url('aruskas/get_data_idr/') ?>" + '2017-03-24' + '/' + '2018-01-01';
        var source =
                {
                    datatype: "json",
                    updaterow: function (rowid, rowdata, commit) {
                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                    datafields:
                            [
                                {name: 'Iid', type: 'string'},
                                {name: 'RowIndex', type: 'number'},
                                {name: 'Debit', type: 'number'},
                                {name: 'Kredit', type: 'number'},
                                {name: 'Kode', type: 'string'},
                                {name: 'Nama', type: 'string'},
                                {name: 'TransDate', type: 'date'},
                                {name: 'Quantity', type: 'number'},
                                {name: 'SaldoAwal', type: 'number'},
                                {name: 'RollBalance', type: 'number'}

                                                            ],
                    id: 'Iid',
                    url: url,
                    root: 'data'
                };
       
        var redRow;
        var greenRow;
        var blueRow;
        var saldoakhir = 0;
        var cellclassname = function (row, column, value, data) {
           
            data.RollBalance = data.SaldoAwal + saldoakhir + data.Quantity;
            saldoakhir = saldoakhir + (data.Quantity*1/2);

            //data.RollBalance = saldoakhir + 1;
            //saldoakhir = saldoakhir + (1/2);
        };

         var cellsrenderer_kredit = function (row, column, value, defaultHtml) {
            if (value < 0) {
                return '';
            } 
        };
        var cellsrenderer_debit = function (row, column, value, defaultHtml) {
            if (value > 0) {
                return '';
            } 
        };
        var dataAdapter = new jQuery_1_4_3.jqx.dataAdapter(source);
        // initialize jqxGrid
        jQuery_1_4_3("#jqxgrid").jqxGrid(
                {
                    width: 1000,
                    height: 450,
                    source: dataAdapter,
                    editable: false,
                    showfilterrow: true,
                    filterable: true,
                    selectionmode: 'singlecell',
//                    editmode: 'selectedrow',
//                    autowidth: true,
//                    autoheight: true,
                    sortable: true,
                    columns: [
                        {text: 'Iid', columntype: 'textbox', datafield: 'Iid', width: 70,hidden: true},
                        {text: 'Kode', columntype: 'textbox', datafield: 'Kode', width: 140, pinned: true},
                        {text: 'Nama', columntype: 'textbox', datafield: 'Nama', width: 220, pinned: true},
                        {
                            text: 'Tanggal Transaksi', datafield: 'TransDate',columntype: 'datetimeinput', width: 170, align: 'right', cellsalign: 'right', cellsformat: 'd/M/yyyy',
                            validation: function (cell, value) {
                                if (value == "")
                                    return true;
                                var year = value.getFullYear();
                                if (year <= 2015) {
                                    return {result: false, message: "Ship Date should be before 1/1/2015"};
                                }
                                return true;
                            }
                        },
                        {
                            text: 'Debit', datafield: 'Debit',cellsrenderer:cellsrenderer_debit,cellclassname: cellclassname, width: 150, align: 'right', cellsformat: "f0", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 0, digits: 12});
                            }
                        },
                        {
                            text: 'Kredit', datafield: 'Kredit',cellsrenderer:cellsrenderer_kredit, width: 150, align: 'right', cellsformat: "f0", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 0, digits: 12});
                            }
                        },
                        {
                            text: 'Saldo Akhir', datafield: 'RollBalance',cellclassname:cellclassname, width: 170, align: 'right', cellsformat: "f0", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 0, digits: 12});
                            }
                        }
                    ]
                });
        // events
        var rowValues = "";
        jQuery_1_4_3("#jqxgrid").on('cellbeginedit', function (event) {
            var args = event.args;
            if (args.datafield === "firstname") {
                rowValues = "";
            }
            rowValues += args.value.toString() + "    ";
            if (args.datafield === "price") {
                $("#cellbegineditevent").text("Begin Row Edit: " + (1 + args.rowindex) + ", Data: " + rowValues);
            }
        });
        jQuery_1_4_3("#jqxgrid").on('cellendedit', function (event) {
            var args = event.args;
            if (args.datafield === "firstname") {
                rowValues = "";
            }
            rowValues += args.value.toString() + "    ";
            if (args.datafield === "price") {
                jQuery_1_4_3("#cellbegineditevent").text("End Row Edit: " + (1 + args.rowindex) + ", Data: " + rowValues);
            }
        });
        jQuery_1_4_3("#jqxgrid").unbind('cellendedit').on('cellendedit', function (event) {
            var field = jQuery_1_4_3("#jqxgrid").jqxGrid('getcolumn', event.args.datafield).datafield;
            var rowBoundIndex = event.args.rowindex;
            var rowData = jQuery_1_4_3('#jqxgrid').jqxGrid('getrowdata', rowBoundIndex);
            var value_iid = rowData['Iid'];
            var value_updated = args.value;

            if (field == 'TanggalTransaksi') {
                value_updated = convert_date(value_updated);
            }
//            alert(value_iid + value_updated);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>akunting/update_data",
                cache: false,
                data: {iid: value_iid, field: field, updated_param: value_updated}, // since, you need to delete post of particular id
                success: function (reaksi) {
//                    alert(reaksi);
                    if (reaksi == 1) {
//                    alert("Success");

                    } else if (reaksi == -9) {
                        alert("User anda tidak ada otoritas merubah data");
                    }
                }
            });
        });
    }

    );
    function delete_data_row(value_iid) {
//        alert(value_iid);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>akunting/delete_data_row",
            cache: false,
            data: {iid: value_iid}, // since, you need to delete post of particular id
            success: function (reaksi) {
//                    alert(reaksi);
                if (reaksi == 1) {
//                    alert("Success");

                } else if (reaksi == -9) {
                    alert("User anda tidak ada otoritas menghapus data! data akan kembali ketika refresh page");
                }
            }
        });
    }
    function convert_date(str) {
        var date = new Date(str),
                mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }
    function get_newgrid() {
        var fromsortdate = document.getElementById('fromsortdate').value;
        var tosortdate = document.getElementById('tosortdate').value;
//        alert(tosortdate);
        var tmpS = jQuery_1_4_3("#jqxgrid").jqxGrid('source');
        tmpS._source.url = "<?php echo site_url('aruskas/get_data/') ?>" + fromsortdate + '/' + tosortdate;
        jQuery_1_4_3("#jqxgrid").jqxGrid('source', tmpS);
    }
</script>
<h3 class="page-title">
    <?php echo $pageform ?></h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="index.html">Laporan</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Arus Kas Valas</a>
        </li>
    </ul>

</div>
<table>
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<table>
    <tr>
        <td>
            <label class="control-label col-md-5">Periode&nbsp;Data&nbsp;
            </label>
        </td>
        <td>
            <form action="<?php echo base_url() . 'indexsort/poproduct' ?>" method='post' name="formsortindex" id="formsortindex">
                <div class="input-group input-large date-picker input-daterange" data-date="" data-date-format="dd-mm-yyyy">
                    <input type="text" class="form-control" name="fromsortdate" id="fromsortdate" value="<?php echo date('d-m-Y', strtotime(date('d-m-Y'))) ?>" autocomplete="on">
                    <span class="input-group-addon">
                        to </span>
                    <input type="text" class="form-control" name="tosortdate" id="tosortdate" value="<?php echo date('d-m-Y', strtotime(date('d-m-Y'))) ?>" autocomplete="on">
                </div>
            </form>
        </td>
        <td>
            <button type="submit" class="btn green" style="margin: -16px 0 0 1px;height: 32px;" onclick="get_newgrid()">Apply</button>
        </td>
    </tr>
</table>
<div class="row">
    <div class="col-md-12">
        <div id='jqxWidget'>
            <div id="jqxgrid"></div>
            <div style="font-size: 12px; font-family: Verdana, Geneva, 'DejaVu Sans', sans-serif; margin-top: 30px;">
                <div id="cellbegineditevent"></div>
                <div style="margin-top: 10px;" id="cellendeditevent"></div>
            </div>
        </div>

    </div>
</div>
<div class="clearfix">
</div>