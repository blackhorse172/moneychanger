
<link rel="stylesheet" href="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/styles/jqx.base.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/scripts/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxdata.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxmenu.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.filter.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.sort.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.edit.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.selection.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxlistbox.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxdropdownlist.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxcheckbox.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxcalendar.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxnumberinput.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxdatetimeinput.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/globalization/globalize.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/scripts/demos.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/demos/sampledata/generatedata.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/globalization/globalize.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/demos/jqxgrid/localization.js"></script>
<style type="text/css">
    .redClass
    {
        background-color: #FF0000;
    }
    .greenClass
    {
        background-color: #228B22;
    }
    .blueClass
    {
        background-color: #87CEEB;
    }
    .orangeClass
    {
        background-color: #FFA500;
    }
    .yellowClass
    {
        background-color: #FFFF00;
    }
    .whiteClass
    {
        background-color: White;
    }
    .columnbuy {
            background-color: #FC6B4B;
            color: black;
        }
    .columnsell {
            background-color: #4BC6FC;
            color: black;
    }
    .rowTotal {
            font-weight: bold;
            font-style: italic;
            font-family: arial;
            font-size: 15px;
            color: black;
    }
</style>
<script type="text/javascript">
    var jQuery_1_4_3 = $.noConflict(true);
    jQuery_1_4_3(document).ready(function () {
        var d = new Date();
        var monthget = d.getMonth() + 1;
        var dateget = d.getDate() + '-' + monthget + '-' + d.getFullYear();
        // prepare the data
//        var url = "http://localhost/smartdeal/akunting/get_data";
        var url = "<?php echo site_url('laporan/get_data_rekapharian/'.$dategetelement .'/'.$dategetelementto) ?>";
        var source =
                {
                    datatype: "json",
                    updaterow: function (rowid, rowdata, commit) {
                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                    datafields:
                            [
                                {name: 'CURR_CODE', type: 'string'},
                                {name: 'QuantityBuy', type: 'number'},
                                {name: 'EstRateBuy', type: 'number'},
                                {name: 'JumlahBuyIDR', type: 'number'},
                                {name: 'QuantitySell', type: 'number'},
                                {name: 'EstRateSell', type: 'number'},
                                {name: 'JumlahSellIDR', type: 'number'}
                            ],
                    id: 'Iid',
                    url: url,
                    root: 'data'
                };
        var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
            if (value == 'TOTAL') { 
                return ".whiteClass";
            } 
        };
//        var cellsrenderer_liratusjuta = function (row, column, value, defaultHtml) {
//            if (value > '499999999') {
//                var element = jQuery_1_4_3(defaultHtml);
//                element.css({'background-color': '#FF0000'});
//                return element[0].outerHTML;
//                return defaultHtml;
//            } else {
//                var element = jQuery_1_4_3(defaultHtml);
//                element.css({'background-color': '#000000'});
//                return element[0].outerHTML;
//                return defaultHtml;
//            }
//        };
        var redRow;
        var greenRow;
        var blueRow;
        var cellclassname = function (row, column, value, data) {
            if (data.CURR_CODE == 'TOTAL') {
                return "rowTotal";
            } 

        };
        var dataAdapter = new jQuery_1_4_3.jqx.dataAdapter(source);
        // initialize jqxGrid
        jQuery_1_4_3("#jqxgrid").jqxGrid(
                {
                    width: 1000,
                    height: 450,
                    source: dataAdapter,
                    editable: false,
                    showfilterrow: true,
                    filterable: true,
                    selectionmode: 'singlecell',
//                    editmode: 'selectedrow',
//                    autowidth: true,
//                    autoheight: true,
                    sortable: true,
                    columns: [
//                         {text: '', datafield: '', columntype: 'button', pinned: true, filterable: false, cellsrenderer: function () {
// //                                return '<img src="<?php echo base_url(); ?>assets/images/delete_icon.png"/>';
//                                 return 'X';
//                             }, buttonclick: function (row) {
//                                 var r = confirm("Anda yakin menghapus data?");
//                                 if (r == true) {
//                                     var rowBoundIndex = row;
//                                     var rowData = jQuery_1_4_3('#jqxgrid').jqxGrid('getrowdata', rowBoundIndex);
//                                     var value_iid = rowData['Iid'];
//                                     jQuery_1_4_3("#jqxgrid").jqxGrid('deleterow', value_iid); // id harus di set Iid di parsing json diatas

//                                     delete_data_row(value_iid);
//                                 } else {
//                                     return false;
//                                 }

//                             }
//                         },
                        {text: 'Valas', pinned:true, cellclassname: cellclassname, columntype: 'textbox', datafield: 'CURR_CODE', width: 80},
                        {
                            text: 'QTY Buy', datafield: 'QuantityBuy',cellclassname: 'columnbuy', width: 150, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'Est. Rate', cellclassname: cellclassname, datafield: 'EstRateBuy',cellclassname: 'columnbuy', width: 110, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'Jumlah IDR', cellclassname: cellclassname, datafield: 'JumlahBuyIDR',cellclassname: 'columnbuy', width: 200, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'QTY SELL', cellclassname: cellclassname, datafield: 'QuantitySell',cellclassname: 'columnsell', width: 150, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'Est. Rate', cellclassname: cellclassname, datafield: 'EstRateSell',cellclassname: 'columnsell', width: 110, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'Jumlah IDR', cellclassname: cellclassname, datafield: 'JumlahSellIDR',cellclassname: 'columnsell', width: 200, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        }
                    ]
                });
        // events
        var rowValues = "";
        jQuery_1_4_3("#jqxgrid").on('cellbeginedit', function (event) {
            var args = event.args;
            if (args.datafield === "firstname") {
                rowValues = "";
            }
            rowValues += args.value.toString() + "    ";
            if (args.datafield === "price") {
                $("#cellbegineditevent").text("Begin Row Edit: " + (1 + args.rowindex) + ", Data: " + rowValues);
            }
        });
        jQuery_1_4_3("#jqxgrid").on('cellendedit', function (event) {
            var args = event.args;
            if (args.datafield === "firstname") {
                rowValues = "";
            }
            rowValues += args.value.toString() + "    ";
            if (args.datafield === "price") {
                jQuery_1_4_3("#cellbegineditevent").text("End Row Edit: " + (1 + args.rowindex) + ", Data: " + rowValues);
            }
        });
        jQuery_1_4_3("#jqxgrid").unbind('cellendedit').on('cellendedit', function (event) {
            var field = jQuery_1_4_3("#jqxgrid").jqxGrid('getcolumn', event.args.datafield).datafield;
            var rowBoundIndex = event.args.rowindex;
            var rowData = jQuery_1_4_3('#jqxgrid').jqxGrid('getrowdata', rowBoundIndex);
            var value_iid = rowData['Iid'];
            var value_updated = args.value;

            if (field == 'TanggalTransaksi') {
                value_updated = convert_date(value_updated);
            }
//            alert(value_iid + value_updated);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>akunting/update_data",
                cache: false,
                data: {iid: value_iid, field: field, updated_param: value_updated}, // since, you need to delete post of particular id
                success: function (reaksi) {
//                    alert(reaksi);
                    if (reaksi == 1) {
//                    alert("Success");

                    } else if (reaksi == -9) {
                        alert("User anda tidak ada otoritas merubah data");
                    }
                }
            });
        });
    }

    );
    function delete_data_row(value_iid) {
//        alert(value_iid);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>akunting/delete_data_row",
            cache: false,
            data: {iid: value_iid}, // since, you need to delete post of particular id
            success: function (reaksi) {
//                    alert(reaksi);
                if (reaksi == 1) {
//                    alert("Success");

                } else if (reaksi == -9) {
                    alert("User anda tidak ada otoritas menghapus data! data akan kembali ketika refresh page");
                }
            }
        });
    }
    function convert_date(str) {
        var date = new Date(str),
                mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }
    function get_newgrid() {
        var fromsortdate = document.getElementById('fromsortdate').value;
        var tosortdate = document.getElementById('tosortdate').value;
//        alert(tosortdate);
        var tmpS = jQuery_1_4_3("#jqxgrid").jqxGrid('source');
        tmpS._source.url = "<?php echo site_url('laporan/get_data_rekapharian/') ?>" + fromsortdate + '/' + tosortdate;
        jQuery_1_4_3("#jqxgrid").jqxGrid('source', tmpS);
    }
</script>
<h3 class="page-title">
    <?php echo $pageform ?></h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="index.html">Laporan</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Transaksi</a>
        </li>
    </ul>

</div>
<table>
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<form action="<?php echo base_url() . 'laporan/rekapharian' ?>" method='post' name="formsortindex" id="formsortindex">
<table>
    <tr>
        <td>
            <label class="control-label col-md-5">Tanggal&nbsp;
            </label>
        </td>
        <td>
            <div class="input-group input-large date-picker input-daterange" data-date="" data-date-format="dd-mm-yyyy">
                    <input type="text" class="form-control" name="fromsortdate" id="fromsortdate" value="<?php echo date('d-m-Y', strtotime($dategetelement)) ?>" autocomplete="on">
                    <span class="input-group-addon">
                        to </span>
                    <input type="text" class="form-control" name="tosortdate" id="tosortdate" value="<?php echo date('d-m-Y', strtotime($dategetelementto)) ?>" autocomplete="on">
                </div>        
        </td>
        <td>
            <button type="submit" class="btn green" style="margin: 0px 0 0 1px;height: 32px;" onclick="">Apply</button>
        </td>
       
        <td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
        <?php if($saldoawalrupiah<0){ ?>
            <label class="caption-subject font-black-steel bold">Saldo Awal Rupiah : &nbsp;</label>
            <label class="caption-subject bold" style="color: #f3565d;">Rp. (<?php echo number_format(abs($saldoawalrupiah), 2, ".", ",") ?>)</label>
        <?php } else { ?>
            <label class="caption-subject font-black-steel bold">Saldo Awal Rupiah : &nbsp; Rp. <?php echo number_format($saldoawalrupiah, 2, ".", ",") ?></label>
        <?php } ?>
        
        </td>
    </tr>
</table>
</form>
<br>
<div class="row">
    <div class="col-md-12">
        <div id='jqxWidget'>
            <div id="jqxgrid"></div>
            <div style="font-size: 12px; font-family: Verdana, Geneva, 'DejaVu Sans', sans-serif; margin-top: 30px;">
                <div id="cellbegineditevent"></div>
                <div style="margin-top: 10px;" id="cellendeditevent"></div>
            </div>
        </div>
<?php 
$dcs = "";
$dcs = $saldoawalrupiah + $rekapjual + $rekapbeli - $totalbiaya;
 ?>
<table align="right">
    <tr>
        <td>
            <label class="caption-subject font-black-steel bold">Saldo Awal Rupiah : &nbsp;</label>
        </td>
        <td align="right">
        <?php if($saldoawalrupiah<0){ ?>
            <label class="caption-subject bold" style="color: #f3565d;">Rp. (<?php echo number_format(abs($saldoawalrupiah), 2, ".", ",") ?>)</label>
        <?php } else { ?>
            <label class="caption-subject font-black-steel bold">Rp. <?php echo number_format($saldoawalrupiah, 2, ".", ",") ?></label>
        <?php } ?>
            
        </td>
    </tr>
    <tr>
        <td>
            <label class="caption-subject font-black-steel bold">Total Rekap Beli : &nbsp;</label>
        </td>
        <td align="right">
        <?php if($rekapbeli<0){ ?>
            <label class="caption-subject bold" style="color: #f3565d;">Rp. (<?php echo number_format(abs($rekapbeli), 2, ".", ",") ?>)</label>
        <?php } else { ?>
            <label class="caption-subject font-black-steel bold">Rp. <?php echo number_format($rekapbeli, 2, ".", ",") ?></label>
        <?php } ?>
            
        </td>
    </tr>
    <tr>
        <td>
            <label class="caption-subject font-black-steel bold">Total Rekap Jual : &nbsp;</label>
        </td>
        <td align="right">
        <?php if($rekapjual<0){ ?>
            <label class="caption-subject bold" style="color: #f3565d;">Rp. (<?php echo number_format(abs($rekapjual), 2, ".", ",") ?>)</label>
        <?php } else { ?>
            <label class="caption-subject font-black-steel bold">Rp. <?php echo number_format($rekapjual, 2, ".", ",") ?></label>
        <?php } ?>
            
        </td>
    </tr>

    <tr>
        <td>
            <label class="caption-subject font-black-steel bold"><a href="<?php echo base_url(); ?>cost">Total Biaya : &nbsp;</a></label>
        </td>
        <td align="right">
            <label class="caption-subject bold"><a href="<?php echo base_url(); ?>cost" style="color: #f3565d;">Rp. (<?php echo number_format($totalbiaya, 2, ".", ",") ?>) </a></label>
        </td>
    </tr>
    <tr>
        <td>
            <label class="caption-subject font-black-steel bold" style="display: inline-block;width: 200px;padding: 10px;border-bottom: 1px solid #ccc;"></label>
        </td>
        <td>
            <label class="caption-subject font-black-steel bold" style="display: inline-block;width: 200px;padding: 10px;border-bottom: 1px solid #ccc;"></label>
        </td>
    </tr>
    <tr>
        <td>
            <label class="caption-subject font-black-steel bold">DCS : &nbsp;</label>
        </td>
        <td align="right">
        <?php if($dcs<0){ ?>
            <label class="caption-subject bold" style="color: #f3565d;">Rp. (<?php echo number_format(abs($dcs), 2, ".", ",") ?>)</label>
        <?php } else { ?>
            <label class="caption-subject font-black-steel bold">Rp. <?php echo number_format($dcs, 2, ".", ",") ?></label>
        <?php } ?>
            
        </td>
    </tr>
</table>
    </div>

</div>
<div class="clearfix">
</div>

