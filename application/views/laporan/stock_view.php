<?php $this->load->view('jqwidgetslink'); ?>
<style type="text/css">
    .redClass
    {
        background-color: #FF0000;
    }
    .greenClass
    {
        background-color: #228B22;
    }
    .blueClass
    {
        background-color: #87CEEB;
    }
    .orangeClass
    {
        background-color: #FFA500;
    }
    .yellowClass
    {
        background-color: #FFFF00;
    }
    .whiteClass
    {
        background-color: White;
    }
</style>
<script type="text/javascript">
    var jQuery_1_4_3 = $.noConflict(true);
    jQuery_1_4_3(document).ready(function () {
        // prepare the data
//        var url = "http://localhost/smartdeal/akunting/get_data";
        var url = "<?php echo site_url('laporan/get_data_stock') ?>";
        var url_awal = "<?php echo site_url('laporan/get_data_stock_awal') ?>";
        var url_profit = "<?php echo site_url('laporan/get_data_gross_profit') ?>";
        var source =
                {
                    datatype: "json",
                    addRow: function (rowID, rowData, position, commit) {
                        // synchronize with the server - send insert command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                        commit(true);
                    },
                    updaterow: function (rowid, rowdata, commit) {
                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                    deleteRow: function (rowID, commit) {
                        // synchronize with the server - send delete command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        commit(true);
                    },
                    datafields:
                            [
                                {name: 'CURR_CODE', type: 'string'},
                                {name: 'CURR_NAME', type: 'string'},
                                {name: 'Quantity', type: 'number'},
                                {name: 'KursTengah', type: 'number'},
                                {name: 'JumlahIDR', type: 'number'}
                            ],
                    id: 'CURR_CODE',
                    url: url,
                    root: 'data'
                };
        var source_awal =
                {
                    datatype: "json",
                    addRow: function (rowID, rowData, position, commit) {
                        // synchronize with the server - send insert command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                        commit(true);
                    },
                    updaterow: function (rowid, rowdata, commit) {
                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                    deleteRow: function (rowID, commit) {
                        // synchronize with the server - send delete command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        commit(true);
                    },
                    datafields:
                            [
                                {name: 'CURR_CODE', type: 'string'},
                                {name: 'CURR_NAME', type: 'string'},
                                {name: 'Quantity', type: 'number'},
                                {name: 'KursTengah', type: 'number'},
                                {name: 'JumlahIDR', type: 'number'}
                            ],
                    id: 'CURR_CODE',
                    url: url_awal,
                    root: 'data'
                };
            var source_profit =
                {
                    datatype: "json",
                    addRow: function (rowID, rowData, position, commit) {
                        // synchronize with the server - send insert command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                        commit(true);
                    },
                    updaterow: function (rowid, rowdata, commit) {
                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                    deleteRow: function (rowID, commit) {
                        // synchronize with the server - send delete command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        commit(true);
                    },
                    datafields:
                            [
                                {name: 'Branch', type: 'string'},
                                {name: 'Tanggal', type: 'date'},
                                {name: 'RekapIDR', type: 'number'},
                                {name: 'StockValasIDR', type: 'number'},
                                {name: 'Total', type: 'number'},
                                {name: 'GrossProfit', type: 'number'}
                            ],
                    id: 'CURR_CODE',
                    url: url_profit,
                    root: 'data'
                };
        var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
            if (value < 0) {
                return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #800000;">' + value + '</span>';
            } else {
                return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #000000;">' + value + '</span>';
            }
        };
        var profit_yesterday = 0;
        var cellsrenderer_profit = function (row, column, value, defaultHtml) {
            value = profit_yesterday;
            profit_yesterday = 1;
        };
        var dataAdapter = new jQuery_1_4_3.jqx.dataAdapter(source);
        var dataAdapter_awal = new jQuery_1_4_3.jqx.dataAdapter(source_awal);
        var dataAdapter_profit = new jQuery_1_4_3.jqx.dataAdapter(source_profit);
        // initialize jqxGrid
        jQuery_1_4_3("#jqxgridawal").jqxGrid(
                {
                    width: 1000,
                    height: 300,
                    source: dataAdapter_awal,
                    editable: false,
                    showfilterrow: true,
                    filterable: true,
                    selectionmode: 'singlecell',
//                    editmode: 'selectedrow',
//                    autowidth: true,
//                    autoheight: true,
                    sortable: true,
                    showtoolbar: true,
                     renderToolbar: function (toolBar)
                        {
                        var toTheme = function (className) {
                            if (theme == "")
                                return className;
                            return className + " " + className + "-" + theme;
                        }

                        var container = jQuery_1_4_3("<div style='overflow: hidden; position: relative; height: 100%; width: 100%;'></div>");
                        var buttonTemplate = "<div style='float: left; padding: 3px; margin: 2px;'><div style='margin: 4px; width: 16px; height: 16px;'></div></div>";
                        var addButton = jQuery_1_4_3(buttonTemplate);

                        container.append(addButton);

                        toolBar.append(container);

                        addButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        addButton.find('div:first').addClass(toTheme('icon-reload'));
                        addButton.jqxTooltip({position: 'bottom', content: "Add"});
                        
                        addButton.click(function (event) {
                            //alert('a');
                            // jQuery_1_4_3('#jqxgridawal').jqxGrid('updatebounddata');
                            location.reload();
                        });

                    },
                    columns: [
                        {text: 'Mata Uang', columntype: 'textbox', datafield: 'CURR_CODE', width: 180},
                        {text: 'Nama Mata Uang', columntype: 'textbox', datafield: 'CURR_NAME', width: 270},
                        {
                            text: 'Jumlah', datafield: 'Quantity', width: 170, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'Kurs Tengah', datafield: 'KursTengah', width: 170, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'Jumlah IDR', datafield: 'JumlahIDR', width: 210, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        }
                    ]
                });
        // events
        jQuery_1_4_3("#jqxgrid").jqxGrid(
                {
                    width: 1000,
                    height: 300,
                    source: dataAdapter,
                    editable: false,
                    showfilterrow: true,
                    filterable: true,
                    selectionmode: 'singlecell',
//                    editmode: 'selectedrow',
//                    autowidth: true,
//                    autoheight: true,
                    sortable: true,
                    showtoolbar: true,
                     renderToolbar: function (toolBar)
                        {
                        var toTheme = function (className) {
                            if (theme == "")
                                return className;
                            return className + " " + className + "-" + theme;
                        }

                        var container = jQuery_1_4_3("<div style='overflow: hidden; position: relative; height: 100%; width: 100%;'></div>");
                        var buttonTemplate = "<div style='float: left; padding: 3px; margin: 2px;'><div style='margin: 4px; width: 16px; height: 16px;'></div></div>";
                        var addButton = jQuery_1_4_3(buttonTemplate);

                        container.append(addButton);

                        toolBar.append(container);

                        addButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        addButton.find('div:first').addClass(toTheme('icon-reload'));
                        addButton.jqxTooltip({position: 'bottom', content: "Add"});
                        
                        addButton.click(function (event) {
                            //alert('a');
                            // jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                            location.reload();
                        });

                    },
                    columns: [
                        {text: 'Mata Uang', columntype: 'textbox', datafield: 'CURR_CODE', width: 180},
                        {text: 'Nama Mata Uang', columntype: 'textbox', datafield: 'CURR_NAME', width: 270},
                        {
                            text: 'Jumlah', datafield: 'Quantity', width: 170, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'Kurs Tengah', datafield: 'KursTengah', width: 170, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'Jumlah IDR', datafield: 'JumlahIDR', width: 210, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        }
                    ]
                });
        // events
        jQuery_1_4_3("#jqxgridgrossprofit").jqxGrid(
                {
                    width: 1000,
                    height: 300,
                    source: dataAdapter_profit,
                    editable: false,
                    showfilterrow: true,
                    filterable: true,
                    selectionmode: 'singlecell',
//                    editmode: 'selectedrow',
//                    autowidth: true,
//                    autoheight: true,
                    sortable: true,
                    showtoolbar: true,
                     renderToolbar: function (toolBar)
                        {
                        var toTheme = function (className) {
                            if (theme == "")
                                return className;
                            return className + " " + className + "-" + theme;
                        }

                        var container = jQuery_1_4_3("<div style='overflow: hidden; position: relative; height: 100%; width: 100%;'></div>");
                        var buttonTemplate = "<div style='float: left; padding: 3px; margin: 2px;'><div style='margin: 4px; width: 16px; height: 16px;'></div></div>";
                        var addButton = jQuery_1_4_3(buttonTemplate);

                        container.append(addButton);

                        toolBar.append(container);

                        addButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        addButton.find('div:first').addClass(toTheme('icon-reload'));
                        addButton.jqxTooltip({position: 'bottom', content: "Add"});
                        
                        addButton.click(function (event) {
                            //alert('a');
                            // jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                            location.reload();
                        });

                    },
                    columns: [
                        {text: 'Kantor', columntype: 'text', datafield: 'Branch', width: 180},
                        {text: 'Tanggal', columntype: 'dateinput', datafield: 'Tanggal', cellsformat: 'd/M/yyyy', width: 100},
                        {
                            text: 'Jumlah IDR', datafield: 'RekapIDR', width: 170, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'Stock Valas IDR', datafield: 'StockValasIDR', width: 170, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'Total', datafield: 'Total', width: 210, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'Profit Harian',cellsrenderer:cellsrenderer_profit, datafield: 'GrossProfit', width: 170, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        }
                    ]
                });
        // events

    }

    );
    function delete_data_row(value_iid) {
//        alert(value_iid);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>akunting/delete_data_row",
            cache: false,
            data: {iid: value_iid}, // since, you need to delete post of particular id
            success: function (reaksi) {
//                    alert(reaksi);
                if (reaksi == 1) {
//                    alert("Success");

                } else if (reaksi == -9) {
                    alert("User anda tidak ada otoritas menghapus data! data akan kembali ketika refresh page");
                }
            }
        });
    }
    function convert_date(str) {
        var date = new Date(str),
                mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }
    function get_newgrid() {
        var fromsortdate = document.getElementById('fromsortdate').value;
        var tosortdate = document.getElementById('tosortdate').value;
//        alert(tosortdate);
        var tmpS = jQuery_1_4_3("#jqxgrid").jqxGrid('source');
        tmpS._source.url = "<?php echo site_url('laporan/get_data/') ?>" + fromsortdate + '/' + tosortdate+ '/transaksi';
        jQuery_1_4_3("#jqxgrid").jqxGrid('source', tmpS);
    }
</script>
<h3 class="page-title">
    <?php echo $pageform ?></h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="index.html">Stok</a>
        </li>
    </ul>

</div>
<table>
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<div class="row">
    <div class="col-md-12">
        <div id='jqxWidget'>
         <label class="caption-subject font-black-steel bold uppercase">Nilai Barang Tersedia Dijual : </label>
         <label class="caption-subject font-black-steel bold uppercase">&nbsp; Rp. <?php echo number_format($totalstokakhir, 2, ".", ",") ?></label>
            <div id="jqxgrid"></div>
            <!-- <div style="font-size: 12px; font-family: Verdana, Geneva, 'DejaVu Sans', sans-serif; margin-top: 30px;"> -->
                <!-- <div id="cellbegineditevent"></div> -->
                <!-- <div style="margin-top: 10px;" id="cellendeditevent"></div> -->
            <!-- </div> -->
        
        <BR>
        <label class="caption-subject font-black-steel bold uppercase">Stok Awal : </label>
        <label class="caption-subject font-black-steel bold uppercase">&nbsp; Rp. <?php echo number_format($totalstokawal, 2, ".", ",") ?></label>
            <div id="jqxgridawal"></div>
           <!--  <div style="font-size: 12px; font-family: Verdana, Geneva, 'DejaVu Sans', sans-serif; margin-top: 30px;">
                <div id="cellbegineditevent"></div>
                <div style="margin-top: 10px;" id="cellendeditevent"></div>
            </div> -->
        
        <BR>
        <label class="caption-subject font-black-steel bold uppercase">Estimate Gross Profit </label>
            <div id="jqxgridgrossprofit"></div>
           <!--  <div style="font-size: 12px; font-family: Verdana, Geneva, 'DejaVu Sans', sans-serif; margin-top: 30px;">
                <div id="cellbegineditevent"></div>
                <div style="margin-top: 10px;" id="cellendeditevent"></div>
            </div> -->
        </div>
    </div>
</div>
<div class="clearfix">
</div>