<?php  
	// @header('Content-Type: application/ms-excel');
	// 	@header('Content-Length: '.strlen($content));
 		@header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		@header('Content-disposition: inline; filename="'.$title.' '.date("d/m/Y").'.xls"');
        @header('Cache-Control: max-age=0');
?>
<table>
	<thead>
		<tr>
			<th>CURRENCY</th>
			<th>SALDO AWAL VALAS</th>
			<th>SALDO AWAL RUPIAH</th>
			<th>PEMBELIAN</th>
			<th>PEMBELIAN DALAM RUPIAH</th>
			<th>PENJUALAN</th>
			<th>PENJUALAN DALAM RUPIAH</th>
			<th>SALDO AKHIR VALAS</th>
			<th>SALDO AKHIR RUPIAH</th>
			<th>KURS TENGAH</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($rows as $key => $value): ?>
		<tr>
			<td><?php echo $value->CURR_CODE ?></td>
			<td><?php echo number_format($value->SaldoAwalValas) ?></td>
			<td><?php echo number_format($value->SaldoAwalRupiah,2)?></td>
			<td><?php echo number_format($value->Pembelian,2)?></td>
			<td><?php echo number_format($value->PembelianOnIDR,2) ?></td>
			<td><?php echo number_format($value->Penjualan,2) ?></td>
			<td><?php echo number_format($value->PenjualanOnIDR,2) ?></td>
			<td><?php echo number_format($value->SaldoAkhirValas,2) ?></td>
			<td><?php echo number_format($value->SaldoAkhirIDR,2) ?></td>
			<td><?php echo number_format($value->MiddeRate) ?></td>
		</tr>
		<?php endforeach ?>
	</tbody>
</table>