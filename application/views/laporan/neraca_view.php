
<link rel="stylesheet" href="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/styles/jqx.base.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/scripts/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxdata.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxmenu.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.filter.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.sort.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.edit.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.selection.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxlistbox.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxdropdownlist.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxcheckbox.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxcalendar.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxnumberinput.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxdatetimeinput.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/globalization/globalize.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/scripts/demos.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/demos/sampledata/generatedata.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/globalization/globalize.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/demos/jqxgrid/localization.js"></script>
<style type="text/css">
    .redClass
    {
        background-color: #FF0000;
    }
    .greenClass
    {
        background-color: #228B22;
    }
    .blueClass
    {
        background-color: #87CEEB;
    }
    .orangeClass
    {
        background-color: #FFA500;
    }
    .yellowClass
    {
        background-color: #FFFF00;
    }
    .whiteClass
    {
        background-color: White;
    }
</style>
<script type="text/javascript">
    var jQuery_1_4_3 = $.noConflict(true);
    jQuery_1_4_3(document).ready(function () {
        var dateget = document.getElementById("fromsortdate").value;
        //var monthget = d.getMonth() + 1;
        //var dateget = d.getDate() + '-' + monthget + '-' + d.getFullYear();
        // prepare the data
//        var url = "http://localhost/smartdeal/akunting/get_data";
        var url = "<?php echo site_url('laporan/get_data/') ?>" + dateget + '/' + dateget + '/neracadebitdetail';
        var urlcredit = "<?php echo site_url('laporan/get_data/') ?>" + dateget + '/' + dateget + '/neracacreditdetail';
        var source =
                {
                    datatype: "json",
                    updaterow: function (rowid, rowdata, commit) {
                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                    datafields:
                            [
                                {name: 'Kode', type: 'string'},
                                {name: 'Nama', type: 'string'},
                                {name: 'TotalTransaksi', type: 'number'}
                            ],
                    id: 'Iid',
                    url: url,
                    root: 'data'
                };
            var sourcecredit =
                {
                    datatype: "json",
                    updaterow: function (rowid, rowdata, commit) {
                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                    datafields:
                            [
                                {name: 'Kode', type: 'string'},
                                {name: 'Nama', type: 'string'},
                                {name: 'TotalTransaksi', type: 'number'}
                            ],
                    id: 'Iid',
                    url: urlcredit,
                    root: 'data'
                };
        var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
            if (value < 0) {
                return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #800000;">' + value + '</span>';
            } else {
                return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #000000;">' + value + '</span>';
            }
        };
//        var cellsrenderer_liratusjuta = function (row, column, value, defaultHtml) {
//            if (value > '499999999') {
//                var element = jQuery_1_4_3(defaultHtml);
//                element.css({'background-color': '#FF0000'});
//                return element[0].outerHTML;
//                return defaultHtml;
//            } else {
//                var element = jQuery_1_4_3(defaultHtml);
//                element.css({'background-color': '#000000'});
//                return element[0].outerHTML;
//                return defaultHtml;
//            }
//        };
        var redRow;
        var greenRow;
        var blueRow;
        var cellclassname = function (row, column, value, data) {
            if (data.Mencurigakan == 1) {
                return "redClass";
            } else if (data.IsResmi == 1) {
                return "whiteClass";
            } else if (data.KategoriTransaksi == 1) {
                return "blueClass";
            } else if (data.KategoriTransaksi == 2) {
                return "greenClass";
            } else if (data.KategoriTransaksi == 3) {
                return "yellowClass";
            } else if (data.KategoriTransaksi == 4) {
                return "orangeClass";
            } else {
                return "whiteClass";
            }


        };
        var dataAdapter = new jQuery_1_4_3.jqx.dataAdapter(source);
        // initialize jqxGrid
        jQuery_1_4_3("#jqxgrid").jqxGrid(
                {
                    width: 500,
                    height: 450,
                    source: dataAdapter,
                    editable: false,
                    showfilterrow: true,
                    filterable: true,
                    selectionmode: 'singlecell',
//                    editmode: 'selectedrow',
//                    autowidth: true,
//                    autoheight: true,
                    sortable: true,
                    columns: [
                        {text: 'Kode Customer', columntype: 'textbox', datafield: 'Kode', width: 150, pinned: true},
                        {text: 'Nama Customer', columntype: 'textbox', datafield: 'Nama', width: 200, pinned: true},
                        {
                            text: 'Total Debit', datafield: 'TotalTransaksi', width: 150, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        }
                    ]
                });
        // events
    var dataAdaptercredit = new jQuery_1_4_3.jqx.dataAdapter(sourcecredit);
        jQuery_1_4_3("#jqxgridneracacredit").jqxGrid(
                {
                    width: 500,
                    height: 250,
                    source: dataAdaptercredit,
                    editable: false,
                    showfilterrow: true,
                    filterable: true,
                    selectionmode: 'singlecell',
//                    editmode: 'selectedrow',
//                    autowidth: true,
//                    autoheight: true,
                    sortable: true,
                    columns: [
                        {text: 'Kode', columntype: 'textbox', datafield: 'Kode', width: 150, pinned: true},
                        {text: 'Nama', columntype: 'textbox', datafield: 'Nama', width: 200, pinned: true},
                        {
                            text: 'Total Kredit', datafield: 'TotalTransaksi', width: 150, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        }
                    ]
                });
        
    }

    );
    
    function convert_date(str) {
        var date = new Date(str),
                mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }
    function get_newgrid() {
        var fromsortdate = document.getElementById('fromsortdate').value;
//        alert(tosortdate);
        var tmpS = jQuery_1_4_3("#jqxgrid").jqxGrid('source');
        tmpS._source.url = "<?php echo site_url('laporan/get_data/') ?>" + fromsortdate + '/' + fromsortdate+ '/neracadebitdetail';
        jQuery_1_4_3("#jqxgrid").jqxGrid('source', tmpS);
    }
</script>
<h3 class="page-title">
    <?php echo $pageform ?></h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="index.html">Laporan</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Neraca</a>
        </li>
    </ul>

</div>
<table>
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<form action="<?php echo base_url() . 'laporan/neraca' ?>" method='post' name="formsortindex" id="formsortindex">

<table>
    <tr>
    
        <td>
            <label class="control-label col-md-5">Tanggal&nbsp;
            </label>
        </td>
        <td>
            <div class="input-group input-small date-picker input-daterange" data-date="" data-date-format="dd-mm-yyyy">
                    <input readonly type="text" class="form-control" name="fromsortdate" id="fromsortdate" value="<?php echo date('d-m-Y', strtotime($dategetelement)) ?>" autocomplete="on">
            </div>
          
        </td>
        <td>
            <button type="submit" class="btn green" style="margin: 0px 0 0 1px;height: 32px;" onclick="">Apply</button>
        </td>
        
    </tr>
</table>
</form>
<table align='center'>
<?php 
    $selisih = $totaldebit - $totalcredit;
?>
    <tr><td><label class="caption-subject font-black-steel bold uppercase">Total Debit&nbsp:&nbsp</label><label class="auto"><?php echo $totaldebit ?></label></td></tr>
    <tr><td><label class="caption-subject font-black-steel bold uppercase">Total Kredit&nbsp:&nbsp</label><label class="auto"><?php echo $totalcredit ?></label></td></tr>
    <tr><td>
    <label class="caption-subject font-black-steel bold uppercase">Selisih D/K&nbsp:&nbsp </label>
    <?php if($selisih != 0){ ?>
    <label class="auto" style="color:red;font-weight: bold;"><?php echo abs($selisih) ?></label>
    <?php } else { ?>
    <label class="auto caption-subject font-black-steel bold uppercase"><?php echo abs($selisih) ?></label>
     <?php }  ?>
    </td></tr>
</table><br>
<label> </label><br>
<div class="row">
    <div class="col-md-6"><label class="caption-subject font-blue-steel bold uppercase">Debit</label>
        <div id='jqxWidget'>
            <div id="jqxgrid"></div>
            <div style="font-size: 12px; font-family: Verdana, Geneva, 'DejaVu Sans', sans-serif; margin-top: 30px;">
                <div id="cellbegineditevent"></div>
                <div style="margin-top: 10px;" id="cellendeditevent"></div>
            </div>
        </div>

    </div>
     <div class="col-md-6"><label class="caption-subject font-blue-steel bold uppercase">Kredit</label>
        <div id='jqxWidget'>
            <div id="jqxgridneracacredit"></div>
            <div style="font-size: 12px; font-family: Verdana, Geneva, 'DejaVu Sans', sans-serif; margin-top: 30px;">
                <div id="cellbegineditevent"></div>
                <div style="margin-top: 10px;" id="cellendeditevent"></div>
            </div>
        </div>

    </div>
</div>
<div class="clearfix">
</div>