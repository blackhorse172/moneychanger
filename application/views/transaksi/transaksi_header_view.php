<?php $this->load->view('jqwidgetslink'); ?>

<script type="text/javascript">
    var jQuery_1_4_3 = $.noConflict(true);
    var base_url = "<?php echo base_url(); ?>";
    var count = 0;
    jQuery_1_4_3(document).ready(function () {
        $('#paidStatus').change(function(event) {
            /* Act on the event */
            if (this.checked) {
                $('#paidStatus').val('paid');
            } else {
                $('#paidStatus').val('');
            }
        });

          jQuery_1_4_3('#transcationTable').on('click', '.removeTransaction', function() {
            /* Act on the event */
            $(this).parent().parent().remove();
        });

        jQuery_1_4_3('#transcationTableBuy').on('click', '.removeTransactionBuy', function() {
            /* Act on the event */
            $(this).parent().parent().remove();
        });

        jQuery_1_4_3("#jqxgrid").on('rowselect', function (event) {
                                var args = event.args;
                                rowKey = args.rowindex;
                                var rowData = args.row;
                                
                                trxId = rowData.Transaction_Iid

                                var tmpS = jQuery_1_4_3("#jqxgridDetail").jqxGrid('source');
                                    tmpS._source.url =base_url + "transaksi/get_data_detail/" + trxId;
                                    jQuery_1_4_3("#jqxgridDetail").jqxGrid('source', tmpS);
    });


        $('#btnTrxSave').on('click', function(event) {
            /* Act on the event */
            $('#addForm').modal('hide');
            count += 1;

            if (count == 2) {
            // come code
                txStore();
            } else {
               txStore();                
            }

            return false;
            //$('#btnTrxSave').prop('disabled', true);
        });
        // prepare the data
        var oldvalue = -1;
        var current_index = 2;
        var url = "<?php echo site_url('transaksi/get_data_header'); ?>";
        var url_dd_curr = "<?php echo site_url('transaksi/get_data_curr'); ?>";
        var url_dd_cust = "<?php echo site_url('transaksi/get_data_customer'); ?>";

        var source =
                {
                    datatype: "json",
                    addRow: function (rowID, rowData, position, commit) {
                        // synchronize with the server - send insert command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        // you can pass additional argument to the commit callback which represents the new ID if it is genejumlahd from a DB.
                        commit(true);
                    },
                    updaterow: function (rowid, rowdata, commit) {
                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                    deleteRow: function (rowID, commit) {
                        // synchronize with the server - send delete command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        commit(true);
                    },
                    datafields:
                            [
                                {name: 'Transaction_Iid', type: 'string'},
                                {name: 'CompanyId', type: 'string'},
                                {name: 'BranchId', type: 'string'},
                                {name: 'InvoiceNo', type: 'string'},
                                {name: 'KodeCustomer', type: 'string'},
                                {name: 'NamaCustomer', type: 'string'},
                                {name: 'Bors', type: 'string'},
                                {name: 'InputBy', type: 'string'},
                                {name: 'InputDate', type: 'datetime'},
                                {name: 'CustId', type: 'string'},
                                {name: 'TglTransaksi', type: 'date'},
                                {name: 'GrandTotal', type: 'number'},
                                {name: 'IsPaid', type: 'string'},
                                {name: 'IsPickup', type: 'string'},
                                {name: 'IsSuspicious', type: 'string'},

                            ],
                    id: 'Transaction_Iid',
                    url: url,
                    root: 'data'
                };
        var cellclassname2 = function (row, column, value, data) {
            if (data.IsPaid == 'IsPaid') {
                return "blueClass";
            } else {
                return "whiteClass";
            }


        };
        var source_dd_curr =
                {
                    datatype: "json",
                    datafields:
                            [
                                {name: 'REF_CURR_ID', type: 'number'},
                                {name: 'CURR_CODE', type: 'string'}
                            ],
                    url: url_dd_curr,
                    async: true
                };
        var source_dd_cust =
                {
                    datatype: "json",
                    datafields:
                            [
                                {name: 'Nama', type: 'string'},
                                {name: 'Name', type: 'string'},
                            ],
                    url: url_dd_cust,
                    async: true
                };
       var dataAdapter = new jQuery_1_4_3.jqx.dataAdapter(source);
        var dataAdapter_dd_curr = new jQuery_1_4_3.jqx.dataAdapter(source_dd_curr);
        var dataAdapter_dd_cust = new jQuery_1_4_3.jqx.dataAdapter(source_dd_cust);

        var columnCheckBox = null;
        var updatingCheckState = false;

        // initialize jqxGrid
        jQuery_1_4_3("#jqxgrid").on('bindingcomplete', function () {
            var oldvalue = -1;
//            jQuery_1_4_3("#jqxgrid").jqxGrid('autoresizecolumns');
        });
        
        jQuery_1_4_3("#jqxgrid").jqxGrid(
                {
                    width: '100%',
                    height: 370,
                    source: dataAdapter,
                    editable: false,
                    showfilterrow: true,
                    filterable: true,
                    altRows: true,
                    selectionmode: 'singlerow',
                    editmode: 'selectedrow',
                    columnsresize: true,
                    pageable: true,

                    sortable: true,
                    ready: function () {
                        jQuery_1_4_3('#jqxCheckBox').on('checked', function (event) {

                        });
                    },
                    showtoolbar: true,
                    renderToolbar: function (toolBar)
                    {
                        var toTheme = function (className) {
                            if (theme == "")
                                return className;
                            return className + " " + className + "-" + theme;
                        }

                        var container = jQuery_1_4_3("<div style='overflow: hidden; position: relative; height: 100%; width: 100%;'></div>");
                        var buttonTemplate = "<div style='float: left; padding: 3px; margin: 2px;'><div style='margin: 4px; width: 16px; height: 16px;'></div></div>";
                        var addButton = jQuery_1_4_3(buttonTemplate);
                        var editButton = jQuery_1_4_3(buttonTemplate);
                        var deleteButton = jQuery_1_4_3(buttonTemplate);
                        var cancelButton = jQuery_1_4_3(buttonTemplate);
                        var updateButton = jQuery_1_4_3(buttonTemplate);
                        var paidButton   = jQuery_1_4_3(buttonTemplate);
                        var pickupButton   = jQuery_1_4_3(buttonTemplate);
                        var cashButton   = jQuery_1_4_3(buttonTemplate);
                        var suspiciousButton   = jQuery_1_4_3(buttonTemplate);
                        var printButton   = jQuery_1_4_3(buttonTemplate);

                        container.append(addButton);
                        //container.append(updateButton);
                        //container.append(editButton);
                        container.append(deleteButton);
                        //container.append(cancelButton);
                        container.append(paidButton);
                        container.append(pickupButton);
                        //container.append(cashButton);
                        container.append(suspiciousButton);
                        container.append(printButton);

                        toolBar.append(container);

                        addButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        addButton.find('div:first').addClass(toTheme('jqx-icon-plus'));
                        addButton.jqxTooltip({position: 'bottom', content: "Add"});
                        editButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        editButton.find('div:first').addClass(toTheme('jqx-icon-edit'));
                        editButton.jqxTooltip({position: 'bottom', content: "Edit"});
                        deleteButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        deleteButton.find('div:first').addClass(toTheme('jqx-icon-delete'));
                        deleteButton.jqxTooltip({position: 'bottom', content: "Void Transaction"});
                        updateButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        updateButton.find('div:first').addClass(toTheme('jqx-icon-save'));
                        updateButton.jqxTooltip({position: 'bottom', content: "Save Changes"});
                        cancelButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        cancelButton.find('div:first').addClass(toTheme('jqx-icon-cancel'));
                        cancelButton.jqxTooltip({position: 'bottom', content: "Cancel"});

                        paidButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        paidButton.find('div:first').addClass(toTheme('icon-wallet'));
                        paidButton.jqxTooltip({position: 'bottom', content: "Paid/unPaid"});

                        pickupButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        pickupButton.find('div:first').addClass(toTheme('icon-arrow-up'));
                        pickupButton.jqxTooltip({position: 'bottom', content: "Pickup/UnPickUp"});

                        cashButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        cashButton.find('div:first').addClass(toTheme('icon-credit-card'));
                        cashButton.jqxTooltip({position: 'bottom', content: "Cash/Uncash"});

                        suspiciousButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        suspiciousButton.find('div:first').addClass(toTheme('icon-eye'));
                        suspiciousButton.jqxTooltip({position: 'bottom', content: " suspicious/unsuspicious"});

                        printButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        printButton.find('div:first').addClass(toTheme('icon-printer'));
                        printButton.jqxTooltip({position: 'bottom', content: " Print to Invoice "});

                        var updateButtons = function (action) {
                            switch (action) {
                                case "Select":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: false});
                                    editButton.jqxButton({disabled: false});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                                case "Unselect":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: true});
                                    editButton.jqxButton({disabled: true});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                                case "Edit":
                                    addButton.jqxButton({disabled: true});
                                    deleteButton.jqxButton({disabled: true});
                                    editButton.jqxButton({disabled: false});
                                    cancelButton.jqxButton({disabled: false});
                                    updateButton.jqxButton({disabled: false});
                                    break;
                                case "End Edit":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: false});
                                    editButton.jqxButton({disabled: false});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                            }
                        }
                        var rowKey = null;
                        addButton.click(function (event) {
                            // if (!addButton.jqxButton('disabled')) {

                            //     jQuery_1_4_3("#jqxgrid").jqxGrid('addrow', {}, {}, 'first');
                            // }
                            //=========================================================================================//a
         $('#addForm').modal({backdrop: 'static', keyboard: false});
         jQuery_1_4_3("#customerId").jqxDropDownList({disabled : false});
       
         // $('#buy').attr('disabled', true);
         // $('#sell').attr('disabled', true);
         $('#date').removeAttr('readonly');
        // add form //
         jQuery_1_4_3('#customerId').jqxDropDownList({
             filterable: true,
             source: dataAdapter_dd_cust,
             displayMember: 'Name',
             valueMember: 'Nama',
             width  : '100%'
          });

        // $('#buy').on('click', function (event) { 
        //     jQuery_1_4_3('#valas').jqxDropDownList({disabled: false});
        //     $('#bs').val(-1);
        // });

        // $('#sell').on('click', function (event) {
        //     jQuery_1_4_3('#valas').jqxDropDownList({disabled: false});
        //     $('#bs').val(1);     
        // });

        // jQuery_1_4_3('#valas').jqxDropDownList({
        //     filterable: true,
        //     source: dataAdapter_dd_curr,
        //     displayMember: 'CURR_CODE',
        //     valueMember: 'CURR_CODE',
        //     selectedIndex: oldvalue,
        //     disabled: true
        // });

         jQuery_1_4_3('#customerId').on('change', function (event) {
               var args = event.args;
               if (args) {
                   // index represents the item's index.                          
                   var index = args.index;
                   var item = args.item;
                   // get item's label and value.
                   var label = item.label;
                   var value = item.value;
                    $('#customer').val(value);
                      // document.getElementById("buy").disabled = false;
                      // document.getElementById("sell").disabled = false;
               }
           });

        // jQuery_1_4_3('#valas').bind('change', function (event) {
        //      var args = event.args;
        //        if (args) {
        //            // index represents the item's index.                          
        //            var index = args.index;
        //            var item = args.item;
        //            // get item's label and value.
        //            var label = item.label;
        //            var value = item.value;
        //             $('#valasValue').val(label);
        //             $('#jumlah').val('');
        //        }
        // $.ajax({
        //     type: "POST",
        //     url:  "<?php echo base_url(); ?>transaksi/get_rate_init",
        //     cache: false,
        //     data: {currencycode: jQuery_1_4_3('#valas').val(), bors: $('#bs').val()}, // since, you need to delete post of particular id
        //         success: function (reaksi) {
        //             if (reaksi != '') {
        //                 $('#rate').autoNumeric('set',reaksi);
        //                 $('#rateValue').val(reaksi);
        //             } else {
        //                    alert('Response gagal');
        //              }
        //         }
        //     });
        // });
//=========================================================================================//

                            $('#addForm').modal('show');

                        });

                           paidButton.click(function (event) {
                            message = 'Data belum di checked';
                            var getRow = checkingRowHeader('count');
                               if (getRow.length == 0) {
                                 $('#alert').show().delay(2000).fadeOut().html(message);//$('#alert').show().html(buttonClose + message);
                                return false;
                               }

                            jQuery_1_4_3('#cashPaid').val("");
                            jQuery_1_4_3('#transferPaid').val("")
                           
                            checkAmountPaid();

                        });

                             pickupButton.click(function (event) {
                               var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                               var rowsIndex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindex');
                               var field = rows[rowsIndex];
                               
                              if (field.IsPickup == 'Belum') {
                                updateStatus('IsPickUp');
                               } else {
                                updateStatus('UnPickUp');
                               }

                        });


                          suspiciousButton.click(function (event) {
                            var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                               var rowsIndex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindex');
                               var field = rows[rowsIndex];
                              
                               if (field.IsSuspicious == 'Tidak') {
                                updateStatus('IsSuspicious');
                               } else {
                                updateStatus('UnSuspicious');
                               }

                          
                            
                        });

                         printButton.click(function (event) {
                            message = 'Data belum di checked';
                            var getRow = checkingRowHeader('count');
                               if (getRow.length <= 0) {
                                $('#alert').show().delay(2000).fadeOut().html(message);// $('#alert').show().html(buttonClose + message);
                               } else {
                                    if (confirm("Apakah yakin mencetak dan generate invoice data ini ?") == true) {
                                        checkPaidStatusHeader('normal');
                                        jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                                    } else {

                                    }     
                               }
                           
                        });

                        cancelButton.click(function (event) {
                            if (!cancelButton.jqxButton('disabled')) {
                                jQuery_1_4_3("#jqxgrid").jqxGrid('endrowedit', rowKey, true); // if true, the changes are canceled.
                                editrow = -1;
                                updateButtons('Unselect');
                                var rowindex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindex');
                                jQuery_1_4_3('#jqxgrid').jqxGrid('unselectrow', rowindex);
                                jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                                oldvalue = -1;
                            }
                        });
                        deleteButton.click(function () {
                             message = 'Data belum di checked';
                            var getRow = checkingRowHeader('count');
                               if (getRow.length == 0) {
                                $('#alert').show().delay(2000).fadeOut().html(message);//$('#alert').show().html(buttonClose + message);
                                return false;
                               }
                                  
                                 var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                 var rowsIndex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindex');
                                 var data = rows[rowsIndex].Transaction_Iid;
                                
                                     if (confirm("Apakah yakin menghapus data ini ?") == true) {
                                              $.ajax({
                                        type: "POST",
                                        url: base_url + "transaksi/delete_data",
                                        cache: false,
                                        data: {iid: JSON.stringify(data)}, // since, you need to delete post of particular id
                                        success: function (status) {
                                                                   if (status == '1') {
                                                                     jQuery_1_4_3.each(ids,function(key, value) {
                                                                           
                                                                                      jQuery_1_4_3('#jqxgrid').jqxGrid('deleterow', ids[key]);
                                                                                         // window.location.reload();
                                                                      });
                                                                       
                                                                    } else {
                                                                        message = status
                                                                        $('#alert').show().html(buttonClose + message);
                                                                        return false;
                                                                    }

                                                                }
                                        });
                                              
                                        } else {
                                           
                                        }
                            
                        });
                        jQuery_1_4_3("#jqxgrid").on('endrowedit', function (event) {
                            updateButtons('End Edit');
                        });
                    },
                    columns: [
                        {text: 'Kode Customer',cellclassname: cellclassname2, columntype: 'textbox', datafield: 'KodeCustomer', width: 150, pinned: true},
                        {text: 'Nama Customer',cellclassname: cellclassname2, columntype: 'textbox', datafield: 'NamaCustomer', width: 200, pinned: true},
                        {text: 'Kode Transaksi',cellclassname: cellclassname2, columntype: 'textbox', datafield: 'Transaction_Iid', width: 200},
                        {text: 'Jual / Beli',cellclassname: cellclassname2, columntype: 'textbox', datafield: 'Bors', width: 50},
                        {text: 'Tanggal Transaksi',cellclassname: cellclassname2, columntype: 'dateinput', datafield: 'TglTransaksi', cellsformat: 'd/M/yyyy', width: 100},
                        {text: 'Total Transaksi',cellclassname: cellclassname2, columntype: 'textbox',cellsformat: "f2", datafield: 'GrandTotal', width: 220},
                        {text: 'Pembayaran',cellclassname: cellclassname2, columntype: 'textbox', datafield: 'IsPaid', width: 100},
                        {text: 'Nomor Invoice',cellclassname: cellclassname2, columntype: 'textbox', datafield: 'InvoiceNo', width: 200},
                        {text: 'Status Pickup',cellclassname: cellclassname2, columntype: 'textbox', datafield: 'IsPickup', width: 100},
                        {text: 'Mencurigakan ?',cellclassname: cellclassname2, columntype: 'textbox', datafield: 'IsSuspicious', width: 100}
                    ]
                });

    });

    function convert_date(str) {
        var date = new Date(str),
                mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }

    function checkingRowHeader(column){
            var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
            var rowsIndex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindexes');
            var data = [];
            var i = 0;
           
            if (column == 'count') {
                if (rowsIndex.length>0) {
                    jQuery_1_4_3.each(rowsIndex,function(key, value) {
                        data[key] = value;
                    });      
                }
              
            } else {
                if (rowsIndex.length>0) {
                    jQuery_1_4_3.each(rowsIndex,function(key, value) {
                        data[key] = rows[key].column;
                    });
                }
            }
                    return data;
    }


    function checkAmountPaid(value) {
                                    var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                    var rowsIndex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindex');
                                    var amount = [];
                                    var namaCustomer = [];
                                    var transactionId = rows[rowsIndex].Transaction_Iid;
                                    var bors = [];
                                    var i = 0;
                                      jQuery_1_4_3.each(rowsIndex,function(key, value) {
                                            // amount[key] = rows[value].TotalTransaksi;
                                            // namaCustomer[key] = rows[value].NamaCustomer;
                                            // iid[key] = rows[value].Iid;
                                            // bors[key] = rows[value].BorS;
                                      });
                                      
                                       $.ajax({
                                        type: "POST",
                                        url: base_url + "transaksi/checkAmountPaid",
                                        cache: false,
                                        data: {transactionId: transactionId},//{bors: JSON.stringify(bors), namaCustomer: JSON.stringify(namaCustomer), iid: JSON.stringify(iid), amount: JSON.stringify(amount)},
                                        dataType: 'json',
                                        success: function (data) {
                                                                    if (data.status == 200) {
                                                                        $('#paidAmountValue').val(data.items.amount);
                                                                        $('#customerName').val(data.items.customerName);
                                                                        $('#paidAmount').autoNumeric('set',data.items.amount);
                                                                        $('#paidAmountIn').autoNumeric('set',data.items.incomingAmount);
                                                                        $('#paidAmountInValue').val(data.items.incomingAmount);
                                                                        $('#differenceAmount').autoNumeric('set',data.items.differenceAmount);
                                                                        $('#differenceAmountValue').val(data.items.differenceAmount);
                                                                        $('#cashPaid').val("");
                                                                        $('#transferPaid').val("");

                                                                        if (data.items.statusPaid > 0) {
                                                                            $('#labelDifferenceAmount').text('Kelebihan pembayaran (+)');
                                                                            $('#fieldsetPaidStatus').show('slow/400/fast');
                                                                            $('#cashPaid').prop('disabled',true);
                                                                            $('#transferPaid').prop('disabled',true);
                                                                            $('#fieldsetPayment').hide();
                                                                        } else {
                                                                            $('#paidStatus').val("");
                                                                            $('#labelDifferenceAmount').text('Kekurangan yang harus dibayar (-) ');
                                                                              $('#fieldsetPaidStatus').hide();
                                                                              $('#cashPaid').prop('disabled',false);
                                                                              $('#transferPaid').prop('disabled',false);
                                                                              $('#fieldsetPayment').show();
                                                                        }

                                                                        $('#bankCashModal').modal('show');
                                                                     showFieldBanktransfer();
                                                                    } else if (data.status == 400) {
                                                                     showAlert(data.messages);
                                                                    } else if (data.status == 500) {
                                                                       
                                                                    } else if (data.status == 403) {
                                                                        
                                                                    }

                                                                }
                                                            });
    }

function addRowTransaction() {
    // body...
    var bool = false;
    var valas = $('#valasValue');
    var rate = $('#rate');
    var jumlah = $('#jumlah');
    var total = $('#total');
    var grandTotal = rate.val().replace(/,/g,'') * jumlah.val().replace(/,/g,'');
    total.autoNumeric('init','aDec').val(grandTotal); 
    $(document).trigger("clear-alert-id.quantityAlert");
    $('.form-group').removeClass('has-error');
     if ( $('#rowTable tr').length == 5 ) {

         $('#addFormAlert').show().delay(2000).fadeOut().html('Batas maksimal detail transaksi hanya 5 (Lima)');
         return false;
    }


    // $('.rowTrxDetail').each(function(index, el) {
    //        if (valas.val() == $('.rowTrxDetail .valasName').eq(index).text()) {
    //             bool = true;
    //        }     
    // });

    // if (bool) {
    //     alert('Tidak bisa menambah valas yang sama!');
    //     return false;
    // }
    
    // if ($('#valasValue').val() == '') {
    //     $('#addFormAlert').show().delay(2000).fadeOut().html('Valas belum dipilih');
    //     return false;
    // }

    if ($('#rate').val() == '') {
        $('#addFormAlert').show().delay(2000).fadeOut().html('Rate tidak boleh kosong');
            return false;
        }

    if ($('#jumlah').val() == '') {
            $('#jumlah').parent('.form-group').addClass('has-error');
      //        $(document).trigger("set-alert-id-quantityAlert", [
      //   {
      //     "message": "Jumlah harus diisi",
      //     "priority": "error"
      //   }
      // ]);
      $('#addFormAlert').show().delay(2000).fadeOut().html('Jumlah tidak boleh kosong');
            return false;
    }


    $('#rowTable').append('<tr class=rowTrxDetail><td class=valasName><input type=hidden name=valasArray[] id=valasArray value='+valas.val()+'>'+valas.val()+'</td><td><input type=hidden name=rateArray[] id=rateArray value='+rate.autoNumeric('init','aDec').val()+'>'+rate.autoNumeric('init','aDec').val()+'</td><td><input type=hidden name=jumlahArray[] id=jumlahArray value='+jumlah.val()+'>'+jumlah.val()+'</td><td><input class="auto" type=hidden id=totalArray value='+total.val()+'>'+'Rp. '+total.autoNumeric('set',grandTotal).val()+'</td><td><a href=javascript:void(0) class=removeTransaction><div class=icon-close></div></a></td></tr>');

    if ( $('#rowTable tr').length > 0 ) {
         jQuery_1_4_3("#customerId").jqxDropDownList({disabled : true});
         // document.getElementById("buy").disabled = true;
         // document.getElementById("sell").disabled = true;
         $('#date').attr('readonly', true);
    }

}

function addRowTransactionBuy() {
    // body...
    var bool = false;
    var valas = $('#valasValue');
    var rate = $('#rateBuy');
    var jumlah = $('#jumlahBuy');
    var total = $('#totalBuy');
     var grandTotal = rate.val().replace(/,/g,'') * jumlah.val().replace(/,/g,'');

    $(document).trigger("clear-alert-id.quantityAlert");
    $('.form-group').removeClass('has-error');
     if ( $('#rowTableBuy tr').length == 5 ) {

         $('#addFormAlert').show().delay(2000).fadeOut().html('Batas maksimal detail transaksi hanya 5 (Lima)');
         return false;
    }


    // $('.rowTrxDetail').each(function(index, el) {
    //        if (valas.val() == $('.rowTrxDetail .valasName').eq(index).text()) {
    //             bool = true;
    //        }     
    // });

    // if (bool) {
    //     alert('Tidak bisa menambah valas yang sama!');
    //     return false;
    // }
    
    // if ($('#valasValue').val() == '') {
    //     $('#addFormAlert').show().delay(2000).fadeOut().html('Valas belum dipilih');
    //     return false;
    // }

    if ($('#rateBuy').val() == '') {
        $('#addFormAlert').show().delay(2000).fadeOut().html('Rate tidak boleh kosong');
            return false;
        }

    if ($('#jumlahBuy').val() == '') {
            $('#jumlahBuy').parent('.form-group').addClass('has-error');
      //        $(document).trigger("set-alert-id-quantityAlert", [
      //   {
      //     "message": "Jumlah harus diisi",
      //     "priority": "error"
      //   }
      // ]);
      $('#addFormAlert').show().delay(2000).fadeOut().html('Jumlah tidak boleh kosong');
            return false;
    }


    $('#rowTableBuy').append('<tr class=rowTrxDetail><td class=valasNameBuy><input type=hidden name=valasArrayBuy[] id=valasArrayBuy value='+valas.val()+'>'+valas.val()+'</td><td><input type=hidden name=rateArrayBuy[] id=rateArrayBuy value='+rate.autoNumeric('init','aDec').val()+'>'+rate.autoNumeric('init','aDec').val()+'</td><td><input type=hidden name=jumlahArrayBuy[] id=jumlahArrayBuy value='+jumlah.val()+'>'+jumlah.val()+'</td><td><input type=hidden id=totalArrayBuy value='+total.val()+'>'+'Rp. '+total.autoNumeric('set',grandTotal).val()+'</td><td><a href=javascript:void(0) class=removeTransactionBuy><div class=icon-close></div></a></td></tr>');

    if ( $('#rowTable tr').length > 0 ) {
         jQuery_1_4_3("#customerId").jqxDropDownList({disabled : true});
         // document.getElementById("buy").disabled = true;
         // document.getElementById("sell").disabled = true;
         $('#date').attr('readonly', true);
    }

}

function checkPaidStatusHeader(type) {
     var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
     var rowsIndex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindexes');
     var amount = [];
     var namaCustomer = [];
     var iid = [];
     var bors = [];
     var customerId= '';
     var invNo = [];
     var i = 0;
     jQuery_1_4_3.each(rowsIndex, function(key, value) {
         amount[key] = rows[value].TotalTransaksi;
         namaCustomer[key] = rows[value].NamaCustomer;
         iid[key] = rows[value].Transaction_Iid;
         bors[key] = rows[value].BorS;
         customerId = rows[value].CustId;
         invNo[key] = rows[value].InvoiceNo;
     });

     $.ajax({
         type: "POST",
         url: base_url + "transaksi/checkStatusPaid",
         cache: false,
         data: {
             bors: JSON.stringify(bors),
             namaCustomer: JSON.stringify(namaCustomer),
             iid: JSON.stringify(iid),
             amount: JSON.stringify(amount),
             customerId: customerId,
             invNo: JSON.stringify(invNo),
             typePrint: type
         },
         dataType: 'json',
         success: function(data) {
             if (data.status == 200) {
                if (data.items.type == 'normal') {
                        window.open(
                    base_url + 'invoice_transaction?id=' + data.items.iid + '&invNo=' + data.items.invNo + '&customerName=' + data.items.customerName + '&custId=' + data.items.customerId,
                    '_blank'
                    );    
                } else {
                      window.open(
                    base_url + 'thermal_print?id=' + data.items.iid + '&invNo=' + data.items.invNo + '&customerName=' + data.items.customerName + '&custId=' + data.items.customerId,
                    '_blank'
                    );    
                }

             } else if (data.status == 400) {
                 showAlert(data.messages);
             } else if (data.status == 500) {

             } else if (data.status == 403) {

             }

         }
     });
 }


 function txStore() {
     if ($('#customer').val() == '') {
         alert('Customer Belum di isi');
         return false;
     }

     if ($('#rowTable td').length == 0 && $('#rowTableBuy td').length == 0) {
         alert('Transaksi Belum di tambahkan');
         return false;
     }

     var customerName = '';
     var customerValue = $('#customerName').val();

     if (customerValue != "") {
         customerName = $('#customerName').val();
     }

     var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
     var rowsIndex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindexes');
     var data = [];
     var i = 0;
     jQuery_1_4_3.each(rowsIndex, function(key, value) {
         data[i++] = rows[value].Iid;
     });

     $.ajax({
         type: "POST",
         url: base_url + "transaksi/store",
         cache: false,
         dataType: "json",
         data: $('#trxForm,#trxFormBuy').serialize() + "&iid=" + JSON.stringify(data) + "&customer=" + $('#customer').val() + "&date=" + $('#date').val(), // since, you need to delete post of particular id
         success: function(data) {
             $('#alertSuccess').show().delay(2000).fadeOut().html('Sukses di Simpan!');
               var index = jQuery_1_4_3('#jqxgrid').jqxGrid('getrowboundindex', 0);
                             jQuery_1_4_3('#jqxgrid').jqxGrid('unselectrow', index);

                 jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                 jQuery_1_4_3('#jqxgridDetail').jqxGrid('clear');
                 $('#addForm').modal('hide');
                 clearTrxForm();
                 
             // if (data.status == 200) {
             //    //alert($('#trxForm').serialize());
               
             // }

         }
     });
 }


 function clearTrxForm() {
     jQuery_1_4_3("#customerId").jqxDropDownList('clear');
     jQuery_1_4_3('#parentBuy span').removeClass('checked');
     jQuery_1_4_3('#parentSell span').removeClass('checked');
     // jQuery_1_4_3("#valas").jqxDropDownList('clear');

     $('#bs').val('');
     $('#valasValue').val('');
     $('#valasLabel').val('-');
     $('#rateValue').val('');
     // $('#rateValueBuy').val('');
     $('#rate').val('');
     $('#rateBuy').val('');
     $('#jumlah').val('');
     $('#jumlahBuy').val('');
     $('#rowTable tr').remove();
     $('#rowTableBuy tr').remove();


 }

 function clearPaidForm() {
     jQuery_1_4_3("#bankAccount").jqxDropDownList('selectedIndex',-1);
     jQuery_1_4_3('#radioBCash').prop('checked', false);
     jQuery_1_4_3('#radioBBank').prop('checked', false);
     jQuery_1_4_3('.radio span').removeClass('checked');

     $('#trxPaidForm').closest('form').find("input[type=text],input[type=date],input[type=hidden]").val("");
     $('#fieldsetTransferPaid').hide('slow/400/fast');
     $('#fieldsetCashPaid').hide('slow/400/fast');
 }

 function getRate(valas,bors) {
     // body...
     $('#valasLabel').val(valas);
     $('#valasValue').val(valas); 
     // $('#valasValueBuy').val(valas);

     if ($('#customer').val() == '') {
         $('#addFormAlert').show().delay(2000).fadeOut().html('Customer belum dipilih');
         return false;
     }

     if (bors === '-1') {
        $('#borsTab a[href="#beli"]').tab('show');
     } else {
        $('#borsTab a[href="#jual"]').tab('show');
     }

       $.ajax({
            type: "POST",
            url:  "<?php echo base_url(); ?>transaksi/get_rate_init",
            cache: false,
            data: {currencycode: valas, bors: bors}, // since, you need to delete post of particular id
                success: function (reaksi) {
                    if (reaksi != '') {
                        if (bors === '-1') {
                        $('#rateBuy').autoNumeric('set',reaksi);
                        // $('#rateValueBuy').val(reaksi);
                    

                        // $('#rate').val('');
                        // $('#rateValue').val('');
                        // $('#valasValue').val('');    

                        } else {
                        $('#rate').autoNumeric('set',reaksi);
                        // $('#rateValue').val(reaksi);
                           

                        // $('#rateBuy').val('');
                        // $('#rateValueBuy').val('');
                        // $('#valasValueBuy').val('');
                        }
                        
                    } else {
                           alert('Response gagal');
                     }
                }
            });
     //alert(bors);
 }
</script>
<h3 class="page-title">
    <?php echo $pageform ?></h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="index.html">Transaksi</a>
            <!--<i class="fa fa-angle-right"></i>-->
        </li>
        <!--        <li>
                    <a href="#">Teller</a>
                </li>-->
    </ul>
</div>
<table>
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<div class="row">
    <div class="col-md-12">
            <div class="box-body table-responsive">
                <div id='jqxWidget'>
                    <div id="jqxgrid"></div>
                    <div>
                        <div id="cellbegineditevent"></div>
                        <div id="cellendeditevent"></div>
                    </div>
                </div>
            </div>
    </div>
</div>
<?php $this->load->view('transaksi/modal_view');?>
<script src="<?php echo base_url(); ?>/assets/js/transaksi/updateStatus.js" type="text/javascript"></script>
<?php $this->load->view('transaksi/transaksi_view');?>