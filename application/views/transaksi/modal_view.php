<!-- bootstrap modal -->
<div class="modal fade" id="addForm" style="background-color:rgba(160,160,160,0.9)">
    <div class="modal-dialog" role="document" style="width:90%;">
        <div class="modal-content">
            <div class="modal-header">
                
                <h4 class="modal-title">Tambah Transaksi</h4>
                <br/>
                <?php $this->load->view('errors/alert'); ?>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8">
                        <fieldset class="form-group">
                            <label for="formGroupExampleInput2">Nama Customer</label>
                            <div id="customerId" required></div>
                            <input type="hidden" name="customer" id="customer" value="">
                        </fieldset>
                        <!--  <fieldset class="form-group">
                            <label class="radio-inline" id="parentSell">
                                <input type="radio" id="sell" name="grupRadio"> Jual</label>
                                <label class="radio-inline" id="parentBuy">
                                <input type="radio" id="buy" name="grupRadio" > Beli </label>
                                <input type="hidden" name="bs" id="bs" value="">
                            </fieldset> -->
                            <fieldset class="form-group">
                                <label for="formGroupExampleInput2">Tanggal Transaksi</label>
                                <input type="date" name="date" class="form-control" id="date" placeholder="Tanggal" value="<?php echo date('Y-m-d');?>">
                            </fieldset>
                              <fieldset class="form-group">
                                <label for="formGroupExampleInput2">Valas</label>
                                <input type="text" class="form-control" id="valasLabel" placeholder="-" value="" readonly="readonly">
                            </fieldset>
                            <!--   <fieldset class="form-group">
                                <label for="formGroupExampleInput2">Valas</label>
                                <div id="valas"></div>
                                <input type="hidden" class="form-control" id="valasValue" name="valasValue" value="">
                            </fieldset> -->
                            <input type="hidden" class="form-control" id="valasValue" name="valasValue" value="">
                            <ul class="nav nav-tabs" id="borsTab" role="tablist">
                                <li class="active"><a data-toggle="tab" href="#jual">Jual</a></li>
                                <li><a data-toggle="tab" href="#beli">Beli</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="jual" class="tab-pane fade in active">
                                    <form id="trxForm" method="post">
                                        <fieldset class="form-group">
                                            <label for="formGroupExampleInput2">Jumlah</label>
                                            <input type="text" class="auto form-control" id="jumlah" onclick="clearJumlahValue()" name="jumlah" placeholder="Jumlah" required>
                                            <!-- <span class="help-block" data-alertid="quantityAlert"></span> -->
                                            <input type="hidden" id="total" class="auto form-control" value="">
                                        </fieldset>
                                          <fieldset class="form-group">
                                            <label for="formGroupExampleInput2">Rate</label>
                                            <input type="text" class="auto form-control" id="rate" onclick="clearRateValue()" onchange="rateChange()" placeholder="Rate">
                                            <!-- <input type="hidden" id="rateValue" name="rateValue"> -->
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <button type="button" onclick="addRowTransaction()" class="btn btn-primary">Tambah</button>
                                        </fieldset>
                                        
                                        <table class="table" id="transcationTable">
                                            <thead>
                                                <tr>
                                                    <th>Valas</th>
                                                    <th>Rate</th>
                                                    <th>Jumlah</th>
                                                    <th>Total Rp.</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody id="rowTable">
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                                <div id="beli" class="tab-pane fade">
                                     <form id="trxFormBuy" method="post">
                                        <fieldset class="form-group">
                                            <label for="formGroupExampleInput2">Jumlah</label>
                                            <input type="text" class="auto form-control" id="jumlahBuy" onclick="clearJumlahValueBuy()" name="jumlahBuy" placeholder="Jumlah" required>
                                            <!-- <span class="help-block" data-alertid="quantityAlert"></span> -->
                                            <input type="hidden" id="totalBuy" class="auto form-control" value="">

                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label for="formGroupExampleInput2">Rate</label>
                                            <input type="text" class="auto form-control" id="rateBuy" onclick="clearRateValueBuy()" onchange="rateChange()" placeholder="Rate">
                                            <!-- <input type="hidden" id="rateValueBuy" name="rateValueBuy"> -->
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <button type="button" onclick="addRowTransactionBuy()" class="btn btn-primary">Tambah</button>
                                        </fieldset>
                                        
                                        <table class="table" id="transcationTableBuy">
                                            <thead>
                                                <tr>
                                                    <th>Valas</th>
                                                    <th>Rate</th>
                                                    <th>Jumlah</th>
                                                    <th>Total Rp.</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody id="rowTableBuy">
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <table class="table table-striped">
                                <thead class="fix-scroll">
                                    <tr>
                                        <th>#</th>
                                        <th style="width:70;">Valas</th>
                                        <th style="width:120;">Rate Beli</th>
                                        <th style="width:90;">Rate Jual</th>
                                    </tr>
                                </thead>
                                <tbody class="scroll">
                                    <?php foreach ($rates as $key => $rate): ?>
                                    <tr>
                                        <td><?php echo $key + 1; ?></td>
                                        <td><?php echo $rate->CURR; ?></td>
                                        <td ondblclick="getRate('<?php echo $rate->CURR; ?>','-1')"><input class="btn btn-danger" style="width:100;" type="button" value="<?php echo number_format($rate->BUY_COMPLETED,2); ?>"></td>
                                        <td ondblclick="getRate('<?php echo $rate->CURR; ?>','1')"><input class="btn btn-info" style="width:100;" type="button" value="<?php echo number_format($rate->SELL_COMPLETED,2); ?>"></td>
                                    </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="clearTrxForm()">Batal</button>
                            <button type="button" id="btnTrxSave" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                    
                    
                </div>
                
                </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- bootstrap paid cash or bank modal -->
                <div class="modal fade" id="bankCashModal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Status Paid</h4>
                                <br>
                                  <div class="alert alert-danger" id="paidFormAlert" style="display:none">
                                                <button class="close" data-close="alert"></button></div>
                            </div>
                            <div class="modal-body">
                                <form action="" method="post" id="trxPaidForm">
                                    <fieldset class="form-group">
                                        <label for="formGroupExampleInput2">Jumlah yang harus dibayar</label>
                                        <input type="text" class="auto form-control" id="paidAmount" name="paidAmount" placeholder="Jumlah dibayar" disabled="disabled">
                                        <input type="hidden" class="auto form-control" id="paidAmountValue" name="paidAmountValue">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label for="formGroupExampleInput2">Jumlah Uang Yang Sudah diBayar</label>
                                        <input type="text" class="auto form-control" id="paidAmountIn" name="paidAmountIn" placeholder="Jumlah dibayar" disabled="disabled">
                                        <input type="hidden" class="auto form-control" id="paidAmountInValue" name="paidAmountInValue">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label for="formGroupExampleInput2" id="labelDifferenceAmount">Kekurangan yang harus dibayar (-) </label>
                                        <input type="text" class="auto form-control" id="differenceAmount" name="differenceAmount" placeholder="Jumlah dibayar" disabled="disabled">
                                        <input type="hidden" class="auto form-control" id="differenceAmountValue" name="differenceAmountValue">
                                    </fieldset>
                                    <fieldset class="form-group" id="fieldsetPayment">
                                        <label> <input type="radio" id="radioBCash" name="paymentBy" value="cash"> Cash</label>
                                        <label> <input type="radio" id="radioBBank" name="paymentBy" value="bank"> Bank Transfer </label>
                                        <!-- <label> <input type="radio" id="radioBBoth" name="paymentBy" value="2"> Keduanya </label> -->
                                        <!-- <input type="hidden" name="paidStatus" id="paidStatus"> -->
                                    </fieldset>
                                    <fieldset class="form-group" id="fieldsetCashPaid" style="display:none;">
                                        <label for="formGroupExampleInput2">Jumlah uang mau dibayar</label>
                                        <input type="text" class="auto form-control" id="cashPaid" name="cashPaid" placeholder="Jumlah Cash">
                                        <input type="hidden"  id="cashPaidValue" name="cashPaidValue" placeholder="Jumlah Cash" >
                                    </fieldset>
                                    
                                    <fieldset class="form-group" id="fieldsetTransferPaid" style="display:none;">
                                        <label for="formGroupExampleInput2">Jumlah Bank Transfer</label>
                                        <input type="text" class="auto form-control" id="transferPaid" name="transferPaid" placeholder="Jumlah Bank Transfer">
                                        <input type="hidden" id="transferPaidValue" name="transferPaidValue">
                                        <br/>
                                        <label for="formGroupExampleInput2">Bank Akun</label>
                                        <div id="bankAccount"></div>
                                        <input type="hidden" id="bankAccountValue"  name="bankAccountValue" value="">
                                        <input type="hidden" id="bankName"  name="bankName" value="">
                                    </fieldset>
                                    <input type="hidden" class="auto form-control" id="changeValue" name="changeValue" value="">
                                    <input type="hidden" class="auto form-control" id="customerName" name="customerName" value="">
                                    <fieldset class="form-group" id="fieldsetPaidStatus" style="display:none">
                                        <label> <input type="checkbox" id="paidStatus" name="paidStatus" value=""> Paid</label>
                                    </fieldset>
                                    <!-- <button type="button" class="btn btn-primary">Paid</button> -->
                                    <!-- <button type="button" class="btn btn-danger">unPaid</button> -->
                                </form>
                                <button type="button" class="btn btn-primary" onclick="updateStatusIspaid()">simpan</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="clearPaidForm()">Batal</button>
                            </div>
                            
                            </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            <!-- bootstrap paid unpaid modal -->
                            <div class="modal fade" id="paidForm">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            <span class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title">Status Paid</h4>
                                        </div>
                                        <div class="modal-body" align="center">
                                            <button type="button" class="btn btn-primary" onclick="updateStatus('IsPaid')">Paid</button>
                                            <button type="button" class="btn btn-danger" onclick="updateStatus('UnPaid')">unPaid</button>
                                        </div>
                                        
                                        </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->
                                        <!-- bootstrap isPickup unpickup modal -->
                                        <div class="modal fade" id="pickupForm">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        <span class="sr-only">Close</span>
                                                        </button>
                                                        <h4 class="modal-title">Status Pickup</h4>
                                                    </div>
                                                    <div class="modal-body" align="center">
                                                        <button id="isPickup" type="button" class="btn btn-primary" onclick="updateStatus('IsPickUp')">pickup</button>
                                                        <button id="unPickup"type="button" class="btn btn-danger" onclick="updateStatus('UnPickUp')">unPickup</button>
                                                    </div>
                                                    
                                                    </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                    </div><!-- /.modal -->
                                                    <!-- bootstrap iscash uncash modal -->
                                                    <div class="modal fade" id="suspiciousForm">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                    <span class="sr-only">Close</span>
                                                                    </button>
                                                                    <h4 class="modal-title">suspicious</h4>
                                                                </div>
                                                                <div class="modal-body" align="center">
                                                                    <button type="button" id="isSuspicious" class="btn btn-primary" onclick="updateStatus('IsSuspicious')">suspicious</button>
                                                                    <button type="button" id="unSuspicious" class="btn btn-danger" onclick="updateStatus('UnSuspicious')">unSuspicious</button>
                                                                </div>
                                                                
                                                                </div><!-- /.modal-content -->
                                                                </div><!-- /.modal-dialog -->
                                                                </div><!-- /.modal -->
                                                                <div class="modal fade" id="messageForm">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                                <span class="sr-only">Close</span>
                                                                                </button>
                                                                                <h4 class="modal-title">Modal title</h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <p>One fine body&hellip;</p>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                                <button type="button" class="btn btn-primary">Save changes</button>
                                                                            </div>
                                                                            </div><!-- /.modal-content -->
                                                                            </div><!-- /.modal-dialog -->
                                                                            </div><!-- /.modal -->
<style type="text/css" media="screen">
    
.scroll {
    display: block;
        height: 490px;       /* Just for the demo          */
        overflow-y: auto;    /* Trigger vertical scroll    */
        overflow-x: hidden;  /* Hide the horizontal scroll */
}

.fix-scroll { display: block; }

</style>