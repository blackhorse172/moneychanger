<?php $this->load->view('jqwidgetslink'); ?>

<script type="text/javascript">
    var jQuery_1_4_3 = $.noConflict(true);
    var base_url = "<?php echo base_url(); ?>";
    var trxId = 'TX';
    var sessionType = '<?php echo $this->general->userType; ?>';
    var isFilter = false;
   //var url_detail = '';
    jQuery_1_4_3(document).ready(function () {
       
    
        var oldvalue = -1;
        var current_index = 2;
        var url_detail =  base_url + "transaksi/get_data_detail/" + trxId;
        var url_dd_curr = "<?php echo site_url('transaksi/get_data_curr'); ?>";
        var url_dd_cust = "<?php echo site_url('transaksi/get_data_customer'); ?>";
                
        var source =
                {
                    datatype: "json",
                    addRow: function (rowID, rowData, position, commit) {
                        // synchronize with the server - send insert command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        // you can pass additional argument to the commit callback which represents the new ID if it is genejumlahd from a DB.
                        commit(true);
                    },
                    updaterow: function (rowid, rowdata, commit) {
                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                    deleteRow: function (rowID, commit) {
                        // synchronize with the server - send delete command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        commit(true);
                    },
                    datafields:
                            [
                                {name: 'Iid', type: 'string'},
                                {name: 'CompanyId', type: 'string'},
                                {name: 'BranchId', type: 'string'},
                                {name: 'InvNo', type: 'string'},
                                {name: 'NamaCustomer', type: 'string'},
                                {name: 'BorS', type: 'string'},
                                {name: 'CurrencyCode', type: 'string'},
                                {name: 'TglTransaksi', type: 'date'},
                                {name: 'Jumlah', type: 'number'},
                                {name: 'Rate', type: 'number'},
                                {name: 'TotalTransaksi', type: 'number'},
                                {name: 'IsPaid', type: 'number'},
                                {name: 'PaidDate', type: 'date'},
                                {name: 'IsPickUp', type: 'number'},
                                {name: 'PickUpDate', type: 'date'},
                                {name: 'IsCash', type: 'number'},
                                {name: 'Remarks', type: 'string'},
                                {name: 'PrintCounter', type: 'string'},
                                {name: 'InputBy', type: 'string'},
                                {name: 'InputDate', type: 'datetime'},
                                {name: 'LastEditBy', type: 'string'},
                                {name: 'LastEditDate', type: 'datetime'},
                                {name: 'paidStatus', type: 'string'},
                                {name: 'pickupStatus', type: 'string'},
                                {name: 'filterStatus', type: 'string'},
                                {name: 'suspiciousStatus', type: 'string'},
                                {name: 'CustId', type: 'string'},
                                {name: 'IsFilter',type: 'string'},
                                {name: 'Transaction_Iid',type: 'string'}
                            ],
                    id: 'Iid',
                    url: url_detail,
                    root: 'data'
                };
        var source_dd_curr =
                {
                    datatype: "json",
                    datafields:
                            [
                                {name: 'REF_CURR_ID', type: 'number'},
                                {name: 'CURR_CODE', type: 'string'}
                            ],
                    url: url_dd_curr,
                    async: true
                };
        var source_dd_cust =
                {
                    datatype: "json",
                    datafields:
                            [
                                {name: 'Nama', type: 'string'}
                            ],
                    url: url_dd_cust,
                    async: true
                };
        var dataAdapter = new jQuery_1_4_3.jqx.dataAdapter(source);
        var dataAdapter_dd_curr = new jQuery_1_4_3.jqx.dataAdapter(source_dd_curr);
        var dataAdapter_dd_cust = new jQuery_1_4_3.jqx.dataAdapter(source_dd_cust);

        var columnCheckBox = null;
        var updatingCheckState = false;

        // initialize jqxGrid
        jQuery_1_4_3("#jqxgridDetail").on('bindingcomplete', function () {
            var oldvalue = -1;
        });
        
        jQuery_1_4_3("#jqxgridDetail").jqxGrid(
                {
                   width: '100%',
                    height: 330,
                    source: dataAdapter,
                    editable: true,
                   
                    altRows: true,
                    selectionmode: 'none',
                    editmode: 'programmatic',
                    columnsresize: true,
                    pageable: true,
                    sortable: true,
                    showtoolbar: true,
                    renderToolbar: function (toolBar)
                    {
                        var toTheme = function (className) {
                            if (theme == "")
                                return className;
                            return className + " " + className + "-" + theme;
                        }

                        var container = jQuery_1_4_3("<div style='overflow: hidden; position: relative; height: 100%; width: 100%;'></div>");
                        var buttonTemplate = "<div style='float: left; padding: 3px; margin: 2px;'><div style='margin: 4px; width: 16px; height: 16px;'></div></div>";
                        var addButton = jQuery_1_4_3(buttonTemplate);
                        var editButton = jQuery_1_4_3(buttonTemplate);
                        var deleteButton = jQuery_1_4_3(buttonTemplate);
                        var cancelButton = jQuery_1_4_3(buttonTemplate);
                        var updateButton = jQuery_1_4_3(buttonTemplate);
                        var paidButton   = jQuery_1_4_3(buttonTemplate);
                        var pickupButton   = jQuery_1_4_3(buttonTemplate);
                        var cashButton   = jQuery_1_4_3(buttonTemplate);
                        var suspiciousButton   = jQuery_1_4_3(buttonTemplate);
                        var printButton   = jQuery_1_4_3(buttonTemplate);
                        var filterButton   = jQuery_1_4_3(buttonTemplate);

                        // container.append(addButton);
                        // container.append(updateButton);
                        // container.append(editButton);
                        // container.append(deleteButton);
                        // container.append(cancelButton);
                        // container.append(paidButton);
                        // container.append(pickupButton);
                        // container.append(cashButton);
                        // container.append(suspiciousButton);
                        // container.append(printButton);
                        if (sessionType == 0) {
                            container.append(filterButton);
                            // isFilter = false;
                        }

                         if (sessionType == 1) {
                             jQuery_1_4_3("#jqxgridDetail").jqxGrid('hidecolumn', 'filterStatus');
                        }

                        toolBar.append(container);

                        addButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        addButton.find('div:first').addClass(toTheme('jqx-icon-plus'));
                        addButton.jqxTooltip({position: 'bottom', content: "Add"});
                        editButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        editButton.find('div:first').addClass(toTheme('jqx-icon-edit'));
                        editButton.jqxTooltip({position: 'bottom', content: "Edit"});
                        deleteButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        deleteButton.find('div:first').addClass(toTheme('jqx-icon-delete'));
                        deleteButton.jqxTooltip({position: 'bottom', content: "Delete"});
                        updateButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        updateButton.find('div:first').addClass(toTheme('jqx-icon-save'));
                        updateButton.jqxTooltip({position: 'bottom', content: "Save Changes"});
                        cancelButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        cancelButton.find('div:first').addClass(toTheme('jqx-icon-cancel'));
                        cancelButton.jqxTooltip({position: 'bottom', content: "Cancel"});

                        paidButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        paidButton.find('div:first').addClass(toTheme('icon-wallet'));
                        paidButton.jqxTooltip({position: 'bottom', content: "Paid/unPaid"});

                        pickupButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        pickupButton.find('div:first').addClass(toTheme('icon-arrow-up'));
                        pickupButton.jqxTooltip({position: 'bottom', content: "Pickup/UnPickUp"});

                        cashButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        cashButton.find('div:first').addClass(toTheme('icon-credit-card'));
                        cashButton.jqxTooltip({position: 'bottom', content: "Cash/Uncash"});

                        suspiciousButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        suspiciousButton.find('div:first').addClass(toTheme('icon-eye'));
                        suspiciousButton.jqxTooltip({position: 'bottom', content: " suspicious/unsuspicious"});

                        printButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        printButton.find('div:first').addClass(toTheme('icon-printer'));
                        printButton.jqxTooltip({position: 'bottom', content: " Print to Invoice "});

                        filterButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        filterButton.find('div:first').addClass(toTheme('icon-arrow-up'));
                        filterButton.jqxTooltip({position: 'bottom', content: "checking data"});

                        var rowKey = null;
                        jQuery_1_4_3("#jqxgridDetail").on('rowselect', function (event) {
                            if (cancelButton.jqxButton('disabled') || updateButton.jqxButton('disabled')) {
                                var args = event.args;
                                rowKey = args.rowindex;
                                var rowData = args.row;
                                //updateButtons('Select');
                            }
                        });
                        addButton.click(function (event) {
                            // if (!addButton.jqxButton('disabled')) {

                            //     jQuery_1_4_3("#jqxgridDetail").jqxGrid('addrow', {}, {}, 'first');
                            // }
                            $('#addForm').modal('show');
                        });

                           paidButton.click(function (event) {
                            jQuery_1_4_3('#cashPaid').val("");
                            jQuery_1_4_3('#transferPaid').val("")
                            message = 'Data belum di checked';
                            var getRow = checkingRow('count');
                               if (getRow.length <= 0) {
                                $('#alert').show().html(buttonClose + message);
                               } else {
                                  checkAmountPaid();     
                               }
                        });
                             pickupButton.click(function (event) {
                            // if (!addButton.jqxButton('disabled')) {

                            //     jQuery_1_4_3("#jqxgridDetail").jqxGrid('addrow', {}, {}, 'first');
                            // }
                            //addForm();
                            $('#pickupForm').modal('show');

                        });

                               cashButton.click(function (event) {
                            // if (!addButton.jqxButton('disabled')) {

                            //     jQuery_1_4_3("#jqxgridDetail").jqxGrid('addrow', {}, {}, 'first');
                            // }
                            //addForm();
                            $('#cashForm').modal('show');

                        });

                          suspiciousButton.click(function (event) {
                            // if (!addButton.jqxButton('disabled')) {

                            //     jQuery_1_4_3("#jqxgridDetail").jqxGrid('addrow', {}, {}, 'first');
                            // }
                            //addForm();
                            $('#suspiciousForm').modal('show');

                        });

                        //  printButton.click(function (event) {
                        //     message = 'Data belum di checked';
                        //     var getRow = checkingRow('count');
                        //        if (getRow.length <= 0) {
                        //         $('#alert').show().html(buttonClose + message);
                        //        } else {
                        //             if (confirm("Apakah yakin mencetak dan generate invoice data ini ?") == true) {
                        //                 checkPaidStatus();
                        //             } else {

                        //             }     
                        //        }
                        // });

                         filterButton.click(function (event) {
                        message = 'Data Detail belum di checked atau memeilih lebih dari satu';
                            var getRow = checkingRow('count');
                               var rows = jQuery_1_4_3('#jqxgridDetail').jqxGrid('getrows');
                               var rowsIndex = jQuery_1_4_3('#jqxgridDetail').jqxGrid('getselectedrowindex');
                               var field = rows[rowsIndex];

                               if (getRow.length > 1) {
                                    messages = 'hanya boleh pilih satu row'
                                    $('#alert').show().html(buttonClose + message).delay(5000).fadeOut();
                                    return false;
                               }

                               if (getRow.length <= 0) {
                                $('#alert').show().html(buttonClose + message).delay(5000).fadeOut();
                                return false;
                               } 
                               console.log(rows);
                               console.log(field);
                            $.ajax({
                                 type: "POST",
                                 url: base_url + "transaksi/checkingIsFilter",
                                 cache: false,
                                 data: {
                                     Transaction_Iid: JSON.stringify(field.Transaction_Iid),
                                     Iid: JSON.stringify(field.Iid)
                                 },
                                 dataType: 'json',
                                 success: function(data) {
                                     if (data.status === 200) {
                                        updateIsFilter();
                                     } else if (data.status === 400) {
                                        if (data.isFilter === 1) {updateIsFilter(); return false;}
                                         $('#alert').show().html(buttonClose + data.messages).delay(5000).fadeOut();
                                         return false;
                                     } else if (data.status == 500) {
                                         return false;
                                     } else if (data.status == 403) {
                                         return false;
                                     }

                                 }
                             });

                               // if (getRow.length <= 0) {
                               //  $('#alert').show().html(buttonClose + message).delay(5000).fadeOut();
                               // } else {
                               //  //updateIsFilter();
                               // }
                        });
                    },
                   
                    columns: [
                        {text: 'Iid', columntype: 'textbox', datafield: 'Iid', width: 120, hidden: true},
                        {text: 'Transaction_Iid', columntype: 'textbox', datafield: 'Transaction_Iid', width: 120, hidden: true},
                        {text: 'CompanyId', columntype: 'textbox', datafield: 'CompanyId', width: 120, hidden: true},
                        {text: 'BranchId', columntype: 'textbox', datafield: 'BranchId', width: 120, hidden: true},
                    {
                      text: '', menu: false, sortable: false, pinned: true ,
                      datafield: 'available', columntype: 'checkbox', width: 40,
                      renderer: function () {
                          return '<div class="custom-checkbox-column" style="margin-left: 13px; margin-top: 5px;"></div>';
                      },
                      rendered: function (element) {
                          jQuery_1_4_3('#jqxgridDetail').bind('cellendedit', function (event) {
                                if (event.args.value) {
                                    jQuery_1_4_3("#jqxgridDetail").jqxGrid('selectrow', event.args.rowindex);
                                }
                                else {
                                    jQuery_1_4_3("#jqxgridDetail").jqxGrid('unselectrow', event.args.rowindex);
                                }
                          });
                      }
                  },
                        {text: 'Valas', datafield: 'CurrencyCode', width: 70, columntype: 'dropdownlist',
                            createeditor: function (row, column, editor) {
                                editor.jqxDropDownList({
                                    filterable: true,
                                    source: dataAdapter_dd_curr,
                                    displayMember: 'CURR_CODE',
                                    valueMember: 'CURR_CODE',
                                    selectedIndex: oldvalue
                                });
                                var rows = jQuery_1_4_3('#jqxgridDetail').jqxGrid('getrows');
                                var selectedrowindex = jQuery_1_4_3('#jqxgridDetail').jqxGrid('getselectedrowindex');
                                editor.on('change', function (event) {

                                    var args = event.args;
                                    if (args) {
                                        var item = args.item;
                                        var value = item.value;
                                        var field = rows[selectedrowindex];
//                                        alert('old:' + oldvalue + ',new:' + item.index);

                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url() ?>transaksi/get_rate_init",
                                            cache: false,
                                            data: {currencycode: value, bors: field.BorS}, // since, you need to delete post of particular id
                                            success: function (reaksi) {
                                                if (reaksi != '') {

                                                    jQuery_1_4_3("#jqxgridDetail").jqxGrid('setcellvalue', selectedrowindex, "Rate", reaksi);
                                                    if (oldvalue != item.index) {
                                                        if (oldvalue != 0) {
                                                            jQuery_1_4_3("#jqxgridDetail").jqxGrid('endrowedit', selectedrowindex, false);
                                                        }
                                                        jQuery_1_4_3("#jqxgridDetail").jqxGrid('beginrowedit', selectedrowindex);
                                                        oldvalue = item.index;
                                                    }


                                                } else {
                                                    alert(reaksi);
                                                }

                                            }
                                        });
                                    }

                                });
                            }

                        },
                        {
                            text: 'Jumlah', datafield: 'Jumlah', width: 110, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                              
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                         {
                            text: 'Rate', editable: false, datafield: 'Rate', width: 110, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                                return true;
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 5, digits: 12});
                            }
                        },
                         {
                            text: 'Total Transaksi', editable: false, datafield: 'TotalTransaksi', width: 150, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) { //                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                                return true;
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'Paid Date', editable: false, datafield: 'PaidDate', columntype: 'dateinput', width: 110, align: 'right', cellsalign: 'right', cellsformat: 'd/M/yyyy'
                        },
                        
                        {text: 'InputBy', editable: false, columntype: 'textbox', datafield: 'InputBy', width: 120},
                        {
                            text: 'InputDate', editable: false, datafield: 'InputDate', columntype: 'dateinput', width: 110, align: 'right', cellsalign: 'right', cellsformat: 'd/M/yyyy'
                        },
                        {text: 'EditBy', editable: false, columntype: 'textbox', datafield: 'LastEditBy', width: 120},
                        {
                            text: 'Check Status', editable: false, datafield: 'filterStatus', columntype: 'text', width: 110, align: 'right', cellsalign: 'right'
                        },
                        {
                            text: 'EditDate', editable: false, datafield: 'LastEditDate', columntype: 'dateinput', width: 110, align: 'right', cellsalign: 'right', cellsformat: 'd/M/yyyy'
                        }
                    ]

                     
                });
        });

      

    function convert_date(str) {
        var date = new Date(str),
                mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }

    function checkingRow(column){
            var rows = jQuery_1_4_3('#jqxgridDetail').jqxGrid('getrows');
            var rowsIndex = jQuery_1_4_3('#jqxgridDetail').jqxGrid('getselectedrowindexes');
            var data = [];
            var i = 0;

            if (column == 'count') {
              jQuery_1_4_3.each(rowsIndex,function(key, value) {
                    data[key] = value;
              });
            } else {
                   jQuery_1_4_3.each(rowsIndex,function(key, value) {
                    data[key] = rows[key].column;
              });
            }
                    return data;
    }

// function checkPaidStatus() {
//      var rows = jQuery_1_4_3('#jqxgridDetail').jqxGrid('getrows');
//      var rowsIndex = jQuery_1_4_3('#jqxgridDetail').jqxGrid('getselectedrowindexes');
//      var amount = [];
//      var namaCustomer = [];
//      var iid = [];
//      var bors = [];
//      var customerId= '';
//      var invNo = [];
//      var i = 0;
//      jQuery_1_4_3.each(rowsIndex, function(key, value) {
//          amount[key] = rows[value].TotalTransaksi;
//          namaCustomer[key] = rows[value].NamaCustomer;
//          iid[key] = rows[value].Iid;
//          bors[key] = rows[value].BorS;
//          customerId = rows[value].CustId;
//          invNo[key] = rows[value].InvNo;
//      });

//      $.ajax({
//          type: "POST",
//          url: base_url + "transaksi/checkStatusPaid",
//          cache: false,
//          data: {
//              bors: JSON.stringify(bors),
//              namaCustomer: JSON.stringify(namaCustomer),
//              iid: JSON.stringify(iid),
//              amount: JSON.stringify(amount),
//              customerId: customerId,
//              invNo: JSON.stringify(invNo)
//          },
//          dataType: 'json',
//          success: function(data) {
//              if (data.status == 200) {
//                 window.open(
//                 base_url + 'invoice_transaction?id=' + data.items.iid + '&invNo=' + data.items.invNo + '&customerName=' + data.items.customerName + '&custId=' + data.items.customerId,
//                 '_blank'
//                 );
//              } else if (data.status == 400) {
//                  showAlert(data.messages);
//              } else if (data.status == 500) {

//              } else if (data.status == 403) {

//              }

//          }
//      });
//  }

 function updateIsFilter() {
     var rows = jQuery_1_4_3('#jqxgridDetail').jqxGrid('getrows');
     var rowsIndex = jQuery_1_4_3('#jqxgridDetail').jqxGrid('getselectedrowindexes');
    
     var iid = [];
     var isFilter = [];
     
     var i = 0;
     jQuery_1_4_3.each(rowsIndex, function(key, value) {
        
         isFilter[key] = rows[value].IsFilter;
         iid[key] = rows[value].Iid;
       
     });

     $.ajax({
         type: "POST",
         url: base_url + "transaksi/updateIsFilter",
         cache: false,
         data: {
             iid: JSON.stringify(iid),
             isFilter: JSON.stringify(isFilter)
         },
         dataType: 'json',
         success: function(data) {
             if (data.status == 200) {
             alert('sukses');
              jQuery_1_4_3('#jqxgridDetail').jqxGrid('updatebounddata');
             } else if (data.status == 400) {
                   jQuery_1_4_3('#jqxgridDetail').jqxGrid('updatebounddata');
             } else if (data.status == 500) {

             } else if (data.status == 403) {

             }

         }
     });
 }

    function showAlert(messages) {
        message = JSON.stringify(messages);
        $('#alert').show().html(buttonClose + message);
    }

    function removeSeparator(value) {
        return parseInt(value.replace(",", ""));;
    }
    function showFieldBanktransfer(argument) {
        // body...

                                                                     $('#radioBBoth').on('change', function(event) {
                                                                         event.preventDefault();
                                                                         /* Act on the event */
                                                                         $('#fieldsetTransferPaid').show('slow/400/fast');
                                                                         $('#fieldsetCashPaid').show('slow/400/fast');
                                                                         cashAmountChange();


                                                                     });

                                                                      $('#radioBBank').on('change', function(event) {
                                                                         event.preventDefault();
                                                                            $('#cashPaid').val('');
                                                                            $('#transferPaid').val('');
                                                                         /* Act on the event */
                                                                        $('#fieldsetCashPaid').hide('slow/400/fast');                                
                                                                        $('#fieldsetTransferPaid').show('slow/400/fast');

                                                                          var bankAccount = "<?php echo site_url('transaksi/get_bank_account'); ?>";

                                                                           var source_bank_account =
                                                                                    {
                                                                                        datatype: "json",
                                                                                        datafields:
                                                                                                [
                                                                                                    {name: 'AccountNo', type: 'string'},
                                                                                                    {name: 'BankName', type: 'string'},
                                                                                                    {name: 'BankNameView', type: 'string'}
                                                                                                ],
                                                                                        url: bankAccount,
                                                                                        async: true
                                                                                    };
                                                                          
                                                                            var dataAdapter_bank_account = new jQuery_1_4_3.jqx.dataAdapter(source_bank_account);


                                                                          jQuery_1_4_3('#bankAccount').jqxDropDownList({
                                                                            filterable: true,
                                                                            source: dataAdapter_bank_account,
                                                                            displayMember: 'BankNameView',
                                                                            valueMember: 'AccountNo',
                                                                            selectedIndex: 0
                                                                        });

                                                                          jQuery_1_4_3('#bankAccount').bind('change', function (event) {
                                                                             var args = event.args;
                                                                               if (args) {
                                                                                   // index represents the item's index.                          
                                                                                   var index = args.index;
                                                                                   var item = args.item;
                                                                                   // get item's label and value.
                                                                                   var label = item.label;
                                                                                   var value = item.value;
                                                                                    $('#bankAccountValue').val(value);
                                                                                    $('#bankName').val(label);
                                                                               }
                                                                        });

                                                                     });
                                                                       $('#radioBCash').on('change', function(event) {
                                                                         event.preventDefault();
                                                                            $('#cashPaid').val('');
                                                                            $('#transferPaid').val('');
                                                                         /* Act on the event */
                                                                        $('#fieldsetCashPaid').show('slow/400/fast');                                

                                                                         $('#fieldsetTransferPaid').hide('slow/400/fast');
                                                                     });

    }

    function cashAmountChange() {
        var value = 0;
        var paidAmount = $('#differenceAmountValue');
        var cashPaid = $('#cashPaid');
        var cashPaidValue = $('#cashPaidValue');
        var transferPaid = $('#transferPaid');
        var transferPaidValue = $('#transferPaidValue');
        cashPaid.focus();

        cashPaid.keyup(function(event) {
          if (cashPaid.val() == 0 || cashPaid.val() == "") {
                transferPaid.val("");
                cashPaid.val("");
                transferPaid.removeAttr('disabled');
                cashPaid.focusin();
                  $('#changeValue').val(0);
                return false;
            }
            
            value = parseInt(paidAmount.val()) - cashPaid.autoNumeric('get',cashPaid.val());
            //cashPaidValue.val(transferPaid.autoNumeric('get',cashPaid.val()));
            cashPaidValue.val(cashPaid.autoNumeric('get',cashPaid.val()));

            if (value > 0) {
                $('#changeValue').val(value);
                transferPaid.autoNumeric('set',value);
                transferPaidValue.val(value)
                $('#changeValue').val(0);
                // transferPaid.prop('disabled', true);               
            } else {
                alert('nilai tidak boleh lebih besar');
                cashPaid.val(0);
                cashPaid.focus();
                $('#changeValue').val(value);
                transferPaid.val(0);
                return false;
                // transferPaid.prop('disabled', true);
            }
            
        });


           transferPaid.keyup(function(event) {
          if (transferPaid.val() == 0 || transferPaid.val() == "") {
                transferPaid.val("");
                cashPaid.val("");
                cashPaid.removeAttr('disabled');
                transferPaid.focusin();
                  $('#changeValue').val(0);
                return false;
            }

            value = parseInt(paidAmount.val()) - transferPaid.autoNumeric('get',transferPaid.val());
            transferPaidValue.val(transferPaid.autoNumeric('get',transferPaid.val()));

            if (value > 0) {
                // cashPaid.prop('disabled', true);
                cashPaid.autoNumeric('set',value);
                cashPaidValue.val(value)
                $('#changeValue').val(0);

            } else {
                 alert('nilai tidak boleh lebih besar');
                transferPaid.val(0);
                transferPaid.focus();
                $('#changeValue').val(value);
                cashPaid.val(0);
                // cashPaid.prop('disabled', true);
            }

        });
        
    }

function clearRateValue() {
    $('#rate').val('');
    $('#rateValue').val('');
}

function clearJumlahValue() {
    $('#jumlah').val('');
}


function clearRateValueBuy() {
    $('#rateBuy').val('');
    $('#rateValueBuy').val('');
}

function clearJumlahValueBuy() {
    $('#jumlahBuy').val('');
}

function rateChange() {
    // body...
      $('#rateValue').val($('#rate').val());
}
</script>
<hr style="margin:5px;">
<table>
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<div class="row">
    <div class="col-md-12">
            <div class="box-body table-responsive">
                <div id='jqxWidget'>
                    <div id="jqxgridDetail"></div>
                    <div>
                        <div id="cellbegineditevent"></div>
                        <div id="cellendeditevent"></div>
                    </div>
                </div>
            </div>
    </div>
</div>

<?php $this->load->view('transaksi/modal_view');?>
<script src="<?php echo base_url(); ?>/assets/js/transaksi/updateStatus.js" type="text/javascript"></script>