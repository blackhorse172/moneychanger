<?php $this->load->view('jqwidgetslink'); ?>

<script type="text/javascript">
    var jQuery_1_4_3 = $.noConflict(true);
     var base_url = "<?php echo base_url(); ?>";
    jQuery_1_4_3(document).ready(function () {


        

        // prepare the data
        var url = "<?php echo site_url('adjustment/get_data'); ?>";
        var url_dd_curr = "<?php echo site_url('transaksi/get_data_curr'); ?>";
        var source =
                {
                    datatype: "json",
                    addRow: function (rowID, rowData, position, commit) {
                        // synchronize with the server - send insert command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                        commit(true);
                    },
                    updaterow: function (rowid, rowdata, commit) {
                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                    deleteRow: function (rowID, commit) {
                        // synchronize with the server - send delete command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        commit(true);
                    },
                    datafields:
                            [
                                {name: 'Iid', type: 'string'},
                                {name: 'AdjustmentType', type: 'string'},
                                {name: 'AdjustmentIid', type: 'string'},
                                {name: 'CurrencyId', type: 'string'},
                                {name: 'CURR_CODE', type: 'string'},
                                {name: 'Quantity', type: 'number'},
                                {name: 'TransDate', type: 'date'},
                                {name: 'Remarks', type: 'string'},
                                {name: 'InputBy', type: 'string'},
                                {name: 'InputDate', type: 'date'},
                                {name: 'CompanyId', type: 'string'},
                                {name: 'BranchId', type: 'string'},
                                {name: 'AccountNo', type: 'string'},
                            ],
                    id: 'Iid',
                    url: url,
                    root: 'data'
                };
         var source_dd_curr =
                {
                    datatype: "json",
                    datafields:
                            [
                                {name: 'REF_CURR_ID', type: 'number'},
                                {name: 'CURR_CODE', type: 'string'}
                            ],
                    url: url_dd_curr,
                    async: true
                };
        var dataAdapter             = new jQuery_1_4_3.jqx.dataAdapter(source);
        var dataAdapter_dd_curr     = new jQuery_1_4_3.jqx.dataAdapter(source_dd_curr);

         jQuery_1_4_3('#Valas').jqxDropDownList({
                                      filterable: true,
                                      source: dataAdapter_dd_curr,
                                      displayMember: 'CURR_CODE',
                                      valueMember: 'CURR_CODE',
                                      disabled : false,
                                      selectedIndex: -1
        });

        // initialize jqxGrid
        jQuery_1_4_3("#jqxgrid").on('bindingcomplete', function () {
            // jQuery_1_4_3("#jqxgrid").jqxGrid('autoresizecolumns');
        });
        jQuery_1_4_3("#jqxgrid").jqxGrid(
                {
                    width:1000,
                    //height: 430,
                    //width: 100,
                    source: dataAdapter,
                    pageable: true,
                    editable: true,
                    showfilterrow: true,
                    filterable: true,
                    altRows: true,
                    selectionmode: 'singlerow',
                    editmode: 'programmatic',
                    // columnsresize: true,
                    // autowidth: true,
//                    autoheight: true,
                    sortable: true,
                    showtoolbar: true,
                    renderToolbar: function (toolBar)
                    {
                        var toTheme = function (className) {
                            if (theme == "")
                                return className;
                            return className + " " + className + "-" + theme;
                        }

                        var container = jQuery_1_4_3("<div style='overflow: hidden; position: relative; height: 100%; width: 100%;'></div>");
                        var buttonTemplate = "<div style='float: left; padding: 3px; margin: 2px;'><div style='margin: 4px; width: 16px; height: 16px;'></div></div>";
                        var addButton = jQuery_1_4_3(buttonTemplate);
                        var editButton = jQuery_1_4_3(buttonTemplate);
                        var deleteButton = jQuery_1_4_3(buttonTemplate);
                        var cancelButton = jQuery_1_4_3(buttonTemplate);
                        var updateButton = jQuery_1_4_3(buttonTemplate);
                        container.append(addButton);
                        // container.append(updateButton);
                        // container.append(editButton);
                        // container.append(deleteButton);
                        // container.append(cancelButton);
                        toolBar.append(container);
                        addButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        addButton.find('div:first').addClass(toTheme('jqx-icon-plus'));
                        addButton.jqxTooltip({position: 'bottom', content: "Add"});
                        editButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        editButton.find('div:first').addClass(toTheme('jqx-icon-edit'));
                        editButton.jqxTooltip({position: 'bottom', content: "Edit"});
                        deleteButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        deleteButton.find('div:first').addClass(toTheme('jqx-icon-delete'));
                        deleteButton.jqxTooltip({position: 'bottom', content: "Delete"});
                        updateButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        updateButton.find('div:first').addClass(toTheme('jqx-icon-save'));
                        updateButton.jqxTooltip({position: 'bottom', content: "Save Changes"});
                        cancelButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        cancelButton.find('div:first').addClass(toTheme('jqx-icon-cancel'));
                        cancelButton.jqxTooltip({position: 'bottom', content: "Cancel"});
                        var updateButtons = function (action) {
                            switch (action) {
                                case "Select":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: false});
                                    editButton.jqxButton({disabled: false});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                                case "Unselect":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: true});
                                    editButton.jqxButton({disabled: true});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                                case "Edit":
                                    addButton.jqxButton({disabled: true});
                                    deleteButton.jqxButton({disabled: true});
                                    editButton.jqxButton({disabled: true});
                                    cancelButton.jqxButton({disabled: false});
                                    updateButton.jqxButton({disabled: false});
                                    break;
                                case "End Edit":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: false});
                                    editButton.jqxButton({disabled: false});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                            }
                        };
                        var rowKey = null;
                        jQuery_1_4_3("#jqxgrid").on('rowselect', function (event) {
                            if (cancelButton.jqxButton('disabled') || updateButton.jqxButton('disabled')) {
                                var args = event.args;
                                rowKey = args.rowindex;
                                var rowData = args.row;
                                updateButtons('Select');
                            }
                        });
                        addButton.click(function (event) {
                            // add form //
                           

                           $('#adjustmentType').on('change', function(event) {
                               /* Act on the event */
                               if ($('#adjustmentType').val() == 'bankaccount') {
                                                                    $('#jumlahGroup label').html('Jumlah');

                                    $('#bankAccountGroup').show('slow/400/fast');
                                    $('#valasGroup').hide('slow/400/fast');
                                    $('#pajakGroup').hide('slow/400/fast');
                                    $('#bungaGroup').hide('slow/400/fast');
                                    var bankAccount = "<?php echo site_url('transaksi/get_bank_account'); ?>";

                                                                           var source_bank_account =
                                                                                    {
                                                                                        datatype: "json",
                                                                                        datafields:
                                                                                                [
                                                                                                    {name: 'AccountNo', type: 'string'},
                                                                                                    {name: 'BankName', type: 'string'},
                                                                                                    {name: 'BankNameView', type: 'string'}
                                                                                                ],
                                                                                        url: bankAccount,
                                                                                        async: true
                                                                                    };
                                                                          
                                                                            var dataAdapter_bank_account = new jQuery_1_4_3.jqx.dataAdapter(source_bank_account);


                                                                          jQuery_1_4_3('#bankAccount').jqxDropDownList({
                                                                            filterable: true,
                                                                            source: dataAdapter_bank_account,
                                                                            displayMember: 'BankNameView',
                                                                            valueMember: 'AccountNo',
                                                                            selectedIndex: -1
                                                                        });
                                   
                                        jQuery_1_4_3('#Valas').jqxDropDownList('selectedIndex',-1);
                               }

                               if ($('#adjustmentType').val() == 'currency') {
                                // clearAdjustmentForm();
                                    $('#valasGroup').show('slow/400/fast');
                                     $('#bankAccountGroup').hide('slow/400/fast');
                                      
                                    $('#pajakGroup').hide('slow/400/fast');
                                    $('#bungaGroup').hide('slow/400/fast');


                                   
                                    $('#jumlahGroup label').html('Jumlah');

                                    

                                        jQuery_1_4_3('#bankAccount').jqxDropDownList('selectedIndex',-1);
                               }

                                if($('#adjustmentType').val() == 'biayadanbunga') {
                                     // clearAdjustmentForm();
                                    $('#pajakGroup').show('slow/400/fast');
                                    $('#bungaGroup').show('slow/400/fast');
                                    $('#valasGroup').hide('slow/400/fast');
                                     $('#bankAccountGroup').show('slow/400/fast');

                                 
                                    $('#jumlahGroup label').html('Biaya Bank');

                                        jQuery_1_4_3('#Valas').jqxDropDownList('selectedIndex',-1);

                                            var bankAccount = "<?php echo site_url('transaksi/get_bank_account'); ?>";

                                                                           var source_bank_account =
                                                                                    {
                                                                                        datatype: "json",
                                                                                        datafields:
                                                                                                [
                                                                                                    {name: 'AccountNo', type: 'string'},
                                                                                                    {name: 'BankName', type: 'string'},
                                                                                                    {name: 'BankNameView', type: 'string'}
                                                                                                ],
                                                                                        url: bankAccount,
                                                                                        async: true
                                                                                    };
                                                                          
                                                                            var dataAdapter_bank_account = new jQuery_1_4_3.jqx.dataAdapter(source_bank_account);


                                                                          jQuery_1_4_3('#bankAccount').jqxDropDownList({
                                                                            filterable: true,
                                                                            source: dataAdapter_bank_account,
                                                                            displayMember: 'BankNameView',
                                                                            valueMember: 'AccountNo',
                                                                            selectedIndex: -1
                                                                        });

                               }

                               if ($('#adjustmentType').val() == '' || $('#adjustmentType').val() == 'cash' ) {
                                  // clearAdjustmentForm();
                                  $('#Quantity').removeAttr('readonly');
                                    $('#valasGroup').hide('slow/400/fast');
                                     $('#bankAccountGroup').hide('slow/400/fast');
                                          $('#pajakGroup').hide('slow/400/fast');
                                    $('#bungaGroup').hide('slow/400/fast');
                                    

                                        jQuery_1_4_3('#Valas').jqxDropDownList('selectedIndex',-1);
                                        jQuery_1_4_3('#bankAccount').jqxDropDownList('selectedIndex',-1);

                               }

                           });

                            

                            $('#adjustmentModal').modal({backdrop: 'static', keyboard: false});
                            clearAdjustmentForm();

                            $('#adjustmentModal').modal('show');
                        });
                        cancelButton.click(function (event) {
                            if (!cancelButton.jqxButton('disabled')) {
                                jQuery_1_4_3("#jqxgrid").jqxGrid('endrowedit', rowKey, true); // if true, the changes are canceled.
                                editrow = -1;
                                updateButtons('Unselect');

                                var rowindex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindex');
                                jQuery_1_4_3('#jqxgrid').jqxGrid('unselectrow', rowindex);
                                jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                            }
                        });
                        updateButton.click(function (event) {//save changes
                            if (!updateButton.jqxButton('disabled')) {
                                jQuery_1_4_3("#jqxgrid").jqxGrid('endrowedit', rowKey, false); //is false, the changes are saved
                                var datarow;
                                var selectedrowindex = jQuery_1_4_3("#jqxgrid").jqxGrid('getselectedrowindex');
                                var rowscount = jQuery_1_4_3("#jqxgrid").jqxGrid('getdatainformation').rowscount;

                                editrow = -1;
                                updateButtons('Unselect');

                                jQuery_1_4_3('#jqxgrid').jqxGrid('unselectrow', selectedrowindex);

                                var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                var field = rows[selectedrowindex];
//                                alert(field.LevelCode);
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>company/update_data",
                                    cache: false,
                                    data: {iid: field.Iid, nama: field.Name, level: field.Level, activestat: field.IsAcvive}, // since, you need to delete post of particular id
                                    success: function (reaksi) {
                                        if (reaksi === '1') {
                                            if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                                                var id = jQuery_1_4_3("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                                                var commit = jQuery_1_4_3("#jqxgrid").jqxGrid('updaterow', id, datarow);
                                            }
                                        } else {
                                            alert(reaksi);
                                        }
                                    }
                                });
                                jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                            }
                        });
                        editButton.click(function () {
                            if (!editButton.jqxButton('disabled')) {
                                clearAdjustmentForm();
                                  $('#addCompanyModal').modal({backdrop: 'static', keyboard: false}); 

                                $('#addCompanyModal').modal('show');

                                var datarow;
                                var selectedrowindex = jQuery_1_4_3("#jqxgrid").jqxGrid('getselectedrowindex');
                                var rowscount = jQuery_1_4_3("#jqxgrid").jqxGrid('getdatainformation').rowscount;

                                var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                var field = rows[selectedrowindex];

                                $('#Iid').val(field.Iid);
                                $('#Name').val(field.Name);

                              
                                if (field.IsActive == "1") {
                                  $('#parentActive span').addClass('checked');
                                } else {
                                    //$('#active').prop('checked', 'true');
                                      $('#parentDisactive span').addClass('checked');
                                }

                                jQuery_1_4_3('#companyLevel').jqxDropDownList({
                                 filterable: true,
                                   source: dataAdapter_dd_level,
                                    displayMember: 'CUST_LEVEL_CODE',
                                    valueMember: 'REF_CUST_LEVEL_ID'
                              });

                                jQuery_1_4_3("#companyLevel").jqxDropDownList('selectItem',"'"+field.Level+"'");


                               jQuery_1_4_3('#companyLevel').on('change', function (event) {
                                   var args = event.args;
                                   if (args) {
                                       // index represents the item's index.                          
                                       var index = args.index;
                                       var item = args.item;
                                       // get item's label and value.
                                       var label = item.label;
                                       var value = item.value;
                                        $('#Level').val(value);
                                   }
                               });
                            companyId = field.Name;
                               //bankAccount
                                  $.ajax({
                                     type: "POST",
                                     url: base_url + "company/get_data_bankAccount/" + companyId,
                                     cache: false,
                                     dataType: "json",
                                     data: {companyId : companyId}, // since, you need to delete post of particular id
                                     success: function(data) {

                                        $.each(data, function(index, val) {
                                             /* iterate through array or object */
                                            $('#rowTable').append('<tr class=rowCompanyBank><td class=BankName><input type=hidden name=BankName[] id=BankName value='+val.BankName+'>'+val.BankName+'</td><td><input type=hidden name=AccountNo[] id=AccountNo value='+val.AccountNo+'>'+val.AccountNo+'</td><td><input type=hidden name=AccountName[] id=AccountName value='+val.AccountName+'>'+val.AccountName+'</td><td><input type=hidden name=AccountAddress[] id=AccountAddress value='+val.AccountAddress+'>'+val.AccountAddress+'</td><td><a href=javascript:void(0) class=removeTransaction><div class=icon-close></div></a></td></tr>');
                                        });
                                       
                                     }
                                     
                                 });


                                $('#addCompanyModal').modal('show');

                            }
                        });
                        deleteButton.click(function () {
                            if (!deleteButton.jqxButton('disabled')) {
                                var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                var selectedrowindex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindex');
                                var field = rows[selectedrowindex];
                                if (field.Name === '') {
                                    var id = jQuery_1_4_3("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                                    jQuery_1_4_3('#jqxgrid').jqxGrid('deleterow', id);
                                } else {
                                    if (confirm("Anda yakin menghapus data dengan Nama '" + field.Name + "'?")) {
                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url() ?>company/delete_data",
                                            cache: false,
                                            data: {iid: field.Iid}, // since, you need to delete post of particular id
                                            success: function (reaksi) {
                                                if (reaksi == '1') {
                                                    var id = jQuery_1_4_3("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                                                    jQuery_1_4_3('#jqxgrid').jqxGrid('deleterow', id);
                                                } else {
                                                    alert(reaksi);
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        });
                        jQuery_1_4_3("#jqxgrid").on('endrowedit', function (event) {
                            updateButtons('End Edit');
                        });
                    },
                    columns: [
                        {text: 'Iid', columntype: 'textbox', datafield: 'Iid', width: 70, hidden: true},
                        {text: 'Tipe Adjustment', columntype: 'textbox', datafield: 'AdjustmentType', width: 110},
                        {text: 'Adjustment Id', datafield: 'AdjustmentIid', width: 110} ,
                        {text: 'Jumlah', columntype: 'textbox',cellsformat: "f2", datafield: 'Quantity', width: 210},
                        {text: 'Tanggal Transaksi', columntype: 'datafield', datafield: 'TransDate', width: 110, cellsformat: 'd/M/yyyy'},
                        {text: 'Catatan', columntype: 'textbox', datafield: 'Remarks', width: 210},
                        {text: 'Valas', columntype: 'textbox', datafield: 'CURR_CODE', width: 210},
                        {text: 'Nomor Akun', columntype: 'textbox', datafield: 'AccountNo', width: 110}
                    ]
                });
    }
    );

function storeAdjustment() {
        var BankAccount = jQuery_1_4_3("#bankAccount").jqxDropDownList('getSelectedItem');
        var BankName = jQuery_1_4_3("#bankAccount").text();
        var Valas = jQuery_1_4_3("#Valas").jqxDropDownList('getSelectedItem');

        if (Valas == null || Valas == undefined) {
            Valas = '';
        } else Valas = Valas.label;

        if (BankAccount == null || BankAccount == undefined) {
            BankAccount = ''
        } else BankAccount = BankAccount.value;

    
        if ($('#adjustmentType').val() == '') {
            alert('Tipe Adjustment harus di isi');
            $('#adjustmentType').focus();
            return false;
        }

        if ($('#Quantity').val() == '') {
            alert('Jumlah harus di isi');
              $('#Quantity').focus();
            return false;
        }

         if ($('#Remarks').val() == '') {
            alert('Catatan harus di isi');
             $('#Remarks').focus();
            return false;
        }

         if ($('#TransDate').val() == '') {
            alert('Tanggal harus di isi');
             $('#TransDate').focus();
            return false;
        }

         $.ajax({
         type: "POST",
         url: base_url + "adjustment/store",
         cache: false,
         dataType: "json",
         data: $('#adjustmentForm').serialize()+"&CurrCode=" + Valas + "&AccountNo=" + BankAccount + "&BankName=" + BankName, // since, you need to delete post of particular id
         success: function(data) {
             if (data.status == 200) {
                 jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                 $('#adjustmentModal').modal('hide');
                 $('#companyForm').closest('form').find("input[type=text], textarea,input[type=date],input[type=hidden]").val("");
             }

         }
     });
 }


 function clearAdjustmentForm() {
     // body...
        $('#adjustmentForm').closest('form').find("input[type=text],input[type=number],input[type=hidden],select,textarea").val("");  
        $('#bankAccountGroup').hide('slow/400/fast');
         jQuery_1_4_3("#bankAccount").jqxDropDownList('getSelectedIndex',-1);
             $('#parentActive span').removeClass('checked');
                            $('#parentDisactive span').removeClass('checked'); 

 }
 
 function checkCompanyCode() {
    $.ajax({
         type: "POST",
         url: base_url + "company/checkCompanyCode",
         cache: false,
         dataType: "json",
         data: $('#companyForm').serialize(), // since, you need to delete post of particular id
         success: function(data) {
             if (data.status == 400) {
                 alert('Kode Company Ada yang sama');
                 $('#Name').val('');
                 $('#Name').focus();
                 //jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                 //$('#addCustomerModal').modal('hide');
                 //$('#customerForm').closest('form').find("input[type=text], textarea,input[type=date],input[type=hidden]").val("");

                 //window.location.reload();
             }
        }
    });
 }

 function addRowBankAccount() {
    // body...
    var bool = false;
    var BankName = $('#BankName');
    var AccountNo = $('#AccountNo');
    var AccountName = $('#AccountName');
    var AccountAddress = $('#AccountAddress');

    $('.rowCompanyBank').each(function(index, el) {
           if (BankName.val() == $('.rowCompanyBank .BankName').eq(index).text()) {
                bool = true;
           }     
    });

    if (bool) {
        alert('Tidak bisa menambah valas yang sama!');
        return false;
    }

    if ($('#BankName').val() == '') {
        alert('Nama Bank Harus Di isi');
        return false;
    }

    if ($('#AccountNo').val() == '') {
            alert('Akun No Harus Di isi');
            return false;
        }

    if ($('#AccountName').val() == '') {
            alert('Nama akun Harus Di isi');
            return false;
    }

    if ($('#AccountAddress').val() == '') {
            alert('Alamat akun Harus Di isi');
            return false;
    }


    $('#rowTable').append('<tr class=rowCompanyBank><td class=BankName><input type=hidden name=BankName[] id=BankName value='+BankName.val()+'>'+BankName.val()+'</td><td><input type=hidden name=AccountNo[] id=AccountNo value='+AccountNo.val()+'>'+AccountNo.val()+'</td><td><input type=hidden name=AccountName[] id=AccountName value='+AccountName.val()+'>'+AccountName.val()+'</td><td><input type=hidden name=AccountAddress[] id=AccountAddress value='+AccountAddress.val()+'>'+AccountAddress.val()+'</td><td><a href=javascript:void(0) class=removeTransaction><div class=icon-close></div></a></td></tr>');

    // if ( $('#rowTable tr').length > 0 ) {
    //      jQuery_1_4_3("#customerId").jqxDropDownList({disabled : true});
    //      document.getElementById("buy").disabled = true;
    //      document.getElementById("sell").disabled = true;
    //      $('#date').attr('readonly', true);
    // }

}

</script>
<h3 class="page-title">
    <?php echo $pageform ?></h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="index.html">Pengaturan</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Company</a>
        </li>
    </ul>
</div>
<table>
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<div class="row">
       <div class="col-md-12">
        <div class='box'>
            <div class="box-body table-responsive">
                <div id='jqxWidget'>
                    <div id="jqxgrid"></div>
                    <div>
                        <div id="cellbegineditevent"></div>
                        <div id="cellendeditevent"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php $this->load->view('adjustment/modal_view');?>
