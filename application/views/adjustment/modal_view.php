		<div class="modal fade" id="adjustmentModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Adjustment</h4>
					</div>
					<div class="modal-body">
						<form id="adjustmentForm" role="form">
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Tipe Adusjment</label>
								<div class="col-sm-10">
									<select name="AdjustmentType" id="adjustmentType"  required class="form-control">
										<option value=""></option>
										<option value="bankaccount">Bank Akun</option>
										<option value="cash">Cash</option>
										<option value="currency">Valas</option>
										<option value="biayadanbunga">Biaya Pajak,Bunga & Admin</option>
									</select>
								</div>
							</div>

							<div class="form-group row" id="bankAccountGroup" style="display:none;">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Bank Akun</label>
								<div class="col-sm-10">
									<div id="bankAccount"></div>
								</div>
							</div>

							<div class="form-group row" id="valasGroup" style="display:none;">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Valas</label>
								<div class="col-sm-10">
								<!-- <select class="form-control" id="Valas" name="Valas">	
								</select> -->
								<div id="Valas"></div>
								</div>
							</div>

							<div class="form-group row" id="pajakGroup" style="display:none;">
								<label for="inputEmail3" class="col-sm-2 form-control-label">Pajak Bunga</label>
								<div class="col-sm-10">
								<input type="text" name="Pajak" value="" class="auto form-control" placeholder="">
								</div>		
							</div>

							<div class="form-group row" id="bungaGroup" style="display:none;">	
								<label for="inputEmail3" class="col-sm-2 form-control-label">Pendapatan Bunga</label>
								<div class="col-sm-10">
								<input type="text" name="Bunga" value="" class="auto form-control" placeholder="">
								</div>
										
							</div>

							<div class="form-group row" id="jumlahGroup">
								<label for="inputtext3" class="col-sm-2 form-control-label">Jumlah</label>
								<div class="col-sm-10">
									<div id=""></div>
									<input type="number" class="form-control" id="Quantity" name="Quantity">
								</div>
							</div>

							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Tanggal</label>
								<div class="col-sm-10">									
									<input type="date" class="form-control" id="TransDate" name="TransDate" value="<?php echo date('Y-m-d');?>">
								</div>
							</div>


							<div class="form-group row">
								<label for="inputtext3" class="col-sm-2 form-control-label">Catatan</label>
								<div class="col-sm-10">
									<div id=""></div>
									<textarea name="Remarks" class="form-control" rows="10"></textarea>
								</div>
							</div>

							
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" onclick="clearAdjustmentForm()" data-dismiss="modal">Batal</button>
						<button type="button" class="btn btn-primary" onclick="storeAdjustment()">Simpan</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

<style type="text/css" media="screen">
.fullWidth {
	width: 75%;
}	
</style>