<?php $this->load->view('jqwidgetslink'); ?>

<script type="text/javascript">
    var jQuery_1_4_3 = $.noConflict(true);
     var base_url = "<?php echo base_url(); ?>";
    jQuery_1_4_3(document).ready(function () {

        // prepare the data
//        var url = "http://localhost/smartdeal/akunting/get_data";
        var url = "<?php echo site_url('bankAccount/get_data'); ?>";
        var url_master_code = "<?php echo site_url('bankAccount/get_data_code'); ?>";
        var source =
                {
                    datatype: "json",
                    addRow: function (rowID, rowData, position, commit) {
                        // synchronize with the server - send insert command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                        commit(true);
                    },
                    updaterow: function (rowid, rowdata, commit) {
                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                    deleteRow: function (rowID, commit) {
                        // synchronize with the server - send delete command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        commit(true);
                    },
                    datafields:
                            [
                                {name: 'Iid', type: 'string'},
                                {name: 'Kode', type: 'string'},
                                {name: 'Jenis', type: 'string'},
                                {name: 'Nama', type: 'string'},
                                {name: 'Remarks', type: 'string'},
                                {name: 'Amount', type: 'number'},
                                {name: 'IsFilter', type: 'string'},
                                {name: 'InputBy', type: 'string'},
                                {name: 'InputDate', type: 'date'},
                                {name: 'LastEditBy', type: 'string'},
                                {name: 'LastEditDate', type: 'date'},
                                {name: 'Transdate', type: 'date'},
                                {name: 'Debit', type: 'number'},
                                {name: 'Kredit', type: 'number'},
                                {name: 'Accounting_Iid', type: 'number'},
                                {name: 'AccountNo', type: 'number'}

                            ],
                    id: 'Iid',
                    url: url,
                    root: 'data'
                };
                  var source_data_code =
                {
                    datatype: "json",
                    datafields:
                            [
                                {name: 'Code', type: 'string'},
                                {name: 'Name', type: 'string'},
                                {name: 'RES', type: 'string'},
                                {name: 'Iid', type: 'string'},
                            ],
                    url: url_master_code,
                    async: true
                };
        var dataAdapter = new jQuery_1_4_3.jqx.dataAdapter(source);
        var dataCodeAdapter = new jQuery_1_4_3.jqx.dataAdapter(source_data_code);
        // initialize jqxGrid
        jQuery_1_4_3("#jqxgrid").on('bindingcomplete', function () {
            // jQuery_1_4_3("#jqxgrid").jqxGrid('autoresizecolumns');
        });
        jQuery_1_4_3("#jqxgrid").jqxGrid(
                {
                    width: 1000,
                    height: 430,
                    source: dataAdapter,
                    pageable: true,
                    showfilterrow: true,
                    filterable: true,
                    altRows: true,
                    selectionmode: 'singlerow',
                    editmode: 'programmatic',
                    sortable: true,
                    showtoolbar: true,
                

                    renderToolbar: function (toolBar)
                    {
                        var toTheme = function (className) {
                            if (theme == "")
                                return className;
                            return className + " " + className + "-" + theme;
                        }

                        var container = jQuery_1_4_3("<div style='overflow: hidden; position: relative; height: 100%; width: 100%;'></div>");
                        var buttonTemplate = "<div style='float: left; padding: 3px; margin: 2px;'><div style='margin: 4px; width: 16px; height: 16px;'></div></div>";
                        // var addButton = jQuery_1_4_3(buttonTemplate);
                        // var editButton = jQuery_1_4_3(buttonTemplate);
                        // var deleteButton = jQuery_1_4_3(buttonTemplate);
                        // var cancelButton = jQuery_1_4_3(buttonTemplate);
                        // var updateButton = jQuery_1_4_3(buttonTemplate);
                        container.append(addButton);
                        container.append(updateButton);
                        container.append(editButton);
                        container.append(deleteButton);
                        container.append(cancelButton);
                        toolBar.append(container);
                        addButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        addButton.find('div:first').addClass(toTheme('jqx-icon-plus'));
                        addButton.jqxTooltip({position: 'bottom', content: "Add"});
                        editButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        editButton.find('div:first').addClass(toTheme('jqx-icon-edit'));
                        editButton.jqxTooltip({position: 'bottom', content: "Edit"});
                        deleteButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        deleteButton.find('div:first').addClass(toTheme('jqx-icon-delete'));
                        deleteButton.jqxTooltip({position: 'bottom', content: "Delete"});
                        updateButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        updateButton.find('div:first').addClass(toTheme('jqx-icon-save'));
                        updateButton.jqxTooltip({position: 'bottom', content: "Save Changes"});
                        cancelButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        cancelButton.find('div:first').addClass(toTheme('jqx-icon-cancel'));
                        cancelButton.jqxTooltip({position: 'bottom', content: "Cancel"});
                        var updateButtons = function (action) {
                            switch (action) {
                                case "Select":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: false});
                                    editButton.jqxButton({disabled: false});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                                case "Unselect":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: true});
                                    editButton.jqxButton({disabled: true});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                                case "Edit":
                                    addButton.jqxButton({disabled: true});
                                    deleteButton.jqxButton({disabled: true});
                                    editButton.jqxButton({disabled: true});
                                    cancelButton.jqxButton({disabled: false});
                                    updateButton.jqxButton({disabled: false});
                                    break;
                                case "End Edit":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: false});
                                    editButton.jqxButton({disabled: false});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                            }
                        };
                        var rowKey = null;
                        jQuery_1_4_3("#jqxgrid").on('rowselect', function (event) {
                            if (cancelButton.jqxButton('disabled') || updateButton.jqxButton('disabled')) {
                                var args = event.args;
                                rowKey = args.rowindex;
                                var rowData = args.row;
                                updateButtons('Select');
                            }
                        });
                        addButton.click(function (event) {
                        jQuery_1_4_3('#code').jqxDropDownList({
                                filterable: true,
                                source: dataCodeAdapter,
                                displayMember: 'RES',
                                valueMember: 'Iid',
                                width: '100%'
                            });
                             jQuery_1_4_3('#code').jqxDropDownList('selectIndex', -1);
                             
                            clearCostForm();
                            $('#addCostModal').modal({backdrop: 'static', keyboard: false});
                            $('#parentActive span').removeClass('checked');
                            $('#parentDisactive span').removeClass('checked');




                            jQuery_1_4_3('#code').bind('change', function (event) {
                                 var args = event.args;
                                   if (args) {
                                       // index represents the item's index.                          
                                       var index = args.index;
                                       var item = args.item;
                                       // get item's label and value.
                                       var label = item.label;
                                       var value = item.value;
                                        $('#AccountingIid').val(value);
                                        // $('#Nama').val(label);

                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url() ?>bankAccount/get_name_code",
                                        cache: false,
                                        dataType: 'json',
                                        data: {code: value},
                                         // since, you need to delete post of particular id
                                        success: function (data) {
                                            if (data.status == 200) {
                                               $('#Nama').val(data.items.Name);
                                               $('#Kode').val(data.items.Code);

                                            } else {
                                                
                                            }
                                        }
                                    });

                                        
                                   }
                            });


                            $('#addCostModal').modal('show');
                            $('#FormType').val('add');
                        });
                        cancelButton.click(function (event) {
                            if (!cancelButton.jqxButton('disabled')) {
                                jQuery_1_4_3("#jqxgrid").jqxGrid('endrowedit', rowKey, true); // if true, the changes are canceled.
                                editrow = -1;
                                updateButtons('Unselect');

                                var rowindex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindex');
                                jQuery_1_4_3('#jqxgrid').jqxGrid('unselectrow', rowindex);
                                jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                            }
                        });
                        updateButton.click(function (event) {//save changes
                            if (!updateButton.jqxButton('disabled')) {
                                jQuery_1_4_3("#jqxgrid").jqxGrid('endrowedit', rowKey, false); //is false, the changes are saved
                                var datarow;
                                var selectedrowindex = jQuery_1_4_3("#jqxgrid").jqxGrid('getselectedrowindex');
                                var rowscount = jQuery_1_4_3("#jqxgrid").jqxGrid('getdatainformation').rowscount;

                                editrow = -1;
                                updateButtons('Unselect');

                                jQuery_1_4_3('#jqxgrid').jqxGrid('unselectrow', selectedrowindex);

                                var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                var field = rows[selectedrowindex];
//                                alert(field.LevelCode);
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>bankAccount/update_data",
                                    cache: false,
                                    data: {iid: field.Iid, nama: field.Name, level: field.Level, activestat: field.IsAcvive}, // since, you need to delete post of particular id
                                    success: function (reaksi) {
                                        if (reaksi === '1') {
                                            if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                                                var id = jQuery_1_4_3("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                                                var commit = jQuery_1_4_3("#jqxgrid").jqxGrid('updaterow', id, datarow);
                                            }
                                        } else {
                                            alert(reaksi);
                                        }
                                    }
                                });
                                jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                            }
                        });
                        editButton.click(function () {
                            if (!editButton.jqxButton('disabled')) {
                                
                                clearCostForm();
                                  $('#addCostModal').modal({backdrop: 'static', keyboard: false}); 


                                var datarow;
                                var selectedrowindex = jQuery_1_4_3("#jqxgrid").jqxGrid('getselectedrowindex');
                                var rowscount = jQuery_1_4_3("#jqxgrid").jqxGrid('getdatainformation').rowscount;

                                var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                var field = rows[selectedrowindex];

                                $('#Iid').val(field.Iid);
                                $('#Kode').val(field.Kode);
                                $('#Jenis').val(field.Jenis);
                                $('#Nama').val(field.Nama);
                                $('#Amount').val(field.Amount);
                                $('#TransDate').val(field.TransDate);
                                $('#Remarks').val(field.Remarks);

                                $('#addCostModal').modal('show');
                            }
                        });
                        deleteButton.click(function () {
                            if (!deleteButton.jqxButton('disabled')) {
                                var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                var selectedrowindex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindex');
                                var field = rows[selectedrowindex];
                                if (field.Name === '') {
                                    var id = jQuery_1_4_3("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                                    jQuery_1_4_3('#jqxgrid').jqxGrid('deleterow', id);
                                } else {
                                    if (confirm("Anda yakin menghapus data dengan Nama '" + field.Jenis + "'?")) {
                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url() ?>bankAccount/delete_data",
                                            cache: false,
                                            data: {iid: field.Iid}, // since, you need to delete post of particular id
                                            success: function (reaksi) {
                                                if (reaksi == '1') {
                                                    var id = jQuery_1_4_3("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                                                    jQuery_1_4_3('#jqxgrid').jqxGrid('deleterow', id);
                                                } else {
                                                    alert(reaksi);
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        });
                        jQuery_1_4_3("#jqxgrid").on('endrowedit', function (event) {
                            updateButtons('End Edit');
                        });
                    },
                    columns: [
                        {text: 'Iid', columntype: 'textbox', datafield: 'Iid', width: 170, hidden: true},
                        {text: 'Kode', columntype: 'textbox', datafield: 'Kode', width: 70},
                        {text: 'No Akun', columntype: 'textbox', datafield: 'AccountNo', width: 70},
                        {text: 'Nama', columntype: 'textbox', datafield: 'Nama', width: 210},
                        {text: 'Jenis', columntype: 'textbox', datafield: 'Jenis', width: 110},
                        {text: 'Debit', columntype: 'textbox', datafield: 'Debit',cellsformat: "f2", width: 110},
                        {text: 'Kredit', columntype: 'textbox', datafield: 'Kredit',cellsformat: "f2", width: 110},
                        // {text: 'Amount', columntype: 'textbox', datafield: 'Amount', cellsformat: "f2", width: 210},
                        // {text: '', columntype: 'textbox', datafield: 'Remarks', width: 210},
                        {text: 'Tanggal Transaksi', columntype: 'date', datafield: 'Transdate', width: 210,cellsformat: 'd/M/yyyy'} 
                       ] 
                });
    }
    );

</script>
<h3 class="page-title">
    <?php echo $pageform ?></h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="index.html">Pengaturan</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Cost</a>
        </li>
    </ul>
</div>
<table>
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<div class="row">
       <div class="col-md-12">
                    <div id="jqxgrid"></div>
    </div>
</div>