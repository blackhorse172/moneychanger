<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="">
                <a href="<?php echo base_url(); ?>beranda">
                    <i class="icon-home"></i>
                    <span class="title">Beranda</span>
                </a>
            </li>
            <li class="">
                <a href="<?php echo base_url(); ?>customer">
                    <i class="icon-book-open"></i>
                    <span class="title">Customer</span>
                </a>
            </li>
            <li class="">
                <a href="<?php echo base_url(); ?>rate">
                    <i class="icon-bar-chart"></i>
                    <span class="title">Rate</span>
                </a>

            </li>
            <li class="">
                <a href="<?php echo base_url(); ?>transaksi">
                    <i class="icon-pencil"></i>
                    <span class="title">Transaksi</span>
                </a>

            </li>
            <li class="">
                <a href="<?php echo base_url(); ?>adjustment">
                    <i class="icon-plus"></i>
                    <span class="title">Adjustment</span>
                </a>

            </li>
            <li class="">
                <a href="<?php echo base_url(); ?>laporan/stock">
                    <i class="icon-wallet"></i>
                    <span class="title">Stok</span>
                </a>

            </li>
                  <li>
                <a href="javascript:;">
                                <i class="icon-docs"></i>
                                <span class="title">Administrasi</span>
                                <span class="arrow "></span>
                </a>
                            <ul class="sub-menu">
                                <li><a href="<?php echo base_url(); ?>employee"> Karyawan</a></li>
                                <li><a href="<?php echo base_url(); ?>cost"> Kas Kecil / Petty Cash</a></li>
                                <li>
                                <a href="javascript:;">
                                    Bank <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                        <li><a href="<?php echo base_url(); ?>bankAccount"> Detail Administrasi</a></li>
                                        <li><a href="<?php echo base_url(); ?>bankAccountMaster"> Akun</a></li>
                                       
                                    </ul>
                                </li>
                                <!-- <li><a href="<?php echo base_url(); ?>bankAccount"> Bank</a></li> -->
                              <!--   <li><a href="<?php echo base_url(); ?>laporan/rekapharian"> Rekap Trans. Harian</a></li>
                                <li><a href="<?php echo base_url(); ?>laporan/ppatk"> PPATK</a></li>
                                <li><a href="<?php echo base_url(); ?>laporan/neraca"> Neraca</a></li>
                                <li><a href="<?php echo base_url(); ?>laporan/bi"> BI</a></li>
                                <li><a href="<?php echo base_url(); ?>aruskas"> Arus KAS</a></li> -->
                                
                            </ul>
                        </li>
            <li>
                <a href="javascript:;">
                                <i class="icon-docs"></i>
                                <span class="title">Laporan</span>
                                <span class="arrow "></span>
                </a>
                            <ul class="sub-menu">
                                <li>
                                <a href="javascript:;">
                                    BI <span class="arrow"></span>
                                </a>
                                    <ul class="sub-menu">
                                        <li><a href="<?php echo base_url(); ?>laporan/bi"> Mingguan</a></li>
                                        <li><a href="<?php echo base_url(); ?>laporan/bulanan_index"> Bulanan</a></li>
                                        <li><a href="<?php echo base_url(); ?>laporan/labaRugi"> Laba Rugi</a></li>
                                        <li><a href="<?php echo base_url(); ?>laporanpajak/jualbeli"> Rekap Penjualan & Pembelian</a> </li>
                                        <li><a href="<?php echo base_url(); ?>laporanpajak/posisiKeuangan"> Laporan Posisi Keuangan BI</a> </li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo base_url(); ?>laporan/transaksi"> Transaksi</a></li>
                                <li><a href="<?php echo base_url(); ?>laporan/rekapharian"> Rekap Transaksi</a></li>
                                <li><a href="<?php echo base_url(); ?>laporan/ppatk"> PPATK</a></li>
                                <li><a href="<?php echo base_url(); ?>laporan/neraca"> Neraca</a></li>
                                <li><a href="<?php echo base_url(); ?>aruskas"> Arus KAS</a></li>
                                
                            </ul>
                        </li>
            <?php
            $arrdata = explode('|', $authmenu); //explode form
            for ($i = 0; $i < count($arrdata); $i++) {
                $arrauth = explode('::', $arrdata[$i]); //explode auth
                for ($j = 0; $j < 1; $j++) {
                    if ($arrauth[0] == 'ms_user' and $arrauth[2] == 'view') {// split form
                        ?>
                        <li>
                            <a href="javascript:;">
                                <i class="icon-settings"></i>
                                <span class="title">Pengaturan</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li><a href="<?php echo base_url(); ?>user/viewuser"><i class="icon-user-following"></i> User</a></li>
                                <li><a href="<?php echo base_url(); ?>company"><i class="icon-user-following"></i> Company</a></li>
                                <li><a href="<?php echo base_url(); ?>branch"><i class="icon-user-following"></i> Branch</a></li>
                            </ul>
                        </li>
                        <?php
                    }
                }
            }
            ?>
            <?php
            $arrdata = explode('|', $authmenu); //explode form
            for ($i = 0; $i < count($arrdata); $i++) {
                $arrauth = explode('::', $arrdata[$i]); //explode auth
                for ($j = 0; $j < 1; $j++) {
                    if ($arrauth[0] == 'ms_user' and $arrauth[2] == 'view') {// split form
                        ?>
                        <li>
                            <a href="javascript:;">
                                <i class="icon-settings"></i>
                                <span class="title">TRIAL</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li><a href="<?php echo base_url(); ?>testsp"><i class="icon-user-following"></i> Test Store Procedure</a></li>

                                <li><a href="<?php echo base_url(); ?>akunting_printbon"><i class="icon-user-following"></i> Terbilang</a></li>
                                <li><a href="<?php echo base_url(); ?>akunting_bonjualbeli/inv_no"><i class="icon-user-following"></i> Inv Auto number</a></li>
                            </ul>
                        </li>
                        <?php
                    }
                }
            }
            ?>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>