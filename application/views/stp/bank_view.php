<?php $this->load->view('jqwidgetslink'); ?>

<script type="text/javascript">
    var jQuery_1_4_3 = $.noConflict(true);
     var base_url = "<?php echo base_url(); ?>";
     var companyId = 'CI'
    jQuery_1_4_3(document).ready(function () {

          jQuery_1_4_3("#jqxgrid").on('rowselect', function (event) {
                                var args = event.args;
                                rowKey = args.rowindex;
                                var rowData = args.row;

                                var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                var field = rows[rowKey];
                                companyId = field.Name;

                                var tmpS = jQuery_1_4_3("#jqxgridBankAccount").jqxGrid('source');
                                    tmpS._source.url =base_url + "company/get_data_bankAccount/" + companyId;
                                    jQuery_1_4_3("#jqxgridBankAccount").jqxGrid('source', tmpS);
    });


         
        // prepare the data
//        var url = "http://localhost/smartdeal/akunting/get_data";
        var url_bankAccount =  base_url + "company/get_data_bankAccount/" + companyId;
        var source_bankAccount =
                {
                    datatype: "json",
                    addRow: function (rowID, rowData, position, commit) {
                        // synchronize with the server - send insert command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                        commit(true);
                    },
                    updaterow: function (rowid, rowdata, commit) {
                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                    deleteRow: function (rowID, commit) {
                        // synchronize with the server - send delete command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        commit(true);
                    },
                    datafields:
                            [
                                {name: 'Iid', type: 'string'},
                                {name: 'CompanyId', type: 'string'},
                                {name: 'AccountNo', type: 'string'},
                                {name: 'AccountName', type: 'number'},
                                {name: 'AccountAddress', type: 'string'},
                                {name: 'IsActive', type: 'date'},
                                {name: 'BankName', type: 'string'}
                            ],
                    id: 'Iid',
                    url: url_bankAccount,
                    root: 'data'
                };
     
        var dataAdapter_bankAccount = new jQuery_1_4_3.jqx.dataAdapter(source_bankAccount);
        // initialize jqxGrid
        jQuery_1_4_3("#jqxgridBankAccount").on('bindingcomplete', function () {
            jQuery_1_4_3("#jqxgridBankAccount").jqxGrid('autoresizecolumns');
        });
        jQuery_1_4_3("#jqxgridBankAccount").jqxGrid(
                {
//                  width: '100%',
                    height: 430,
                    source: dataAdapter_bankAccount,
                    pageable: true,
                    editable: true,
                    showfilterrow: true,
                    filterable: true,
                    altRows: true,
                    selectionmode: 'singlerow',
                    editmode: 'programmatic',
                    columnsresize: true,
                    autowidth: true,
//                    autoheight: true,
                    sortable: true,
                    showtoolbar: true,
                    renderToolbar: function (toolBar)
                    {
                        var toTheme = function (className) {
                            if (theme == "")
                                return className;
                            return className + " " + className + "-" + theme;
                        }

                        var container = jQuery_1_4_3("<div style='overflow: hidden; position: relative; height: 100%; width: 100%;'></div>");
                        var buttonTemplate = "<div style='float: left; padding: 3px; margin: 2px;'><div style='margin: 4px; width: 16px; height: 16px;'></div></div>";
                        var addButton = jQuery_1_4_3(buttonTemplate);
                        var editButton = jQuery_1_4_3(buttonTemplate);
                        var deleteButton = jQuery_1_4_3(buttonTemplate);
                        var cancelButton = jQuery_1_4_3(buttonTemplate);
                        var updateButton = jQuery_1_4_3(buttonTemplate);
                        // container.append(addButton);
                        // container.append(updateButton);
                        // container.append(editButton);
                        // container.append(deleteButton);
                        // container.append(cancelButton);
                        toolBar.append(container);
                        addButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        addButton.find('div:first').addClass(toTheme('jqx-icon-plus'));
                        addButton.jqxTooltip({position: 'bottom', content: "Add"});
                        editButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        editButton.find('div:first').addClass(toTheme('jqx-icon-edit'));
                        editButton.jqxTooltip({position: 'bottom', content: "Edit"});
                        deleteButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        deleteButton.find('div:first').addClass(toTheme('jqx-icon-delete'));
                        deleteButton.jqxTooltip({position: 'bottom', content: "Delete"});
                        updateButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        updateButton.find('div:first').addClass(toTheme('jqx-icon-save'));
                        updateButton.jqxTooltip({position: 'bottom', content: "Save Changes"});
                        cancelButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        cancelButton.find('div:first').addClass(toTheme('jqx-icon-cancel'));
                        cancelButton.jqxTooltip({position: 'bottom', content: "Cancel"});
                        var updateButtons = function (action) {
                            switch (action) {
                                case "Select":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: false});
                                    editButton.jqxButton({disabled: false});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                                case "Unselect":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: true});
                                    editButton.jqxButton({disabled: true});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                                case "Edit":
                                    addButton.jqxButton({disabled: true});
                                    deleteButton.jqxButton({disabled: true});
                                    editButton.jqxButton({disabled: true});
                                    cancelButton.jqxButton({disabled: false});
                                    updateButton.jqxButton({disabled: false});
                                    break;
                                case "End Edit":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: false});
                                    editButton.jqxButton({disabled: false});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                            }
                        };
                        var rowKey = null;
                        jQuery_1_4_3("#jqxgridBankAccount").on('rowselect', function (event) {
                            if (cancelButton.jqxButton('disabled') || updateButton.jqxButton('disabled')) {
                                var args = event.args;
                                rowKey = args.rowindex;
                                var rowData = args.row;
                                updateButtons('Select');
                            }
                        });
                        addButton.click(function (event) {
                            // add form //
                             jQuery_1_4_3('#companyLevel').jqxDropDownList({
                                 filterable: true,
                                   source: dataAdapter_dd_level,
                                    displayMember: 'CUST_LEVEL_CODE',
                                    valueMember: 'REF_CUST_LEVEL_ID'
                              });

                               jQuery_1_4_3('#companyLevel').on('change', function (event) {
                                   var args = event.args;
                                   if (args) {
                                       // index represents the item's index.                          
                                       var index = args.index;
                                       var item = args.item;
                                       // get item's label and value.
                                       var label = item.label;
                                       var value = item.value;
                                        $('#Level').val(value);
                                   }
                               });

                            $('#addCompanyModal').modal({backdrop: 'static', keyboard: false});
                            clearCompanyForm();
                            $('#parentActive span').removeClass('checked');
                            $('#parentDisactive span').removeClass('checked');

                            $('#addCompanyModal').modal('show');
                        });
                        cancelButton.click(function (event) {
                            if (!cancelButton.jqxButton('disabled')) {
                                jQuery_1_4_3("#jqxgridBankAccount").jqxGrid('endrowedit', rowKey, true); // if true, the changes are canceled.
                                editrow = -1;
                                updateButtons('Unselect');

                                var rowindex = jQuery_1_4_3('#jqxgridBankAccount').jqxGrid('getselectedrowindex');
                                jQuery_1_4_3('#jqxgridBankAccount').jqxGrid('unselectrow', rowindex);
                                jQuery_1_4_3('#jqxgridBankAccount').jqxGrid('updatebounddata');
                            }
                        });
                        updateButton.click(function (event) {//save changes
                            if (!updateButton.jqxButton('disabled')) {
                                jQuery_1_4_3("#jqxgridBankAccount").jqxGrid('endrowedit', rowKey, false); //is false, the changes are saved
                                var datarow;
                                var selectedrowindex = jQuery_1_4_3("#jqxgridBankAccount").jqxGrid('getselectedrowindex');
                                var rowscount = jQuery_1_4_3("#jqxgridBankAccount").jqxGrid('getdatainformation').rowscount;

                                editrow = -1;
                                updateButtons('Unselect');

                                jQuery_1_4_3('#jqxgridBankAccount').jqxGrid('unselectrow', selectedrowindex);

                                var rows = jQuery_1_4_3('#jqxgridBankAccount').jqxGrid('getrows');
                                var field = rows[selectedrowindex];
//                                alert(field.LevelCode);
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>company/update_data",
                                    cache: false,
                                    data: {iid: field.Iid, nama: field.Name, level: field.Level, activestat: field.IsAcvive}, // since, you need to delete post of particular id
                                    success: function (reaksi) {
                                        if (reaksi === '1') {
                                            if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                                                var id = jQuery_1_4_3("#jqxgridBankAccount").jqxGrid('getrowid', selectedrowindex);
                                                var commit = jQuery_1_4_3("#jqxgridBankAccount").jqxGrid('updaterow', id, datarow);
                                            }
                                        } else {
                                            alert(reaksi);
                                        }
                                    }
                                });
                                jQuery_1_4_3('#jqxgridBankAccount').jqxGrid('updatebounddata');
                            }
                        });
                        editButton.click(function () {
                            if (!editButton.jqxButton('disabled')) {
                                clearCompanyForm();
                                  $('#addCompanyModal').modal({backdrop: 'static', keyboard: false}); 

                                $('#addCompanyModal').modal('show');

                                var datarow;
                                var selectedrowindex = jQuery_1_4_3("#jqxgridBankAccount").jqxGrid('getselectedrowindex');
                                var rowscount = jQuery_1_4_3("#jqxgridBankAccount").jqxGrid('getdatainformation').rowscount;

                                var rows = jQuery_1_4_3('#jqxgridBankAccount').jqxGrid('getrows');
                                var field = rows[selectedrowindex];

                                $('#Iid').val(field.Iid);
                                $('#Name').val(field.Name);
                              
                                if (field.IsActive == "1") {
                                  $('#parentActive span').addClass('checked');
                                } else {
                                    //$('#active').prop('checked', 'true');
                                      $('#parentDisactive span').addClass('checked');
                                }

                                jQuery_1_4_3('#companyLevel').jqxDropDownList({
                                 filterable: true,
                                   source: dataAdapter_dd_level,
                                    displayMember: 'CUST_LEVEL_CODE',
                                    valueMember: 'REF_CUST_LEVEL_ID'
                              });

                                jQuery_1_4_3("#companyLevel").jqxDropDownList('selectItem',"'"+field.Level+"'");


                               jQuery_1_4_3('#companyLevel').on('change', function (event) {
                                   var args = event.args;
                                   if (args) {
                                       // index represents the item's index.                          
                                       var index = args.index;
                                       var item = args.item;
                                       // get item's label and value.
                                       var label = item.label;
                                       var value = item.value;
                                        $('#Level').val(value);
                                   }
                               });

                                
                                

                                $('#addCompanyModal').modal('show');

                            }
                        });
                        deleteButton.click(function () {
                            if (!deleteButton.jqxButton('disabled')) {
                                var rows = jQuery_1_4_3('#jqxgridBankAccount').jqxGrid('getrows');
                                var selectedrowindex = jQuery_1_4_3('#jqxgridBankAccount').jqxGrid('getselectedrowindex');
                                var field = rows[selectedrowindex];
                                if (field.Name === '') {
                                    var id = jQuery_1_4_3("#jqxgridBankAccount").jqxGrid('getrowid', selectedrowindex);
                                    jQuery_1_4_3('#jqxgridBankAccount').jqxGrid('deleterow', id);
                                } else {
                                    if (confirm("Anda yakin menghapus data dengan Nama '" + field.Name + "'?")) {
                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url() ?>company/delete_data",
                                            cache: false,
                                            data: {iid: field.Iid}, // since, you need to delete post of particular id
                                            success: function (reaksi) {
                                                if (reaksi == '1') {
                                                    var id = jQuery_1_4_3("#jqxgridBankAccount").jqxGrid('getrowid', selectedrowindex);
                                                    jQuery_1_4_3('#jqxgridBankAccount').jqxGrid('deleterow', id);
                                                } else {
                                                    alert(reaksi);
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        });
                        jQuery_1_4_3("#jqxgridBankAccount").on('endrowedit', function (event) {
                            updateButtons('End Edit');
                        });
                    },
                    columns: [
                        {text: 'Iid', columntype: 'textbox', datafield: 'Iid', width: 70, hidden: true},
                        {text: 'Bank Name', columntype: 'textbox', datafield: 'BankName', width: 210},
                        {text: 'Bank Account No', columntype: 'textbox', datafield: 'AccountNo', width: 210},
                        {text: 'Bank Account Name', columntype: 'textbox', datafield: 'AccountName', width: 210}
                        
                    ]
                });
    }
    );



</script>
<div class="row">
       <div class="col-md-6" style="width:34%;">
        <div class='box'>
            <div class="box-body table-responsive">
                <div id='jqxWidget'>
                    <div id="jqxgrid"></div>
                    <div>
                        <div id="cellbegineditevent"></div>
                        <div id="cellendeditevent"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class='box'>
            <div class="box-body table-responsive">
                <div id='jqxWidget'>
                    <div id="jqxgridBankAccount"></div>
                    <div>
                        <div id="cellbegineditevent"></div>
                        <div id="cellendeditevent"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
