<?php $this->load->view('jqwidgetslink'); ?>

<script type="text/javascript">
    var jQuery_1_4_3 = $.noConflict(true);
     var base_url = "<?php echo base_url(); ?>";
    jQuery_1_4_3(document).ready(function () {

         jQuery_1_4_3('#BankAccountTable').on('click', '.removeTransaction', function() {
            /* Act on the event */
            $(this).parent().parent().remove();
        });

        // prepare the data
//        var url = "http://localhost/smartdeal/akunting/get_data";
        var url = "<?php echo site_url('company/get_data'); ?>";
        var url_dd_level = "<?php echo site_url('company/get_cust_level'); ?>";
        var source =
                {
                    datatype: "json",
                    addRow: function (rowID, rowData, position, commit) {
                        // synchronize with the server - send insert command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                        commit(true);
                    },
                    updaterow: function (rowid, rowdata, commit) {
                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                    deleteRow: function (rowID, commit) {
                        // synchronize with the server - send delete command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        commit(true);
                    },
                    datafields:
                            [
                                {name: 'Iid', type: 'string'},
                                {name: 'Name', type: 'string'},
                                {name: 'Level', type: 'number'},
                                {name: 'IsActive', type: 'number'},
                                {name: 'InputBy', type: 'string'},
                                {name: 'InputDate', type: 'date'},
                                {name: 'LastEditBy', type: 'string'},
                                {name: 'LastEditDate', type: 'date'}
                            ],
                    id: 'Iid',
                    url: url,
                    root: 'data'
                };
        var source_dd_level =
                {
                    datatype: "json",
                    datafields:
                            [
                                {name: 'REF_CUST_LEVEL_ID', type: 'number'},
                                {name: 'CUST_LEVEL_CODE', type: 'string'},
                                {name: 'CUST_LEVEL_NAME', type: 'string'}
                            ],
                    url: url_dd_level,
                    async: true
                };
        var dataAdapter = new jQuery_1_4_3.jqx.dataAdapter(source);
        var dataAdapter_dd_level = new jQuery_1_4_3.jqx.dataAdapter(source_dd_level);
        // initialize jqxGrid
        jQuery_1_4_3("#jqxgrid").on('bindingcomplete', function () {
            // jQuery_1_4_3("#jqxgrid").jqxGrid('autoresizecolumns');
        });
        jQuery_1_4_3("#jqxgrid").jqxGrid(
                {
                 //width: 1000,
                    height: 430,
                    source: dataAdapter,
                    pageable: true,
                    editable: true,
                    showfilterrow: true,
                    filterable: true,
                    altRows: true,
                    selectionmode: 'singlerow',
                    editmode: 'programmatic',
                    columnsresize: true,
                    autowidth: true,
//                    autoheight: true,
                    sortable: true,
                    showtoolbar: true,
                    renderToolbar: function (toolBar)
                    {
                        var toTheme = function (className) {
                            if (theme == "")
                                return className;
                            return className + " " + className + "-" + theme;
                        }

                        var container = jQuery_1_4_3("<div style='overflow: hidden; position: relative; height: 100%; width: 100%;'></div>");
                        var buttonTemplate = "<div style='float: left; padding: 3px; margin: 2px;'><div style='margin: 4px; width: 16px; height: 16px;'></div></div>";
                        var addButton = jQuery_1_4_3(buttonTemplate);
                        var editButton = jQuery_1_4_3(buttonTemplate);
                        var deleteButton = jQuery_1_4_3(buttonTemplate);
                        var cancelButton = jQuery_1_4_3(buttonTemplate);
                        var updateButton = jQuery_1_4_3(buttonTemplate);
                        container.append(addButton);
                        container.append(updateButton);
                        container.append(editButton);
                        container.append(deleteButton);
                        container.append(cancelButton);
                        toolBar.append(container);
                        addButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        addButton.find('div:first').addClass(toTheme('jqx-icon-plus'));
                        addButton.jqxTooltip({position: 'bottom', content: "Add"});
                        editButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        editButton.find('div:first').addClass(toTheme('jqx-icon-edit'));
                        editButton.jqxTooltip({position: 'bottom', content: "Edit"});
                        deleteButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        deleteButton.find('div:first').addClass(toTheme('jqx-icon-delete'));
                        deleteButton.jqxTooltip({position: 'bottom', content: "Delete"});
                        updateButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        updateButton.find('div:first').addClass(toTheme('jqx-icon-save'));
                        updateButton.jqxTooltip({position: 'bottom', content: "Save Changes"});
                        cancelButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        cancelButton.find('div:first').addClass(toTheme('jqx-icon-cancel'));
                        cancelButton.jqxTooltip({position: 'bottom', content: "Cancel"});
                        var updateButtons = function (action) {
                            switch (action) {
                                case "Select":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: false});
                                    editButton.jqxButton({disabled: false});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                                case "Unselect":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: true});
                                    editButton.jqxButton({disabled: true});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                                case "Edit":
                                    addButton.jqxButton({disabled: true});
                                    deleteButton.jqxButton({disabled: true});
                                    editButton.jqxButton({disabled: true});
                                    cancelButton.jqxButton({disabled: false});
                                    updateButton.jqxButton({disabled: false});
                                    break;
                                case "End Edit":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: false});
                                    editButton.jqxButton({disabled: false});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                            }
                        };
                        var rowKey = null;
                        jQuery_1_4_3("#jqxgrid").on('rowselect', function (event) {
                            if (cancelButton.jqxButton('disabled') || updateButton.jqxButton('disabled')) {
                                var args = event.args;
                                rowKey = args.rowindex;
                                var rowData = args.row;
                                updateButtons('Select');
                            }
                        });
                        addButton.click(function (event) {
                            // add form //
                             jQuery_1_4_3('#companyLevel').jqxDropDownList({
                                 filterable: true,
                                   source: dataAdapter_dd_level,
                                    displayMember: 'CUST_LEVEL_CODE',
                                    valueMember: 'REF_CUST_LEVEL_ID'
                              });

                             $('#activeGroup').attr('hidden', 'true');
                             $('#Name').removeAttr('readonly', 'false');

                               jQuery_1_4_3('#companyLevel').on('change', function (event) {
                                   var args = event.args;
                                   if (args) {
                                       // index represents the item's index.                          
                                       var index = args.index;
                                       var item = args.item;
                                       // get item's label and value.
                                       var label = item.label;
                                       var value = item.value;
                                        $('#Level').val(value);
                                   }
                               });

                            $('#addCompanyModal').modal({backdrop: 'static', keyboard: false});
                            clearCompanyForm();
                            $('#parentActive span').removeClass('checked');
                            $('#parentDisactive span').removeClass('checked');

                            $('#addCompanyModal').modal('show');
                            $('#FormType').val('add');
                        });
                        cancelButton.click(function (event) {
                            if (!cancelButton.jqxButton('disabled')) {
                                jQuery_1_4_3("#jqxgrid").jqxGrid('endrowedit', rowKey, true); // if true, the changes are canceled.
                                editrow = -1;
                                updateButtons('Unselect');

                                var rowindex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindex');
                                jQuery_1_4_3('#jqxgrid').jqxGrid('unselectrow', rowindex);
                                jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                            }
                        });
                        updateButton.click(function (event) {//save changes
                            if (!updateButton.jqxButton('disabled')) {
                                jQuery_1_4_3("#jqxgrid").jqxGrid('endrowedit', rowKey, false); //is false, the changes are saved
                                var datarow;
                                var selectedrowindex = jQuery_1_4_3("#jqxgrid").jqxGrid('getselectedrowindex');
                                var rowscount = jQuery_1_4_3("#jqxgrid").jqxGrid('getdatainformation').rowscount;

                                editrow = -1;
                                updateButtons('Unselect');

                                jQuery_1_4_3('#jqxgrid').jqxGrid('unselectrow', selectedrowindex);

                                var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                var field = rows[selectedrowindex];
//                                alert(field.LevelCode);
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>company/update_data",
                                    cache: false,
                                    data: {iid: field.Iid, nama: field.Name, level: field.Level, activestat: field.IsAcvive}, // since, you need to delete post of particular id
                                    success: function (reaksi) {
                                        if (reaksi === '1') {
                                            if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                                                var id = jQuery_1_4_3("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                                                var commit = jQuery_1_4_3("#jqxgrid").jqxGrid('updaterow', id, datarow);
                                            }
                                        } else {
                                            alert(reaksi);
                                        }
                                    }
                                });
                                jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                            }
                        });
                        editButton.click(function () {
                            if (!editButton.jqxButton('disabled')) {
                                
                                clearCompanyForm();
                                $('#addCompanyModal').modal('show');
                                $('#FormType').val('edit');

                                  $('#addCompanyModal').modal({backdrop: 'static', keyboard: false}); 

                                $('#addCompanyModal').modal('show');

                                $('#activeGroup').removeAttr('hidden');
                                   $('#Name').attr('readonly', 'true');

                                var datarow;
                                var selectedrowindex = jQuery_1_4_3("#jqxgrid").jqxGrid('getselectedrowindex');
                                var rowscount = jQuery_1_4_3("#jqxgrid").jqxGrid('getdatainformation').rowscount;

                                var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                var field = rows[selectedrowindex];
                               
                                $('#Iid').val(field.Iid);
                                $('#Name').val(field.Name);
                                $('#Level').val(field.Level);

                              
                                if (field.IsActive == "1") {
                                  $('#parentActive span').addClass('checked');
                                } else {
                                    //$('#active').prop('checked', 'true');
                                      $('#parentDisactive span').addClass('checked');
                                }

                                jQuery_1_4_3('#companyLevel').jqxDropDownList({
                                 filterable: true,
                                   source: dataAdapter_dd_level,
                                    displayMember: 'CUST_LEVEL_CODE',
                                    valueMember: 'CUST_LEVEL_CODE'
                              });

                                jQuery_1_4_3("#companyLevel").jqxDropDownList('selectItem', "'"+field.Level+"'");
                             


                               jQuery_1_4_3('#companyLevel').on('change', function (event) {
                                   var args = event.args;
                                   if (args) {
                                       // index represents the item's index.                          
                                       var index = args.index;
                                       var item = args.item;
                                       // get item's label and value.
                                       var label = item.label;
                                       var value = item.value;
                                        $('#Level').val(value);
                                   }
                               });
                            companyId = field.Name;
                               //bankAccount
                                  $.ajax({
                                     type: "POST",
                                     url: base_url + "company/get_data_bankAccount/" + companyId,
                                     cache: false,
                                     dataType: "json",
                                     data: {companyId : companyId}, // since, you need to delete post of particular id
                                     success: function(data) {
                                        var string = '';

                                        $.each(data, function(index, val) {
                                            if (val.Category == '0') {
                                                string = 'Tidak Terlapor';
                                            } else string = 'Terlapor';
                                             /* iterate through array or object */
                                            $('#rowTable').append('<tr class=rowCompanyBank><td class=BankName><input type=hidden name=BankName[] id=BankName value='+val.BankName+'>'+val.BankName+'</td><td><input type=hidden name=AccountNo[] id=AccountNo value='+val.AccountNo+'>'+val.AccountNo+'</td><td><input type=hidden name=AccountName[] id=AccountName value='+val.AccountName+'>'+val.AccountName+'</td><td><input type=hidden name=AccountAddress[] id=AccountAddress value='+val.AccountAddress+'>'+val.AccountAddress+'</td><td><input type=hidden name=Category[] id=Category value='+val.Category+'>'+string+'</td><td><a href=javascript:void(0) class=removeTransaction><div class=icon-close></div></a></td></tr>');
                                        });
                                       
                                     }
                                     
                                 });


                              
                            }
                        });
                        deleteButton.click(function () {
                            if (!deleteButton.jqxButton('disabled')) {
                                var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                var selectedrowindex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindex');
                                var field = rows[selectedrowindex];
                                if (field.Name === '') {
                                    var id = jQuery_1_4_3("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                                    jQuery_1_4_3('#jqxgrid').jqxGrid('deleterow', id);
                                } else {
                                    if (confirm("Anda yakin menghapus data dengan Nama '" + field.Name + "'?")) {
                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url() ?>company/delete_data",
                                            cache: false,
                                            data: {iid: field.Iid}, // since, you need to delete post of particular id
                                            success: function (reaksi) {
                                                if (reaksi == '1') {
                                                    var id = jQuery_1_4_3("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                                                    jQuery_1_4_3('#jqxgrid').jqxGrid('deleterow', id);
                                                } else {
                                                    alert(reaksi);
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        });
                        jQuery_1_4_3("#jqxgrid").on('endrowedit', function (event) {
                            updateButtons('End Edit');
                        });
                    },
                    columns: [
                        {text: 'Iid', columntype: 'textbox', datafield: 'Iid', width: 70, hidden: true},
                        {text: 'Nama', columntype: 'textbox', datafield: 'Name', width: 210},
                        {text: 'Level', datafield: 'Level', width: 150, columntype: 'dropdownlist',
                            createeditor: function (row, column, editor) {
                                editor.jqxDropDownList({
                                    filterable: true,
                                    source: dataAdapter_dd_level,
                                    displayMember: 'CUST_LEVEL_CODE',
                                    valueMember: 'CUST_LEVEL_CODE'
                                });
                            }},
                        {text: 'Active Status', columntype: 'textbox', datafield: 'IsActive', width: 210}
                    ]
                });
    }
    );

function storeCompany() {
        if ($('#Name').val() == '') {
            alert('Name Harus D isi');
            $('#Name').focus();
            return false;
        }

        if ($('#Level').val() == '') {
            alert('Nama Harus D isi');
              $('#Level').focus();
            return false;
        }

         if ($('#Active').val() == '') {
            alert('Alamat Harus D isi');
             $('#Alamat').focus();
            return false;
        }

         $.ajax({
         type: "POST",
         url: base_url + "company/store",
         cache: false,
         dataType: "json",
         data: $('#companyForm').serialize(), // since, you need to delete post of particular id
         success: function(data) {
             if (data.status == 200) {
                 jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                 $('#addCompanyModal').modal('hide');
                 $('#companyForm').closest('form').find("input[type=text], textarea,input[type=date],input[type=hidden]").val("");
                 jQuery_1_4_3('#jqxgrid').jqxGrid('jqxgridBankAccount');

                 //window.location.reload();
             }

         }
     });
 }


 function clearCompanyForm() {
     // body...
        $('#companyForm').closest('form').find("input[type=text],input[type=date],input[type=hidden]").val("");  
             $('#parentActive span').removeClass('checked');
                            $('#parentDisactive span').removeClass('checked');    
                             $('#rowTable tr').remove();
 }
 
 function checkCompanyCode() {
    $.ajax({
         type: "POST",
         url: base_url + "company/checkCompanyCode",
         cache: false,
         dataType: "json",
         data: $('#companyForm').serialize(), // since, you need to delete post of particular id
         success: function(data) {
             if (data.status == 400) {
                 alert('Kode Company Ada yang sama');
                 $('#Name').val('');
                 $('#Name').focus();
                 //jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                 //$('#addCustomerModal').modal('hide');
                 //$('#customerForm').closest('form').find("input[type=text], textarea,input[type=date],input[type=hidden]").val("");

                 //window.location.reload();
             }
        }
    });
 }

 function addRowBankAccount() {
    // body...
    var bool = false;
    var BankName = $('#BankName');
    var AccountNo = $('#AccountNo');
    var AccountName = $('#AccountName');
    var AccountAddress = $('#AccountAddress');
    var Category = $('#Category :selected');


    $('.rowCompanyBank').each(function(index, el) {
           if (BankName.val() == $('.rowCompanyBank .BankName').eq(index).text()) {
                bool = true;
           }     
    });

    if (bool) {
        alert('Tidak bisa menambah Bank yang sama!');
        return false;
    }

    if ($('#BankName').val() == '') {
        alert('Nama Bank Harus Di isi');
        return false;
    }

    if ($('#AccountNo').val() == '') {
            alert('Akun No Harus Di isi');
            return false;
        }

    if ($('#AccountName').val() == '') {
            alert('Nama akun Harus Di isi');
            return false;
    }

    if ($('#AccountAddress').val() == '') {
            alert('Alamat akun Harus Di isi');
            return false;
    }


    $('#rowTable').append('<tr class=rowCompanyBank><td class=BankName><input type=hidden name=BankName[] id=BankName value='+BankName.val()+'>'+BankName.val()+'</td><td><input type=hidden name=AccountNo[] id=AccountNo value='+AccountNo.val()+'>'+AccountNo.val()+'</td><td><input type=hidden name=AccountName[] id=AccountName value='+AccountName.val()+'>'+AccountName.val()+'</td><td><input type=hidden name=AccountAddress[] id=AccountAddress value='+AccountAddress.val()+'>'+AccountAddress.val()+'</td><td><input type=hidden name=Category[] id=Category value='+Category.val()+'>'+Category.text()+'</td><td><a href=javascript:void(0) class=removeTransaction><div class=icon-close></div></a></td></tr>');

    // if ( $('#rowTable tr').length > 0 ) {
    //      jQuery_1_4_3("#customerId").jqxDropDownList({disabled : true});
    //      document.getElementById("buy").disabled = true;
    //      document.getElementById("sell").disabled = true;
    //      $('#date').attr('readonly', true);
    // }

}

</script>
<h3 class="page-title">
    <?php echo $pageform ?></h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="index.html">Pengaturan</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Company</a>
        </li>
    </ul>
</div>
<table>
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<div class="row">
       <div class="col-md-12">
        <div class='box'>
            <div class="box-body table-responsive">
                <div id='jqxWidget'>
                    <div id="jqxgrid"></div>
                    <div>
                        <div id="cellbegineditevent"></div>
                        <div id="cellendeditevent"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('vmaster/company/modal_view');?>
<?php //$this->load->view('stp/bank_view') ?>