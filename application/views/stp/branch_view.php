<?php $this->load->view('jqwidgetslink'); ?>

<script type="text/javascript">
    var jQuery_1_4_3 = $.noConflict(true);
    var base_url = "<?php echo base_url(); ?>";
    jQuery_1_4_3(document).ready(function () {
        // prepare the data
        var url = "<?php echo site_url('branch/get_data'); ?>";
        var url_dd_company = "<?php echo site_url('dropdownlist/company'); ?>";
        var source =
                {
                    datatype: "json",
                    addRow: function (rowID, rowData, position, commit) {
                        // synchronize with the server - send insert command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                        commit(true);
                    },
                    updaterow: function (rowid, rowdata, commit) {
                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                    deleteRow: function (rowID, commit) {
                        // synchronize with the server - send delete command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        commit(true);
                    },
                    datafields:
                            [
                                {name: 'Iid', type: 'string'},
                                {name: 'CompanyId', type: 'string'},
                                {name: 'Code', type: 'string'},
                                {name: 'Name', type: 'string'},
                                {name: 'Address', type: 'string'},
                                {name: 'Phone1', type: 'string'},
                                {name: 'Phone2', type: 'string'},
                                {name: 'Fax', type: 'string'},
                                {name: 'Mobile', type: 'string'},
                                {name: 'NPWPNo', type: 'string'},
                                {name: 'NPWPName', type: 'string'},
                                {name: 'NPWPAddress', type: 'string'},
                                {name: 'IsActive', type: 'string'},
                                {name: 'Remarks', type: 'string'},
                                {name: 'DefaultFormatCode', type: 'string'}

                            ],
                    id: 'Iid',
                    url: url,
                    root: 'data'
                };
        var source_dd_company =
                {
                    datatype: "json",
                    datafields:
                            [
                                {name: 'Name', type: 'string'}
                            ],
                    url: url_dd_company,
                    async: true
                };
        var dataAdapter = new jQuery_1_4_3.jqx.dataAdapter(source);
        var dataAdapter_dd_company = new jQuery_1_4_3.jqx.dataAdapter(source_dd_company);
        // initialize jqxGrid
        jQuery_1_4_3("#jqxgrid").on('bindingcomplete', function () {
            jQuery_1_4_3("#jqxgrid").jqxGrid('autoresizecolumns');
        });
        jQuery_1_4_3("#jqxgrid").jqxGrid(
                {
//                    width: '100%',
                    height: 430,
                    source: dataAdapter,
                    editable: true,
                    showfilterrow: true,
                    filterable: true,
                    altRows: true,
                    selectionmode: 'singlerow',
                    editmode: 'programmatic',
                    columnsresize: true,
                    autowidth: true,
//                    autoheight: true,
                    sortable: true,
                    pageable: true,
                    showtoolbar: true,
                    renderToolbar: function (toolBar)
                    {
                        var toTheme = function (className) {
                            if (theme == "")
                                return className;
                            return className + " " + className + "-" + theme;
                        }

                        var container = jQuery_1_4_3("<div style='overflow: hidden; position: relative; height: 100%; width: 100%;'></div>");
                        var buttonTemplate = "<div style='float: left; padding: 3px; margin: 2px;'><div style='margin: 4px; width: 16px; height: 16px;'></div></div>";
                        var addButton = jQuery_1_4_3(buttonTemplate);
                        var editButton = jQuery_1_4_3(buttonTemplate);
                        var deleteButton = jQuery_1_4_3(buttonTemplate);
                        var cancelButton = jQuery_1_4_3(buttonTemplate);
                        var updateButton = jQuery_1_4_3(buttonTemplate);
                        container.append(addButton);
                        container.append(updateButton);
                        container.append(editButton);
                        container.append(deleteButton);
                        container.append(cancelButton);
                        toolBar.append(container);
                        addButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        addButton.find('div:first').addClass(toTheme('jqx-icon-plus'));
                        addButton.jqxTooltip({position: 'bottom', content: "Add"});
                        editButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        editButton.find('div:first').addClass(toTheme('jqx-icon-edit'));
                        editButton.jqxTooltip({position: 'bottom', content: "Edit"});
                        deleteButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        deleteButton.find('div:first').addClass(toTheme('jqx-icon-delete'));
                        deleteButton.jqxTooltip({position: 'bottom', content: "Delete"});
                        updateButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        updateButton.find('div:first').addClass(toTheme('jqx-icon-save'));
                        updateButton.jqxTooltip({position: 'bottom', content: "Save Changes"});
                        cancelButton.jqxButton({cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25});
                        cancelButton.find('div:first').addClass(toTheme('jqx-icon-cancel'));
                        cancelButton.jqxTooltip({position: 'bottom', content: "Cancel"});
                        var updateButtons = function (action) {
                            switch (action) {
                                case "Select":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: false});
                                    editButton.jqxButton({disabled: false});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                                case "Unselect":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: true});
                                    editButton.jqxButton({disabled: true});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                                case "Edit":
                                    addButton.jqxButton({disabled: true});
                                    deleteButton.jqxButton({disabled: true});
                                    editButton.jqxButton({disabled: true});
                                    cancelButton.jqxButton({disabled: false});
                                    updateButton.jqxButton({disabled: false});
                                    break;
                                case "End Edit":
                                    addButton.jqxButton({disabled: false});
                                    deleteButton.jqxButton({disabled: false});
                                    editButton.jqxButton({disabled: false});
                                    cancelButton.jqxButton({disabled: true});
                                    updateButton.jqxButton({disabled: true});
                                    break;
                            }
                        };
                        var rowKey = null;
                        jQuery_1_4_3("#jqxgrid").on('rowselect', function (event) {
                            if (cancelButton.jqxButton('disabled') || updateButton.jqxButton('disabled')) {
                                var args = event.args;
                                rowKey = args.rowindex;
                                var rowData = args.row;
                                updateButtons('Select');
                            }
                        });
                        addButton.click(function (event) {
                           
                            $('#Code').prop('readonly', true);//('readonly');
                            $('#Name').prop('readonly', true);//.removeProp('readonly');
                            $('#DefaultFormatCode').prop('readonly', true);//.removeProp('readonly');
                            clearBranchForm();
                            $('#addBranchModal').modal({backdrop: 'static', keyboard: false}); 
                            $('.modal-dialog').css('width', '50%');
                            $('#addBranchModal').modal('show');
                            jQuery_1_4_3('#companySelect').jqxDropDownList({
                                    filterable: true,
                                    source: dataAdapter_dd_company,
                                    displayMember: 'Name',
                                    valueMember: 'Name'
                            });

                             jQuery_1_4_3('#companySelect').on('change', function (event) {
                                   var args = event.args;
                                   if (args) {
                                       // index represents the item's index.                          
                                       var index = args.index;
                                       var item = args.item;
                                       // get item's label and value.
                                       var label = item.label;
                                       var value = item.value;

                                        $('#Code').removeProp('readonly');//('readonly');
                                        $('#Name').removeProp('readonly');
                                        $('#DefaultFormatCode').removeProp('readonly');

                                        $('#CompanyId').val(value);
                                   }
                               });



                        });
                        cancelButton.click(function (event) {
                            if (!cancelButton.jqxButton('disabled')) {
                                jQuery_1_4_3("#jqxgrid").jqxGrid('endrowedit', rowKey, true); // if true, the changes are canceled.
                                editrow = -1;
                                updateButtons('Unselect');

                                var rowindex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindex');
                                jQuery_1_4_3('#jqxgrid').jqxGrid('unselectrow', rowindex);
                                jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                            }
                        });
                        updateButton.click(function (event) {//save changes
                            if (!updateButton.jqxButton('disabled')) {
                                jQuery_1_4_3("#jqxgrid").jqxGrid('endrowedit', rowKey, false); //is false, the changes are saved
                                var datarow;
                                var selectedrowindex = jQuery_1_4_3("#jqxgrid").jqxGrid('getselectedrowindex');
                                var rowscount = jQuery_1_4_3("#jqxgrid").jqxGrid('getdatainformation').rowscount;

                                editrow = -1;
                                updateButtons('Unselect');

                                jQuery_1_4_3('#jqxgrid').jqxGrid('unselectrow', selectedrowindex);

                                var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                var field = rows[selectedrowindex];
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url() ?>branch/update_data",
                                    cache: false,
                                    data: {iid: field.Iid, companyid: field.CompanyId, kode: field.Code,
                                        nama: field.Name, alamat: field.Address,
                                        phone1: field.Phone1, phone2: field.Phone2, fax: field.Fax, mobile: field.Mobile,
                                        npwpno: field.NPWPNo, npwpname: field.NPWPName, npwpaddress: field.NPWPAddress, activestatus: field.IsActive, catatan: field.Remarks}, // since, you need to delete post of particular id
                                    success: function (reaksi) {
                                        if (reaksi == '1') {
                                            if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                                                var id = jQuery_1_4_3("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                                                var commit = jQuery_1_4_3("#jqxgrid").jqxGrid('updaterow', id, datarow);
                                            }
                                        } else {
                                            alert(reaksi);
                                        }
                                    }
                                });
                                jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                            }
                        });
                        editButton.click(function () {
                            if (!editButton.jqxButton('disabled')) {

                                var datarow;
                                var selectedrowindex = jQuery_1_4_3("#jqxgrid").jqxGrid('getselectedrowindex');
                                var rowscount = jQuery_1_4_3("#jqxgrid").jqxGrid('getdatainformation').rowscount;

                                var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                var field = rows[selectedrowindex];

                              var source = [field.CompanyId];
                                          // Create a jqxDropDownList
                                          jQuery_1_4_3("#companySelect").jqxDropDownList({
                                              source: source,
                                              selectedIndex: 0,
                                              disabled: true
                                          });

                                   jQuery_1_4_3('#companySelect').on('change', function (event) {
                                       var args = event.args;
                                       if (args) {
                                           // index represents the item's index.                          
                                           var index = args.index;
                                           var item = args.item;
                                           // get item's label and value.
                                           var label = item.label;
                                           var value = item.value;
                                            $('#CompanyId').val(value);
                                       }
                                   });


                                $('#Code').val(field.Code);
                                $('#CompanyId').val(field.CompanyId);
                                $('#Name').val(field.Name);
                                $('#Address').val(field.Address);
                                $('#Phone1').val(field.Phone1);
                                $('#Phone2').val(field.Phone2);
                                $('#Fax').val(field.Fax);
                                $('#Mobile').val(field.Mobile);
                                $('#NPWPNo').val(field.NPWPNo);
                                $('#NPWPName').val(field.NPWPName);
                                $('#NPWPAddress').val(field.NPWPAddress);
                                $('#Remarks').val(field.Remarks);
                                $('#DefaultFormatCode').val(field.DefaultFormatCode);
                                $('#Iid').val(field.Iid);
                                $('#Code').prop('readonly', true);
                                $('#Name').prop('readonly', true);
                                $('#DefaultFormatCode').prop('readonly', true);
                                // $('#Telp').val(field.Telp);
                                // $('#Rekening').val(field.Rekening);
                                // $('#PemilikRekening').val(field.PemilikRekening);
                                // $('#Kuasa1_ID').val(field.Kuasa1_ID);
                                // $('#Kuasa1_Nama').val(field.Kuasa1_Nama);
                                // $('#Kuasa2_Address').val(field.Kuasa2_Address);

                                    if (field.IsActive == "1") {
                                        $('#parentActive span').addClass('checked');
                                } else {
                                    //$('#active').prop('checked', 'true');
                                        $('#parentDisactive span').addClass('checked');
                                }

                                $('#addBranchModal').modal({backdrop: 'static', keyboard: false}); 

                                $('.modal-dialog').css('width', '75%');

                                $('#addBranchModal').modal('show');

                            }
                        });
                        deleteButton.click(function () {
                            if (!deleteButton.jqxButton('disabled')) {
                                var rows = jQuery_1_4_3('#jqxgrid').jqxGrid('getrows');
                                var selectedrowindex = jQuery_1_4_3('#jqxgrid').jqxGrid('getselectedrowindex');
                                var field = rows[selectedrowindex];
                                if (field.Code === '') {
                                    var id = jQuery_1_4_3("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                                    jQuery_1_4_3('#jqxgrid').jqxGrid('deleterow', id);
                                } else {
                                    if (confirm("Anda yakin menghapus data dengan Kode '" + field.Code + "'?")) {
                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url() ?>branch/delete_data",
                                            cache: false,
                                            data: {iid: field.Iid}, // since, you need to delete post of particular id
                                            success: function (reaksi) {
                                                if (reaksi == '1') {
                                                    var id = jQuery_1_4_3("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                                                    jQuery_1_4_3('#jqxgrid').jqxGrid('deleterow', id);
                                                } else {
                                                    alert(reaksi);
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        });
                        jQuery_1_4_3("#jqxgrid").on('endrowedit', function (event) {
                            updateButtons('End Edit');
                        });
                    },
                    columns: [
                        {text: 'Iid', columntype: 'textbox', datafield: 'Iid', width: 70, hidden: true},
                        {text: 'Kode Branch', columntype: 'textbox', datafield: 'Code', width: 210},
                        {text: 'Nama Branch', columntype: 'textbox', datafield: 'Name', width: 210},
                        {text: 'Company', datafield: 'CompanyId', width: 150, columntype: 'dropdownlist',
                            createeditor: function (row, column, editor) {
                                editor.jqxDropDownList({
                                    filterable: true,
                                    source: dataAdapter_dd_company,
                                    displayMember: 'Name',
                                    valueMember: 'Name'
                                });
                            }},
                        {text: 'Alamat', columntype: 'textbox', datafield: 'Address', width: 210},
                        {text: 'Phone 1', columntype: 'textbox', datafield: 'Phone1', width: 210},
                        {text: 'Phone 2', columntype: 'textbox', datafield: 'Phone2', width: 210},
                        {text: 'Fax', columntype: 'textbox', datafield: 'Fax', width: 210},
                        {text: 'Mobile', columntype: 'textbox', datafield: 'Mobile', width: 210},
                        {text: 'No NPWP', columntype: 'textbox', datafield: 'NPWPNo', width: 210},
                        {text: 'Nama NPWP', columntype: 'textbox', datafield: 'NPWPName', width: 210},
                        {text: 'Alamat NPWP', columntype: 'textbox', datafield: 'NPWPAddress', width: 210},
                        {text: 'Active Status', columntype: 'textbox', datafield: 'IsActive', width: 210},
                        {text: 'Catatan', columntype: 'textbox', datafield: 'Remarks', width: 220}
                    ]
                });
    }
    );

function storeBranch() {
        if ($('#CompanyId').val() == '') {
            alert('Company Harus Di isi');
            return false;
        }

        if ($('#Code').val() == '') {
            alert('Kode  Harus D isi');
              $('#Code').focus();
            return false;
        }

         if ($('#DefaultFormatCode').val() == '') {
            alert('Kode sistem  Harus D isi');
              $('#DefaultFormatCode').focus();
            return false;
        }

        if ($('#Name').val() == '') {
            alert('Nama Harus D isi');
              $('#Nama').focus();
            return false;
        }

         if ($('#Address').val() == '') {
            alert('Alamat Harus D isi');
             $('#Address').focus();
            return false;
        }

         if ($('#Telp').val() == '') {
            alert('Telp Harus D isi');
              $('#Telp').focus();
            return false;
        }

         $.ajax({
         type: "POST",
         url: base_url + "branch/store",
         cache: false,
         dataType: "json",
         data: $('#branchForm').serialize(), // since, you need to delete post of particular id
         success: function(data) {
             if (data.status == 200) {
                 jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                 $('#addBranchModal').modal('hide');
                 $('#branchForm').closest('form').find("input[type=text], textarea,input[type=date],input[type=hidden]").val("");
                 //window.location.reload();
             }

         }
     });
 }


 function clearBranchForm() {
     // body...
      $('#Code').prop('readonly', true);//('readonly');
                            $('#Name').prop('readonly', true);//.removeProp('readonly');
                            $('#DefaultFormatCode').prop('readonly', true);//.removeProp('readonly');
        $('#branchForm').closest('form').find("input[type=text],input[type=number],input[type=hidden]").val(""); 
 }

 function checkBranchCode() {
    $.ajax({
         type: "POST",
         url: base_url + "branch/checkBranchCode",
         cache: false,
         dataType: "json",
         data: $('#branchForm').serialize(), // since, you need to delete post of particular id
         success: function(data) {
             if (data.status == 400) {
                 alert('Kode Branch Ada yang sama');
                 $('#Code').val('');
                 $('#Code').focus();
                 //jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                 //$('#addCustomerModal').modal('hide');
                 //$('#customerForm').closest('form').find("input[type=text], textarea,input[type=date],input[type=hidden]").val("");

                 //window.location.reload();
             }
        }
    });
 }

 function checkBranchName() {
    $.ajax({
         type: "POST",
         url: base_url + "branch/checkBranchName",
         cache: false,
         dataType: "json",
         data: $('#branchForm').serialize(), // since, you need to delete post of particular id
         success: function(data) {
             if (data.status == 400) {
                 alert('Kode Branch Ada yang sama');
                 $('#Name').val('');
                 $('#Name').focus();
                 //jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                 //$('#addCustomerModal').modal('hide');
                 //$('#customerForm').closest('form').find("input[type=text], textarea,input[type=date],input[type=hidden]").val("");

                 //window.location.reload();
             }
        }
    });
 }

 function checkDefaultFormatCode() {
    $.ajax({
         type: "POST",
         url: base_url + "branch/checkDefaultFormatCode",
         cache: false,
         dataType: "json",
         data: $('#branchForm').serialize(), // since, you need to delete post of particular id
         success: function(data) {
             if (data.status == 400) {
                 alert('Kode Sistem Ada yang sama');
                 $('#DefaultFormatCode').val('');
                 $('#DefaultFormatCode').focus();
                 //jQuery_1_4_3('#jqxgrid').jqxGrid('updatebounddata');
                 //$('#addCustomerModal').modal('hide');
                 //$('#customerForm').closest('form').find("input[type=text], textarea,input[type=date],input[type=hidden]").val("");

                 //window.location.reload();
             }
        }
    });
 }


</script>
<h3 class="page-title">
    <?php echo $pageform ?></h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="index.html">Pengaturan</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Branch</a>
        </li>
    </ul>
</div>
<table>
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<div class="row">
    <div class="col-md-12">
        <div class='box'>
            <div class="box-body table-responsive">
                <div id='jqxWidget'>
                    <div id="jqxgrid"></div>
                    <div>
                        <div id="cellbegineditevent"></div>
                        <div id="cellendeditevent"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('vmaster/branch/modal_view');?>