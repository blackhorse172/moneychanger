<?php
header('Cache-Control: max-age=900');
$session = $this->session->userdata('session_data');
if ($session['id'] == '') {
    redirect('user');
}
?>
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" http-equiv="refresh" content="900"/>
        <title>Smartdeal | Admin Dashboard Template</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
        <link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN PAGE STYLES -->
        <link href="<?php echo base_url() ?>assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/clockface/css/clockface.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME STYLES -->
        <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
        <link href="<?php echo base_url() ?>assets/global/css/components-md.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/global/css/plugins-md.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/admin/layout2/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/admin/layout2/css/themes/grey.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?php echo base_url() ?>assets/admin/layout2/css/custom.css" rel="stylesheet" type="text/css"/>

       

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/tooltipster-master/css/tooltipster.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/tooltipster-master/css/themes/tooltipster-light.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/tooltipster-master/css/themes/tooltipster-noir.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/tooltipster-master/css/themes/tooltipster-punk.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/tooltipster-master/css/themes/tooltipster-shadow.css" />
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico"/>
        <link href="<?php echo base_url(); ?>assets/js/autocomplete/css/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css"/>
        <script src="<?php echo base_url(); ?>assets/js/autocomplete/jquery-1.10.2.js" type="text/javascript"></script>
        <?php $this->load->view('autocompletescript') ?>
        <script type="text/javascript">
            var messages    = $('.messages');
            var message     = '';
            var buttonClose = '<button type="button" class="close" id="alertClose" onclick="alertClose()" data-hide="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'


            function alertClose() {
                $('#alert').hide('slow/400/fast', function() {
                    
                });
            }
        </script>
    </head>

    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
    <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
    <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
    <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
    <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->

    <body class="page-md page-boxed page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
        <div class="page-header md-shadow-z-1-i navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner container">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="index.html">
                        <img src="<?php echo base_url() ?>assets/images/logo.png" alt="logo" style="height:50px;" class="logo-default"/>
                    </a>
                    <div class="menu-toggler sidebar-toggler">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN PAGE ACTIONS -->
                <!-- DOC: Remove "hide" class to enable the page header actions -->
                <div class="page-actions hide">
                    <div class="btn-group">
                        <button type="button" class="btn btn-circle red-pink dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-bar-chart"></i>&nbsp;<span class="hidden-sm hidden-xs">New&nbsp;</span>&nbsp;<i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-user"></i> New User </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-present"></i> New Event <span class="badge badge-success">4</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-basket"></i> New order </a>
                            </li>
                            <li class="divider">
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-flag"></i> Pending Orders <span class="badge badge-danger">4</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-users"></i> Pending Users <span class="badge badge-warning">12</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-circle green-haze dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-bell"></i>&nbsp;<span class="hidden-sm hidden-xs">Post&nbsp;</span>&nbsp;<i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-docs"></i> New Post </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-tag"></i> New Comment </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-share"></i> Share </a>
                            </li>
                            <li class="divider">
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-flag"></i> Comments <span class="badge badge-success">4</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-users"></i> Feedbacks <span class="badge badge-danger">2</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- END PAGE ACTIONS -->
                <!-- BEGIN PAGE TOP -->
                <div class="page-top">
              
                    <!-- BEGIN HEADER SEARCH BOX -->
                    <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
                    <form class="search-form search-form-expanded" action="#" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="" name="" title="Software Valid Until : <?php echo $this->general->getExpiredApp('','','ValidDate') ?>" value=" Software Information">
                          

                        </div>

                    </form>
                    <!-- END HEADER SEARCH BOX -->
                    <!-- BEGIN TOP NAVIGATION MENU -->

                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown-user">
                                <a href="" class="dropdown-toggle">
                                    <!--<img alt="" class="img-circle" src="<?php echo base_url() ?>assets/admin/layout2/img/avatar_small.jpg"/>-->
                                  
                                        <!-- Special Thanks to Ricky </span> -->
                                        <p class="username">Last Rate Update : </p> <?php echo $this->general->getLastRateUpdate() ?>
                                </a>
                                
                            </li>
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                          <!--   <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-bell"></i>
                                    <span class="badge badge-default">
                                        0 </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="external">
                                        <h3><span class="bold">0 pending</span> notifications</h3>
                                        <a href="extra_profile.html">view all</a>
                                    </li>
                                                                        </li>
                                </ul>
                            </li> -->
                            <!-- END NOTIFICATION DROPDOWN -->
                            <!-- BEGIN INBOX DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <!--                            <li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
                                                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                                                <i class="icon-envelope-open"></i>
                                                                <span class="badge badge-default">
                                                                    4 </span>
                                                            </a>
                                                            <ul class="dropdown-menu">
                                                                <li class="external">
                                                                    <h3>You have <span class="bold">7 New</span> Messages</h3>
                                                                    <a href="page_inbox.html">view all</a>
                                                                </li>
                                                                <li>
                                                                    <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                                                        <li>
                                                                            <a href="inbox.html?a=view">
                                                                                <span class="photo">
                                                                                    <img src="<?php echo base_url() ?>assets/admin/layout3/img/avatar2.jpg" class="img-circle" alt="">
                                                                                </span>
                                                                                <span class="subject">
                                                                                    <span class="from">
                                                                                        Lisa Wong </span>
                                                                                    <span class="time">Just Now </span>
                                                                                </span>
                                                                                <span class="message">
                                                                                    Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="inbox.html?a=view">
                                                                                <span class="photo">
                                                                                    <img src="<?php echo base_url() ?>assets/admin/layout3/img/avatar3.jpg" class="img-circle" alt="">
                                                                                </span>
                                                                                <span class="subject">
                                                                                    <span class="from">
                                                                                        Richard Doe </span>
                                                                                    <span class="time">16 mins </span>
                                                                                </span>
                                                                                <span class="message">
                                                                                    Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="inbox.html?a=view">
                                                                                <span class="photo">
                                                                                    <img src="<?php echo base_url() ?>assets/admin/layout3/img/avatar1.jpg" class="img-circle" alt="">
                                                                                </span>
                                                                                <span class="subject">
                                                                                    <span class="from">
                                                                                        Bob Nilson </span>
                                                                                    <span class="time">2 hrs </span>
                                                                                </span>
                                                                                <span class="message">
                                                                                    Vivamus sed nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="inbox.html?a=view">
                                                                                <span class="photo">
                                                                                    <img src="<?php echo base_url() ?>assets/admin/layout3/img/avatar2.jpg" class="img-circle" alt="">
                                                                                </span>
                                                                                <span class="subject">
                                                                                    <span class="from">
                                                                                        Lisa Wong </span>
                                                                                    <span class="time">40 mins </span>
                                                                                </span>
                                                                                <span class="message">
                                                                                    Vivamus sed auctor 40% nibh congue nibh... </span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="inbox.html?a=view">
                                                                                <span class="photo">
                                                                                    <img src="<?php echo base_url() ?>assets/admin/layout3/img/avatar3.jpg" class="img-circle" alt="">
                                                                                </span>
                                                                                <span class="subject">
                                                                                    <span class="from">
                                                                                        Richard Doe </span>
                                                                                    <span class="time">46 mins </span>
                                                                                </span>
                                                                                <span class="message">
                                                                                    Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>-->
                            <!-- END INBOX DROPDOWN -->
                            <!-- BEGIN TODO DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <!--                            <li class="dropdown dropdown-extended dropdown-tasks" id="header_task_bar">
                                                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                                                <i class="icon-calendar"></i>
                                                                <span class="badge badge-default">
                                                                    3 </span>
                                                            </a>
                                                            <ul class="dropdown-menu extended tasks">
                                                                <li class="external">
                                                                    <h3>You have <span class="bold">12 pending</span> tasks</h3>
                                                                    <a href="page_todo.html">view all</a>
                                                                </li>
                                                                <li>
                                                                    <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                                                        <li>
                                                                            <a href="javascript:;">
                                                                                <span class="task">
                                                                                    <span class="desc">New release v1.2 </span>
                                                                                    <span class="percent">30%</span>
                                                                                </span>
                                                                                <span class="progress">
                                                                                    <span style="width: 40%;" class="progress-bar progress-bar-success" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"><span class="sr-only">40% Complete</span></span>
                                                                                </span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="javascript:;">
                                                                                <span class="task">
                                                                                    <span class="desc">Application deployment</span>
                                                                                    <span class="percent">65%</span>
                                                                                </span>
                                                                                <span class="progress">
                                                                                    <span style="width: 65%;" class="progress-bar progress-bar-danger" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"><span class="sr-only">65% Complete</span></span>
                                                                                </span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="javascript:;">
                                                                                <span class="task">
                                                                                    <span class="desc">Mobile app release</span>
                                                                                    <span class="percent">98%</span>
                                                                                </span>
                                                                                <span class="progress">
                                                                                    <span style="width: 98%;" class="progress-bar progress-bar-success" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100"><span class="sr-only">98% Complete</span></span>
                                                                                </span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="javascript:;">
                                                                                <span class="task">
                                                                                    <span class="desc">Database migration</span>
                                                                                    <span class="percent">10%</span>
                                                                                </span>
                                                                                <span class="progress">
                                                                                    <span style="width: 10%;" class="progress-bar progress-bar-warning" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"><span class="sr-only">10% Complete</span></span>
                                                                                </span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="javascript:;">
                                                                                <span class="task">
                                                                                    <span class="desc">Web server upgrade</span>
                                                                                    <span class="percent">58%</span>
                                                                                </span>
                                                                                <span class="progress">
                                                                                    <span style="width: 58%;" class="progress-bar progress-bar-info" aria-valuenow="58" aria-valuemin="0" aria-valuemax="100"><span class="sr-only">58% Complete</span></span>
                                                                                </span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="javascript:;">
                                                                                <span class="task">
                                                                                    <span class="desc">Mobile development</span>
                                                                                    <span class="percent">85%</span>
                                                                                </span>
                                                                                <span class="progress">
                                                                                    <span style="width: 85%;" class="progress-bar progress-bar-success" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"><span class="sr-only">85% Complete</span></span>
                                                                                </span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="javascript:;">
                                                                                <span class="task">
                                                                                    <span class="desc">New UI release</span>
                                                                                    <span class="percent">38%</span>
                                                                                </span>
                                                                                <span class="progress progress-striped">
                                                                                    <span style="width: 38%;" class="progress-bar progress-bar-important" aria-valuenow="18" aria-valuemin="0" aria-valuemax="100"><span class="sr-only">38% Complete</span></span>
                                                                                </span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>-->
                            <!-- END TODO DROPDOWN -->
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <!--<img alt="" class="img-circle" src="<?php echo base_url() ?>assets/admin/layout2/img/avatar_small.jpg"/>-->
                                    <span class="username username-hide-on-mobile">
                                        Hello, <?php echo $session['id']; ?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <!--                                    <li>
                                                                            <a href="extra_profile.html">
                                                                                <i class="icon-user"></i> My Profile </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="page_calendar.html">
                                                                                <i class="icon-calendar"></i> My Calendar </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="inbox.html">
                                                                                <i class="icon-envelope-open"></i> My Inbox <span class="badge badge-danger">
                                                                                    3 </span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="page_todo.html">
                                                                                <i class="icon-rocket"></i> My Tasks <span class="badge badge-success">
                                                                                    7 </span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="divider">
                                                                        </li>
                                                                        <li>
                                                                            <a href="extra_lock.html">
                                                                                <i class="icon-lock"></i> Lock Screen </a>
                                                                        </li>-->
                                    <li>
                                        <a href="<?php echo base_url(); ?>user/logout">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END PAGE TOP -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="container">
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <?php $this->load->view('menu') ?>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <div class="page-content">
                        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Modal title</h4>
                                    </div>
                                    <div class="modal-body">
                                        Widget settings form goes here
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn blue">Save changes</button>
                                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                        <!-- BEGIN STYLE CUSTOMIZER -->

                        <!-- END STYLE CUSTOMIZER -->
                        <!-- BEGIN PAGE HEADER-->

                         <div class="custom-alerts alert alert-danger fade in" role="alert" id="alert" style="display:none;"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><div class="messages">    
                         </div></div>

                          <div class=" alert alert-success" role="alert" id="alertSuccess" style="display:none;"></div>


                        <?php $this->load->view($isi) ?>
                    </div>
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->
                <!--Cooming Soon...-->
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner">
                    2016 &copy; Smartdeal. <a href="#" target="_blank">smartest exchange and remit parner</a>
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="<?php echo base_url() ?>assets/global/plugins/respond.min.js"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/excanvas.min.js"></script>
        <![endif]-->
        <script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<?php echo base_url() ?>assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url() ?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
        <script src="<?php echo base_url() ?>assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/clockface/js/clockface.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/select2/select2.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url(); ?>assets/global/scripts/metronic.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/admin/pages/scripts/index.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/admin/pages/scripts/table-advanced.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/pages/scripts/components-pickers.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
        <script src="<?php echo base_url(); ?>assets/global/plugins/icheck/icheck.min.js"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url(); ?>assets/js/autoNumeric.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.bsFormAlerts.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/tooltipster-master/doc/js/jquery.jgfeed.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/tooltipster-master/doc/js/prettify.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/tooltipster-master/doc/js/lang-css.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/tooltipster-master/doc/js/scripts.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/tooltipster-master/js/jquery.tooltipster.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/tooltipster-master/js/jquery.tooltipster.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/admin/pages/scripts/form-icheck.js"></script>
        <script type="text/javascript">
            jQuery(function ($) {
                $('.auto').autoNumeric('init', {vMin: -9999999999999.99, mDec: 2});
            });
        </script>

        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core componets
                Layout.init(); // init layout
                QuickSidebar.init(); // init quick sidebar
                Demo.init(); // init demo features
                ComponentsPickers.init();
                Index.init();
                Index.initDashboardDaterange();
                Index.initJQVMAP(); // init index page's custom scripts
                Index.initCalendar(); // init index page's custom scripts
                Index.initCharts(); // init index page's custom scripts
                Index.initChat();
                Index.initMiniCharts();
                Tasks.initDashboardWidget();
                TableAdvanced.init();
                UIExtendedModals.init();
                FormiCheck.init();
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>