
<link rel="stylesheet" href="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/styles/jqx.base.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/scripts/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxdata.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxmenu.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.filter.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.sort.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.columnsresize.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.edit.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.selection.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxtooltip.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxinput.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxlistbox.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxdatatable.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxdropdownlist.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxcheckbox.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxcalendar.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxnumberinput.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxdatetimeinput.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/globalization/globalize.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/scripts/demos.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/demos/sampledata/generatedata.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/globalization/globalize.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/demos/jqxgrid/localization.js"></script>
<style type="text/css">
    .redClass
    {
        background-color: #FF0000;
    }
    .greenClass
    {
        background-color: #228B22;
    }
    .blueClass
    {
        background-color: #87CEEB;
    }
    .orangeClass
    {
        background-color: #FFA500;
    }
    .yellowClass
    {
        background-color: #FFFF00;
    }
    .whiteClass
    {
        background-color: White;
    }
</style>
<script type="text/javascript">
    var jQuery_1_4_3 = $.noConflict(true);
    jQuery_1_4_3(document).ready(function () {
        // prepare the data
//        var url = "http://localhost/smartdeal/akunting/get_data";
        var url = "<?php echo site_url('smartreward/getdata_currencypoint_category'); ?>";
        var source =
                {
                    datatype: "json",
                    addRow: function (rowID, rowData, position, commit) {
                        // synchronize with the server - send insert command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                        commit(true);
                    },
                    updaterow: function (rowid, rowdata, commit) {
                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                    deleteRow: function (rowID, commit) {
                        // synchronize with the server - send delete command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failed.
                        commit(true);
                    },
                    datafields:
                            [
                                {name: 'Iid', type: 'string'},
                                {name: 'CategoryId', type: 'string'},
                                {name: 'IDRAmountGetPoint', type: 'number'},
                                {name: 'PointEqual', type: 'number'},
                                {name: 'Remarks', type: 'string'}
                            ],
                    addrow: function (rowid, rowdata, position, commit) {
                        // synchronize with the server - send insert command
                        // call commit with parameter true if the synchronization with the server is successful
                        //and with parameter false if the synchronization failed.
                        // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                        commit(true);
                    },

                    id: 'Iid',
                    url: url,
                    root: 'data'
                };
        var dataAdapter = new jQuery_1_4_3.jqx.dataAdapter(source);
        var CategoryAdapter = new jQuery_1_4_3.jqx.dataAdapter(source, {
            autoBind: true
        });

        var url2 = "<?php echo site_url('smartreward/getdata_currencypoint'); ?>";
        var source2 =
                {
                    datatype: "json",
                    updaterow: function (rowid, rowdata, commit) {
                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                    datafields:
                            [
                                {name: 'Iid', type: 'string'},
                                {name: 'CategoryId', type: 'string'},
                                {name: 'Currency', type: 'string'},
                                {name: 'Remarks', type: 'string'},
                                {name: 'EditBy', type: 'string'},
                                {name: 'EditDate', type: 'string'}
                            ],
                    addrow: function (rowid, rowdata, position, commit) {
                        // synchronize with the server - send insert command
                        // call commit with parameter true if the synchronization with the server is successful
                        //and with parameter false if the synchronization failed.
                        // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                        commit(true);
                    },
                    id: 'Iid',
                    url: url2,
                    root: 'data'
                };
        var dataAdapter2 = new jQuery_1_4_3.jqx.dataAdapter(source2);
        // initialize jqxGrid
        jQuery_1_4_3("#jqxgrid").jqxGrid(
                {
                    width: 1000,
                    height: 230,
                    source: dataAdapter,
                    editable: true,
                    showfilterrow: true,
                    filterable: true,
                    altRows: true,
                    selectionmode: 'singlecell',
//                    editmode: 'click',
//                    autowidth: true,
//                    autoheight: true,
                    sortable: true,
                    showtoolbar: true,
                    renderToolbar: function (toolBar)
                    {
                        var toTheme = function (className) {
                            if (theme == "")
                                return className;
                            return className + " " + className + "-" + theme;
                        }

                        var container = jQuery_1_4_3("<div style='overflow: hidden; position: relative; height: 100%; width: 100%;'></div>");
                        var buttonTemplate = "<div style='float: left; padding: 3px; margin: 2px;'><div style='margin: 4px; width: 16px; height: 16px;'></div></div>";
                        var addButton = jQuery_1_4_3(buttonTemplate);

                        container.append(addButton);

                        toolBar.append(container);
                        addButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        addButton.find('div:first').addClass(toTheme('jqx-icon-plus'));
                        addButton.jqxTooltip({position: 'bottom', content: "Tambah Kategori"});



                        addButton.click(function (event) {
                            if (!addButton.jqxButton('disabled')) {
                                var id = jQuery_1_4_3("#jqxgrid").jqxGrid('getdatainformation').rowscount;
                                jQuery_1_4_3("#jqxgrid").jqxGrid('addrow', {}, {}, 'first');

                                jQuery_1_4_3("#jqxgrid").jqxGrid('selectcell', 0, 'CategoryId');
                                jQuery_1_4_3("#jqxgrid").jqxGrid('setcellvalue', 0, "Iid", "0");

                            }
                        });

//                        var me = this;
//                        var container = jQuery_1_4_3("<div style='margin: 5px;'></div>");
//                        toolbar.append(container);
//                        container.append('<input id="addrowbutton" type="button" value="Tambah Kategori" />');
//                        jQuery_1_4_3("#addrowbutton").jqxButton();
                        // create new row.
                    },

                    columns: [
                        {text: '', datafield: '', columntype: 'button', pinned: true, filterable: false, cellsrenderer: function () {
                                return 'X';
                            }, buttonclick: function (row) {
                                var r = confirm("Anda yakin menghapus data?");
                                if (r == true) {
                                    var rowBoundIndex = row;
                                    var rowData = jQuery_1_4_3('#jqxgrid').jqxGrid('getrowdata', rowBoundIndex);
                                    var value_iid = rowData['Iid'];
                                    jQuery_1_4_3("#jqxgrid").jqxGrid('deleterow', value_iid); // id harus di set Iid di parsing json diatas

                                    delete_data_row_category(value_iid);
                                } else {
                                    return false;
                                }

                            }
                        },
                        {text: 'Iid', columntype: 'textbox', datafield: 'Iid', width: 70, hidden: true},
                        {text: 'Kode Kategori', columntype: 'textbox', datafield: 'CategoryId', width: 210},
//                        {text: 'Kode Kategori', columntype: 'textbox', datafield: 'IDRAmountGetPoint', width: 210},
//                        {text: 'Kode Kategori', columntype: 'textbox', datafield: 'PointEqual', width: 210},
                        {
                            text: 'IDR Amount', datafield: 'IDRAmountGetPoint', width: 210, align: 'right', cellsformat: "f", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) {
                                if (value < 0) {
                                    return {result: false, message: "Tidak boleh minus"};
                                }
                                return true;
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 0, digits: 12});
                            }
                        },
                        {
                            text: 'Poin', datafield: 'PointEqual', width: 210, align: 'right', cellsformat: "f", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) {
                                if (value < 1) {
                                    return {result: false, message: "Tidak boleh kurang dari 1"};
                                }
                                return true;
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 0, digits: 12});
                            }
                        },
                        {text: 'Catatan', columntype: 'textbox', datafield: 'Remarks', width: 370}
                    ]
                });
        jQuery_1_4_3("#jqxgrid_2").jqxGrid(
                {
                    width: 1000,
                    height: 350,
                    source: dataAdapter2,
                    editable: true,
                    showfilterrow: true,
                    filterable: true,
                    altRows: true,
                    selectionmode: 'singlecell',
//                    editmode: 'selectedrow',
//                    autowidth: true,
//                    autoheight: true,
                    sortable: true,
                    showtoolbar: true,
                    renderToolbar: function (toolBar)
                    {
                        var toTheme = function (className) {
                            if (theme == "")
                                return className;
                            return className + " " + className + "-" + theme;
                        }

                        var container = jQuery_1_4_3("<div style='overflow: hidden; position: relative; height: 100%; width: 100%;'></div>");
                        var buttonTemplate = "<div style='float: left; padding: 3px; margin: 2px;'><div style='margin: 4px; width: 16px; height: 16px;'></div></div>";
                        var addButton = jQuery_1_4_3(buttonTemplate);
                        container.append(addButton);

                        toolBar.append(container);
                        addButton.jqxButton({cursor: "pointer", enableDefault: false, height: 25, width: 25});
                        addButton.find('div:first').addClass(toTheme('jqx-icon-plus'));
                        addButton.jqxTooltip({position: 'bottom', content: "Tambah Valas"});

                        addButton.click(function (event) {
                            if (!addButton.jqxButton('disabled')) {
                                var id = jQuery_1_4_3("#jqxgrid_2").jqxGrid('getdatainformation').rowscount;
//                            jQuery_1_4_3("#jqxgrid_2").jqxGrid('addrow', id, {});
                                jQuery_1_4_3("#jqxgrid_2").jqxGrid('addrow', {}, {}, 'first');

                                jQuery_1_4_3('#jqxgrid_2').jqxGrid('selectcell', 0, 'Currency');
                                jQuery_1_4_3("#jqxgrid_2").jqxGrid('setcellvalue', 0, "Iid", "0");
                            }
                        });

//                        var me = this;
//                        var container = jQuery_1_4_3("<div style='margin: 5px;'></div>");
//                        toolbar.append(container);
//                        container.append('<input id="addrowbutton1" type="button" value="Tambah Valas" />');
//                        jQuery_1_4_3("#addrowbutton1").jqxButton();
//                        // create new row.
//                        jQuery_1_4_3("#addrowbutton1").on('click', function () {
//                            var id = jQuery_1_4_3("#jqxgrid_2").jqxGrid('getdatainformation').rowscount;
////                            jQuery_1_4_3("#jqxgrid_2").jqxGrid('addrow', id, {});
//                            jQuery_1_4_3("#jqxgrid_2").jqxGrid('addrow', {}, {}, 'first');
//
//                            jQuery_1_4_3('#jqxgrid_2').jqxGrid('selectcell', 0, 'Currency');
//                            jQuery_1_4_3("#jqxgrid_2").jqxGrid('setcellvalue', 0, "Iid", "0");
//                        });
                    },
                    columns: [
                        {text: '', datafield: '', columntype: 'button', pinned: true, filterable: false, cellsrenderer: function () {
                                return 'X';
                            }, buttonclick: function (row) {
                                var r = confirm("Anda yakin menghapus data?");
                                if (r == true) {
                                    var rowBoundIndex = row;
                                    var rowData = jQuery_1_4_3('#jqxgrid_2').jqxGrid('getrowdata', rowBoundIndex);
                                    var value_iid = rowData['Iid'];
                                    jQuery_1_4_3("#jqxgrid_2").jqxGrid('deleterow', value_iid); // id harus di set Iid di parsing json diatas

                                    delete_data_row(value_iid);
                                } else {
                                    return false;
                                }

                            }
                        },
                        {text: 'Iid', columntype: 'textbox', datafield: 'Iid', width: 70, hidden: true},
                        {text: 'Valas', columntype: 'textbox', datafield: 'Currency', width: 210},
                        {text: 'Kode Kategori', datafield: 'CategoryId', columntype: "dropdownlist",
                            createeditor: function (row, value, editor) {
                                editor.jqxDropDownList({source: CategoryAdapter, displayMember: 'CategoryId', valueMember: 'CategoryId'});
                            }},
                        {text: 'Catatan', columntype: 'textbox', datafield: 'Remarks', width: 220},
                        {text: 'LastEdit Date', columntype: 'textbox', datafield: 'EditDate', width: 180},
                        {text: 'LastEdit By', columntype: 'textbox', datafield: 'EditBy', width: 170}
                    ]
                });
        // events
        var rowValues = "";
        jQuery_1_4_3("#jqxgrid").on('cellbeginedit', function (event) {
            var args = event.args;
            if (args.datafield === "firstname") {
                rowValues = "";
            }
            rowValues += args.value.toString() + "    ";
            if (args.datafield === "price") {
                $("#cellbegineditevent").text("Begin Row Edit: " + (1 + args.rowindex) + ", Data: " + rowValues);
            }
        });
        jQuery_1_4_3("#jqxgrid").on('cellendedit', function (event) {
            var args = event.args;
            if (args.datafield === "firstname") {
                rowValues = "";
            }
            rowValues += args.value.toString() + "    ";
            if (args.datafield === "price") {
                jQuery_1_4_3("#cellbegineditevent").text("End Row Edit: " + (1 + args.rowindex) + ", Data: " + rowValues);
            }
        });
        jQuery_1_4_3("#jqxgrid").unbind('cellendedit').on('cellendedit', function (event) {
            var field = jQuery_1_4_3("#jqxgrid").jqxGrid('getcolumn', event.args.datafield).datafield;
            var rowBoundIndex = event.args.rowindex;
            var rowData = jQuery_1_4_3('#jqxgrid').jqxGrid('getrowdata', rowBoundIndex);
            var value_iid = rowData['Iid'];
            var value_updated = args.value;
//            alert(value_iid + value_updated);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>smartreward/update_data_category",
                cache: false,
                data: {iid: value_iid, field: field, updated_param: value_updated}, // since, you need to delete post of particular id
                success: function (reaksi) {
//                    alert(reaksi);
                    if (reaksi == 1) {
//                    alert("Success");

                    } else if (reaksi == 2) {
                        location.reload();
                    } else if (reaksi == -8) {
                        alert("Kode Kategori sudah digunakan");

                    } else if (reaksi == -9) {
                        alert("User anda tidak ada otoritas merubah data");
                    }
                }
            });
        });
        jQuery_1_4_3("#jqxgrid_2").unbind('cellendedit').on('cellendedit', function (event) {
            var field = jQuery_1_4_3("#jqxgrid_2").jqxGrid('getcolumn', event.args.datafield).datafield;
            var rowBoundIndex = event.args.rowindex;
            var rowData = jQuery_1_4_3('#jqxgrid_2').jqxGrid('getrowdata', rowBoundIndex);
            var value_iid = rowData['Iid'];
            var value_updated = args.value;
//            alert(value_iid + value_updated);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>smartreward/update_data",
                cache: false,
                data: {iid: value_iid, field: field, updated_param: value_updated}, // since, you need to delete post of particular id
                success: function (reaksi) {
//                    alert(reaksi);
                    if (reaksi == 1) {
//                    alert("Success");
                    } else if (reaksi == 2) {
                        location.reload();
                    } else if (reaksi == -8) {
                        alert("Valas sudah ada");
                    } else if (reaksi == -9) {
                        alert("User anda tidak ada otoritas merubah data");
                    }
                }
            });
        });
    }

    );
    function delete_data_row_category(value_iid) {
//        alert(value_iid);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>smartreward/delete_data_row_category",
            cache: false,
            data: {iid: value_iid}, // since, you need to delete post of particular id
            success: function (reaksi) {
//                    alert(reaksi);
                if (reaksi == 1) {
//                    alert("Success");

                } else if (reaksi == -9) {
                    alert("User anda tidak ada otoritas menghapus data! data akan kembali ketika refresh page");
                }
            }
        });
    }
    function delete_data_row(value_iid) {
//        alert(value_iid);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>smartreward/delete_data_row",
            cache: false,
            data: {iid: value_iid}, // since, you need to delete post of particular id
            success: function (reaksi) {
//                    alert(reaksi);
                if (reaksi == 1) {
//                    alert("Success");

                } else if (reaksi == -9) {
                    alert("User anda tidak ada otoritas menghapus data! data akan kembali ketika refresh page");
                }
            }
        });
    }
    function convert_date(str) {
        var date = new Date(str),
                mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }
    function get_newgrid() {
        var fromsortdate = document.getElementById('fromsortdate').value;
        var tosortdate = document.getElementById('tosortdate').value;
//        alert(tosortdate);
        var tmpS = jQuery_1_4_3("#jqxgrid").jqxGrid('source');
        tmpS._source.url = "<?php echo site_url('akunting/get_data/') ?>" + fromsortdate + '/' + tosortdate;
        jQuery_1_4_3("#jqxgrid").jqxGrid('source', tmpS);
    }
</script>
<h3 class="page-title">
    <?php echo $pageform ?></h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="index.html">Smart Reward</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Set Point</a>
        </li>
    </ul>

</div>
<table>
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<!--<table>
    <tr>
        <td>
            <label class="control-label col-md-5">Periode&nbsp;Data&nbsp;
            </label>
        </td>
        <td>
            <form action="<?php echo base_url() . 'indexsort/poproduct' ?>" method='post' name="formsortindex" id="formsortindex">
                <div class="input-group input-large date-picker input-daterange" data-date="" data-date-format="dd-mm-yyyy">
                    <input type="text" class="form-control" name="fromsortdate" id="fromsortdate" value="<?php echo date('d-m-Y', strtotime(date('d-m-Y') . "-1 days")) ?>" autocomplete="on">
                    <span class="input-group-addon">
                        to </span>
                    <input type="text" class="form-control" name="tosortdate" id="tosortdate" value="<?php echo date('d-m-Y', strtotime(date('d-m-Y') . "-1 days")) ?>" autocomplete="on">

                </div>

            </form>
        </td>
        <td>
            <button type="submit" class="btn green" style="margin: -16px 0 0 1px;height: 32px;" onclick="get_newgrid()">Apply</button>
        </td>
    </tr>
</table>-->
<div class="row">
    <div class="col-md-12">
        <div id='jqxWidget'>
            <div id="jqxgrid"></div>
            <div style="font-size: 12px; font-family: Verdana, Geneva, 'DejaVu Sans', sans-serif; margin-top: 30px;">
                <div id="cellbegineditevent"></div>
                <div style="margin-top: 10px;" id="cellendeditevent"></div>
            </div>
        </div>

    </div>
    <div class="col-md-12">
        <div id='jqxWidget'>
            <div id="jqxgrid_2"></div>
            <div style="font-size: 12px; font-family: Verdana, Geneva, 'DejaVu Sans', sans-serif; margin-top: 30px;">
                <div id="cellbegineditevent"></div>
                <div style="margin-top: 10px;" id="cellendeditevent"></div>
            </div>
        </div>

    </div>
</div>