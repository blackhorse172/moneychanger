<script type="text/javascript">
    $(document).ready(function () {
        $('#custauto').autocomplete({
            source: "<?php echo site_url('autocomplete/custauto'); ?>"
        });
        $('#companyidauto').autocomplete({
            source: "<?php echo site_url('autocomplete/companyidauto'); ?>"
        });
        $('#useridauto').autocomplete({
            source: "<?php echo site_url('autocomplete/useridauto'); ?>"
        });
        $('#rolecode_auto').autocomplete({
            source: "<?php echo site_url('autocomplete/rolecode_auto'); ?>"
        });
        $('#kodecabang_auto').autocomplete({
            source: "<?php echo site_url('autocomplete/kodecabang_auto'); ?>"
        });
    });
</script>