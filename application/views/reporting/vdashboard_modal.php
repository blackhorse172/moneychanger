<div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Man Power - Detail
            </div>
            <!--<div class="row">-->
            <div class="col-md-12">
                <div id='jqxWidget'>
                    <div id="jqxgrid"></div>
                    <div style="font-size: 12px; font-family: Verdana, Geneva, 'DejaVu Sans', sans-serif; margin-top: 30px;">
                        <div id="cellbegineditevent"></div>
                        <div style="margin-top: 10px;" id="cellendeditevent"></div>
                    </div>
                </div>

            </div>
            <!--</div>-->
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <!--                <button type="button" class="btn blue">Save changes</button>-->
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>