<h3 class="page-title">
    <?php echo $pageform ?></h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="index.html">Reporting</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Kebersihan</a>
        </li>
    </ul>
    <div class="page-toolbar">
        <div id="dashboard-report-range" class="tooltips btn btn-fit-height btn-sm green-haze btn-dashboard-daterange" data-container="body" data-placement="left" data-original-title="Change dashboard date range">
            <i class="icon-calendar"></i>
            &nbsp;&nbsp; <i class="fa fa-angle-down"></i>
        </div>
    </div>
</div>
<table>
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<div class="portlet light bg-inverse">
    <div class="row">
        <div class="col-md-12">

            <form enctype="multipart/form-data" action="<?php echo base_url() . 'Kebersihan/save' ?>" method='post'>
                <div class="modal-body">
                    <div class="scroller" style="height:300px" data-always-visible="1" data-rail-visible1="1">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" data-toggle="tooltip">Kebersihan Area Teller&nbsp;:&nbsp;</label>
                                    <div class="radio-list">
                                        <div class="col-md-9">
                                            <label class="radio-inline">
                                                <input type="radio" name="teller" id="tipe1" value="0" checked> 0% </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="teller" id="tipe2" value="25"> 25% </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="teller" id="tipe2" value="50"> 50% </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="teller" id="tipe2" value="100"> 100% </label>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" data-toggle="tooltip">Kebersihan Area Pantry&nbsp;:&nbsp;</label>
                                    <div class="radio-list">
                                        <div class="col-md-9">
                                            <label class="radio-inline">
                                                <input type="radio" name="pantry" id="tipe1" value="0" checked> 0% </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="pantry" id="tipe2" value="25"> 25% </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="pantry" id="tipe2" value="50"> 50% </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="pantry" id="tipe2" value="100"> 100% </label>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" data-toggle="tooltip">Kebersihan Area Dealing&nbsp;:&nbsp;</label>
                                    <div class="radio-list">
                                        <div class="col-md-9">
                                            <label class="radio-inline">
                                                <input type="radio" name="dealing" id="tipe1" value="0" checked> 0% </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="dealing" id="tipe2" value="25"> 25% </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="dealing" id="tipe2" value="50"> 50% </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="dealing" id="tipe2" value="100"> 100% </label>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" data-toggle="tooltip">Kebersihan Ruang Tunggu&nbsp;:&nbsp;</label>
                                    <div class="radio-list">
                                        <div class="col-md-9">
                                            <label class="radio-inline">
                                                <input type="radio" name="ruangtunggu" id="tipe1" value="0" checked> 0% </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="ruangtunggu" id="tipe2" value="25"> 25% </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="ruangtunggu" id="tipe2" value="50"> 50% </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="ruangtunggu" id="tipe2" value="100"> 100% </label>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" data-toggle="tooltip">Kebersihan Area Tak Terpakai&nbsp;:&nbsp;</label>
                                    <div class="radio-list">
                                        <div class="col-md-9">
                                            <label class="radio-inline">
                                                <input type="radio" name="takterpakai" id="tipe1" value="0" checked> 0% </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="takterpakai" id="tipe2" value="25"> 25% </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="takterpakai" id="tipe2" value="50"> 50% </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="takterpakai" id="tipe2" value="100"> 100% </label>
                                        </div>
                                    </div>
                                </div><br>
                                <br>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Catatan :</label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" rows="2" name="catatan" id="catatan" placeholder="Catatan"></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" formaction="<?php echo base_url() . 'beranda' ?>" class="btn default">Tutup</button>
                        <button type="submit" class="btn green">Submit</button>
                    </div>
            </form>
        </div>
    </div>
    <div class="clearfix">
    </div>
</div>
