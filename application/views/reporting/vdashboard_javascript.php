
<link rel="stylesheet" href="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/styles/jqx.base.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/scripts/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxdata.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxmenu.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.filter.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.sort.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.edit.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.selection.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxlistbox.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxdropdownlist.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxcheckbox.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxcalendar.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxnumberinput.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxdatetimeinput.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/globalization/globalize.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/scripts/demos.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/demos/sampledata/generatedata.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/globalization/globalize.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/demos/jqxgrid/localization.js"></script>
<style type="text/css">
    .redClass
    {
        background-color: #FF0000;
    }
    .greenClass
    {
        background-color: #228B22;
    }
    .blueClass
    {
        background-color: #87CEEB;
    }
    .orangeClass
    {
        background-color: #FFA500;
    }
    .yellowClass
    {
        background-color: #FFFF00;
    }
    .whiteClass
    {
        background-color: White;
    }
</style>
<script type="text/javascript">
    var jQuery_1_4_3 = $.noConflict(true);
    jQuery_1_4_3(document).ready(function () {
        // prepare the data
//        var url = "http://localhost/smartdeal/akunting/get_data";
        var url = "<?php echo site_url(''); ?>";
        var source =
                {
                    datatype: "json",
                    updaterow: function (rowid, rowdata, commit) {
                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                    datafields:
                            [
                                {name: 'Iid', type: 'string'},
                                {name: 'Nomor', type: 'string'},
                                {name: 'BranchId', type: 'string'},
                                {name: 'NIK', type: 'string'},
                                {name: 'Nama', type: 'string'},
                                {name: 'Jabatan', type: 'string'},
                                {name: 'Grade', type: 'string'},
                                {name: 'TanggalMasuk', type: 'date'},
                                {name: 'StatusKontrak', type: 'string'},
                                {name: 'TanggalKontrak_Pengangkatan', type: 'date'},
                                {name: 'Gaji', type: 'number'},
                                {name: 'UangMakan', type: 'number'},
                                {name: 'TanggalPenyesuaianGajiAkhir', type: 'date'},
                                {name: 'NominalPenyesuaianGajiAkhir', type: 'number'},
                                {name: 'Catatan', type: 'string'}
                            ],
                    id: 'id',
                    url: url,
                    root: 'data'
                };

        var dataAdapter = new jQuery_1_4_3.jqx.dataAdapter(source);
        // initialize jqxGrid
        jQuery_1_4_3("#jqxgrid").jqxGrid(
                {
                    width: 870,
                    height: 450,
                    source: dataAdapter,
//                    editable: true,
//                    showfilterrow: true,
                    filterable: true,
                    selectionmode: 'singlecell',
//                    editmode: 'selectedrow',
//                    autowidth: true,
//                    autoheight: true,
                    sortable: true,
                    columns: [
                        {text: 'Iid', columntype: 'textbox', datafield: 'Iid', width: 70, hidden: true},
                        {text: 'No.', columntype: 'textbox', datafield: 'Nomor', width: 70, pinned: true},
                        {text: 'NIK', columntype: 'textbox', datafield: 'NIK', width: 100, pinned: true},
                        {text: 'Nama', columntype: 'textbox', datafield: 'Nama', width: 100, pinned: true},
                        {text: 'Jabatan', columntype: 'textbox', datafield: 'Jabatan', width: 100},
                        {text: 'Grade', columntype: 'textbox', datafield: 'Grade', width: 100},
                        {
                            text: 'Tanggal Masuk', datafield: 'TanggalMasuk', columntype: 'datetimeinput', width: 110, align: 'right', cellsalign: 'right', cellsformat: 'd/M/yyyy',
                            validation: function (cell, value) {
                                if (value == "")
                                    return true;
                                var year = value.getFullYear();
                                if (year <= 2015) {
                                    return {result: false, message: "Ship Date should be before 1/1/2015"};
                                }
                                return true;
                            }
                        },
                        {text: 'Status', columntype: 'textbox', datafield: 'StatusKontrak', width: 100},
                        {
                            text: 'Tgl. Kontrak/Pengangkatan', datafield: 'TanggalKontrak_Pengangkatan', columntype: 'datetimeinput', width: 110, align: 'right', cellsalign: 'right', cellsformat: 'd/M/yyyy',
                            validation: function (cell, value) {
                                if (value == "")
                                    return true;
                                var year = value.getFullYear();
                                if (year <= 2015) {
                                    return {result: false, message: "Ship Date should be before 1/1/2015"};
                                }
                                return true;
                            }
                        },
                        {
                            text: 'Gaji', datafield: 'Gaji', width: 110, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) {
//                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                                return true;
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'Uang Makan', datafield: 'UangMakan', width: 110, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) {
//                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                                return true;
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'Tgl. Penyesuaian', datafield: 'TanggalPenyesuaianGajiAkhir', columntype: 'datetimeinput', width: 110, align: 'right', cellsalign: 'right', cellsformat: 'd/M/yyyy',
                            validation: function (cell, value) {
                                if (value == "")
                                    return true;
                                var year = value.getFullYear();
                                if (year <= 2015) {
                                    return {result: false, message: "Ship Date should be before 1/1/2015"};
                                }
                                return true;
                            }
                        },
                        {
                            text: 'Nominal Penyesuaian', datafield: 'NominalPenyesuaianGajiAkhir', width: 110, align: 'right', cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) {
//                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                                return true;
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {text: 'Catatan', columntype: 'textbox', datafield: 'Catatan', width: 100}
                    ]
                });
        // events
    }

    );
    function convert_date(str) {
        var date = new Date(str),
                mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }
    function get_newgrid($cabang) {
//        alert(tosortdate);
        var tmpS = jQuery_1_4_3("#jqxgrid").jqxGrid('source');
        tmpS._source.url = "<?php echo site_url('dashboard/get_data_hrd/') ?>" + $cabang;
        jQuery_1_4_3("#jqxgrid").jqxGrid('source', tmpS);
    }
</script>