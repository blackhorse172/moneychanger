<?php $this->load->view('reporting/vdashboard_javascript') ?>
<h3 class="page-title">
    <?php echo $pageform ?></h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="index.html">Reporting</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Dashboard</a>
        </li>
    </ul>
    <div class="page-toolbar">
        <div id="dashboard-report-range" class="tooltips btn btn-fit-height btn-sm green-haze btn-dashboard-daterange" data-container="body" data-placement="left" data-original-title="Change dashboard date range">
            <i class="icon-calendar"></i>
            &nbsp;&nbsp; <i class="fa fa-angle-down"></i>
        </div>
    </div>
</div>
<table>
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<div class="portlet light bg-inverse">
    <div class="row">
        <div class="row">
            <div class="col-md-6">
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box green-haze">
                    <div class="portlet-title">
                        <div class="caption">
                            Man Power
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Penempatan
                                    </th>
                                    <th>
                                        Man Power
                                    </th>
                                    <th class="hidden-480">
                                        Total Gaji
                                    </th>
                                    <th>
                                        Total Uang Makan
                                    </th>

                                </tr>
                            </thead>
                            <tbody align="center">
                                <?php foreach ($db_hrd->result() as $baris): ?>
                                    <tr data-toggle="modal" href="#large" onclick="get_newgrid('<?php echo $baris->BranchId ?>')">
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $baris->BranchId; ?></td>
                                        <td><?php echo $baris->ManPower; ?></td>
                                        <td><?php echo number_format($baris->TotalGaji, 0, ".", ","); ?></td>
                                        <td><?php echo number_format($baris->TotalUM, 0, ".", ","); ?></td>
                                    </tr>
                                    </a>
                                    <?php
                                    $no++;
                                endforeach;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>

        </div>

    </div>
    <div class="clearfix">
    </div>
</div>
<?php $this->load->view('reporting/vdashboard_modal') ?>