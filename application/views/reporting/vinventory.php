<h3 class="page-title">
    <?php echo $pageform ?></h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="index.html">Reporting</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Kinerja</a>
        </li>
    </ul>
    <div class="page-toolbar">
        <div id="dashboard-report-range" class="tooltips btn btn-fit-height btn-sm green-haze btn-dashboard-daterange" data-container="body" data-placement="left" data-original-title="Change dashboard date range">
            <i class="icon-calendar"></i>
            &nbsp;&nbsp; <i class="fa fa-angle-down"></i>
        </div>
    </div>
</div>
<table>
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<div class="portlet light bg-inverse">
    <div class="row">
        <div class="col-md-12">

            <form enctype="multipart/form-data" action="<?php echo base_url() . 'manpower/save' ?>" method='post'>
                <div class="modal-body">
                    <div class="scroller" style="height:300px" data-always-visible="1" data-rail-visible1="1">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Mobil&nbsp;: </label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="nik" id="nik">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Motor&nbsp;:</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="nama" id="nama">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">CCTV&nbsp;:</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="jabatan" id="jabatan">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Mesin Hitung Standing&nbsp;:</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="grade" id="grade">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Mesin Deteksi Uang&nbsp;:</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="gaji" id="gaji">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">TV dan Kegunaan &nbsp;:</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="uangmakan" id="uangmakan">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">AC&nbsp;:</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="nominal" id="nominal">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Kamera CCTV&nbsp;:</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="nominal" id="nominal">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">


                                <div class="form-group">
                                    <label class="col-md-5 control-label">Kursi Back Office&nbsp;:</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="nominal" id="nominal">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Kursi Kasir&nbsp;:</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="nominal" id="nominal">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Komputer Teller&nbsp;:</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="nominal" id="nominal">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Komputer Back Office&nbsp;:</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="nominal" id="nominal">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Komp. Display Harga&nbsp;:</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="nominal" id="nominal">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Printer Thermal&nbsp;:</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="nominal" id="nominal">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Printer Back Office&nbsp;:</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="nominal" id="nominal">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Laptop&nbsp;:</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="nominal" id="nominal">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Catatan :</label>
                                    <div class="col-md-7">
                                        <textarea class="form-control" rows="2" name="catatan" id="catatan" placeholder="Catatan"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" formaction="<?php echo base_url() . 'beranda' ?>" class="btn default">Tutup</button>
                        <button type="submit" class="btn green">Submit</button>
                    </div>
            </form>
        </div>
    </div>
    <div class="clearfix">
    </div>
</div>
