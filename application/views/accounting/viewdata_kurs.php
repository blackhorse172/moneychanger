
<link rel="stylesheet" href="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/styles/jqx.base.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/scripts/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxdata.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxmenu.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.filter.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.sort.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.edit.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxgrid.selection.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxlistbox.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxdropdownlist.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxcheckbox.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxcalendar.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxnumberinput.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/jqxdatetimeinput.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/globalization/globalize.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/scripts/demos.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/demos/sampledata/generatedata.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/jqwidgets/globalization/globalize.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/jqwidgets-ver4.1.2/demos/jqxgrid/localization.js"></script>
<style type="text/css">
    .readonlyClass
    {
        background-color: #efefef;
    }
    .whiteClass
    {
        background-color: White;
    }
    .blueClass
    {
        background-color: #87CEEB;
    }
    .editedCell
    {
        background-color: #b90f0f !important;
        color: #87CEEB;
        font-style: italic;
    }
</style>
<script type="text/javascript">
    var jQuery_1_4_3 = $.noConflict(true);
    jQuery_1_4_3(document).ready(function () {
        // prepare the data
//        var url = "http://localhost/smartdeal/akunting/get_data";
        var url = "<?php echo site_url('akunting/get_data_kurs'); ?>";
        var source =
                {
                    datatype: "json",
                    updaterow: function (rowid, rowdata, commit) {
                        // synchronize with the server - send update command
                        // call commit with parameter true if the synchronization with the server is successful
                        // and with parameter false if the synchronization failder.
                        commit(true);
                    },
                    datafields:
                            [
                                {name: 'Iid', type: 'string'},
                                {name: 'IsCursBi', type: 'string'},
                                {name: 'Cur', type: 'string'},
                                {name: 'SaldoAwalValas', type: 'number'},
                                {name: 'SaldoAwalRupiah', type: 'number'},
                                {name: 'KursTengah', type: 'number'},
                                {name: 'LastEditBy', type: 'string'}
                            ],
                    id: 'id',
                    url: url,
                    root: 'data'
                };
        var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
            if (value < 0) {
                return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #800000;">' + value + '</span>';
            } else {
                return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #000000;">' + value + '</span>';
            }
        };
//        var cellsrenderer_liratusjuta = function (row, column, value, defaultHtml) {
//            if (value > '499999999') {
//                var element = jQuery_1_4_3(defaultHtml);
//                element.css({'background-color': '#FF0000'});
//                return element[0].outerHTML;
//                return defaultHtml;
//            } else {
//                var element = jQuery_1_4_3(defaultHtml);
//                element.css({'background-color': '#000000'});
//                return element[0].outerHTML;
//                return defaultHtml;
//            }
//        };
        var redRow;
        var greenRow;
        var blueRow;
        var cellclassname = function (row, column, value, data) {
            if (data.LastEditBy != '') {
                return "blueClass";
            } else if (data.IsCursBi == 1) {
                return "readonlyClass";
            } else {
                return "whiteClass";
            }
        };
        var cellclassname_updated = function (row, column, value, data) {
            if (data.LastEditBy != '') {
                return "blueClass";
            }
        };
        editedCells = new Array();
        var dataAdapter = new jQuery_1_4_3.jqx.dataAdapter(source);
        // initialize jqxGrid
        jQuery_1_4_3("#jqxgrid").jqxGrid(
                {
                    width: 1000,
                    height: 450,
                    source: dataAdapter,
                    editable: true,
                    showfilterrow: true,
                    filterable: true,
                    selectionmode: 'singlecell',
                    sortable: true,
                    columns: [
                        {text: 'Iid', columntype: 'textbox', datafield: 'Iid', width: 70, hidden: true},
                        {text: 'Currency', columntype: 'textbox', datafield: 'Cur', width: 170, editable: false, pinned: true, cellclassname: cellclassname},

                        {
                            text: 'Saldo Awal Valas', datafield: 'SaldoAwalValas', width: 290, align: 'right', cellclassname: cellclassname, cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) {
//                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                                return true;
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'Saldo Awal Rupiah', datafield: 'SaldoAwalRupiah', width: 290, align: 'right', cellclassname: cellclassname, cellsalign: 'right', cellsformat: "f2", columntype: 'numberinput',
                            validation: function (cell, value) {
//                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                                return true;
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        },
                        {
                            text: 'Kurs Tengah', datafield: 'KursTengah', width: 230, align: 'right', cellclassname: cellclassname, cellsformat: "f2", cellsalign: 'right', columntype: 'numberinput',
                            validation: function (cell, value) {
//                                if (value < 0 || value > 150) {
//                                    return {result: false, message: "Quantity should be in the 0-150 interval"};
//                                }
                                return true;
                            },
                            createeditor: function (row, cellvalue, editor) {
                                editor.jqxNumberInput({decimalDigits: 2, digits: 12});
                            }
                        }
                    ]
                });
        // events
        var rowValues = "";
        jQuery_1_4_3("#jqxgrid").on('cellbeginedit', function (event) {
            var args = event.args;
            if (args.datafield === "firstname") {
                rowValues = "";
            }
            rowValues += args.value.toString() + "    ";
            if (args.datafield === "price") {
                $("#cellbegineditevent").text("Begin Row Edit: " + (1 + args.rowindex) + ", Data: " + rowValues);
            }
        });
        jQuery_1_4_3("#jqxgrid").on('cellendedit', function (event) {
            var args = event.args;
            if (args.datafield === "firstname") {
                rowValues = "";
            }
            rowValues += args.value.toString() + "    ";
            if (args.datafield === "price") {
                jQuery_1_4_3("#cellbegineditevent").text("End Row Edit: " + (1 + args.rowindex) + ", Data: " + rowValues);
            }
        });
        jQuery_1_4_3("#jqxgrid").unbind('cellendedit').on('cellendedit', function (event) {
            var field = jQuery_1_4_3("#jqxgrid").jqxGrid('getcolumn', event.args.datafield).datafield;
            var rowBoundIndex = event.args.rowindex;
            var rowData = jQuery_1_4_3('#jqxgrid').jqxGrid('getrowdata', rowBoundIndex);
            var value_iid = rowData['Iid'];
            var value_updated = args.value;

            if (field == 'TanggalTransaksi') {
                value_updated = convert_date(value_updated);
            }
//            alert(value_iid + value_updated);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>akunting/update_data_kurs",
                cache: false,
                data: {iid: value_iid, field: field, updated_param: value_updated}, // since, you need to delete post of particular id
                success: function (reaksi) {
//                    alert(reaksi);
                    if (reaksi == 1) {
//                    alert("Success");
                    } else if (reaksi == -9) {
                        alert("User anda tidak ada otoritas merubah data");
                    }
                }
            });
        });
    }

    );
    function convert_date(str) {
        var date = new Date(str),
                mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }
    function get_newgrid() {
        var datetahun = document.getElementById('datetahun').value;
        var tmpS = jQuery_1_4_3("#jqxgrid").jqxGrid('source');
        tmpS._source.url = "<?php echo site_url('akunting/get_data_kurs/') ?>" + datetahun;
        jQuery_1_4_3("#jqxgrid").jqxGrid('source', tmpS);
    }
</script>
<h3 class="page-title">
    <?php echo $pageform ?></h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="index.html">Accounting</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Data</a>
        </li>
    </ul>

</div>
<table>
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<table>
    <tr>
        <td>
            <label class="control-label col-md-5">Periode&nbsp;Data&nbsp;
            </label>
        </td>
        <td>
            <form action="<?php echo base_url() . 'indexsort/poproduct' ?>" method='post' name="formsortindex" id="formsortindex">
                <div class="col-md-3">
                    <div class="input-group input-medium date date-picker" data-date="" data-date-format="mm-yyyy" data-date-viewmode="years" data-date-minviewmode="months">
                        <input type="text" class="form-control" id="datetahun" name="datetahun" value="<?php echo date('m-Y', strtotime(date('d-m-Y'))) ?>" readonly>
                        <span class="input-group-btn">
                            <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                        </span>
                    </div>
                    <!-- /input-group -->
<!--                    <span class="help-block">
                        Select month only </span>-->
                </div>

            </form>
        </td>
        <td>
            <button type="submit" class="btn green" style="margin: 0 0 0 1px;height: 32px;" onclick="get_newgrid()">Apply</button>
        </td><br
        </tr>
</table>
<br>
<div class="row">
    <div class="col-md-12">
        <div id='jqxWidget'>
            <div id="jqxgrid"></div>
            <div style="font-size: 12px; font-family: Verdana, Geneva, 'DejaVu Sans', sans-serif; margin-top: 30px;">
                <div id="cellbegineditevent"></div>
                <div style="margin-top: 10px;" id="cellendeditevent"></div>
            </div>
        </div>

    </div>
</div>
<div class="clearfix">
</div>
