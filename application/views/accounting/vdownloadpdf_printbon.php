<?php

/* setting zona waktu */
//if (function_exists('date_default_timezone_set')) disabled by fandi for tanggal edit
date_default_timezone_set('Asia/Jakarta');
$date = date_create(date('d-m-Y H:i:s'));
//date_add($date, date_interval_create_from_date_string('-11 hours')); disabled by fandi for tanggal edit
$this->fpdf->FPDF("P", "cm", "A4");
// kita set marginnya dimulai dari kiri, atas, kanan. jika tidak diset, defaultnya 1 cm
$this->fpdf->SetMargins(1, 0.2, 1);
/* AliasNbPages() merupakan fungsi untuk menampilkan total halaman
  di footer, nanti kita akan membuat page number dengan format : number page / total page
 */
$this->fpdf->AliasNbPages();

// **************************************** LOOPING ISI *******************************//
$indx = 1;
foreach ($hasildetail as $data) {
    $isitable[$indx] = $data;
    $indx++;
}
// **************************************** LOOPING ISI *******************************//
// **************************************** CREATE HEADER DAN JUMLAH HALAMAN LOOPING BEGIN *******************************//
$recordall = count($hasildetail); // DI ISI DENGAN SELURUH JUMLAH RECORD YG INGIN DI CETAK
$recordperpage = 34; // 26 JUMLAH RECORD DALAM 1 HALAMAN
$activepage = $recordall / $recordperpage; // JUMLAH ACTIVE PAGE
$firstrecord = 1;
$lastrecord = $recordperpage;
$idr_saldo_awal = 0;
$idr_pembelian = 0;
$idr_penjualan = 0;
$idr_saldoakhir = 0;
for ($i = 0; $i < $activepage; $i++) {
    // ******* HEADER BEGIN ******* //
    $this->fpdf->AddPage();

    $imageurl = base_url() . 'assets/images/SMARTREMIT.png';
//    $this->fpdf->SetY(2.9);
//    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 1, $this->fpdf->Image($imageurl, 2, 1.6, 3.28), 0, 0, 'L', false);

    $this->fpdf->SetFont('Times', 'B', 29);

    $this->fpdf->SetY(2);
    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 0, 'PT. SOLUSI MULTI ARTHA', 0, 'LR', 'C');

    $this->fpdf->SetFont('Arial', 'B', 14);
    $this->fpdf->SetY(2.9);
    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 0, 'Jl. Cideng Timur No. 16A, Jakarta Pusat, Indonesia', 0, 'LR', 'C');
    $this->fpdf->SetFont('Arial', 'B', 14);
    $this->fpdf->SetY(3.5);
    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 0, 'Telp. 021 - 6305 210 (Hunting) Fax. 021 - 6324 873', 0, 'LR', 'C');

    $this->fpdf->Line(2, 4.2, 19, 4.2);

    $this->fpdf->SetFont('Arial', 'B', 10);
    $this->fpdf->SetY(4.7);
    $this->fpdf->SetX(10);
    $this->fpdf->Cell(1, 0, 'Formulir Pengiriman Uang', 0, 'LR', 'C');
    $this->fpdf->SetY(5.2);
    $this->fpdf->SetX(10);
    $this->fpdf->Cell(1, 0, '( Application For Fund Transfer )', 0, 'LR', 'C');

    $this->fpdf->Line(2, 5.7, 19, 5.7);
    $this->fpdf->Line(2, 6.4, 19, 6.4);
    $this->fpdf->Line(2, 5.7, 2, 12); // garis kiri
    $this->fpdf->Line(19, 5.7, 19, 12); // garis kanan

    $this->fpdf->SetFont('Times', 'B', 8);
    $this->fpdf->SetY(5.9);
    $this->fpdf->SetX(10);
    $this->fpdf->Cell(1, 0, 'Penerima', 0, 'LR', 'C');
    $this->fpdf->SetFont('Times', 'IB', 8);
    $this->fpdf->SetY(6.2);
    $this->fpdf->SetX(10);
    $this->fpdf->Cell(1, 0, '(Beneficiary)', 0, 'LR', 'C');


    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(6.7);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Nama Penerima', 0, 'LR', 'L');
    $this->fpdf->SetX(13.5);
    $this->fpdf->Cell(1, 0, 'NPWP', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(7);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Beneficiary's Name", 0, 'LR', 'L');
    $this->fpdf->SetX(8);
    $this->fpdf->Cell(1, 0, "...............................................................................            .............................................................", 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(7.3);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Alamat Penerima', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(7.6);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Beneficiary's Address", 0, 'LR', 'L');
    $this->fpdf->SetX(8);
    $this->fpdf->Cell(1, 0, "........................................................................................................................................................", 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(7.9);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Negara', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(8.2);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Country", 0, 'LR', 'L');
    $this->fpdf->SetX(8);
    $this->fpdf->Cell(1, 0, "........................................................................................................................................................", 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(8.5);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Nomor Rekening Penerima', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(8.8);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Beneficiary's Account Number", 0, 'LR', 'L');
    $this->fpdf->SetX(8);
    $this->fpdf->Cell(1, 0, "........................................................................................................................................................", 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(9.1);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Nama Bank', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(9.4);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Bank's Name", 0, 'LR', 'L');
    $this->fpdf->SetX(8);
    $this->fpdf->Cell(1, 0, "........................................................................................................................................................", 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(9.7);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Alamat Bank', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(10);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Bank's Address", 0, 'LR', 'L');
    $this->fpdf->SetX(8);
    $this->fpdf->Cell(1, 0, "........................................................................................................................................................", 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(10.3);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Negara', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(10.6);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Country", 0, 'LR', 'L');
    $this->fpdf->SetX(8);
    $this->fpdf->Cell(1, 0, "........................................................................................................................................................", 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(10.9);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Kode Penerima', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(11.2);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Beneficiary's Code", 0, 'LR', 'L');
    $this->fpdf->SetX(8);
    $this->fpdf->Cell(1, 0, "........................................................................................................................................................", 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(11.5);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Berita', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(11.8);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Message", 0, 'LR', 'L');
    $this->fpdf->SetX(8);
    $this->fpdf->Cell(1, 0, "........................................................................................................................................................", 0, 'LR', 'L');
    $this->fpdf->Line(2, 12, 19, 12);

    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(12.5);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Jumlah Yang Dikirim', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(12.8);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Amount Transferred", 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(13.4);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Terbilang', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(13.7);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Amount in Words", 0, 'LR', 'L');
    $this->fpdf->SetX(8);
    $this->fpdf->Cell(1, 0, "........................................................................................................................................................", 0, 'LR', 'L');

    $this->fpdf->Line(8, 12.3, 19, 12.3);
    $this->fpdf->Line(8, 13, 19, 13);
    $this->fpdf->Line(8, 12.3, 8, 13); // garis kiri
    $this->fpdf->Line(19, 12.3, 19, 13); // garis kanan

    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(14.7);
    $this->fpdf->SetX(14);
    $this->fpdf->Cell(1, 0, '...................................................................', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(15);
    $this->fpdf->SetX(15.9);
    $this->fpdf->Cell(1, 0, "Pemohon ( Application )", 0, 'LR', 'L');

    $this->fpdf->Line(2, 15.4, 19, 15.4);
    $this->fpdf->Line(2, 16.1, 19, 16.1);
    $this->fpdf->Line(2, 15.4, 2, 23.1); // garis kiri
    $this->fpdf->Line(19, 15.4, 19, 23.1); // garis kanan

    $this->fpdf->SetFont('Times', 'B', 8);
    $this->fpdf->SetY(15.57);
    $this->fpdf->SetX(10);
    $this->fpdf->Cell(1, 0, 'Pengirim', 0, 'LR', 'C');
    $this->fpdf->SetFont('Times', 'IB', 8);
    $this->fpdf->SetY(15.87);
    $this->fpdf->SetX(10);
    $this->fpdf->Cell(1, 0, '(Remitter)', 0, 'LR', 'C');

    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(16.4);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Nama Pengirim', 0, 'LR', 'L');
    $this->fpdf->SetX(13.5);
    $this->fpdf->Cell(1, 0, 'NPWP', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(16.7);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Remitter's Name", 0, 'LR', 'L');
    $this->fpdf->SetX(8);
    $this->fpdf->Cell(1, 0, "...............................................................................            .............................................................", 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(17);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Alamt Pengirim', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(17.3);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Remitter's Address", 0, 'LR', 'L');
    $this->fpdf->SetX(8);
    $this->fpdf->Cell(1, 0, "........................................................................................................................................................", 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(17.6);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Nama yang dapat dihubungi', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(17.9);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Contact Person", 0, 'LR', 'L');
    $this->fpdf->SetX(8);
    $this->fpdf->Cell(1, 0, "........................................................................................................................................................", 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(18.2);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'No. Handphone', 0, 'LR', 'L');
    $this->fpdf->SetX(12.5);
    $this->fpdf->Cell(1, 0, 'No. Telepon', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(18.5);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Cellular Phone No.", 0, 'LR', 'L');
    $this->fpdf->SetX(8);
    $this->fpdf->Cell(1, 0, "............................................................", 0, 'LR', 'L');
    $this->fpdf->SetX(12.5);
    $this->fpdf->Cell(1, 0, 'Telephone No.', 0, 'LR', 'L');
    $this->fpdf->SetX(14.5);
    $this->fpdf->Cell(1, 0, "............................................................", 0, 'LR', 'L');

    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(18.8);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'ID Pengirim', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(19.1);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Remitter's ID", 0, 'LR', 'L');
    $this->fpdf->SetX(11);
    $this->fpdf->Cell(1, 0, ".............................................................................................................", 0, 'LR', 'L');

    $this->fpdf->Line(8, 18.7, 8.3, 18.7); // garis atas
    $this->fpdf->Line(8, 18.9, 8.3, 18.9); // garis bawah
    $this->fpdf->Line(8, 18.7, 8, 18.9); // garis kiri
    $this->fpdf->Line(8.3, 18.7, 8.3, 18.9); // garis kanan

    $this->fpdf->Line(8, 19, 8.3, 19); // garis atas
    $this->fpdf->Line(8, 19.2, 8.3, 19.2); // garis bawah
    $this->fpdf->Line(8, 19, 8, 19.2); // garis kiri
    $this->fpdf->Line(8.3, 19, 8.3, 19.2); // garis kanan

    $this->fpdf->Line(10, 18.7, 10.3, 18.7); // garis atas
    $this->fpdf->Line(10, 18.9, 10.3, 18.9); // garis bawah
    $this->fpdf->Line(10, 18.7, 10, 18.9); // garis kiri
    $this->fpdf->Line(10.3, 18.7, 10.3, 18.9); // garis kanan
    //
    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(18.8);
    $this->fpdf->SetX(8.3);
    $this->fpdf->Cell(1, 0, "ID", 0, 'LR', 'L');
    $this->fpdf->SetX(10.5);
    $this->fpdf->Cell(1, 0, "Paspor", 0, 'LR', 'L');
    $this->fpdf->SetY(19.1);
    $this->fpdf->SetX(8.3);
    $this->fpdf->Cell(1, 0, "Lainnya ( Others )", 0, 'LR', 'L');

    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(19.4);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'No. Identitas', 0, 'LR', 'L');
    $this->fpdf->SetX(12.5);
    $this->fpdf->Cell(1, 0, 'Kewarganegaraan', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(19.7);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Identity No.", 0, 'LR', 'L');
    $this->fpdf->SetX(8);
    $this->fpdf->Cell(1, 0, "............................................................", 0, 'LR', 'L');
    $this->fpdf->SetX(12.5);
    $this->fpdf->Cell(1, 0, 'Nationality', 0, 'LR', 'L');
    $this->fpdf->SetX(14.5);
    $this->fpdf->Cell(1, 0, "............................................................", 0, 'LR', 'L');

    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(20);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Hubungan Keuangan', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(20.3);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Financial Relationship", 0, 'LR', 'L');
    $this->fpdf->SetY(20.6);
    $this->fpdf->SetX(11);
    $this->fpdf->Cell(1, 0, ".............................................................................................................", 0, 'LR', 'L');

    $this->fpdf->Line(8, 19.9, 8.3, 19.9); // garis atas
    $this->fpdf->Line(8, 20.1, 8.3, 20.1); // garis bawah
    $this->fpdf->Line(8, 19.9, 8, 20.1); // garis kiri
    $this->fpdf->Line(8.3, 19.9, 8.3, 20.1); // garis kanan

    $this->fpdf->Line(8, 20.2, 8.3, 20.2); // garis atas
    $this->fpdf->Line(8, 20.4, 8.3, 20.4); // garis bawah
    $this->fpdf->Line(8, 20.2, 8, 20.4); // garis kiri
    $this->fpdf->Line(8.3, 20.2, 8.3, 20.4); // garis kanan

    $this->fpdf->Line(8, 20.5, 8.3, 20.5); // garis atas
    $this->fpdf->Line(8, 20.7, 8.3, 20.7); // garis bawah
    $this->fpdf->Line(8, 20.5, 8, 20.7); // garis kiri
    $this->fpdf->Line(8.3, 20.5, 8.3, 20.7); // garis kanan
    //
    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(20);
    $this->fpdf->SetX(8.3);
    $this->fpdf->Cell(1, 0, "Keluarga ( Family )", 0, 'LR', 'L');
    $this->fpdf->SetY(20.3);
    $this->fpdf->SetX(8.3);
    $this->fpdf->Cell(1, 0, "Bisnis ( Business )", 0, 'LR', 'L');
    $this->fpdf->SetY(20.6);
    $this->fpdf->SetX(8.3);
    $this->fpdf->Cell(1, 0, "Lainnya ( Others )", 0, 'LR', 'L');

    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(20.9);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Sumber Dana', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(21.2);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Source Of Fund", 0, 'LR', 'L');
    $this->fpdf->SetY(22.2);
    $this->fpdf->SetX(11);
    $this->fpdf->Cell(1, 0, ".............................................................................................................", 0, 'LR', 'L');

    $this->fpdf->Line(8, 20.9, 8.3, 20.9); // garis atas
    $this->fpdf->Line(8, 21.1, 8.3, 21.1); // garis bawah
    $this->fpdf->Line(8, 20.9, 8, 21.1); // garis kiri
    $this->fpdf->Line(8.3, 20.9, 8.3, 21.1); // garis kanan

    $this->fpdf->Line(8, 21.2, 8.3, 21.2); // garis atas
    $this->fpdf->Line(8, 21.4, 8.3, 21.4); // garis bawah
    $this->fpdf->Line(8, 21.2, 8, 21.4); // garis kiri
    $this->fpdf->Line(8.3, 21.2, 8.3, 21.4); // garis kanan
    //
    $this->fpdf->Line(8, 21.5, 8.3, 21.5); // garis atas
    $this->fpdf->Line(8, 21.7, 8.3, 21.7); // garis bawah
    $this->fpdf->Line(8, 21.5, 8, 21.7); // garis kiri
    $this->fpdf->Line(8.3, 21.5, 8.3, 21.7); // garis kanan

    $this->fpdf->Line(8, 21.8, 8.3, 21.8); // garis atas
    $this->fpdf->Line(8, 22, 8.3, 22); // garis bawah
    $this->fpdf->Line(8, 21.8, 8, 22); // garis kiri
    $this->fpdf->Line(8.3, 21.8, 8.3, 22); // garis kanan
    //
    $this->fpdf->Line(8, 22.1, 8.3, 22.1); // garis atas
    $this->fpdf->Line(8, 22.3, 8.3, 22.3); // garis bawah
    $this->fpdf->Line(8, 22.1, 8, 22.3); // garis kiri
    $this->fpdf->Line(8.3, 22.1, 8.3, 22.3); // garis kanan
    //
    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(21);
    $this->fpdf->SetX(8.3);
    $this->fpdf->Cell(1, 0, "Gaji ( Salary )", 0, 'LR', 'L');
    $this->fpdf->SetY(21.3);
    $this->fpdf->SetX(8.3);
    $this->fpdf->Cell(1, 0, "Tabungan ( Savings )", 0, 'LR', 'L');
    $this->fpdf->SetY(21.6);
    $this->fpdf->SetX(8.3);
    $this->fpdf->Cell(1, 0, "Pinjaman ( Loan )", 0, 'LR', 'L');
    $this->fpdf->SetY(21.9);
    $this->fpdf->SetX(8.3);
    $this->fpdf->Cell(1, 0, "Usaha Dagang ( Business )", 0, 'LR', 'L');
    $this->fpdf->SetY(22.2);
    $this->fpdf->SetX(8.3);
    $this->fpdf->Cell(1, 0, "Lainnya ( Others )", 0, 'LR', 'L');

    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(22.5);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Tujuan Transaksi', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(22.8);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Transaction Purpose", 0, 'LR', 'L');
    $this->fpdf->SetX(8);
    $this->fpdf->Cell(1, 0, "........................................................................................................................................................", 0, 'LR', 'L');

    $this->fpdf->Line(2, 23.1, 19, 23.1);

    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(23.5);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Diisi oleh Staf', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(23.8);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Maker", 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(25);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Nama Jelas", 0, 'LR', 'L');

    $this->fpdf->SetFont('Times', 'I', 8);
    $this->fpdf->SetY(23.8);
    $this->fpdf->SetX(13);
    $this->fpdf->Cell(1, 0, "Checker", 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->SetY(25);
    $this->fpdf->SetX(13);
    $this->fpdf->Cell(1, 0, "Nama Jelas", 0, 'LR', 'L');


    $this->fpdf->SetFont('Times', '', 6);
    $this->fpdf->SetY(26);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "Saya / Kami dengan ini : ", 0, 'LR', 'L');
    $this->fpdf->SetY(26.2);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "I / we hereby", 0, 'LR', 'L');
    $this->fpdf->SetY(26.4);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "1. Menyetujui syarat-syarat dan ketentuan yang berlaku dalam Smart Remit, mengenai penyampaian informasi kepada pihak ketiga dan ", 0, 'LR', 'L');
    $this->fpdf->SetY(26.6);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "meminta kepada penyelenggara untuk melaksanakan kiriman uang seperti di atas. ", 0, 'LR', 'L');
    $this->fpdf->SetY(26.8);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "( Unconditional accept all the terms and condition on Smart Remit, about information to 3rd party and give instruction to remit fund as", 0, 'LR', 'L');
    $this->fpdf->SetY(27);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "above )", 0, 'LR', 'L');
    $this->fpdf->SetY(27.2);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "2. Menegaskan bahwa informasi yang ditulis adalah benar, jika terdapat perubahan infomasi akan disampaikan kembali.", 0, 'LR', 'L');
    $this->fpdf->SetY(27.4);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, "( Guarantee that all infromation is correct; if any changes will inform later )", 0, 'LR', 'L');

    $this->fpdf->Line(2, 25.8, 17, 25.8);
    $this->fpdf->Line(2, 27.6, 17, 27.6);
    $this->fpdf->Line(2, 25.8, 2, 27.6); // garis kiri
    $this->fpdf->Line(17, 25.8, 17, 27.6); // garis kanan
    // ************************** ISI BEGIN ******************** //
    $this->fpdf->SetFont('Times', '', 9);
    $this->fpdf->SetY(5.4);
    $this->fpdf->SetX(15.3);
    $this->fpdf->Cell(1, 0, 'Tgl. Trans. : ', 0, 'LR', 'L');
    $this->fpdf->SetX(17.2);
    $this->fpdf->Cell(1, 0, date('d-m-Y', strtotime($db->TglTransaksiExcel)), 0, 'LR', 'L');
    $this->fpdf->SetY(6.9);
    $this->fpdf->SetX(8.2);
    $this->fpdf->Cell(1, 0, strtoupper($db->Beneficiary), 0, 'LR', 'L');
    $this->fpdf->SetY(8.1);
    $this->fpdf->SetX(8.2);
    $this->fpdf->Cell(1, 0, strtoupper($db->CountryPenerima), 0, 'LR', 'L');
    $this->fpdf->SetY(8.7);
    $this->fpdf->SetX(8.2);
    $this->fpdf->Cell(1, 0, strtoupper($db->Account), 0, 'LR', 'L');
    $this->fpdf->SetY(9.3);
    $this->fpdf->SetX(8.2);
    $this->fpdf->Cell(1, 0, strtoupper($db->Bank), 0, 'LR', 'L');

    $this->fpdf->SetY(12.7);
    $this->fpdf->SetX(8.2);
    if (strtoupper($db->BorS) == 'SELL') {
        $this->fpdf->Cell(1, 0, strtoupper('IDR') . ' ' . number_format(str_replace('-', '', $db->Amount), 0, ".", ","), 0, 'LR', 'L');
    } else {
        $this->fpdf->Cell(1, 0, strtoupper($db->Curr) . ' ' . number_format(str_replace('-', '', $db->Equal), 0, ".", ","), 0, 'LR', 'L');
    }
    if (strlen($terbilang) > 52) {
        $this->fpdf->SetFont('Times', '', 6);
    }
    $this->fpdf->SetY(13.5);
    $this->fpdf->SetX(8.2);
    $this->fpdf->Cell(1, 0, $terbilang, 0, 'LR', 'L');

    if (substr($db->Beneficiary, 0, 2) == 'PT') {
        $db->Sender = 'MOHAMAD ALI BIN HAJI MOHAMAD';
        $db->Address = '000 44034';
        $db->NoId = '000 44034';
        $db->Country = '000 44034';
    } else if (strtoupper($db->BorS) == 'SELL' && str_replace('-', '', $db->Amount) >= 100000000) {
        $db->Sender = 'MOHAMAD SOHAIL';
        $db->Address = 'TURKEY';
        $db->NoId = 'U 0487153';
        $db->Country = 'TURKEY';
    } else if (strtoupper($db->BorS) == 'SELL' && str_replace('-', '', $db->Amount) < 100000000) {
        $db->Sender = 'MOHAMED TAIB BIN DATU';
        $db->Address = 'BRUNAI DARUSALLAM';
        $db->NoId = '000 44034';
        $db->Country = 'BRUNAI DARUSALLAM';
    }
    $this->fpdf->SetFont('Times', '', 9);
    $this->fpdf->SetY(14.5);
    $this->fpdf->SetX(14.2);
    $this->fpdf->Cell(1, 0, strtoupper($db->Sender), 0, 'LR', 'L');

    $this->fpdf->SetY(16.6);
    $this->fpdf->SetX(8.2);
    $this->fpdf->Cell(1, 0, strtoupper($db->Sender), 0, 'LR', 'L');
    $this->fpdf->SetY(17.8);
    $this->fpdf->SetX(8.2);
    $this->fpdf->Cell(1, 0, strtoupper($db->Address), 0, 'LR', 'L');
    $this->fpdf->SetY(19);
    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 0, strtoupper($db->NoId), 0, 'LR', 'L');
    $this->fpdf->SetY(19.6);
    $this->fpdf->SetX(15);
    $this->fpdf->Cell(1, 0, strtoupper($db->Country), 0, 'LR', 'L');
    // ************************** ISI END ******************** //
}


// **************************************** CREATE HEADER DAN JUMLAH HALAMAN LOOPING END *******************************//
/* generate pdf jika semua konstruktor, data yang akan ditampilkan, dll sudah selesai */
$this->fpdf->Output("Print_Out_" . date('d/m/Y') . ".pdf", "I");
?>