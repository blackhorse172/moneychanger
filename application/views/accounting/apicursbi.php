<?php

$kurs_page = wp_remote_get('http://www.bi.go.id/id/moneter/informasi-kurs/transaksi-bi/', array('timeout' => 20));
if (!is_wp_error($kurs_page)) {
    $result = $kurs_page['body'];
    $data_table = explode('<div id="right-cell">', $result);
    $data_table = explode('<table class="table1" cellspacing="0" rules="all" border="1" id="ctl00_PlaceHolderMain_biWebKursTransaksiBI_GridView1">', $data_table[1]);
    $data_table = explode('</table>', $data_table[1]);
    $dom = new DOMDocument();
    $html = $data_table[0];
    @$dom->loadHTML($html);
    $rows = $dom->getElementsByTagName('tr');
    $counter = 0;
    // Loop each rows
    foreach ($rows as $row) {
        if (!empty($row->nodeValue)) {
            // Loop each columns
            foreach ($row->childNodes as $column) {
                if (!empty($column->nodeValue)) {
                    $data_kurs[$counter][] = $column->nodeValue;
                }
            }
            $counter++;
        }
    }
}