<h3 class="page-title">
    <?php echo $pageform ?></h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="index.html">Akunting</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Upload</a>
        </li>
    </ul>
    <div class="page-toolbar">
        <div id="dashboard-report-range" class="tooltips btn btn-fit-height btn-sm green-haze btn-dashboard-daterange" data-container="body" data-placement="left" data-original-title="Change dashboard date range">
            <i class="icon-calendar"></i>
            &nbsp;&nbsp; <i class="fa fa-angle-down"></i>
        </div>
    </div>
</div>
<table>
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<div class="portlet light bg-inverse">
    <div class="row">
        <div class="col-md-12">

            <form enctype="multipart/form-data" action="<?php echo base_url() . 'akunting_upload/add_user_data' ?>" method='post'>
                <div class="modal-body">
                    <div class="scroller" style="height:300px" data-always-visible="1" data-rail-visible1="1">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Tanggal Transaksi&nbsp;:</label>
                                    <div class="col-md-7">
                                        <div class="input-group date date-picker" data-date="" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                            <input type="text" class="form-control" value="<?php echo date('d-m-Y', strtotime(date('d-m-Y') . "-1 days")) ?>" name="transdate" id="transdate" readonly>

                                        </div>
                                    </div>
                                </div>
                                <!--                                <div class="form-group">
                                                                    <label class="col-md-5 control-label">Catatan :</label>
                                                                    <div class="col-md-7">
                                                                        <textarea class="form-control" rows="2" name="catatan" id="catatan" placeholder="Catatan"></textarea>
                                                                    </div>
                                                                </div>-->
                                <div class="form-group">
                                    <label class="col-md-5 control-label">File : <span class="required">*</span></label>
                                    <div class="col-md-7">
                                        <input type="file" id="userfile" name="userfile"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" formaction="<?php echo base_url() . 'beranda' ?>" class="btn default">Tutup</button>
                        <button type="submit" class="btn green">Upload</button>
                    </div>
            </form>
        </div>
    </div>
    <div class="clearfix">
    </div>
</div>
