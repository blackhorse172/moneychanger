<?php

/* setting zona waktu */
//if (function_exists('date_default_timezone_set')) disabled by fandi for tanggal edit
date_default_timezone_set('Asia/Jakarta');
$date = date_create(date('d-m-Y H:i:s'));
//date_add($date, date_interval_create_from_date_string('-11 hours')); disabled by fandi for tanggal edit
$this->fpdf->FPDF("P", "cm", "A4");
// kita set marginnya dimulai dari kiri, atas, kanan. jika tidak diset, defaultnya 1 cm
$this->fpdf->SetMargins(1, 0.2, 1);
/* AliasNbPages() merupakan fungsi untuk menampilkan total halaman
  di footer, nanti kita akan membuat page number dengan format : number page / total page
 */
$this->fpdf->AliasNbPages();

// **************************************** LOOPING ISI *******************************//
$indx = 1;
foreach ($hasildetail as $data) {
    $isitable[$indx] = $data;
    $indx++;
}
// **************************************** LOOPING ISI *******************************//
// **************************************** CREATE HEADER DAN JUMLAH HALAMAN LOOPING BEGIN *******************************//
$recordall = count($hasildetail); // DI ISI DENGAN SELURUH JUMLAH RECORD YG INGIN DI CETAK
$recordperpage = 34; // 26 JUMLAH RECORD DALAM 1 HALAMAN
$activepage = $recordall / $recordperpage; // JUMLAH ACTIVE PAGE
$firstrecord = 1;
$lastrecord = $recordperpage;
$idr_saldo_awal = 0;
$idr_pembelian = 0;
$idr_penjualan = 0;
$imageurl = base_url() . 'assets/images/SMARTREMIT.png';
for ($i = 0; $i < 1; $i++) {
    // ******* HEADER BEGIN ******* //
    $this->fpdf->AddPage();

    $this->fpdf->SetY(8);
    $this->fpdf->SetX(2);
    $this->fpdf->setFillColor(80, 240, 240);
    $this->fpdf->Cell(17, 1, '', 0, 2, 'L', 10); //your cell

    $this->fpdf->SetY(19);
    $this->fpdf->SetX(2);
    $this->fpdf->setFillColor(80, 240, 240);
    $this->fpdf->Cell(17, 1, '', 0, 2, 'L', 10); //your cell

    $imageurl = base_url() . 'assets/images/LOGO_SMARTDEAL.png';
//    $this->fpdf->SetY(2.9);
//    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 1, $this->fpdf->Image($imageurl, 3, 1.6, 4.28), 0, 0, 'L', false);

    $this->fpdf->SetFont('Times', 'B', 16);

    $this->fpdf->SetY(2);
    $this->fpdf->SetX(13);
    $this->fpdf->Cell(1, 0, 'PUSAT', 0, 'LR', 'L');
    $this->fpdf->SetY(3);
    $this->fpdf->SetX(13);
    $this->fpdf->Cell(1, 0, 'CABANG : ..................', 0, 'LR', 'L');


    $this->fpdf->SetFont('Times', 'B', 14);

    $this->fpdf->SetY(5);
    $this->fpdf->SetX(3.3);
    $this->fpdf->Cell(1, 0, 'PT. SOLUSI MEGA ARTHA', 0, 'LR', 'L');
    $this->fpdf->SetY(6);
    $this->fpdf->SetX(3.3);
    $this->fpdf->Cell(1, 0, 'PT. SOLUSI MULTI ARTHA', 0, 'LR', 'L');

    $this->fpdf->SetFont('Times', 'B', 11);
    $this->fpdf->SetY(4.3);
    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 0, 'Tanggal : ', 0, 'LR', 'L');
    $this->fpdf->SetY(5);
    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 0, 'Lama     : ', 0, 'LR', 'L');
    $this->fpdf->SetY(5.7);
    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 0, 'Tujuan  : ', 0, 'LR', 'L');

    $this->fpdf->SetFont('Times', 'UB', 12);
    $this->fpdf->SetY(7.3);
    $this->fpdf->SetX(10);
    $this->fpdf->Cell(1, 0, 'FORMULIR PERJALANAN DINAS', 0, 'LR', 'C');

    $this->fpdf->SetFont('Times', 'B', 12);
    $this->fpdf->SetY(8.5);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'No.', 0, 'LR', 'C');
    $this->fpdf->SetX(7);
    $this->fpdf->Cell(1, 0, 'KETERANGAN', 0, 'LR', 'C');
    $this->fpdf->SetX(15);
    $this->fpdf->Cell(1, 0, 'JUMLAH (Rp)', 0, 'LR', 'C');
    $this->fpdf->SetY(19.6);
    $this->fpdf->SetX(6.8);
    $this->fpdf->Cell(1, 0, 'TOTAL (Rp)', 0, 'LR', 'C');

    $this->fpdf->SetFont('Times', 'B', 11);
    $this->fpdf->SetY(9.6);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, '1', 0, 'LR', 'C');
    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 0, 'Rp.', 0, 'LR', 'L');
    $this->fpdf->SetY(10.6);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, '2', 0, 'LR', 'C');
    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 0, 'Rp.', 0, 'LR', 'L');
    $this->fpdf->SetY(11.6);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, '3', 0, 'LR', 'C');
    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 0, 'Rp.', 0, 'LR', 'L');
    $this->fpdf->SetY(12.6);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, '4', 0, 'LR', 'C');
    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 0, 'Rp.', 0, 'LR', 'L');
    $this->fpdf->SetY(13.6);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, '5', 0, 'LR', 'C');
    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 0, 'Rp.', 0, 'LR', 'L');
    $this->fpdf->SetY(14.6);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, '6', 0, 'LR', 'C');
    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 0, 'Rp.', 0, 'LR', 'L');
    $this->fpdf->SetY(15.6);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, '7', 0, 'LR', 'C');
    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 0, 'Rp.', 0, 'LR', 'L');
    $this->fpdf->SetY(16.6);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, '8', 0, 'LR', 'C');
    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 0, 'Rp.', 0, 'LR', 'L');
    $this->fpdf->SetY(17.6);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, '9', 0, 'LR', 'C');
    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 0, 'Rp.', 0, 'LR', 'L');
    $this->fpdf->SetY(18.6);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, '10', 0, 'LR', 'C');
    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 0, 'Rp.', 0, 'LR', 'L');
    $this->fpdf->SetY(19.6);
    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 0, 'Rp.', 0, 'LR', 'C');

    $this->fpdf->Line(2, 8, 19, 8);
    $this->fpdf->Line(2, 9, 19, 9);
    $this->fpdf->Line(2, 10, 19, 10);
    $this->fpdf->Line(2, 11, 19, 11);
    $this->fpdf->Line(2, 12, 19, 12);
    $this->fpdf->Line(2, 13, 19, 13);
    $this->fpdf->Line(2, 14, 19, 14);
    $this->fpdf->Line(2, 15, 19, 15);
    $this->fpdf->Line(2, 16, 19, 16);
    $this->fpdf->Line(2, 17, 19, 17);
    $this->fpdf->Line(2, 18, 19, 18);
    $this->fpdf->Line(2, 19, 19, 19);
    $this->fpdf->Line(2, 20, 19, 20);

    $this->fpdf->Line(2, 8, 2, 20);
    $this->fpdf->Line(3, 8, 3, 19);
    $this->fpdf->Line(12, 8, 12, 20);
    $this->fpdf->Line(19, 8, 19, 20);

    $this->fpdf->Line(12, 1.7, 12.8, 1.7); // garis atas
    $this->fpdf->Line(12, 2.3, 12.8, 2.3); // garis bawah
    $this->fpdf->Line(12, 1.7, 12, 2.3); // garis kiri
    $this->fpdf->Line(12.8, 1.7, 12.8, 2.3); // garis kanan

    $this->fpdf->Line(12, 1.7, 12.8, 2.3); // garis atas SILANG
    $this->fpdf->Line(12, 2.3, 12.8, 1.7); // garis atas SILANG

    $this->fpdf->Line(12, 2.7, 12.8, 2.7); // garis atas
    $this->fpdf->Line(12, 3.3, 12.8, 3.3); // garis bawah
    $this->fpdf->Line(12, 2.7, 12, 3.3); // garis kiri
    $this->fpdf->Line(12.8, 2.7, 12.8, 3.3); // garis kanan
    //
    $this->fpdf->Line(2.2, 4.7, 3, 4.7); // garis atas
    $this->fpdf->Line(2.2, 5.3, 3, 5.3); // garis bawah
    $this->fpdf->Line(2.2, 4.7, 2.2, 5.3); // garis kiri
    $this->fpdf->Line(3, 4.7, 3, 5.3); // garis kanan

    $this->fpdf->Line(2.2, 4.7, 3, 5.3); // garis atas SILANG
    $this->fpdf->Line(2.2, 5.3, 3, 4.7); // garis atas SILANG

    $this->fpdf->Line(2.2, 5.7, 3, 5.7); // garis atas
    $this->fpdf->Line(2.2, 6.3, 3, 6.3); // garis bawah
    $this->fpdf->Line(2.2, 5.7, 2.2, 6.3); // garis kiri
    $this->fpdf->Line(3, 5.7, 3, 6.3); // garis kanan
    //
    $this->fpdf->SetFont('Times', '', 11);
    $this->fpdf->SetY(21);
    $this->fpdf->SetX(2.2);
    $this->fpdf->Cell(1, 0, 'Pemberi Tugas,', 0, 'LR', 'CS');
    $this->fpdf->SetY(24);
    $this->fpdf->SetX(3);
    $this->fpdf->Cell(1, 0, "( ................................ )", 0, 'LR', 'C');

    $this->fpdf->SetY(21);
    $this->fpdf->SetX(7.5);
    $this->fpdf->Cell(1, 0, 'Mengetahui,', 0, 'LR', 'CS');
    $this->fpdf->SetY(24);
    $this->fpdf->SetX(8);
    $this->fpdf->Cell(1, 0, "( ................................ )", 0, 'LR', 'C');

    $this->fpdf->SetY(21);
    $this->fpdf->SetX(12.5);
    $this->fpdf->Cell(1, 0, 'Kasir,', 0, 'LR', 'CS');
    $this->fpdf->SetY(24);
    $this->fpdf->SetX(12.7);
    $this->fpdf->Cell(1, 0, "( ................................ )", 0, 'LR', 'C');

    $this->fpdf->SetY(21);
    $this->fpdf->SetX(16);
    $this->fpdf->Cell(1, 0, 'Penerima Tugas,', 0, 'LR', 'CS');
    $this->fpdf->SetY(24);
    $this->fpdf->SetX(16.9);
    $this->fpdf->Cell(1, 0, "( ................................ )", 0, 'LR', 'C');


    // ************************** ISI BEGIN ******************** //
    $this->fpdf->SetFont('Times', 'B', 11);
    $this->fpdf->SetY(9.6);
    $this->fpdf->SetX(3.2);
    $this->fpdf->Cell(1, 0, 'Tiket', 0, 'LR', 'L');
    $this->fpdf->SetX(12.8);
    $this->fpdf->Cell(1, 0, number_format($db->BiayaTiket, 0, ",", "."), 0, 'LR', 'L');
    $this->fpdf->SetY(10.6);
    $this->fpdf->SetX(3.2);
    $this->fpdf->Cell(1, 0, 'Transportasi', 0, 'LR', 'L');
    $this->fpdf->SetX(12.8);
    $this->fpdf->Cell(1, 0, number_format($db->BiayaTransportasi, 0, ",", "."), 0, 'LR', 'L');
    $this->fpdf->SetY(11.6);
    $this->fpdf->SetX(3.2);
    $this->fpdf->Cell(1, 0, 'Makan / Minum', 0, 'LR', 'L');
    $this->fpdf->SetX(12.8);
    $this->fpdf->Cell(1, 0, number_format($db->BiayaMakanMinum, 0, ",", "."), 0, 'LR', 'L');
    $this->fpdf->SetY(12.6);
    $this->fpdf->SetX(3.2);
    $this->fpdf->Cell(1, 0, 'Telekomunikasi', 0, 'LR', 'L');
    $this->fpdf->SetX(12.8);
    $this->fpdf->Cell(1, 0, number_format($db->BiayaPulsaTelpon, 0, ",", "."), 0, 'LR', 'L');

    $this->fpdf->SetY(19.6);
    $this->fpdf->SetX(12.9);
    $this->fpdf->Cell(1, 0, number_format($db->Total, 0, ",", "."), 0, 'LR', 'L');

    $this->fpdf->SetY(4.3);
    $this->fpdf->SetX(13.8);
    $this->fpdf->Cell(1, 0, date('d-m-Y', strtotime($db->TanggalBerangkat)), 0, 'LR', 'L');
    $this->fpdf->SetY(5);
    $this->fpdf->SetX(13.8);
    $this->fpdf->Cell(1, 0, '1 Hari', 0, 'LR', 'L');
    $this->fpdf->SetY(5.7);
    $this->fpdf->SetX(13.8);
    $this->fpdf->Cell(1, 0, $db->NegaraTujuan, 0, 'LR', 'L');

    $this->fpdf->SetFont('Times', '', 11);
    $this->fpdf->SetY(23.9);
    $this->fpdf->SetX(3);
    $this->fpdf->Cell(1, 0, "Tintus C. Natanael", 0, 'LR', 'C');
    $this->fpdf->SetY(23.9);
    $this->fpdf->SetX(8);
    $this->fpdf->Cell(1, 0, "Lengny Majaya", 0, 'LR', 'C');
    if (strlen($db->Pelaksana) > 19) {
        $this->fpdf->SetFont('Times', '', 8);
    }
    $this->fpdf->SetY(23.9);
    $this->fpdf->SetX(16.9);
    $this->fpdf->Cell(1, 0, $db->Pelaksana, 0, 'LR', 'C');
    // ************************** ISI END ******************** //
}


// **************************************** CREATE HEADER DAN JUMLAH HALAMAN LOOPING END *******************************//
/* generate pdf jika semua konstruktor, data yang akan ditampilkan, dll sudah selesai */
$this->fpdf->Output("Print_Out_" . date('d/m/Y') . ".pdf", "I");
?>