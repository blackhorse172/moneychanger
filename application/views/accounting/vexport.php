<script type="text/javascript">
    $(document).ready(function () {
        document.getElementById("tipe").value = 'Harian';
        $("#tipe").change(function () {
            var value_tipe = document.getElementById('tipe').value;
            if (value_tipe != 'Bulanan') {
                document.getElementById("bulanelement").hidden = true;
                document.getElementById("periode_input").hidden = false;
            } else {
                document.getElementById("bulanelement").hidden = false;
                document.getElementById("periode_input").hidden = true;
            }
        });
    });
</script>

<h3 class="page-title">
    <?php echo $pageform ?></h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="index.html">Accounting</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Export</a>
        </li>
    </ul>
    <div class="page-toolbar">
        <div id="dashboard-report-range" class="tooltips btn btn-fit-height btn-sm green-haze btn-dashboard-daterange" data-container="body" data-placement="left" data-original-title="Change dashboard date range">
            <i class="icon-calendar"></i>
            &nbsp;&nbsp; <i class="fa fa-angle-down"></i>
        </div>
    </div>
</div>
<table>
    <tr>
        <?php if ($error == '') { ?>
            <?php
        } else {
            $error = explode(":::", $error);
            if ($error[0] == 1) {
                ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else if ($error[0] == 2) { ?>
            <div class="alert alert-warning">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
        <?php } else { ?>
            <div class="alert alert-danger">
                <strong>Warning!</strong> <?php echo $error[1]; ?>
            </div>
            <?php
        }
    }
    ?>
</tr>
</table>
<div class="portlet light bg-inverse">
    <div class="row">
        <div class="col-md-12">

            <form enctype="multipart/form-data" action="<?php echo base_url() . 'akunting_download/export' ?>" method='post'>
                <div class="modal-body">
                    <div class="scroller" style="height:300px" data-always-visible="1" data-rail-visible1="1">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Tipe Laporan: <span class="required">*</span>
                                    </label>
                                    <div class="col-md-7">
                                        <select class="form-control" name="tipe" id="tipe">
                                            <option value="Harian">Harian</option>
                                            <option value="Mingguan">Mingguan BI</option>
                                            <option value="Bulanan">Bulanan BI</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" id="bulanelement" hidden="true">
                                    <label class="col-md-5 control-label">Bulan : <span class="required">*</span>
                                    </label>
                                    <div class="col-md-7">
                                        <div class="input-group input-medium date date-picker" data-date="" data-date-format="mm-yyyy" data-date-viewmode="years" data-date-minviewmode="months">
                                            <input type="text" class="form-control" id="datetahun" name="datetahun" readonly>
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="tanggal_harian_input" hidden="true">
                                    <label class="col-md-5 control-label">Tanggal Transaksi&nbsp;:</label>
                                    <div class="col-md-7">
                                        <div class="input-group date date-picker" data-date="" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                            <input type="text" class="form-control" value="<?php echo date('d-m-Y', strtotime(date('d-m-Y') . "-1 days")) ?>" name="transdate" id="transdate" readonly>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="periode_input">
                                    <label class="col-md-5 control-label">Periode&nbsp;: <span class="required">*</span></label>
                                    <div class="col-md-7">
                                        <div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" name="fromsortdate" id="fromsortdate">
                                            <span class="input-group-addon">
                                                to </span>
                                            <input type="text" class="form-control" name="tosortdate" id="tosortdate">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" formaction="<?php echo base_url() . 'beranda' ?>" class="btn default">Tutup</button>
                        <button type="submit" class="btn green">Download</button>
                    </div>
            </form>
        </div>
    </div>
    <div class="clearfix">
    </div>
</div>
