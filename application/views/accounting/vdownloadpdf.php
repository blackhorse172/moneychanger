<?php

/* setting zona waktu */
//if (function_exists('date_default_timezone_set')) disabled by fandi for tanggal edit
date_default_timezone_set('Asia/Jakarta');
$date = date_create(date('d-m-Y H:i:s'));
//date_add($date, date_interval_create_from_date_string('-11 hours')); disabled by fandi for tanggal edit
$this->fpdf->FPDF("L", "cm", "A4");
// kita set marginnya dimulai dari kiri, atas, kanan. jika tidak diset, defaultnya 1 cm
$this->fpdf->SetMargins(1, 0.2, 1);
/* AliasNbPages() merupakan fungsi untuk menampilkan total halaman
  di footer, nanti kita akan membuat page number dengan format : number page / total page
 */
$this->fpdf->AliasNbPages();

// **************************************** LOOPING ISI *******************************//
$indx = 1;
foreach ($hasildetail as $data) {
    $isitable[$indx] = $data;
    $indx++;
}
// **************************************** LOOPING ISI *******************************//
// **************************************** CREATE HEADER DAN JUMLAH HALAMAN LOOPING BEGIN *******************************//
$recordall = count($hasildetail); // DI ISI DENGAN SELURUH JUMLAH RECORD YG INGIN DI CETAK
$recordperpage = 34; // 26 JUMLAH RECORD DALAM 1 HALAMAN
$activepage = $recordall / $recordperpage; // JUMLAH ACTIVE PAGE
$firstrecord = 1;
$lastrecord = $recordperpage;
$idr_saldo_awal = 0;
$idr_pembelian = 0;
$idr_penjualan = 0;
$idr_saldoakhir = 0;
for ($i = 0; $i < $activepage; $i++) {
    // ******* HEADER BEGIN ******* //
    $this->fpdf->AddPage();
    $this->fpdf->SetY(1);
    $this->fpdf->Setx(2);
    $this->fpdf->Line(1, 1, 28.8, 1);
    $this->fpdf->Line(1, 1.05, 28.8, 1.05);
    $this->fpdf->Line(1, 1.75, 28.8, 1.75);
    $this->fpdf->Ln(0.4);
    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->Cell(2, 0, 'CURRENCY', 0, 'LR', 'C');
    $this->fpdf->SetX(2.9);
    $this->fpdf->Cell(1, 0, 'SALDO AWAL VALAS', 0, 'LR', 'L');
    $this->fpdf->SetX(6.1);
    $this->fpdf->Cell(1, 0, 'SALDO AWAL RUPIAH', 0, 'LR', 'L');
    $this->fpdf->SetX(10);
    $this->fpdf->Cell(1, 0, 'PEMBELIAN', 0, 'LR', 'L');
    $this->fpdf->SetX(15);
    $this->fpdf->Cell(1, 0, 'PENJUALAN', 0, 'LR', 'L');
    $this->fpdf->SetX(20);
    $this->fpdf->Cell(1, 0, 'SALDO AKHIR VALAS', 0, 'LR', 'L');
    $this->fpdf->SetX(23.1);
    $this->fpdf->Cell(1, 0, 'SALDO AKHIR RUPIAH', 0, 'LR', 'L');
    $this->fpdf->SetX(26.4);
    $this->fpdf->Cell(1, 0, 'KURS TENGAH', 0, 'LR', 'L');

//    $this->fpdf->Line(1, 4.5, 20, 4.5);


    $this->fpdf->Ln();
    $this->fpdf->SetFont('Times', '', 8);
    $this->fpdf->Ln(0.7);
    $kordinaty = 2.3;
    // ******* TABLE END ******* //
    // ******* LOOP ISI BEGIN ******* //
    $garisakhir = 2.8;
    $no = $firstrecord;
    $eartagtampung = "-";
    $tanggaltampung = "-";
    for ($j = $firstrecord; $j <= $lastrecord; $j++) {
        if ($j > $recordall) {
            break;
        }
        $saldo_akhir_valas = ($isitable[$j]->SaldoAwalValas + $isitable[$j]->JumlahPembelian) - $isitable[$j]->JumlahPenjualan;
        $saldo_akhir_rupiah = $saldo_akhir_valas * $isitable[$j]->KursTengah;
        $idr_saldo_awal = $idr_saldo_awal + $isitable[$j]->SaldoAwalRupiah;
        $idr_pembelian = $idr_pembelian + $isitable[$j]->JumlahPembelianIDR;
        $idr_penjualan = $idr_penjualan + $isitable[$j]->JumlahPenjualanIDR;
        $idr_saldoakhir = $idr_saldoakhir + $saldo_akhir_rupiah;

        $this->fpdf->Cell(1, 0, $isitable[$j]->Cur, 0, 'LR', 'C');
        $this->fpdf->SetX(4.8);
        $this->fpdf->Cell(1, 0, number_format($isitable[$j]->SaldoAwalValas, 0, ".", ","), 0, 'LR', 'R');
        $this->fpdf->SetX(8.1);
        $this->fpdf->Cell(1, 0, number_format($isitable[$j]->SaldoAwalRupiah, 0, ".", ","), 0, 'LR', 'R');
        $this->fpdf->SetX(10.9);
        $this->fpdf->Cell(1, 0, number_format($isitable[$j]->JumlahPembelian, 0, ".", ","), 0, 'LR', 'R');
        $this->fpdf->SetX(13.5);
        $this->fpdf->Cell(1, 0, number_format($isitable[$j]->JumlahPembelianIDR, 0, ".", ","), 0, 'LR', 'R');
        $this->fpdf->SetX(16.2);
        $this->fpdf->Cell(1, 0, number_format($isitable[$j]->JumlahPenjualan, 0, ".", ","), 0, 'LR', 'R');
        $this->fpdf->SetX(18.9);
        $this->fpdf->Cell(1, 0, number_format($isitable[$j]->JumlahPenjualanIDR, 0, ".", ","), 0, 'LR', 'R');
        $this->fpdf->SetX(22);
        $this->fpdf->Cell(1, 0, number_format($saldo_akhir_valas, 0, ".", ","), 0, 'LR', 'R');
        $this->fpdf->SetX(25.2);
        $this->fpdf->Cell(1, 0, number_format($saldo_akhir_rupiah, 0, ".", ","), 0, 'LR', 'R');
        $this->fpdf->SetX(27.7);
        $this->fpdf->Cell(1, 0, number_format($isitable[$j]->KursTengah, 0, ".", ","), 0, 'LR', 'R');
        $this->fpdf->Ln(0.5);
        $no++;
        $this->fpdf->Line(1, $kordinaty, 28.8, $kordinaty);
        $kordinaty = $kordinaty + 0.5;
    }
    $kordinaty = $kordinaty;
    $this->fpdf->Line(1, $kordinaty, 28.8, $kordinaty);
    $this->fpdf->Line(1, 1, 1, $kordinaty);
    $this->fpdf->Line(2.9, 1, 2.9, $kordinaty);
    $this->fpdf->Line(6, 1, 6, $kordinaty);
    $this->fpdf->Line(9.3, 1, 9.3, $kordinaty);
    $this->fpdf->Line(12, 1, 12, $kordinaty);
    $this->fpdf->Line(14.6, 1, 14.6, $kordinaty);
    $this->fpdf->Line(17.3, 1, 17.3, $kordinaty);
    $this->fpdf->Line(20, 1, 20, $kordinaty);
    $this->fpdf->Line(23.1, 1, 23.1, $kordinaty);
    $this->fpdf->Line(26.3, 1, 26.3, $kordinaty);
    $this->fpdf->Line(28.8, 1, 28.8, $kordinaty);

//    $this->fpdf->Line(1, $kordinaty, 20, $kordinaty);

    $firstrecord = $firstrecord + $recordperpage;
    $lastrecord = $lastrecord + $recordperpage;
    // ******* LOOP ISI END ******* //
    // ******* FOOTER BEGIN ******* //
    $this->fpdf->SetFont('Times', 'B', 8);
    $this->fpdf->SetY($kordinaty - 0.5);
    $this->fpdf->SetX(8.1);
    $this->fpdf->Cell(1, 0.5, number_format($idr_saldo_awal, 0, ".", ","), 0, 'LR', 'R');
    $this->fpdf->SetX(13.5);
    $this->fpdf->Cell(1, 0.5, number_format($idr_pembelian, 0, ".", ","), 0, 'LR', 'R');
    $this->fpdf->SetX(18.9);
    $this->fpdf->Cell(1, 0.5, number_format($idr_penjualan, 0, ".", ","), 0, 'LR', 'R');
    $this->fpdf->SetX(25.2);
    $this->fpdf->Cell(1, 0.5, number_format($idr_saldoakhir, 0, ".", ","), 0, 'LR', 'R');
//    $this->fpdf->SetY($kordinaty + 2);
//    $this->fpdf->SetX(2.1);
//    $this->fpdf->Cell(1, 0.6, 'Disetujui', 0, 0, 'C');
//    $this->fpdf->SetX(9.2);
//    $this->fpdf->Cell(1, 0.6, 'Mengetahui', 0, 0, 'C');
//    $this->fpdf->SetX(16.1);
//    $this->fpdf->Cell(1, 0.6, 'Yang Memohon', 0, 0, 'C');
//    $this->fpdf->SetY($kordinaty + 4);
//    $this->fpdf->SetX(1);
//    $this->fpdf->Cell(1, 0.6, '(................................)', 0, 0, 'l');
//    $this->fpdf->SetX(8);
//    $this->fpdf->Cell(1, 0.6, '(................................)', 0, 0, 'l');
//    $this->fpdf->SetX(15);
//    $this->fpdf->Cell(1, 0.6, '(................................)', 0, 0, 'l');
//    /* setting posisi footer 3 cm dari bawah */
//    $this->fpdf->SetY(-3);
//    /* setting font untuk footer */
//    $this->fpdf->SetFont('Times', '', 10);
    /* setting cell untuk waktu pencetakan */

    /* setting cell untuk page number */
//    $this->fpdf->Cell(17.5, 0.5, 'Page ' . $this->fpdf->PageNo() . '/{nb}', 0, 0, 'R');
    // ******* FOOTER END ******* //
}
// **************************************** CREATE HEADER DAN JUMLAH HALAMAN LOOPING END *******************************//
/* generate pdf jika semua konstruktor, data yang akan ditampilkan, dll sudah selesai */
$this->fpdf->Output("BULANAN_BI_" . date('d/m/Y') . ".pdf", "I");
?>