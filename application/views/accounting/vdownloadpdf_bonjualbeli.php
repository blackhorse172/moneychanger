<?php

/* setting zona waktu */
//if (function_exists('date_default_timezone_set')) disabled by fandi for tanggal edit
date_default_timezone_set('Asia/Jakarta');
$date = date_create(date('d-m-Y H:i:s'));
//date_add($date, date_interval_create_from_date_string('-11 hours')); disabled by fandi for tanggal edit
$this->fpdf->FPDF("L", "cm", "A5");
// kita set marginnya dimulai dari kiri, atas, kanan. jika tidak diset, defaultnya 1 cm
$this->fpdf->SetMargins(1, 0.2, 1);
/* AliasNbPages() merupakan fungsi untuk menampilkan total halaman
  di footer, nanti kita akan membuat page number dengan format : number page / total page
 */
$this->fpdf->AliasNbPages();

// **************************************** LOOPING ISI *******************************//
$indx = 1;
foreach ($hasildetail as $data) {
    $isitable[$indx] = $data;
    $indx++;
}
// **************************************** LOOPING ISI *******************************//
// **************************************** CREATE HEADER DAN JUMLAH HALAMAN LOOPING BEGIN *******************************//
$recordall = count($hasildetail); // DI ISI DENGAN SELURUH JUMLAH RECORD YG INGIN DI CETAK
$recordperpage = 34; // 26 JUMLAH RECORD DALAM 1 HALAMAN
$activepage = $recordall / $recordperpage; // JUMLAH ACTIVE PAGE
$firstrecord = 1;
$lastrecord = $recordperpage;
$idr_saldo_awal = 0;
$idr_pembelian = 0;
$idr_penjualan = 0;
$idr_saldoakhir = 0;
for ($i = 0; $i < 1; $i++) {
    // ******* HEADER BEGIN ******* //
    $this->fpdf->AddPage();

//    $this->fpdf->SetY(2.9);
//    $this->fpdf->SetX(12);

    $this->fpdf->SetFont('Times', 'B', 12);

    $this->fpdf->SetY(1.5);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'PT. SOLUSI MEGA ARTHA', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', '', 11);
    $this->fpdf->SetX(16);
    $this->fpdf->Cell(1, 0, 'Nota Pembelian / Penjualan', 0, 'LR', 'C');

    $this->fpdf->SetFont('Times', 'B', 12);
    $this->fpdf->SetY(2);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'AUTHORIZED MONEY CHANGER', 0, 'LR', 'L');
    $this->fpdf->SetFont('Times', '', 11);
    $this->fpdf->SetX(16);
    $this->fpdf->Cell(1, 0, 'Purchase / Sales Voucher', 0, 'LR', 'C');

    $this->fpdf->SetFont('Arial', '', 11);
    $this->fpdf->SetY(2.5);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, $db->Address, 0, 'LR', 'L');
    $this->fpdf->SetY(3);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Tel', 0, 'LR', 'L');
    $this->fpdf->SetX(4);
    $this->fpdf->Cell(1, 0, $db->Telp, 0, 'LR', 'L');
    $this->fpdf->SetY(3.5);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Fax', 0, 'LR', 'L');
    $this->fpdf->SetX(4);
    $this->fpdf->Cell(1, 0, $db->Fax, 0, 'LR', 'L');
    $this->fpdf->SetX(14);
    $this->fpdf->Cell(1, 0, $db->InvNo, 0, 'LR', 'L');

    $this->fpdf->SetFont('Arial', 'B', 10);
    $this->fpdf->SetY(4.1);
    $this->fpdf->SetX(2.4);
    $this->fpdf->Cell(1, 0, 'NO', 0, 'LR', 'L');
    $this->fpdf->SetX(5);
    $this->fpdf->Cell(1, 0, 'CURR', 0, 'LR', 'C');
    $this->fpdf->SetX(8.8);
    $this->fpdf->Cell(1, 0, 'AMOUNT', 0, 'LR', 'C');
    $this->fpdf->SetX(12);
    $this->fpdf->Cell(1, 0, 'RATE', 0, 'LR', 'C');
    $this->fpdf->SetX(16);
    $this->fpdf->Cell(1, 0, 'TOTAL', 0, 'LR', 'C');

    $this->fpdf->SetFont('Arial', '', 10);
    $this->fpdf->SetY(4.8);
    $this->fpdf->SetX(2.5);
    $this->fpdf->Cell(1, 0, '1', 0, 'LR', 'L');
    $this->fpdf->SetX(5);
    $this->fpdf->Cell(1, 0, $db->Cur, 'LR', 'C');
    $this->fpdf->SetX(10);
    $this->fpdf->Cell(1, 0, number_format(str_replace('-', '', $db->Jumlah), 2, ",", "."), 0, 'LR', 'R');
    $this->fpdf->SetX(13);
    $this->fpdf->Cell(1, 0, number_format(str_replace('-', '', $db->Rate), 2, ",", "."), 0, 'LR', 'R');
    $this->fpdf->SetX(18);
    $this->fpdf->Cell(1, 0, number_format(str_replace('-', '', $db->Jumlah_Rate), 2, ",", "."), 0, 'LR', 'R');
    $this->fpdf->SetY(5.3);
    $this->fpdf->SetX(2.5);
    $this->fpdf->Cell(1, 0, '2', 0, 'LR', 'L');
    $this->fpdf->SetX(18);
    $this->fpdf->Cell(1, 0, number_format(str_replace('-', '', 0), 2, ",", "."), 0, 'LR', 'R');
    $this->fpdf->SetY(5.8);
    $this->fpdf->SetX(2.5);
    $this->fpdf->Cell(1, 0, '3', 0, 'LR', 'L');
    $this->fpdf->SetX(18);
    $this->fpdf->Cell(1, 0, number_format(str_replace('-', '', 0), 2, ",", "."), 0, 'LR', 'R');
    $this->fpdf->SetY(6.3);
    $this->fpdf->SetX(2.5);
    $this->fpdf->Cell(1, 0, '4', 0, 'LR', 'L');
    $this->fpdf->SetX(18);
    $this->fpdf->Cell(1, 0, number_format(str_replace('-', '', 0), 2, ",", "."), 0, 'LR', 'R');
    $this->fpdf->SetY(6.8);
    $this->fpdf->SetX(2.5);
    $this->fpdf->Cell(1, 0, '5', 0, 'LR', 'L');
    $this->fpdf->SetX(18);
    $this->fpdf->Cell(1, 0, number_format(str_replace('-', '', 0), 2, ",", "."), 0, 'LR', 'R');

    $this->fpdf->SetFont('Arial', 'B', 10);
    $this->fpdf->SetY(7.7);
    $this->fpdf->SetX(18);
    $this->fpdf->Cell(1, 0, number_format(str_replace('-', '', $db->Jumlah_Rate), 2, ",", "."), 0, 'LR', 'R');

    $this->fpdf->Line(14.2, 1.75, 18.8, 1.75);
    $this->fpdf->Line(14.2, 1.78, 18.8, 1.78);

    if ($db->BorS == 1) {
        $this->fpdf->Line(15.2, 1.5, 16.9, 1.5);
        $this->fpdf->Line(14.5, 2, 15.9, 2);
    } else {
        $this->fpdf->Line(17.1, 1.5, 18.7, 1.5);
        $this->fpdf->Line(16.2, 2, 17.1, 2);
    }

    $this->fpdf->Line(2, 3.8, 19, 3.8);
    $this->fpdf->Line(2, 4.4, 19, 4.4);
    $this->fpdf->Line(2, 4.45, 19, 4.45);
    $this->fpdf->Line(2, 3.8, 2, 7.4); // garis kiri
    $this->fpdf->Line(19, 3.8, 19, 7.4); // garis kanan
    $this->fpdf->Line(2, 7.4, 19, 7.4); //garis bawah

    $this->fpdf->Line(14, 7.4, 19, 7.4);
    $this->fpdf->Line(14, 8, 19, 8);
    $this->fpdf->Line(14, 7.4, 14, 8); // garis kiri
    $this->fpdf->Line(19, 7.4, 19, 8); // garis kanan

    $this->fpdf->Line(3.6, 3.8, 3.6, 7.4); // garis kiri
    $this->fpdf->Line(7.5, 3.8, 7.5, 7.4); // garis kiri
    $this->fpdf->Line(11, 3.8, 11, 7.4); // garis kiri
    $this->fpdf->Line(14, 3.8, 14, 7.4); // garis kiri

    $this->fpdf->SetY(7.7);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Customer Details', 0, 'LR', 'L');
    $this->fpdf->SetX(13.2);
    $this->fpdf->Cell(1, 0, 'Rp', 0, 'LR', 'L');

    $this->fpdf->SetFont('Arial', '', 10);
    $this->fpdf->SetY(8.2);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Nama', 0, 'LR', 'L');
    $this->fpdf->SetX(4);
    $this->fpdf->Cell(1, 0, $db->Nama, 0, 'LR', 'L');
    $this->fpdf->SetY(8.7);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'ID', 0, 'LR', 'L');
    $this->fpdf->SetX(4);
    $this->fpdf->Cell(1, 0, $db->ID, 0, 'LR', 'L');
    $this->fpdf->SetY(9.2);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Add', 0, 'LR', 'L');
    $this->fpdf->SetX(4);
    $this->fpdf->Cell(1, 0, $db->Alamat, 0, 'LR', 'L');
    $this->fpdf->SetY(9.7);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Tel', 0, 'LR', 'L');
    $this->fpdf->SetX(4);
    $this->fpdf->Cell(1, 0, $db->Telp, 0, 'LR', 'L');
    $this->fpdf->SetY(10.2);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Asal Dana', 0, 'LR', 'L');
    $this->fpdf->SetX(4);
    $this->fpdf->Cell(1, 0, $db->AsalDana, 0, 'LR', 'L');
    $this->fpdf->SetY(10.7);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Tuj. Trans.', 0, 'LR', 'L');
    $this->fpdf->SetX(4);
    $this->fpdf->Cell(1, 0, $db->TujuanTransakasi, 0, 'LR', 'L');
    $this->fpdf->SetY(11.2);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Kuasa', 0, 'LR', 'L');
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, $db->Kuasa_Nama, 0, 'LR', 'L');
    $this->fpdf->SetY(11.7);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'ID Kuasa', 0, 'LR', 'L');
    $this->fpdf->SetX(4);
    $this->fpdf->Cell(1, 0, $db->Kuasa_ID, 0, 'LR', 'L');

    $this->fpdf->SetY(9.5);
    $this->fpdf->SetX(14);
    $this->fpdf->Cell(1, 0, 'Jakarta, 28 Oktober 2016', 0, 'LR', 'L');

    $this->fpdf->SetFont('Arial', 'B', 9);
    $this->fpdf->SetY(12.2);
    $this->fpdf->SetX(15.5);
    $this->fpdf->Cell(1, 0, '( Kasir )', 0, 'LR', 'L');
    $this->fpdf->SetFont('Arial', 'B', 8);
    $this->fpdf->SetY(12.2);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Perhatian / Notice', 0, 'LR', 'L');
    $this->fpdf->SetX(10.5);
    $this->fpdf->Cell(1, 0, '( customer )', 0, 'LR', 'L');
    $this->fpdf->SetFont('Arial', '', 8);
    $this->fpdf->SetY(12.5);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Kekurangan penerimaan uang tidak ditanggung setelah meninggalkan loket / kasir', 0, 'LR', 'L');
    $this->fpdf->SetY(12.8);
    $this->fpdf->SetX(2);
    $this->fpdf->Cell(1, 0, 'Claim for shortage of cash after leaving our counter can not be considered', 0, 'LR', 'L');
}


// **************************************** CREATE HEADER DAN JUMLAH HALAMAN LOOPING END *******************************//
/* generate pdf jika semua konstruktor, data yang akan ditampilkan, dll sudah selesai */
$this->fpdf->Output("Print_Out_" . date('d/m/Y') . ".pdf", "I");
?>