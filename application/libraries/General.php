<?php

/*
 * Libraries for core
 * AUTH : A.P
 */

class General {

    var $ci;
    public $branchCode;
    public $branchId;
    public $companyId;
    public $companyName;
    public $companyAddress;
    public $companyTelp;
    public $companyFax;
    public $userType;
    private $timeStamp;
    private $session;
    private $userId;
    public function __construct() {
        $this->ci = &get_instance();
        $this->ci->load->database();
        $this->session = $this->ci->session->userdata('session_data');
        $this->branchCode = $this->session['branchdefaultcode'];
        $this->branchId = $this->session['branchid'];
        $this->userId = $this->session['id'];
        $this->userType = $this->session['usertype'];

        $this->timeStamp = "CONVERT(VARCHAR,GETDATE(),12) + REPLACE(CONVERT(varchar,GETDATE(),14),':','')";
        //hardcode
        $this->companyId = $this->session['companyid'];
        $this->companyName = $this->session['companyname'];
        $this->companyAddress = 'Jl Dekana III no 99 Jakarta Barat';
        $this->companyTelp = '021-888999888';
        $this->companyFax = '021-123499888';

     }

     public function getCustomerId($customerName = "")
    {
        if (!empty($customerName)) {
            # code...
                try {
                $result = $this->ci->db->query("select Iid from MS_Customer Where Nama like '$customerName' ");
                    if (! $result) {
                        throw new Exception();
                        
                    }
                } catch (Exception $e) {
                    return $e;
                    exit;
                }
            return $result->row()->Iid;
        } else return NULL;
    	 
         
    }


     public function getCustomerData($customerName = "")
    {
        if (!empty($customerName)) {
            # code...
                try {
                $result = $this->ci->db->query("select * from MS_Customer Where Nama like '$customerName' and IsActive = 1 ")->row();
                    if (! $result) {
                        throw new Exception();
                        
                    }
                } catch (Exception $e) {
                    return $e;
                    exit;
                }


            return array(
                'iid'=> $result->Iid,
                'companyId'=> $result->CompanyId,
                'branchId'=> $result->BranchId,
                'kode'=> $result->Kode,
                'nama'=> $result->Nama,
                'alamat'=> $result->Alamat,
                'telp'=> $result->Telp,
                'sumberDana'=> $result->SumberDana,
                'tujuanTransaksi'=> $result->TujuanTransaksi,
                'kuasa1_ID'=> $result->Kuasa1_ID,
                'kuasa1_Nama' => $result->Kuasa1_Nama
                );
        } else return NULL;
         
         
    }

    function getCurrency($curr_code = "")
    {
        if (!empty($curr_code)) {
            try {
                $result = $this->ci->db->query("select REF_CURR_ID, CURR_CODE from SIRIUSDB_DEV.dbo.REF_CURR "
                        . "Inner join MS_Rate on MS_Rate.CurrencyId = SIRIUSDB_DEV.dbo.REF_CURR.REF_CURR_ID "
                        . "where IS_ACTIVE = 1 and CURR_CODE != 'IDR' and CURR_CODE = '$curr_code' ");
                  if (! $result) {
                    throw new Exception();
                    
                }
            } catch (Exception $e) {
                return $e;
                exit;
            }

            return $result->row()->REF_CURR_ID;    
        } else  return NULL; 

    }

    function getCurrencyData($type)
    {
        $companyid = $this->companyId;
        $branchid =  $this->branchId;
            try {
                 $query = "exec SP_GetCurrency @companyid='$companyid', @branchid='$branchid', @type='$type' ";
                 $result = $this->ci->db->query($query);
                if (! $result) {
                    throw new Exception();
                    
                }
            } catch (Exception $e) {
                return $e;
                exit;
            }
    
              return $result->result();    
    
    }

       function getCurrencyBySP($curr_code = "")
    {
        if (!empty($curr_code)) {
            try {
                 $query = "exec SP_GetCurrencyId @currencyCode='$curr_code' ";
                 $result = $this->ci->db->query($query)->row()->ret;
                if (! $result) {
                    throw new Exception();
                    
                }
            } catch (Exception $e) {
                return $e;
                exit;
            }
            
            return $result;    
        } else  return NULL; 

    }

    function getCurrencyById($curr_id = "")
    {
        if (!empty($curr_id)) {
            try {
                 $query = "exec SP_GetCurrencyCode @currencyId='$curr_id' ";
                 $result = $this->ci->db->query($query)->row()->ret;
                if (! $result) {
                    throw new Exception();
                    
                }
            } catch (Exception $e) {
                return $e;
                exit;
            }
            
            return $result;    
        } else  return NULL; 

    }

    /** 
        Table TX_Payment
    **/
    function checkingPaymentByTransactionId($iid= "")
    {
        if (!empty($iid)) {
            try {
                $result = $this->ci->db->query("select * from TX_Payment where transcationId = '$iid' ");
                
                if (! $result) {    
                    throw new Exception();
                    
                }
            } catch (Exception $e) {
                return $e;
                exit;
            }
            
            
            return $result->num_row();    
        } else  return NULL; 

    }

    function getAutoNumber()
    {
        
        return "'".$this->branchCode."'". '+' . $this->timeStamp;
        # code...
    }

      function getPaymentNo()
    {
            $code = 'PM/'.$this->branchCode;
            $companyId = $this->companyId;
            $branchId = $this->branchId;
     
          $query = "exec SP_GetAutoNumber_Ms @id='PaymentId',@table='TX_Payment',@formattengah='$code',@transdate='',"
                        . "@companyId='$companyId',@branchid='$branchId' ";

            $result = $this->ci->db->query($query);

         return $result->row()->ret;
    }

    function getInvoiceNo()
    {
            $code = 'INV/'.$this->branchCode.date('ymdhis');
          //   $companyId = $this->companyId;
          //   $branchId = $this->branchId;

          // $query = "exec SP_GetAutoNumber_Inv @id='InvoiceNo',@table='TX_Invoice',@formattengah='$code',@transdate='',"
          //               . "@companyId='$companyId',@branchid='$branchId' ";

          //   $result = $this->ci->db->query($query);
         return $code;
    }

    function getCustomerIid()
    {
            $code = 'CM/'.$this->branchCode;
            $companyId = $this->companyId;
            $branchId = $this->branchId;
     
          $query = "exec SP_GetAutoNumber_Ms @id='Iid',@table='MS_Customer',@formattengah='$code',@transdate='',"
                        . "@companyId='$companyId',@branchid='$branchId' ";

            $result = $this->ci->db->query($query);

         return $result->row()->ret;
    }

     function getStpIid($string,$table)
    {
          $result = null;
            if (!empty($string)) {
                # code...
                $code = '';
                if ($table == 'MS_Branch') {
                    # code...
                    $code = $string;
                } else if ($table = 'STP_Company') {
                    $code = $string;
                } else {
                    $code = $string.'/'.$this->branchCode;     
                }
               
                $companyId = $this->companyId;
                $branchId = $this->branchId;
         
              $query = "exec SP_GetAutoNumber_Stp @id='Iid',@table='$table',@formattengah='$code',@transdate=''";

                $result = $this->ci->db->query($query);
               
            }
          
            if (!is_null($result)) {
                # code...
                $result =  $result->row()->ret;
            }
         return $result;
    }


    function getIid($string,$table)
    {
        $result = null;
            if (!empty($string)) {
                # code...
                $code = $string.'/'.$this->branchCode;
                $companyId = $this->companyId;
                $branchId = $this->branchId;
         
              $query = "exec SP_GetAutoNumber @id='Iid',@table='$table',@formattengah='$code',@transdate='',"
                            . "@companyId='$companyId',@branchid='$branchId', @ret=output ";

                $result = $this->ci->db->query($query);
               
            }
          
            if (!is_null($result)) {
                # code...
                $result =  $result->row()->ret;
            }
         return $result;
    }

    function getIids($id,$string,$table)
    {
        $result = null;
            if (!empty($string)) {
                # code...
                $code = $string.'/'.$this->branchCode;
                $companyId = $this->companyId;
                $branchId = $this->branchId;
         
              $query = "exec SP_GetAutoNumber @id='$id',@table='$table',@formattengah='$code',@transdate='',"
                            . "@companyId='$companyId',@branchid='$branchId', @ret=output ";

                $result = $this->ci->db->query($query);
               
            }
          
            if (!is_null($result)) {
                # code...
                $result =  $result->row()->ret;
            }
         return $result;
    }



      function getUserIdActive()
    {
        
        return $this->userId;   
    }

      function getDateNow()
    {
        
        return date('Y-m-d H:i:s');   
    }

      function convertSeparator($value)
    {
        //$pos = '';
        $pos = strpos($value, '-');
        $replace = str_replace(',', '', $value);
        $newValue = abs($replace);
        if ($pos===0) {
            # code...
            $newValue = $newValue * -1;   
        }
       
        return $newValue;
    }

    function kekata($x) {
              if ($x < 0) {
            # code...
            $x = $x * -1;
        }

    $x = abs($x);
    $angka = array("", "satu", "dua", "tiga", "empat", "lima",
    "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($x <12) {
        $temp = " ". $angka[$x];
    } else if ($x <20) {
        $temp = $this->kekata($x - 10). " belas";
    } else if ($x <100) {
        $temp = $this->kekata($x/10)." puluh". $this->kekata($x % 10);
    } else if ($x <200) {
        $temp = " seratus" . kekata($x - 100);
    } else if ($x <1000) {
        $temp = $this->kekata($x/100) . " ratus" . $this->kekata($x % 100);
    } else if ($x <2000) {
        $temp = " seribu" . kekata($x - 1000);
    } else if ($x <1000000) {
        $temp = $this->kekata($x/1000) . " ribu" . $this->kekata($x % 1000);
    } else if ($x <1000000000) {
        $temp = $this->kekata($x/1000000) . " juta" . $this->kekata($x % 1000000);
    } else if ($x <1000000000000) {
        $temp = $this->kekata($x/1000000000) . " milyar" . $this->kekata(fmod($x,1000000000));
    } else if ($x <1000000000000000) {
        $temp = $this->kekata($x/1000000000000) . " trilyun" . $this->kekata(fmod($x,1000000000000));
    }     
        return $temp;
}


function terbilang($x, $style=1) {
    
        $hasil = trim($this->kekata($x));
        
    switch ($style) {
        case 1:
            $hasil = strtoupper($hasil);
            break;
        case 2:
            $hasil = strtolower($hasil);
            break;
        case 3:
            $hasil = ucwords($hasil);
            break;
        default:
            $hasil = ucfirst($hasil);
            break;
    }     
    return $hasil;
}


     function getCompanyData() {
        //$companyId = $this->companyId;

        $this->ci->db->from('MS_Branch');
        $this->ci->db->where('CompanyId',$this->companyId);
        
        $result = $this->ci->db->get()->row();

        if (count($result) > 0) {
            # code...
            return $result;
        }

        return array();

    }


    function getTLabaRugi() {
        
    }

     public function getLastRateUpdate()
    {
        // if (!empty($customerName)) {
            # code...
                try {
                $result = $this->ci->db->query("exec SP_GetLastUpdateRate ");
                    if (! $result) {
                        throw new Exception();
                        
                    }
                } catch (Exception $e) {
                    return $e;
                    exit;
                }
            return $result->row()->LAST_UPD_DTM;
        // } else return NULL;
         
         
    }

    public function getExpiredApp($cp,$br,$resfield)
    {
        $companyId = $this->companyId;
        $branchId = $this->branchId;
        if (is_null($companyId) and is_null($branchId)){
            $companyId = $cp;
            $branchId = $br;
        }
        
        // if (!empty($customerName)) {
            # code...
                try {
                $conn = $this->check_dbsync_connection();
                if ($conn == 1) {
                    $DBsync = $this->ci->load->database('smartapi', TRUE);
                    $result = $DBsync->query("exec SP_ExCheck @companyid='$companyId',@branchid='$branchId' ");
                        if (! $result) {
                            throw new Exception();
                            
                        }
                    }
                } catch (Exception $e) {
                    return $e;
                    exit;
                }
            return $result->row()->$resfield;
        // } else return NULL;
         
         
    }

    function check_dbsync_connection() {
        $DBsync = $this->ci->load->database('smartapi', TRUE);
        $connected = $DBsync->initialize();
        if ($connected) {
            return 1;
        } else {
            return 0;
        }
    }
}



?>
