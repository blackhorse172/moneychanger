<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tx_PaymentModel extends CI_Model {

	private $table = 'TX_Payment';

	public function __construct()
	{
		parent::__construct();
		
	}

	/*** 
		CHECK TRANSACTION ID IN THIS TABLE
		@RETURN ARRAY()
	 **/
	public function getPaymentIdByTransactionId($id = array())
	{
		# code...
		$this->db->from($this->table);
		$this->db->where_in('transactionId',$id);
		$result = $this->db->get()->result();

		if (count($result) > 0) {
			# code...
			return $result[0]->paymentId;
		}

		return null;
		
	}

		/*** 
		CHECK TRANSACTION ID IN THIS TABLE
		@RETURN ARRAY()
	 **/
	public function getPaymentIdByTrxId($id)
	{
		# code...
		$this->db->from($this->table);
		$this->db->where('transactionId',$id);
		$result = $this->db->get()->result();

		if (count($result) > 0) {
			# code...
			return $result[0]->paymentId;
		}

		return null;
		
	}

		/*** 
		CHECK TRANSACTION ID IN THIS TABLE
		@RETURN ROW
	 **/
	public function getIidByTransaction($id)
	{
		# code...
		$this->db->from($this->table);
		$this->db->where_in('transactionId',$id);
		$result = $this->db->get()->row();

		if (count($result) > 0) {
			# code...
			return $result->paymentId;
		}

		return null;
		
	}

		/*** 
		CHECK TRANSACTION ID IN THIS TABLE
		@RETURN ARRAY()
	 **/
	public function checkPaymentIdByTransactionId($id)
	{
	
		$this->db->from($this->table);
		$this->db->where('transactionId',$id);
		$result = $this->db->get();
		return $result->result_array();
	}


	/*** 
	TRANSACTION ID IN THIS TABLE
	RETURN ARRAY()
	 **/
	public function getChangeAmountByTransactionId($id = array())
	{
		$paymentIds = [];
		$this->db->from($this->table);
		$this->db->where_in('transactionId',$id);
		$results = $this->db->get();
		
		foreach ($results->result() as $key => $value) {
			# code...
			$paymentIds[$key] = $value->paymentId;
		}
		
		//get data in payment detail
		$cashAmount 		= 0;
		$bankAmount 		= 0;
		$totalPayment		= 0;
		$this->db->from('TX_Payment_Detail');
		$this->db->where_in('paymentId',$paymentIds);
		$resultsDetails = $this->db->get()->result();

		foreach ($resultsDetails as $key => $value) {
			# code...
			$cashAmount = $cashAmount + $value->cashAmount;
			$bankAmount = $bankAmount + $value->bankAmount;
		}

		$totalPayment = $cashAmount + $bankAmount;

		$grantotal = 0;
		$grand     = 0;
		$this->db->from('TX_Transaction');
		$this->db->where_in('Iid',$id);
		$resultsIdTrx = $this->db->get()->result();		
		foreach ($resultsIdTrx as $key => $value) {
			# code...
			$grantotal = $grantotal + $value->TotalTransaksi;
		}

		if ($grantotal < 0) {
			# code...
			$grantotal = $grantotal * -1;
			$grand = ($grantotal - $totalPayment) * -1;
		} else {
			$grand = $grantotal - $totalPayment;
		}
	
		return $grand;
	}





}

/* End of file Trx_PaymentModel.php */
/* Location: ./application/models/Trx_PaymentModel.php */