<?php

class Muser extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function cekuser($puser) {
        $query = "select * from stp_user where UserId = '$puser'";
        $q = $this->db->query($query);

        return $q->row();
    }

    function cekcompany($pid) {
        $query = "select * from stp_company where Iid = '$pid'";
        $q = $this->db->query($query);

        return $q->row();
    }

    function cekcompanyable($user) {
        $query = "select distinct CompanyId from stp_userroleauthority where UserId = '$user'";

        $q = $this->db->query($query);

        return $q->row();
    }

    function getbranchid($puser) {
        $qbranch = "select distinct BranchId from stp_userroleauthority where UserId = '$puser'";
        $q = $this->db->query($qbranch);

        return $q->row();
    }

    function getbranchformat($branchid) {
        $qbranch = "select distinct DefaultFormatCode from MS_Branch where Name = '$branchid'";
        $q = $this->db->query($qbranch);

        return $q->row();
    }


    function getbranchidcompany($puser, $pcompanyid) {
        $qbranch = "select distinct BranchId from stp_userroleauthority where UserId = '$puser' AND companyid = '$pcompanyid'";
        $q = $this->db->query($qbranch);

        return $q->row();
    }

    function deleteuser($id) {
        $query = "delete from stp_user where UserId = '$id'";
        $this->db->query($query);
    }

    function deleteuserroleauth($id) {

        $query = "delete from stp_userroleauthority where Iid = '$id'";
        $this->db->query($query);
    }

    function deleteuserrole($id) {

        $query = "delete from stp_userrole where Iid = '$id'";
        $this->db->query($query);
    }

}
