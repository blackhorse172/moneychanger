<?php

if (!defined('BASEPATH'))
    exit('No direct access allowed');

class TransactionModel extends CI_Model {
    
    private $table =  'TX_Transaction';
    private $status = 0;
    public function __construct() {
        parent::__construct();
    }

    function cekPaidStatus($id = array()) {
        $this->db->from($this->table);
        $this->db->where_in('Iid',$id);
        $this->db->where('isPaid',1);
        $result = $this->db->get()->result();

        if (count($result) > 0) {
            # code...
            return $result;
        }

        return array();
    }

      function cekPaidStatusByTrxId($id) {
      
        $this->db->from($this->table.'_Header');
        $this->db->where('Transaction_Iid',$id);
        $this->db->where('IsPaid','1');
        $result = $this->db->get()->row();

        if (count($result) > 0) {
            # code...
            return $result;
        }

        return array();
    }

    function getDataByIids($id = array()) {
        # code...
        $this->db->from($this->table);
        $this->db->where_in('Iid',$id);
        $result = $this->db->get();

        if (count($result) > 0) {
            # code...
            return $result;
        }

        return null;
    }

     function getDataHeaderByIid($id) {
        # code...
        $this->db->from($this->table.'_Header');
        $this->db->where('Transaction_Iid',$id);
        $result = $this->db->get();

        if (count($result) > 0) {
            # code...
            return $result->row()->Bors;
        }

        return null;
    }

      function getTotalTransactionByTrxid($id) {
        # code...
        $this->db->from($this->table);
        $this->db->where('Transaction_Iid',$id);
        $result = $this->db->get();
        
        $total = 0;
        if (count($result) > 0) {
            # code...
            foreach ($result->result() as $key => $value) {
                # code...
                $total = $total + $value->TotalTransaksi;
            }
        }

        return $this->general->convertSeparator($total);
    }

    function getDataIspaidByIids($id = array()) {
        # code...
        $this->db->from($this->table);
        $this->db->where_in('Iid',$id);
        $result = $this->db->get();

        if (count($result) > 0) {
            # code...
            return $result;
        }

        return null;
    }

     function getInvNoByIid($id) {
        # code...
        $this->db->from($this->table);
        $this->db->where('Iid',$id);
        $result = $this->db->get();

        if (count($result) > 0) {
            # code...
            return $result->row()->InvNo;
        }

        return null;
    }


     function getRowHeader($id) {
        # code...
        $this->db->from($this->table.'_Header');
        $this->db->where('Transaction_Iid',$id);
        $result = $this->db->get();

        if (count($result) > 0) {
            # code...
            return $result->row();
        }

        return null;
    }


    function getDataByInvNo($id) {
        # code...
        $this->db->from($this->table);
        $this->db->where('InvNo',$id);
        $result = $this->db->get();

        if (count($result) > 0) {
            # code...
            return $result->result_array();
        }

        return null;
    }

    function getDataDetailByTransactionId($id) {
        # code...
        $this->db->from($this->table);
        $this->db->where('Transaction_Iid',$id);
        $result = $this->db->get();

        if (count($result) > 0) {
            # code...
            return $result->result_array();
        }

        return null;
    }

    function getDataIspaidByCustId($id) {
        # code...
        $this->db->from($this->table);
        $this->db->where('CustId',$id);
        $this->db->where('IsPaid',1);
        $result = $this->db->get();

        if (count($result) > 0) {
            # code...
            return $result->row();
        }

        return null;
    }


    function generateInvoice($value='')
    {
        # code...
    }

     function getIdsNotHaveInvoice($id)
    {
        # code...
          # code...
        $this->db->from($this->table.'_Header');
        $this->db->where('Transaction_Iid',$id);
        $this->db->where('IsPaid',1);
        $this->db->where('InvoiceNo',NULL);
        $result = $this->db->get();

        if (count($result) > 0) {
            # code...
            return $result->result_array();
        }

        return null;
    }

    function getTransactionHeader() {

        $companyId = $this->general->companyId;
        $branchId = $this->general->branchId;

        $this->db->select('T.*, C.Nama');
        $this->db->from($this->table.'_Header T');
        $this->db->join('MS_Customer C', 'C.Iid = T.CustId');
        $this->db->where('T.CompanyId',$companyId);
        $this->db->where('T.BranchId',$branchId);
        //$this->db->where('T.TglTransaksi',$branchId);
       
        // a.TglTransaksi >= DATEADD(day,-1,convert(date,getdate(),105))
        //             Order by a.Iid Desc
        $result = $this->db->get()->result();

        if (count($result) > 0) {
            # code...
            return $result;
        }

        return NULL;
    }

    function getTransactionHeaderByTransactioId($id) {

        $companyId = $this->general->companyId;
        $branchId = $this->general->branchId;

        $this->db->select('T.*, C.Nama');
        $this->db->from($this->table.'_Header T');
        $this->db->join('MS_Customer C', 'C.Iid = T.CustId');
        $this->db->where('T.CompanyId',$companyId);
        $this->db->where('T.BranchId',$branchId);
        $this->db->where('T.Transaction_Iid',$id);
        //$this->db->where('T.TglTransaksi',$branchId);
       
        // a.TglTransaksi >= DATEADD(day,-1,convert(date,getdate(),105))
        //             Order by a.Iid Desc
        $result = $this->db->get()->result();

        if (count($result) > 0) {
            # code...
            return $result;
        }

        return NULL;
    }

    function getDetailByTransactionId($trxId) {

        $companyId = $this->general->companyId;
        $branchId = $this->general->branchId;

        $this->db->select('T.*, C.Nama as NamaCustomer');
        $this->db->from($this->table.' T');
        $this->db->join('MS_Customer C', 'C.Iid = T.CustId');
        $this->db->where('T.CompanyId',$companyId);
        $this->db->where('T.BranchId',$branchId);
        $this->db->where('T.Transaction_Iid',$trxId);
        //$this->db->where('T.TglTransaksi',$branchId);
       
        // a.TglTransaksi >= DATEADD(day,-1,convert(date,getdate(),105))
        //             Order by a.Iid Desc
        $result = $this->db->get()->result();

        if (count($result) > 0) {
            # code...
            return $result;
        }

        return NULL;
    }

}

?>