<?php

class Serverinfo extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $server = "RPH";

    // ****************** Different Server Function BEGIN ************* //
    function checkpooldatabase($action = null) {
        $conn = $this->check_dbsync_connection();
        if ($conn == 1) {
            $DBsync = $this->load->database('sync', TRUE);
            $query = "SELECT xIid,xTable,xQuery FROM trx_pool_query ORDER BY Iid";
            $queryinpool = $this->db->query($query);
            $poolcount = $this->db->query($query)->num_rows();
            if ($poolcount > 0) {
                foreach ($queryinpool->result() as $baris) {
                    $this->db->trans_begin();
                    $DBsync->trans_begin();

                    $DBsync->query($baris->xQuery);

                    $this->db->where('xIid', $baris->xIid);
                    $this->db->delete('trx_pool_query');

                    if ($this->db->trans_status() === FALSE || $DBsync->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        $DBsync->trans_rollback();
                    } else {
                        $this->db->trans_commit();
                        $DBsync->trans_commit();
                    }
                }
            }
        }
    }

    function dbsync_query($id, $table, $query) {
        $conn = $this->check_dbsync_connection();
        if ($conn == 1) {
            $DBsync = $this->load->database('sync', TRUE);
            $DBsync->query($query); // Run the Pool Query
        } else {
            $idpool = $this->mautonumber->mstxautonumber('Iid', 'trx_pool_query', 'POOLQ');
            $query = '"' . $query . '"';
            $this->db->query("insert into trx_pool_query (Iid,xIid,xTable,xQuery,InputDate) "
                    . "values('$idpool','$id','$table',$query,GETDATE())");
        }
    }

    function check_dbsync_connection() {
        $DBsync = $this->load->database('sync', TRUE);
        $connected = $DBsync->initialize();
        if ($connected) {
            return 1;
        } else {
            return 0;
        }
    }

    function programversion() {
        $progversi = $this->db->query("SELECT top 1 Version FROM STP_ProgramVersion ORDER BY Version DESC")->row();
        return $progversi->Version;
    }

    function serverformat() {
        return $this->server;
    }

    function authorityserver($table, $id) {
        $result = $this->db->query("select ServerSource from $table Where Iid = '$id'")->row();
        if (count($result) > 0) { // ada atau tidak ?
            if ($result->ServerSource == $this->programserver()) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    function authorityserver_forinput($server_param) {
        if ($server_param == $this->server) {
            return 1;
        } else {
            return 0;
        }
    }

    // ****************** Different Server Function END ************* //
}
