<?php

class Sqlserver_connector extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function exec_query($query) {
        $this->db->trans_begin();

        $DBsync = $this->load->database('sqlserver', TRUE);
        $res = $DBsync->query($query);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
            return $res;
        }
    }

}
