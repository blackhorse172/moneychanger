<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Hash extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $key1 = "sVFD23sklooO0f";
    public $key2 = "adFd234s57kK";

    function cekhash($str) {
        $str_decoded = base64_decode($str);
        $key1_length = strlen($this->key1);
        $key2_length = strlen($this->key2);
        try {
            $key1_getter = substr($str_decoded, 8, $key1_length);
            $key2_getter = substr($str_decoded, 12 + $key1_length, $key2_length);
            $timestamp = substr($str_decoded, 8 + $key1_length, 2) . '-' . substr($str_decoded, $key1_length + $key2_length + 12, 2) . '-' . substr($str_decoded, 4, 2) . ' ' . substr($str_decoded, $key1_length + $key2_length + 14, 2) . ':' . substr($str_decoded, 2, 2) . ':' . substr($str_decoded, 0, 2);

            $date1 = date_create(date($timestamp));
            $date2 = date_create(date("y-m-d H:i:s"));
            $interval = date_diff($date1, $date2);

            if ($interval->format('%i') <= 60 && $key1_getter == $this->key1 && $key2_getter == $this->key2) {
                return 1; //passed
            } else {
                return 0; //failed
            }
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
    }

    function gethash() {
        $d = date('d', strtotime(date('d-m-Y H:i:s')));
        $md = date('m', strtotime(date('d-m-Y')));
        $y = date('y', strtotime(date('d-m-Y H:i:s')));
        $h = date('H', strtotime(date('H:i:s')));
        $i = date('i', strtotime(date('H:i:s')));
        $s = date('s', strtotime(date('d-m-Y H:i:s')));
        $str = $s . $i . $d . $s . $this->key1 . $y . $s . $this->key2 . $md . $h . $s;
        return base64_encode($str);
    }

}
