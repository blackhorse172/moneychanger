<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mautonumber extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function autonumber($id, $table, $awalan) { // Used Permanent For Master
        $query = "SELECT MAX(RIGHT($id, 5)) AS max_id FROM $table";
        $q = $this->db->query($query)->row();
        $str = $q->max_id;
        $number = substr($str, -5);
        if (substr($str, -5, 1) == 0) {
            if (substr($str, -4, 1) == 0) {
                if (substr($str, -3, 1) == 0) {
                    if (substr($str, -2, 1) == 0) {
                        if (substr($str, -1, 1) == 0) {
                            $nol = "0000";
                        } else {
                            if (substr($str, -1, 1) == 9) {
                                $nol = "000";
                            } else {
                                $nol = "0000";
                            }
                        }
                    } else {
                        if (substr($str, -2, 2) == 99) {
                            $nol = "00";
                        } else {
                            $nol = "000";
                        }
                    }
                } else {
                    if (substr($str, -3, 3) == 999) {
                        $nol = "0";
                    } else {
                        $nol = "00";
                    }
                }
            } else {
                if (substr($str, -4, 4) == 9999) {
                    $nol = "";
                } else {
                    $nol = "0";
                }
            }
        }

        $newnumber = $number + 1;
        return $awalan . $nol . $newnumber;
    }

    // ************** autonumber dengan Transdate Begin ********* //
    function txautonumberv2($id, $table, $formattengah, $transdate) { // Used Permanent
        $yearmonth = date("y", strtotime($transdate)) . date("m", strtotime($transdate)) . date("d", strtotime($transdate));
        $query = "SELECT MAX(RIGHT($id, 11)) AS max_id FROM $table WHERE Iid LIKE '%$formattengah-$yearmonth%' ORDER BY $id";
        $q = $this->db->query($query)->row();
        $str = $q->max_id;
        $nol = "";
        if (substr($str, -11, 6) == $yearmonth) {
            $number = substr($str, -5);
            if (substr($str, -5, 1) == 0) { //1
                if (substr($str, -4, 1) == 0) { //2
                    if (substr($str, -3, 1) == 0) { //3
                        if (substr($str, -2, 1) == 0) {//4
                            if (substr($str, -1, 1) == 0) {//5
                                $nol = "0000";
                            } else {
                                if (substr($str, -1, 1) == 9) {
                                    $nol = "000";
                                } else {
                                    $nol = "0000";
                                }
                            }
                        } else {
                            if (substr($str, -2, 2) == 99) {
                                $nol = "00";
                            } else {
                                $nol = "000";
                            }
                        }
                    } else {
                        if (substr($str, -3, 3) == 999) {
                            $nol = "0";
                        } else {
                            $nol = "00";
                        }
                    }
                } else {
                    if (substr($str, -4, 4) == 9999) {
                        $nol = "";
                    } else {
                        $nol = "0";
                    }
                }
            }
            $newnumber = $number + 1;
            return $formattengah . "-" . $yearmonth . $nol . $newnumber;
        } else {
            return $formattengah . "-" . $yearmonth . "00001";
        }
    }

    function txautonumberv2_noinv($id, $table, $formattengah, $transdate) { // Used Permanent
        $yearmonth_forprint = date("Y", strtotime($transdate)) . '/' . date("m", strtotime($transdate)) . '/' . date("d", strtotime($transdate));
        $query = "SELECT MAX(RIGHT($id, 16)) AS max_id FROM $table WHERE InvNo LIKE '%$yearmonth_forprint%' ORDER BY $id";
        $q = $this->db->query($query)->row();
        $str = $q->max_id;
        $nol = "";
        if (substr($str, -16, 10) == $yearmonth_forprint) {
            $number = substr($str, -5);
            if (substr($str, -5, 1) == 0) {
                if (substr($str, -4, 1) == 0) {
                    if (substr($str, -3, 1) == 0) {
                        if (substr($str, -2, 1) == 0) {
                            if (substr($str, -1, 1) == 0) {
                                $nol = "0000";
                            } else {
                                if (substr($str, -1, 1) == 9) {
                                    $nol = "000";
                                } else {
                                    $nol = "0000";
                                }
                            }
                        } else {
                            if (substr($str, -2, 2) == 99) {
                                $nol = "00";
                            } else {
                                $nol = "000";
                            }
                        }
                    } else {
                        if (substr($str, -3, 3) == 999) {
                            $nol = "0";
                        } else {
                            $nol = "00";
                        }
                    }
                } else {
                    if (substr($str, -4, 4) == 9999) {
                        $nol = "";
                    } else {
                        $nol = "0";
                    }
                }
            }
            $newnumber = $number + 1;
            return $formattengah . "/" . $yearmonth_forprint . '/' . $nol . $newnumber;
        } else {
            return $formattengah . "/" . $yearmonth_forprint . '/' . "00001";
        }
    }

    // ************** autonumber dengan Transdate END ********* //


    function txautonumber($id, $table, $formattengah) { // Akan Dihapus
        $session = $this->session->userdata('session_data');
        $companyidsession = $session['companyid'];
        $year = substr(date("Y"), -2);
        $yearmonth = $year . date("m");
        $query = "SELECT MAX(RIGHT($id, 9)) AS max_id FROM $table ORDER BY $id";
        $q = $this->db->query($query)->row();
        $str = $q->max_id;
        $nol = "";
        if (substr($str, -9, 4) == $yearmonth) {
            $number = substr($str, -5);
            if (substr($str, -5, 1) == 0) {
                if (substr($str, -4, 1) == 0) {
                    if (substr($str, -3, 1) == 0) {
                        if (substr($str, -2, 1) == 0) {
                            if (substr($str, -1, 1) == 0) {
                                $nol = "0000";
                            } else {
                                if (substr($str, -1, 1) == 9) {
                                    $nol = "000";
                                } else {
                                    $nol = "0000";
                                }
                            }
                        } else {
                            if (substr($str, -2, 2) == 99) {
                                $nol = "00";
                            } else {
                                $nol = "000";
                            }
                        }
                    } else {
                        if (substr($str, -3, 3) == 999) {
                            $nol = "0";
                        } else {
                            $nol = "00";
                        }
                    }
                } else {
                    if (substr($str, -4, 4) == 9999) {
                        $nol = "";
                    } else {
                        $nol = "0";
                    }
                }
            }
            $newnumber = $number + 1;
            return $companyidsession . "-" . $formattengah . "-" . $yearmonth . $nol . $newnumber;
        } else {
            return $companyidsession . "-" . $formattengah . "-" . $yearmonth . "00001";
        }
    }

    function mstxautonumber($id, $table, $formattengah) { // Used Permanent For Master Animal
        $year = substr(date("Y"), -2);
        $yearmonth = $year . date("m");
        $query = "SELECT MAX(right($id, 9)) AS max_id FROM $table ORDER BY $id";
        $q = $this->db->query($query)->row();
        $str = $q->max_id;
        $nol = "";
        if (substr($str, -9, 4) == $yearmonth) {
            $number = substr($str, -5);
            if (substr($str, -5, 1) == 0) {
                if (substr($str, -4, 1) == 0) {
                    if (substr($str, -3, 1) == 0) {
                        if (substr($str, -2, 1) == 0) {
                            if (substr($str, -1, 1) == 0) {
                                $nol = "0000";
                            } else {
                                if (substr($str, -1, 1) == 9) {
                                    $nol = "000";
                                } else {
                                    $nol = "0000";
                                }
                            }
                        } else {
                            if (substr($str, -2, 2) == 99) {
                                $nol = "00";
                            } else {
                                $nol = "000";
                            }
                        }
                    } else {
                        if (substr($str, -3, 3) == 999) {
                            $nol = "0";
                        } else {
                            $nol = "00";
                        }
                    }
                } else {
                    if (substr($str, -4, 4) == 9999) {
                        $nol = "";
                    } else {
                        $nol = "0";
                    }
                }
            }
            $newnumber = $number + 1;
            return $formattengah . "-" . $yearmonth . $nol . $newnumber;
        } else {
            return $formattengah . "-" . $yearmonth . "00001";
        }
    }

    function cekroleid() {
        $session = $this->session->userdata('session_data');
        $user = $session['id'];
        $companyid = $session['companyid'];
        $query = "SELECT RoleId from stp_userroleauthority "
                . "WHERE UserId = '$user' "
                . "AND CompanyId = '$companyid'";
        $result = $this->db->query($query)->row();

        if ($result->RoleId == 'STP-ROLE-00001' || $result->RoleId == 'STP-ROLE-00015') {
            return 1;
        } else {
            return 0;
        }
    }

    function cekauthority($form, $auth) {
        $session = $this->session->userdata('session_data');
        $user = $session['id'];
        $companyid = $session['companyid'];
        $query = "SELECT top 1 stp_userrole.RoleAuthority FROM stp_userrole WHERE "
                . "Iid = (SELECT top 1 stp_userroleauthority.RoleId FROM stp_userroleauthority WHERE stp_userroleauthority.UserId = '$user')";
        $result = $this->db->query($query)->row();
        $string = $result->RoleAuthority;

        $arrdata = explode('|', $string); //explode form
        for ($i = 0; $i < count($arrdata); $i++) {
            $arrauth = explode('::', $arrdata[$i]); //explode auth
            for ($j = 0; $j < count($arrauth); $j++) {
                if ($arrauth[$j] == strtolower($form)) {// split form
                    for ($z = 0; $z < count($arrauth); $z++) {
                        if ($arrauth[$z] == strtolower($auth)) {// split auth
                            return 1;
                        }
                    }
                }
            }
        }
    }

    function authoritymenu() {
        $session = $this->session->userdata('session_data');
        $user = $session['id'];
        $companyid = $session['companyid'];
        $query = "SELECT top 1 stp_userrole.RoleAuthority FROM stp_userrole WHERE "
                . "Iid = (SELECT top 1 stp_userroleauthority.RoleId FROM stp_userroleauthority WHERE stp_userroleauthority.UserId = '$user')";
        $result = $this->db->query($query)->row();
//        $string = $result->RoleAuthority;
//        $data['stringauth'] = $result->RoleAuthority;
        return $result->RoleAuthority;
    }

    function validationcode($table, $field, $param, $param2) {
        $query = "Select * from $table where $field = '$param' AND Iid != '$param2'";
        $result = $this->db->query($query);
        if ($result->num_rows() >= 1) {
            return 1;
        } else {
            return 0;
        }
    }

    function validationanimal($table, $param1, $param2, $param3, $param4) {
        $query = "Select * from $table where $param1 = '$param2' AND ShipmentNo = '$param3' AND AnimalId != '$param4'";
        $result = $this->db->query($query);
        if ($result->num_rows() >= 1) {
            return 1;
        } else {
            return 0;
        }
    }

    function validationcontact($table, $field, $param, $param2) {
        $query = "Select * from $table where $field = '$param' AND ContactId != '$param2'";
        $result = $this->db->query($query);
        if ($result->num_rows() >= 1) {
            return 1;
        } else {
            return 0;
        }
    }

    function activitylog($branchid, $transno, $transrefno, $transdate, $module, $logtype, $remarks) {
        $progversi = $this->db->query("SELECT VERSION FROM stp_programversion ORDER BY VERSION DESC LIMIT 0,1")->row();
        $ip = $this->input->ip_address();
        $session = $this->session->userdata('session_data');
        $user = $session['id'];
        $companyid = $session['companyid'];

        $query = "INSERT INTO stp_activitylog (CompanyId,BranchId,UserId,TransNo,TransRefNo, "
                . "TransDate,Module,LogType,IpAddress,ProgVersi,Remarks,InputBy,InputDate) "
                . "VALUES ('$companyid','$branchid','$user','$transno','$transrefno','$transdate','$module','$logtype','$ip','$this->serverinfo->programversion()','$remarks','$user',GETDATE())";
        $this->db->query($query);
    }

    function cektransno($table, $transno, $iid) {
        $res = $this->db->query("SELECT TransNo FROM $table Where TransNo = '$transno' AND Iid !='$iid' ")->row();
        if ($res->TransNo == NULL || $res->TransNo == '') {
            return 1;
        } else {
            return 0;
        }
    }

}
