<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tx_PaymentDetailModel extends CI_Model {
	
	private $table = 'TX_Payment_Detail';

	public function __construct()
	{
		parent::__construct();
		
	}

	/*** 
		CHECK TRANSACTION ID IN THIS TABLE
		@RETURN ARRAY()
	 **/
	public function getIncomingAmountByPaymentId($id)
	{
		# code...
		$total =  0;
		$cashAmount = 0;
		$bankAmount = 0;
		$this->db->from($this->table);
		$this->db->where('paymentId',$id);
		$query = $this->db->get()->result();

		foreach ($query as $key => $value) {
			# code...
			$cashAmount = $cashAmount + $value->cashAmount;
			$bankAmount = $bankAmount + $value->bankAmount;
		}

		$total = $cashAmount + $bankAmount;

		return $total;
	}
	

}

/* End of file Tx_PaymentDetailModel.php */
/* Location: ./application/models/Tx_PaymentDetailModel.php */